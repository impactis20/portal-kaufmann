﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
    [Serializable]
    public class SistemaEntity
    {
        public SistemaEntity()
        {

        }

        public SistemaEntity(int idSis)
        {
            this.IdSistema = idSis;
        }

        public SistemaEntity(int idSis, string Nombre, string Url)
        {
            this.IdSistema = idSis;
            this.Nombre = Nombre;
            this.Url = Url;
        }

        public SistemaEntity(int idSis, string Nombre, string Url, string NombreImg, int Estado)
        {
            this.IdSistema = idSis;
            this.Nombre = Nombre;
            this.Url = Url;
            this.NombreIMG = NombreImg;
            this.Estado = Estado;
        }

        public SistemaEntity(int idSis, string nombre, string url, string userDesa, string userSoli, int Estado, string Desc, string NombreImg, List<SistemaEntity.Modulo> Mods)
        {
            this.IdSistema = idSis;
            this.Nombre = nombre;
            this.Url = url;
            this.UserDesarrollador = userDesa;
            this.UserSolicitante = userSoli;
            this.Estado = Estado;
            this.Descripcion = Desc;
            this.NombreIMG = NombreImg;
            this.Modulos = Mods;
        }

        public int IdSistema { get; set; }
        public string Nombre { get; set; }
        public string Url { get; set; }
        public string NombreIMG { get; set; }
        public string UserDesarrollador { get; set; }
        public string UserSolicitante { get; set; }
        public int Estado { get; set; }
        public string Descripcion { get; set; }
        public List<Modulo> Modulos { get; set; }

        [Serializable]
        public class Modulo
        {
            public Modulo()
            {

            }

            public Modulo(int id, string nombre, string pag, string desc, int esusado, int estado)
            {
                this.IdModulo = id;
                this.Nombre = nombre;
                this.Descripcion = desc;
                this.Pagina = pag;
                this.EsUsado = esusado;
                this.Estado = estado;
            }

            public Modulo(int id, string nombre, string pag, string desc)
            {
                this.IdModulo = id;
                this.Nombre = nombre;
                this.Descripcion = desc;
                this.Pagina = pag;
            }

            public int IdModulo { get; set; }
            public string Nombre { get; set; }
            public string Pagina { get; set; }
            public string Descripcion { get; set; }
            public int Estado { get; set; }
            public int EsUsado { get; set; } //Atributo que se le asigna un 1 = cuando el
            public List<UsuarioProveedorEntity> UsuariosAcceso { get; set; }
        }

        public class Roles
        {
            public Roles() { }

            public Roles(int IdRol, int IdSistema, string Nombre, int TipoUsuarioRol)
            {
                this.IdRol = IdRol;
                this.IdSistema = IdSistema;
                this.Nombre = Nombre;
                this.TipoUsuarioRol = TipoUsuarioRol;
            }

            public int IdRol { get; set; }
            public int IdSistema { get; set; }
            public string Nombre { get; set; }
            public int TipoUsuarioRol { get; set; }
        }
    }
}
