﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
    [Serializable]
    public class PagosEntity
    {
        public PagosEntity()
        {

        }

        public PagosEntity(int SociedadPagadora, string NroDocumento, string EstadoDocumento, string FormaPago, int Monto, string Moneda, int NroCheque, DateTime FechaPago, DateTime FechaCompensa, bool DocumentoBloqueado, string estado, string rut, string razon, string noTransa, int total)
        {
            this.total = total;
            this.noTransa = noTransa;
            this.estado = estado;
            this.rut = rut;
            this.razon = rut;
            this.SociedadPagadora = SociedadPagadora;
            this.NroDocumento = NroDocumento;
            this.EstadoDocumento = EstadoDocumento;
            this.FormaPago = FormaPago;
            this.Monto = Monto;
            this.Moneda = Moneda;
            this.NroCheque = NroCheque;
            this.FechaPago = FechaPago;
            this.FechaCompensa = FechaCompensa;
            this.DocumentoBloqueado = DocumentoBloqueado;
        }
        public int total { get; set; }
        public string noTransa { get; set; }
        public string rut { get; set; }
        public string razon { get; set; }
        public string estado { get; set; }
        public int SociedadPagadora { get; set; }
        public string NroDocumento { get; set; } //NROFACTURA en WS
        public string EstadoDocumento { get; set; }
        public string FormaPago { get; set; }
        public int Monto { get; set; }
        public string Moneda { get; set; }
        public int NroCheque { get; set; }
        public bool DocumentoBloqueado { get; set; }
        //public DateTime FechaContab { get; set; }
        public DateTime FechaPago { get; set; }
        public DateTime FechaCompensa { get; set; }
    }

    [Serializable]
    public class SociedadPagadoraEntity
    {
        public SociedadPagadoraEntity()
        {

        }

        public SociedadPagadoraEntity(int IdSoc)
        {
            this.IdSociedad = IdSoc;
        }

        public SociedadPagadoraEntity(int IdSoc, string RutSoc, string RazonSocial)
        {
            this.IdSociedad = IdSoc;
            this.Rut = RutSoc;
            this.RazonSocial = RazonSocial;
        }

        public SociedadPagadoraEntity(int IdSoc, string RutSoc, string RazonSocial, List<PagosEntity> ListPagos)
        {
            this.IdSociedad = IdSoc;
            this.Rut = RutSoc;
            this.RazonSocial = RazonSocial;
            this.Pagos = ListPagos;
        }

        public int IdSociedad { get; set; }
        public string Rut { get; set; }
        public string RazonSocial { get; set; }
        public List<PagosEntity> Pagos { get; set; }

        public static List<SociedadPagadoraEntity> GetSociedadesPagadoras()
        {
            SociedadPagadoraEntity InmobiliariaKSA = new SociedadPagadoraEntity(1800, "96572370-6", "Inmobiliaria Kaufmann S.A.");
            SociedadPagadoraEntity CKSA = new SociedadPagadoraEntity(1000, "96572360-9", "Comercial Kaufmann S.A.");
            SociedadPagadoraEntity KSA = new SociedadPagadoraEntity(1100, "92475000-6", "Kaufmann S.A.");
            SociedadPagadoraEntity KCS = new SociedadPagadoraEntity(1900, "78777840-2", "Kaufmann Corredora Seguro");
            SociedadPagadoraEntity LEL = new SociedadPagadoraEntity(1700, "77919930-4", "La Estrella Leasing Ltda.");
            SociedadPagadoraEntity CEKSA = new SociedadPagadoraEntity(2000, "76620000-1", "Centro Entrenamiento KSA.");
            SociedadPagadoraEntity InversionesKSA = new SociedadPagadoraEntity(1650, "76246708-9", "IKSA Chile.");
            SociedadPagadoraEntity CMLAL = new SociedadPagadoraEntity(2100, "76005909-9", "Com. Motores de los Andes LTDA.");
            List<SociedadPagadoraEntity> ListSocs = new List<SociedadPagadoraEntity>();
            ListSocs.Add(CKSA);
            ListSocs.Add(KSA);
            ListSocs.Add(InversionesKSA);
            ListSocs.Add(LEL);
            ListSocs.Add(InmobiliariaKSA);
            ListSocs.Add(KCS);
            ListSocs.Add(CEKSA);
            ListSocs.Add(CMLAL);
            return ListSocs;
        }

        public static SociedadPagadoraEntity GetSociedadPagadora(int IdSociedad)
        {
            SociedadPagadoraEntity SociedadPagadora = GetSociedadesPagadoras().First(a => a.IdSociedad == IdSociedad);
            return SociedadPagadora;
        }
    }
}
