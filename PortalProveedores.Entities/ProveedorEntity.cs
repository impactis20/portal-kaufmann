﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
    [Serializable]
    public class ProveedorEntity
    {
        public int IdProv { get; set; } //Id del proveedor
        public string Rut { get; set; }
        public string RazonSocial { get; set; }
        public string Password { get; set; }
        public int AccesoEnSistema { get; set; }
        public DateTime Fecha { get; set; } //dependiendo de que clase lo ocupe, puede ser fecha solicitud o fecha creación.
        public int IdSolicitud { get; set; }

        public ProveedorEntity() { }

        public ProveedorEntity(int Id, string Rut, string RazonS)
        {
            this.IdProv = Id;
            this.Rut = Rut;
            this.RazonSocial = RazonS;
        }

        public ProveedorEntity(int Id, string Rut, string RazonSocial, int AccesoSistema)
        {
            this.IdProv = Id;
            this.Rut = Rut;
            this.RazonSocial = RazonSocial;
            this.AccesoEnSistema = AccesoSistema;
        }

        //Registro de un proveedor
        public ProveedorEntity(string Rut, string RazonSocial, string Password, DateTime FechaSolicitud)
        {
            this.Rut = Rut;
            this.RazonSocial = RazonSocial;
            this.Password = Password;
            this.Fecha = FechaSolicitud;
        }
    }
}
