﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.Entities
{
    [Serializable]
    public class UsuarioProveedorEntity
    {
        public UsuarioProveedorEntity()
        {
        }

        public UsuarioProveedorEntity(int IdPRov)
        {
            this.IdUserProv = IdPRov;
        }

        public UsuarioProveedorEntity(int IdPRov, string Nombre, string Ap, string Am)
        {
            this.IdUserProv = IdPRov;
            this.Nombre = Nombre;
            this.ApellidoPat = Ap;
            this.ApellidoMat = Am;
        }

        public UsuarioProveedorEntity(int IdUserProv, string Run, string Nom, string Ap, string Am, string Email, int IdProv, int EsAdmin)
        {
            this.IdUserProv = IdUserProv;
            this.Run = Run;
            this.Nombre = Nom;
            this.ApellidoMat = Am;
            this.ApellidoPat = Ap;
            this.Email = Email;
            this.IdProveedor = new int[1];
            this.IdProveedor[0] = IdProv;
            this.EsAdmin = EsAdmin;
        }

        public UsuarioProveedorEntity(int IdUserProv, string Run, string Nom, string Ap, string Am, string Email, int Fono, int FonoMovil, int AccesoPortal)
        {
            this.IdUserProv = IdUserProv;
            this.Run = Run;
            this.Nombre = Nom;
            this.ApellidoMat = Am;
            this.ApellidoPat = Ap;
            this.Email = Email;
            this.Fono = Fono;
            this.FonoMovil = FonoMovil;
            this.AccesoPortal = AccesoPortal;
        }

        public UsuarioProveedorEntity(int IdUserProv, string Run, string Nom, string Ap, string Am, string Email, int Fono, int FonoMovil, int AccesoPortal, int EsAdmin)
        {
            this.IdUserProv = IdUserProv;
            this.Run = Run;
            this.Nombre = Nom;
            this.ApellidoMat = Am;
            this.ApellidoPat = Ap;
            this.Email = Email;
            this.Fono = Fono;
            this.FonoMovil = FonoMovil;
            this.AccesoPortal = AccesoPortal;
            this.EsAdmin = EsAdmin;
        }
        public UsuarioProveedorEntity( string descripcionTipo)
        {
            this.descripcionTipo = descripcionTipo;
        }
        public string descripcionTipo { get; set; }
        public int IdUserProv { get; set; }
        public string Run { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPat { get; set; }
        public string ApellidoMat { get; set; }
        public string Email { get; set; }
        public int Fono { get; set; }
        public int FonoMovil { get; set; }
        public string MotivoRegistro { get; set; } //motivo del por qué se necesita ingresar al portal
        public int EstadoSolicitud { get; set; }
        public int AccesoPortal { get; set; }
        public int Vigente { get; set; }
        public int EsAdmin { get; set; }
        public PermisosSistemas Permisos { get; set; }
        public int[] IdProveedor { get; set; } // un usuario puede pertenecer a más de una razón social

        [Serializable]
        public class Consulta : UsuarioProveedorEntity
        {
            public Consulta()
            {

            }

            public int IdConsulta { get; set; }
            public string Mensaje { get; set; }
            public DateTime Fecha { get; set; }
            public string RespuestaUsuario { get; set; } //Usuario que respondió la consulta
        }

        [Serializable]
        public class UsuarioKaufmann : UsuarioProveedorEntity
        {
            public UsuarioKaufmann()
            {

            }

            public UsuarioKaufmann(string UserName)
            {
                this.UserName = UserName;
            }

            public UsuarioKaufmann(string UserName, string Nombre, string Email, int EsAdmin)
            {
                this.UserName = UserName;
                this.Nombre = Nombre;
                this.Email = Email;
                this.EsAdmin = EsAdmin;
            }

            public UsuarioKaufmann(int IdUserProv, string Run, string Nom, string Ap, string Am, int AccesoPortal, int IdProveedor, int EsAdmin)
            {
                this.IdUserProv = IdUserProv;
                this.Run = Run;
                this.Nombre = Nom;
                this.ApellidoMat = Am;
                this.ApellidoPat = Ap;
                this.AccesoPortal = AccesoPortal;
                this.IdProveedor = new int[1];
                this.IdProveedor[0] = IdProveedor;
                this.EsAdmin = EsAdmin;
            }

            public UsuarioKaufmann(string UserName, string Run, string Nombre, string Ap, string Am, string Em)
            {
                this.UserName = UserName;
                this.Nombre = Nombre;
                this.ApellidoPat = Ap;
                this.ApellidoMat = Am;
                this.Email = Em;
            }

            public string UserName { get; set; }
        }

        [Serializable]
        public class PermisosSistemas
        {
            public PermisosSistemas() { }

            public PermisosSistemas(string NombreSistema, int EstadoSistema, string RolUsuario)
            {
                this.NombreSistema = NombreSistema;
                this.EstadoSistema = EstadoSistema;
                this.RolUsuario = RolUsuario;
            }

            public string NombreSistema { get; set; }
            public int EstadoSistema { get; set; }
            public string RolUsuario { get; set; }
        }
    }
}