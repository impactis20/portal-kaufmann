﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using PortalProveedores.Entities;

namespace PortalProveedores.DAL
{
    public class AdministradorSistemasDAL
    {
        public static List<Tuple<ProveedorEntity, UsuarioProveedorEntity>> GetRegistrosProveedores(int Filtro)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetRegistrosProveedores");

            OracleParameter p_Filtro = Command.Parameters.Add("p_filtro", OracleDbType.Int32, ParameterDirection.Input);
            p_Filtro.Value = Filtro;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            try
            {
                Conn.Open();
                OracleDataReader dReader = Command.ExecuteReader();
                
                List<Tuple<ProveedorEntity, UsuarioProveedorEntity>> List = new List<Tuple<ProveedorEntity, UsuarioProveedorEntity>>();
                
                while (dReader.Read())
                {
                    Tuple<ProveedorEntity, UsuarioProveedorEntity> RegistroProveedor = new Tuple<ProveedorEntity, UsuarioProveedorEntity>(new  ProveedorEntity(), new UsuarioProveedorEntity());
                
                    RegistroProveedor.Item1.IdSolicitud = Convert.ToInt32(dReader["id_solicitud"].ToString());
                    RegistroProveedor.Item1.RazonSocial = dReader["razon_social"].ToString();
                    RegistroProveedor.Item1.Fecha = Convert.ToDateTime(dReader["fecha_solicitud"].ToString());
                    RegistroProveedor.Item2.MotivoRegistro = dReader["mensaje"].ToString();
                    RegistroProveedor.Item2.Nombre = dReader["nombre"].ToString();
                    RegistroProveedor.Item2.ApellidoPat = dReader["apellido_paterno"].ToString();
                    RegistroProveedor.Item2.EstadoSolicitud = Convert.ToInt32(dReader["estado_solicitud"].ToString());
                
                    List.Add(RegistroProveedor);
                }
                
                dReader.Dispose();
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();
                
                return List;
            }
            catch
            {
                return null;
            }
        }

        public static Tuple<ProveedorEntity, UsuarioProveedorEntity, UsuarioProveedorEntity.UsuarioKaufmann, string> GetRegistroProveedor(int IdSolicitud)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetRegistroProveedor");

            OracleParameter p_IdSolicitud = Command.Parameters.Add("p_id_solicitud", OracleDbType.Int32, ParameterDirection.Input);
            p_IdSolicitud.Value = IdSolicitud;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            try
            {
                Conn.Open();
                OracleDataReader dReader = Command.ExecuteReader();

                if (dReader.Read())
                {
                    Tuple<ProveedorEntity, UsuarioProveedorEntity, UsuarioProveedorEntity.UsuarioKaufmann, string> RegistroUs = new Tuple<ProveedorEntity, UsuarioProveedorEntity, UsuarioProveedorEntity.UsuarioKaufmann, string>(new ProveedorEntity(), new UsuarioProveedorEntity(), new UsuarioProveedorEntity.UsuarioKaufmann(), dReader["motivo_decision"].ToString());

                    RegistroUs.Item1.IdSolicitud = Convert.ToInt32(dReader["id_solicitud"].ToString());
                    RegistroUs.Item1.Rut = dReader["rut_proveedor"].ToString();
                    RegistroUs.Item1.RazonSocial = dReader["razon_social"].ToString();
                    RegistroUs.Item1.Fecha = Convert.ToDateTime(dReader["fecha_solicitud"].ToString());
                    RegistroUs.Item2.MotivoRegistro = dReader["mensaje"].ToString();
                    RegistroUs.Item2.Run = dReader["rut_usuario_proveedor"].ToString();
                    RegistroUs.Item2.Nombre = dReader["nombre"].ToString();
                    RegistroUs.Item2.ApellidoPat = dReader["apellido_paterno"].ToString();
                    RegistroUs.Item2.ApellidoMat = dReader["apellido_materno"].ToString();
                    RegistroUs.Item2.Email = dReader["email"].ToString();
                    RegistroUs.Item2.Fono = Convert.ToInt32(dReader["telefono"].ToString());
                    RegistroUs.Item2.FonoMovil = Convert.ToInt32(dReader["telefono_movil"].ToString());
                    RegistroUs.Item2.EstadoSolicitud = Convert.ToInt32(dReader["estado_solicitud"].ToString());
                    RegistroUs.Item3.UserName = dReader["responsable"].ToString();
                    dReader.Dispose();
                    Command.Dispose();
                    Conn.Close();
                    Conn.Dispose();
                    return RegistroUs;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        public static int InsertRegistroProveedor(int IdSolicitud, int Decision, string Responsable, string MotivoDecision, DateTime FechaDecision)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.InsertRegistroProveedor");

            OracleParameter p_IdSolicitud = Command.Parameters.Add("p_id_solicitud", OracleDbType.Int32, ParameterDirection.Input);
            p_IdSolicitud.Value = IdSolicitud;

            OracleParameter p_Decision = Command.Parameters.Add("p_decision", OracleDbType.Int32, ParameterDirection.Input);
            p_Decision.Value = Decision;

            OracleParameter p_Responsable = Command.Parameters.Add("p_responsable", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Responsable.Value = Responsable;

            OracleParameter p_MotivoDecision = Command.Parameters.Add("p_motivo_decision", OracleDbType.Varchar2, ParameterDirection.Input);
            p_MotivoDecision.Value = MotivoDecision;

            OracleParameter p_FechaDecision = Command.Parameters.Add("p_fecha_decision", OracleDbType.Date, ParameterDirection.Input);
            p_FechaDecision.Value = FechaDecision;

            OracleParameter p_Respuesta = Command.Parameters.Add("p_respuesta", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_Respuesta.Size = 100;
            p_Respuesta.Value = 0;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();

                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                int Respuesta = Convert.ToInt32(p_Respuesta.Value.ToString());
                // p_respuesta: 
                // 2 = usuario rechazado
                // 1 = solicitud insertada en el sistema
                // 0 = error

                return Respuesta;
            }
            catch
            {
                return -1;
            }
        }

        public static List<UsuarioProveedorEntity.Consulta> GetConsultas(int Estado)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetConsultas");

            OracleParameter p_Estado = Command.Parameters.Add("p_estado", OracleDbType.Int32, ParameterDirection.Input);
            p_Estado.Value = Estado;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            try
            {
                Conn.Open();
                OracleDataReader dReader = Command.ExecuteReader();

                List<UsuarioProveedorEntity.Consulta> List = null;
                
                List = new List<UsuarioProveedorEntity.Consulta>();

                while (dReader.Read())
                {
                    UsuarioProveedorEntity.Consulta Consulta = new UsuarioProveedorEntity.Consulta();
                    Consulta.IdConsulta = Convert.ToInt32(dReader["id_consulta"]);
                    Consulta.Nombre = dReader["nombre"].ToString();
                    Consulta.ApellidoPat = dReader["apellido_paterno"].ToString();
                    Consulta.ApellidoMat = dReader["apellido_materno"].ToString();
                    Consulta.Email = dReader["email"].ToString();
                    Consulta.Mensaje = dReader["mensaje"].ToString();
                    Consulta.Fecha = Convert.ToDateTime(dReader["fecha"]);
                    Consulta.RespuestaUsuario = Convert.IsDBNull(dReader["username"]) ? string.Empty : dReader["username"].ToString();

                    List.Add(Consulta);
                }

                dReader.Dispose();
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();
                return List;

            }
            catch
            {
                return null;
            }
        }

        public static UsuarioProveedorEntity.Consulta GetConsulta(int IdConsulta)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetConsulta");

            OracleParameter p_IdConsulta = Command.Parameters.Add("p_id_consulta", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_IdConsulta.Value = IdConsulta;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            try
            {
                Conn.Open();
                OracleDataReader dReader = Command.ExecuteReader();

                if (dReader.Read())
                {
                    UsuarioProveedorEntity.Consulta Consulta = new UsuarioProveedorEntity.Consulta();
                    Consulta.IdConsulta = Convert.ToInt32(dReader["id_consulta"]);
                    Consulta.Nombre = dReader["nombre"].ToString();
                    Consulta.ApellidoPat = dReader["apellido_paterno"].ToString();
                    Consulta.ApellidoMat = dReader["apellido_materno"].ToString();
                    Consulta.Email = dReader["email"].ToString();
                    Consulta.Fono = Convert.ToInt32(dReader["telefono"].ToString());
                    Consulta.FonoMovil = Convert.ToInt32(dReader["telefono_movil"].ToString());
                    Consulta.Mensaje = dReader["mensaje"].ToString();
                    Consulta.Fecha = Convert.ToDateTime(dReader["fecha"]);
                    Consulta.RespuestaUsuario = Convert.IsDBNull(dReader["username"]) ? string.Empty : dReader["username"].ToString();

                    dReader.Dispose();
                    Command.Dispose();
                    Conn.Close();
                    Conn.Dispose();
                    return Consulta;
                }
                else
                    return null;
            }
            catch
            {
                return null;
            }
        }

        public static int InsertResponsableConsulta(int IdConsulta, string Resp)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.InsertResponsableConsulta");

            OracleParameter p_IdConsulta = Command.Parameters.Add("p_id_consulta", OracleDbType.Int32, ParameterDirection.Input);
            p_IdConsulta.Value = IdConsulta;

            OracleParameter p_Resp = Command.Parameters.Add("p_responsable", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Resp.Value = Resp;

            OracleParameter p_Respuesta = Command.Parameters.Add("p_respuesta", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_Respuesta.Size = 100;
            p_Respuesta.Value = 0;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();

                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                int Respuesta = Convert.ToInt32(p_Respuesta.Value.ToString());
                return Respuesta;
            }
            catch
            {
                return -1; //Error
            }
        }

        public static int SetCambiarAdmRS(int IdUserProvAdmNuevo, int IdRazonSocial)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.SetCambiarAdmRS");

            OracleParameter p_IdUserProvAdmNuevo = Command.Parameters.Add("p_id_userprov_nuevo_adm", OracleDbType.Int32, ParameterDirection.Input);
            p_IdUserProvAdmNuevo.Value = IdUserProvAdmNuevo;

            OracleParameter p_IdRS = Command.Parameters.Add("p_id_rs", OracleDbType.Int32, ParameterDirection.Input);
            p_IdRS.Value = IdRazonSocial;

            OracleParameter p_Respuesta = Command.Parameters.Add("p_respuesta", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_Respuesta.Value = 0;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();

                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                int Respuesta = Convert.ToInt16(p_Respuesta.Value.ToString());

                return Respuesta;
            }
            catch
            {
                return -1;
            }
        }

        public static int InsertUserCorpPortal(string UserName, string Email, DateTime FechaReg)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.InsertUserCorpPortal");

            OracleParameter p_UserName = Command.Parameters.Add("p_username", OracleDbType.Varchar2, ParameterDirection.Input);
            p_UserName.Value = UserName;

            OracleParameter p_Email = Command.Parameters.Add("p_email", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Email.Value = Email;

            OracleParameter p_Fecha = Command.Parameters.Add("p_fecha_reg", OracleDbType.Date, ParameterDirection.Input);
            p_Fecha.Value = FechaReg;

            OracleParameter p_Resp = Command.Parameters.Add("p_resp", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_Resp.Value = 0;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();

                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                int Respuesta = Convert.ToInt32(p_Resp.Value);
                return Respuesta;
            }
            catch
            {
                return -1;
            }
        }

        public static int ValidaUserCoprPortal(string UserName)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.ValidaUserCoprPortal");

            OracleParameter p_UserName = Command.Parameters.Add("p_username", OracleDbType.Varchar2, ParameterDirection.Input);
            p_UserName.Value = UserName;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            try
            {
                Conn.Open();
                OracleDataReader dReader = Command.ExecuteReader();

                if (dReader.Read())
                {
                    //Usuario existe en la tabla usuario_ksa y devuelve el estado de ese usuario
                    int Respuesta = Convert.ToInt32(dReader["ACCESO_A_SISTEMA"].ToString());
                    dReader.Dispose();
                    Command.Dispose();
                    Conn.Close();
                    Conn.Dispose();
                    return Respuesta;
                }
                else
                {
                    dReader.Dispose();
                    Command.Dispose();
                    Conn.Close();
                    Conn.Dispose();
                    return 0; //No existe el usuario
                }
            }
            catch
            {
                return -1;
            }
        }

        // Actualiza el permiso de acceso al portal, del usuario corporativo
        public static int SetUserCorpPortal(string UserName, int Permiso)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.SetUserCorpPortal");

            OracleParameter p_UserName = Command.Parameters.Add("p_username", OracleDbType.Varchar2, ParameterDirection.Input);
            p_UserName.Value = UserName;

            OracleParameter p_Permiso = Command.Parameters.Add("p_permiso", OracleDbType.Int32, ParameterDirection.Input);
            p_Permiso.Value = Permiso;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();

                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                return 1;
            }
            catch
            {
                return -1;
            }
        }

        // Actualiza a un usuario corporativo, dejándolo como administrador o viceversa
        public static int SetUserCorpToAdm(string UserName)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.SetUserCorpToAdm");

            OracleParameter p_UserName = Command.Parameters.Add("p_username", OracleDbType.Varchar2, ParameterDirection.Input);
            p_UserName.Value = UserName;

            OracleParameter p_Valor = Command.Parameters.Add("p_respuesta", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_Valor.Value = 0;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();

                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                int Respuesta = Convert.ToInt32(p_Valor.Value.ToString());

                return Respuesta;
            }
            catch
            {
                return -1;
            }
        }

        // Actualiza el permiso de acceso al portal, del usuario proveedor
        public static int SetUserProvPortal(int IdUserProv, int Permiso)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.SetUserProvPortal");

            OracleParameter p_IdUserProv = Command.Parameters.Add("p_id_user_prov", OracleDbType.Int32, ParameterDirection.Input);
            p_IdUserProv.Value = IdUserProv;

            OracleParameter p_Permiso = Command.Parameters.Add("p_permiso", OracleDbType.Int32, ParameterDirection.Input);
            p_Permiso.Value = Permiso;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();

                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                int Respuesta = Convert.ToInt32(p_Permiso.Value.ToString());

                return Respuesta;
            }
            catch
            {
                return -1;
            }
        }

        // Actualiza el permiso de acceso al portal, de la razón social
        public static int SetRazonSocialPortal(int IdProv, int Permiso)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.SetRazonSocialPortal");

            OracleParameter p_IdProv = Command.Parameters.Add("p_id_prov", OracleDbType.Int32, ParameterDirection.Input);
            p_IdProv.Value = IdProv;

            OracleParameter p_Permiso = Command.Parameters.Add("p_permiso", OracleDbType.Int32, ParameterDirection.Input);
            p_Permiso.Value = Permiso;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();

                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                return 1;
            }
            catch
            {
                return -1;
            }
        }

        public static UsuarioProveedorEntity.UsuarioKaufmann GetUserCorp_RolEstado(UsuarioProveedorEntity.UsuarioKaufmann user)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetUserCorp_RolEstado");

            OracleParameter p_Username = Command.Parameters.Add("p_username", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Username.Value = user.UserName;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            Conn.Open();
            OracleDataReader dReader = Command.ExecuteReader();
            
            if (dReader.Read())
            {
                user.EsAdmin = Convert.ToInt32(dReader["EsAdmin"].ToString()); // 1 = si ... 0 = no
                user.AccesoPortal = Convert.ToInt32(dReader["ACCESO_A_SISTEMA"].ToString()); // 1 = si ... 2 = no
            }

            dReader.Dispose();
            Command.Dispose();
            Conn.Close();
            Conn.Dispose();
            return user;

        }

        public static List<UsuarioProveedorEntity.UsuarioKaufmann> GetUsuarioCorporativo(string Username)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetUsuarioCorporativo");

            OracleParameter p_Username = Command.Parameters.Add("p_username", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Username.Value = Username;

            //OracleParameter p_Usuarios = Command.Parameters.Add("Usuario", OracleDbType.RefCursor, ParameterDirection.InputOutput);

            //OracleParameter p_Permisos = Command.Parameters.Add("Permisos", OracleDbType.RefCursor, ParameterDirection.InputOutput);

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            Conn.Open();
            //Command.ExecuteNonQuery();
            //OracleDataReader dReader1 = Util.InstanciarCursor(p_Usuarios, 50);
            //OracleDataReader dReader2 = Util.InstanciarCursor(p_Permisos, 50);

            OracleDataReader dReader = Command.ExecuteReader();

            List<UsuarioProveedorEntity.UsuarioKaufmann> ListUsuarios = new List<UsuarioProveedorEntity.UsuarioKaufmann>();

            while (dReader.Read())
            {
                //Busco los datos del usuario
                UsuarioProveedorEntity.UsuarioKaufmann user = new UsuarioProveedorEntity.UsuarioKaufmann(dReader["username"].ToString());
                user.Nombre = UsuarioDAL.GetNombre(user.UserName);

                string Apellidos = UsuarioDAL.GetApellido(user.UserName);

                if (Apellidos.Contains(" ")) //Tiene dos apellidos?
                {
                    user.ApellidoPat = Apellidos.Split(' ')[0];
                    user.ApellidoMat = Apellidos.Split(' ')[1];
                }
                else
                    user.ApellidoPat = Apellidos;

                user.Run = UsuarioDAL.GetRut(user.UserName);
                user.Email = UsuarioDAL.GetEmail(user.UserName);
                user.AccesoPortal = Convert.ToInt32(dReader["acceso_generico"].ToString());
                user.EsAdmin = Convert.ToInt16(dReader["es_admin"]);

                ListUsuarios.Add(user);
            }

            //if (!((OracleRefCursor)(p_Permisos.Value)).IsNull)
            //{
            //    while (dReader2.Read())
            //    {

            dReader.NextResult();

            while (dReader.Read())
            {
                string usuario = dReader["username"].ToString();

                foreach (UsuarioProveedorEntity.UsuarioKaufmann user in ListUsuarios)
                {
                    if (user.UserName == usuario)
                    {
                        UsuarioProveedorEntity.PermisosSistemas Permisos = new UsuarioProveedorEntity.PermisosSistemas(dReader["nombre_sistema"].ToString(), Convert.ToInt32(dReader["estado_sistema"].ToString()), dReader["nombre_rol"].ToString());
                        user.Permisos = Permisos;
                    }

                    break;
                }
            }
            

            dReader.Dispose();
            Command.Dispose();
            Conn.Close();
            Conn.Dispose();

            return ListUsuarios;
        }


        public static int AddSistema(string NombreSis, string URL, string UserDesa, string UserSoli, string DescSis, string NombreImg)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.AddSistema");

            OracleParameter p_Nombre = Command.Parameters.Add("p_nombre", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Nombre.Value = NombreSis;

            OracleParameter p_URL = Command.Parameters.Add("p_url", OracleDbType.Varchar2, ParameterDirection.Input);
            p_URL.Value = URL;

            OracleParameter p_UserDesa = Command.Parameters.Add("p_user_desa", OracleDbType.Varchar2, ParameterDirection.Input);
            p_UserDesa.Value = UserDesa;

            OracleParameter p_UserSoli = Command.Parameters.Add("p_user_solicitante", OracleDbType.Varchar2, ParameterDirection.Input);
            p_UserSoli.Value = UserSoli;

            OracleParameter p_DescSis = Command.Parameters.Add("p_desc", OracleDbType.Varchar2, ParameterDirection.Input);
            p_DescSis.Value = DescSis;

            OracleParameter p_NombreImg = Command.Parameters.Add("p_nombre_img", OracleDbType.Varchar2, ParameterDirection.Input);
            p_NombreImg.Value = NombreImg;

            OracleParameter IdSisAdded = Command.Parameters.Add("p_id_sistema", OracleDbType.Int32, ParameterDirection.InputOutput);
            IdSisAdded.Value = -1;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();

                int IdSistema = Convert.ToInt16(IdSisAdded.Value.ToString());
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                return IdSistema;
            }
            catch
            {
                return -1;
            }
        }

        public static void AddModuloSistema(int IdSistema, string NombreMod, string Pagina, string Descripcion, int Estado)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.AddModuloSistema");

            OracleParameter p_IdSistema = Command.Parameters.Add("p_id_sistema", OracleDbType.Int32, ParameterDirection.Input);
            p_IdSistema.Value = IdSistema;

            OracleParameter p_Nombre = Command.Parameters.Add("p_nombre_mod", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Nombre.Value = NombreMod;

            OracleParameter p_Pagina = Command.Parameters.Add("p_pagina", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Pagina.Value = Pagina;

            OracleParameter p_Desc = Command.Parameters.Add("p_desc_mod", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Desc.Value = Descripcion;

            OracleParameter p_Estado = Command.Parameters.Add("p_estado", OracleDbType.Int32, ParameterDirection.Input);
            p_Estado.Value = Estado;

            Conn.Open();
            Command.ExecuteNonQuery();
            Command.Dispose();
            Conn.Close();
            Conn.Dispose();
        }

        public static List<SistemaEntity> GetSistema(int IdSistema)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetSistema");

            OracleParameter p_IDS = Command.Parameters.Add("p_id_sistema", OracleDbType.Int32, ParameterDirection.Input);
            p_IDS.Value = IdSistema;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            try
            {
                Conn.Open();
                OracleDataReader dReader = Command.ExecuteReader();
                
                List<SistemaEntity> List = new List<SistemaEntity>();

                while (dReader.Read())
                    List.Add(new SistemaEntity(Convert.ToInt32(dReader["id_sistema"].ToString()), dReader["nombre"].ToString(), dReader["url"].ToString(), dReader["usuario_desarrollador"].ToString(), dReader["usuario_solicitante"].ToString(), Convert.ToInt32(dReader["estado"].ToString()), dReader["desc_sistema"].ToString(), dReader["nombre_img"].ToString(), GetModulos(IdSistema)));

                return List;
            }
            catch { return null; }
        }

        public static List<SistemaEntity.Modulo> GetModulos(int IdSistema)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetModulos");

            OracleParameter p_IDS = Command.Parameters.Add("p_id_sistema", OracleDbType.Int32, ParameterDirection.Input);
            p_IDS.Value = IdSistema;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            try
            {
                Conn.Open();
                OracleDataReader dReader = Command.ExecuteReader();
                
                List<SistemaEntity.Modulo> listModulos = new List<SistemaEntity.Modulo>();

                while (dReader.Read())
                {
                    int EsUsado = string.IsNullOrEmpty(dReader["es_usado"].ToString()) ? 2 : 1; //1= es usado, 2= no es usado
                    listModulos.Add(new SistemaEntity.Modulo(Convert.ToInt32(dReader["id_modulo_sistema"].ToString()), dReader["nombre"].ToString(), dReader["nombre_pagina"].ToString(), dReader["desc_modulo"].ToString(), EsUsado, Convert.ToInt32(dReader["estado"])));
                }

                dReader.Dispose();
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();
                return listModulos;
            }
            catch { return null; }
        }

        public static void SetSistema(int IdSistema, string Nombre, string Url, string Descripcion, string UserDesa, string UserSoli, string NombreImg, int Estado)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.SetSistema");

            OracleParameter p_IDS = Command.Parameters.Add("p_id_sistema", OracleDbType.Int32, ParameterDirection.Input);
            p_IDS.Value = IdSistema;

            OracleParameter p_Nombre = Command.Parameters.Add("p_nombre", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Nombre.Value = Nombre;

            OracleParameter p_URL = Command.Parameters.Add("p_url", OracleDbType.Varchar2, ParameterDirection.Input);
            p_URL.Value = Url;

            OracleParameter p_Desc = Command.Parameters.Add("p_desc", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Desc.Value = Descripcion;

            OracleParameter p_UD = Command.Parameters.Add("p_user_desa", OracleDbType.Varchar2, ParameterDirection.Input);
            p_UD.Value = UserDesa;

            OracleParameter p_US = Command.Parameters.Add("p_user_soli", OracleDbType.Varchar2, ParameterDirection.Input);
            p_US.Value = UserSoli;

            OracleParameter p_NombreImg = Command.Parameters.Add("p_nombre_img", OracleDbType.Varchar2, ParameterDirection.Input);
            p_NombreImg.Value = NombreImg;

            OracleParameter p_Estado = Command.Parameters.Add("p_estado", OracleDbType.Int32, ParameterDirection.Input);
            p_Estado.Value = Estado;

            Conn.Open();
            Command.ExecuteNonQuery();
            Command.Dispose();
            Conn.Close();
            Conn.Dispose();
        }

        public static void SetModulo(int IdModulo, string NombreMod, string Pagina, string DescMod, int Estado)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.SetModulo");

            OracleParameter p_IDM = Command.Parameters.Add("p_id_modulo", OracleDbType.Int32, ParameterDirection.Input);
            p_IDM.Value = IdModulo;

            OracleParameter p_Nombre = Command.Parameters.Add("p_nombre_mod", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Nombre.Value = NombreMod;

            OracleParameter p_Pagina = Command.Parameters.Add("p_pagina", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Pagina.Value = Pagina;

            OracleParameter p_Desc = Command.Parameters.Add("p_desc_mod", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Desc.Value = DescMod;

            OracleParameter p_Estado = Command.Parameters.Add("p_estado", OracleDbType.Int32, ParameterDirection.Input);
            p_Estado.Value = Estado;

            Conn.Open();
            Command.ExecuteNonQuery();

            Command.Dispose();
            Conn.Close();
            Conn.Dispose();
        }

        public static List<UsuarioProveedorEntity> GetBuscaUsuarioProveedor(int IdUserProv, string ValorBusqueda)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetBuscaUsuarioProveedor");

            OracleParameter p_IDUserProv = Command.Parameters.Add("p_id_usuario_prov", OracleDbType.Int32, ParameterDirection.Input);
            p_IDUserProv.Value = IdUserProv;

            OracleParameter p_ValorBusqueda = Command.Parameters.Add("p_valor_busqueda", OracleDbType.Varchar2, ParameterDirection.Input);
            p_ValorBusqueda.Value = ValorBusqueda;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            Conn.Open();
            OracleDataReader dReader = Command.ExecuteReader();
            
            List<UsuarioProveedorEntity> listUserProv = new List<UsuarioProveedorEntity>();

            while (dReader.Read())
                listUserProv.Add(new UsuarioProveedorEntity(Convert.ToInt32(dReader["id_usuario_proveedor"].ToString()),
                                                            dReader["rut_usuario_proveedor"].ToString(),
                                                            dReader["nombre"].ToString(),
                                                            dReader["apellido_paterno"].ToString(),
                                                            dReader["apellido_materno"].ToString(),
                                                            dReader["email"].ToString(),
                                                            Convert.ToInt32(dReader["telefono"].ToString()),
                                                            Convert.ToInt32(dReader["telefono_movil"].ToString()),
                                                            Convert.ToInt32(dReader["acceso_a_sistema"].ToString()),
                                                            Convert.ToInt32(dReader["tipo_usuario_prov"].ToString())));

            dReader.Dispose();
            Command.Dispose();
            Conn.Close();
            Conn.Dispose();
            return listUserProv;
        }

        //Obtiene los sistemas que administra un usuario específico
        public static List<ProveedorEntity> GetRSPerteneceComoUP(int IdUserProv, int TipoUsuario)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetRSPerteneceComoUP");

            OracleParameter p_IDUserProv = Command.Parameters.Add("p_id_usuario_prov", OracleDbType.Int32, ParameterDirection.Input);
            p_IDUserProv.Value = IdUserProv;

            OracleParameter p_TipoUsuario = Command.Parameters.Add("p_tipo_usuario", OracleDbType.Int32, ParameterDirection.Input);
            p_TipoUsuario.Value = TipoUsuario;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            Conn.Open();
            OracleDataReader dReader = Command.ExecuteReader();
            
            List<ProveedorEntity> listProveedores = new List<ProveedorEntity>();

            while (dReader.Read())
                listProveedores.Add(new ProveedorEntity(Convert.ToInt32(dReader["id_proveedor"].ToString()), dReader["rut_proveedor"].ToString(), dReader["razon_social"].ToString()));

            dReader.Dispose();
            Command.Dispose();
            Conn.Close();
            Conn.Dispose();
            return listProveedores;
        }

        // Obtiene los sistemas a los que el usuario proveedor tiene acceso
        public static List<SistemaEntity> GetSistemaAccesoProv(int IdUserProv)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetSistemaAccesoProv");

            OracleParameter p_IDUserProv = Command.Parameters.Add("p_id_user_prov", OracleDbType.Int32, ParameterDirection.Input);
            p_IDUserProv.Value = IdUserProv;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            Conn.Open();
            OracleDataReader dReader = Command.ExecuteReader();
            
            List<SistemaEntity> listSistemas = new List<SistemaEntity>();

            while (dReader.Read())
                listSistemas.Add(new SistemaEntity(Convert.ToInt32(dReader["id_sistema"].ToString()), dReader["nombre"].ToString(), dReader["url"].ToString()));

            dReader.Dispose();
            Command.Dispose();
            Conn.Close();
            Conn.Dispose();
            return listSistemas;
        }

        // Obtiene las razones sociales mediante un parametro de búsqueda
        public static List<ProveedorEntity> GetRazonesSociales(string TextoBusqueda)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetRazonesSociales");

            OracleParameter p_IDUserProv = Command.Parameters.Add("p_valor_busqueda", OracleDbType.Varchar2, ParameterDirection.Input);
            p_IDUserProv.Value = TextoBusqueda;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            Conn.Open();
            OracleDataReader dReader = Command.ExecuteReader();
            
            List<ProveedorEntity> listProveedores = new List<ProveedorEntity>();
            
            while (dReader.Read())
                listProveedores.Add(new ProveedorEntity(Convert.ToInt32(dReader["id_proveedor"].ToString()), dReader["rut_proveedor"].ToString(),  dReader["razon_social"].ToString(), Convert.ToInt32(dReader["acceso_a_sistema"].ToString())));
            
            dReader.Dispose();
            Command.Dispose();
            Conn.Close();
            Conn.Dispose();
            return listProveedores;
        }

        public static int InsertRolSistema(int IdSistema, string NombreRol, int TipoUsuarioRol)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.InsertRolSistema");

            OracleParameter p_IdSistema = Command.Parameters.Add("p_id_sistema", OracleDbType.Int32, ParameterDirection.Input);
            p_IdSistema.Value = IdSistema;

            OracleParameter p_NombreRol = Command.Parameters.Add("p_nombre_rol", OracleDbType.Varchar2, ParameterDirection.Input);
            p_NombreRol.Value = NombreRol;

            OracleParameter p_TipoUsuarioRol = Command.Parameters.Add("p_tipo_user_rol", OracleDbType.Int32, ParameterDirection.Input);
            p_TipoUsuarioRol.Value = TipoUsuarioRol;

            OracleParameter p_Respuesta = Command.Parameters.Add("p_respuesta", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_Respuesta.Size = 100;
            p_Respuesta.Value = 0;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();

                Command.Dispose();
                Conn.Close();
                Conn.Dispose();
                return Convert.ToInt32(p_Respuesta.Value.ToString());
            }
            catch
            {
                return -1;
            }
        }

        public static List<SistemaEntity.Roles> GetRolesSistema(int IdSistema)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetRolesSistema");

            OracleParameter p_IdSistema = Command.Parameters.Add("p_id_sistema", OracleDbType.Int32, ParameterDirection.Input);
            p_IdSistema.Value = IdSistema;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            Conn.Open();
            OracleDataReader dReader = Command.ExecuteReader();
            
            List<SistemaEntity.Roles> listRoles = new List<SistemaEntity.Roles>();

            while (dReader.Read())
            {
                listRoles.Add(new SistemaEntity.Roles(Convert.ToInt32(dReader["id_rol"].ToString()), Convert.ToInt32(dReader["id_sistema"].ToString()), dReader["nombre_rol"].ToString(), Convert.ToInt32(dReader["tipo_user_rol"].ToString())));
            }

            return listRoles;
        }
    }
}