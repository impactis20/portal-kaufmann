﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;

namespace PortalProveedores.DAL
{
    public class DAO_Conexion
    {
        private OracleConnection _Conn { get; set; }
        private OracleCommand _Command { get; set; }

        public OracleConnection getConexion()
        {
            string Conexion = ConfigurationManager.ConnectionStrings["Conexion"].ToString();
            _Conn = new OracleConnection(Conexion);

            return _Conn;
        }

        public OracleCommand getCommand(string Query)
        {
            _Command = new OracleCommand(Query, _Conn);
            _Command.CommandType = System.Data.CommandType.StoredProcedure;

            return _Command;
        }
    }
}
