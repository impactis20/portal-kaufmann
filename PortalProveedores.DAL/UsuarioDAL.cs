﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.ActiveDirectory;
using System.DirectoryServices;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalProveedores.DAL
{
    public static class UsuarioDAL
    {
        public static string GetUserName(string username)
        {
            return "";
        }

        public static string GetApellido(string username)
        {
            return "";
        }

        public static string GetNombre(string username)
        {
            return "";
        }

        public static string GetRut(string username)
        {
            return "";
        }

        public static string GetEmail(string username)
        {
            return "";
        }

        public static string[] GetUserNameLIKE(string username)
        {
            //DirectoryEntry credencialAD = GetCredencialAD();
            //DirectorySearcher buscarAC = new DirectorySearcher(credencialAD);
            //buscarAC.PageSize = 0;
            //buscarAC.SizeLimit = 8;
            //buscarAC.Filter = string.Format("(&(objectCategory=person)(objectClass=user)(sAMAccountName=*)(sAMAccountName=*{0}*))", prefix);
            //buscarAC.PropertiesToLoad.Add("sAMAccountName"); //Nombre de usuario

            //SearchResultCollection resultado = buscarAC.FindAll();

            //List<string> Lst = new List<string>();

            //if (resultado != null)
            //    foreach (SearchResult result in resultado)
            //        Lst.Add(result.Properties["sAMAccountName"][0].ToString());

            //return Lst;

            string[] dd = new string[7];
            return dd;
        }

        //public static List<string> GetUserNameAD(string prefix)
        //{
        //    DirectoryEntry credencialAD = GetCredencialAD();
        //    DirectorySearcher buscarAC = new DirectorySearcher(credencialAD);
        //    buscarAC.PageSize = 0;
        //    buscarAC.SizeLimit = 8;
        //    buscarAC.Filter = string.Format("(&(objectCategory=person)(objectClass=user)(sAMAccountName=*)(sAMAccountName=*{0}*))", prefix);
        //    buscarAC.PropertiesToLoad.Add("sAMAccountName"); //Nombre de usuario

        //    SearchResultCollection resultado = buscarAC.FindAll();

        //    List<string> Lst = new List<string>();

        //    if (resultado != null)
        //        foreach (SearchResult result in resultado)
        //            Lst.Add(result.Properties["sAMAccountName"][0].ToString());

        //    return Lst;
        //}

        public static List<Entities.UsuarioProveedorEntity> GetUsuarioAD(string UserName)
        {
            DirectoryEntry credencialAD = GetCredencialAD();
            DirectorySearcher buscarAC = new DirectorySearcher(credencialAD);
            buscarAC.PageSize = 0;
            buscarAC.SizeLimit = 8;
            buscarAC.Filter = string.Format("(&(objectCategory=person)(objectClass=user)(mail=*)(sAMAccountName=*)(givenName=*)(sn=*)((sAMAccountName=*{0}*)))", UserName); //(description=*)
                                                                                                                                                                            //buscarAC.PropertiesToLoad.Add("description"); //descripción de usuario
            buscarAC.PropertiesToLoad.Add("sAMAccountName"); //Nombre de usuario
            buscarAC.PropertiesToLoad.Add("givenName"); //Primer nombre
            buscarAC.PropertiesToLoad.Add("sn"); //Primer apellido
            buscarAC.PropertiesToLoad.Add("mail"); //email del usuario
                                                   //buscarAC.PropertiesToLoad.Add("cn"); //nombre y apellido

            SearchResultCollection resultado = buscarAC.FindAll();

            if (resultado != null)
            {
                List<Entities.UsuarioProveedorEntity> Lst = new List<Entities.UsuarioProveedorEntity>();

                foreach (SearchResult result in resultado)
                {
                    Entities.UsuarioProveedorEntity Usuario = new Entities.UsuarioProveedorEntity();

                    //Usuario. = result.Properties["sAMAccountName"][0].ToString();
                    Usuario.Nombre = result.Properties["givenName"][0].ToString();

                    if (result.Properties["SN"][0].ToString().Contains(" "))
                    {
                        Usuario.ApellidoPat = result.Properties["sn"][0].ToString().Split(' ')[0];
                        Usuario.ApellidoMat = result.Properties["sn"][0].ToString().Split(' ')[1];
                    }
                    else
                    {
                        Usuario.ApellidoPat = result.Properties["sn"][0].ToString();
                        Usuario.ApellidoMat = "-";
                    }

                    Usuario.Email = result.Properties["mail"][0].ToString();

                    Lst.Add(Usuario);
                }

                return Lst;
            }

            return null;
        }

        public static string AutenticarUsuario(string userName, string Password)
        {
            try
            {
                string url = "LDAP://" + ConfigurationManager.AppSettings["dominio"].ToString();
                DirectoryEntry ad = new DirectoryEntry(url, userName, Password);
                object nativeObject = ad.NativeObject;

                return userName;
            }
            catch (DirectoryServicesCOMException ex)
            {
                //return string.Empty;
                string e = ex.Message;
                string UserName = "";
                return UserName;
            }
        }

        private static DirectoryEntry GetCredencialAD()
        {
            return new DirectoryEntry("LDAP://kaufmann.cl", "lcarvaja@kaufmann.cl", "impactis.01");
        }
    }
}
