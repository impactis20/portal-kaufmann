﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using PortalProveedores.Entities;

namespace PortalProveedores.DAL
{
    public class PortalProveedorDAL
    {
        public static int InsertSolicitudRegistro(string RutProveedor, string RazonSocial, string Password, DateTime FechaSolicitud, string Mensaje, string RutUserPRov, string Nombre, string ApellidoPat, string ApellidoMat, string Email, int Fono, int FonoMovil, string tipo_razon)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.InsertSolicitudRegistro");

            OracleParameter p_RutProveedor = Command.Parameters.Add("p_rut_proveedor", OracleDbType.Varchar2, ParameterDirection.Input);
            p_RutProveedor.Value = RutProveedor;

            OracleParameter p_RazonSocial = Command.Parameters.Add("p_razon_social", OracleDbType.Varchar2, ParameterDirection.Input);
            p_RazonSocial.Value = RazonSocial;

            OracleParameter p_Password = Command.Parameters.Add("p_password", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Password.Value = Password;

            OracleParameter p_FechaSolicitud = Command.Parameters.Add("p_fecha_solicitud", OracleDbType.Date, ParameterDirection.Input);
            p_FechaSolicitud.Value = FechaSolicitud;

            OracleParameter p_Mensaje = Command.Parameters.Add("p_mensaje", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Mensaje.Value = Mensaje;

            OracleParameter p_RutUserProv = Command.Parameters.Add("p_rut_user_proveedor", OracleDbType.Varchar2, ParameterDirection.Input);
            p_RutUserProv.Value = RutUserPRov;

            OracleParameter p_Nombre = Command.Parameters.Add("p_nombre", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Nombre.Value = Nombre;

            OracleParameter p_ApellidoPat = Command.Parameters.Add("p_apellido_pa", OracleDbType.Varchar2, ParameterDirection.Input);
            p_ApellidoPat.Value = ApellidoPat;

            OracleParameter p_ApellidoMat = Command.Parameters.Add("p_apellido_ma", OracleDbType.Varchar2, ParameterDirection.Input);
            p_ApellidoMat.Value = ApellidoMat;

            OracleParameter p_Email = Command.Parameters.Add("p_email", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Email.Value = Email;

            OracleParameter p_Fono = Command.Parameters.Add("p_telefono", OracleDbType.Int32, ParameterDirection.Input);
            p_Fono.Value = Fono;

            OracleParameter p_FonoMovil = Command.Parameters.Add("p_telefono_movil", OracleDbType.Int32, ParameterDirection.Input);
            p_FonoMovil.Value = FonoMovil;
            OracleParameter p_tiporazon = Command.Parameters.Add("p_tiporazon", OracleDbType.Varchar2, ParameterDirection.Input);
            p_tiporazon.Value = tipo_razon;

            OracleParameter p_Respuesta = Command.Parameters.Add("p_respuesta", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_Respuesta.Size = 100;
            p_Respuesta.Value = 0;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();

                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                int Respuesta = Convert.ToInt32(p_Respuesta.Value.ToString());
                return Respuesta;
            }
            catch
            {
                return 0; //0 = el registro no fue insertado ni encontrado.
            }
        }

        public static Tuple<ProveedorEntity, UsuarioProveedorEntity> ValidaEmail(int IdSolicitud, int ValidaCorreo)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.ValidaEmail");

            OracleParameter p_IdSolicitud = Command.Parameters.Add("p_id_solicitud", OracleDbType.Int32, ParameterDirection.Input);
            p_IdSolicitud.Value = IdSolicitud;

            OracleParameter p_ValidaCorreo = Command.Parameters.Add("p_valida_correo", OracleDbType.Int32, ParameterDirection.Input);
            p_ValidaCorreo.Value = ValidaCorreo;

            OracleParameter p_Respuesta = Command.Parameters.Add("p_respuesta", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_Respuesta.Size = 100;
            p_Respuesta.Value = 0;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;
            
            try
            {
                Conn.Open();
                OracleDataReader dReader = Command.ExecuteReader();

                int Respuesta = Convert.ToInt32(p_Respuesta.Value.ToString());

                if (dReader.Read())
                {
                    Tuple<ProveedorEntity, UsuarioProveedorEntity> DatosRegistrados = new Tuple<ProveedorEntity, UsuarioProveedorEntity>(new ProveedorEntity(), new UsuarioProveedorEntity());
                    DatosRegistrados.Item1.IdSolicitud = Respuesta; //Devuelvo la respuesta
                    DatosRegistrados.Item1.Rut = dReader["rut_proveedor"].ToString();
                    DatosRegistrados.Item1.RazonSocial = dReader["razon_social"].ToString();
                    DatosRegistrados.Item1.Password = dReader["password"].ToString();
                    DatosRegistrados.Item1.Fecha = Convert.ToDateTime(dReader["fecha_solicitud"].ToString());

                    DatosRegistrados.Item2.Run = dReader["rut_usuario_proveedor"].ToString();
                    DatosRegistrados.Item2.Nombre = dReader["nombre"].ToString();
                    DatosRegistrados.Item2.ApellidoPat = dReader["apellido_paterno"].ToString();
                    DatosRegistrados.Item2.ApellidoMat = dReader["apellido_materno"].ToString();
                    DatosRegistrados.Item2.Email = dReader["email"].ToString();
                    DatosRegistrados.Item2.Fono = Convert.ToInt32(dReader["telefono"].ToString());
                    DatosRegistrados.Item2.FonoMovil = Convert.ToInt32(dReader["telefono_movil"].ToString());
                    DatosRegistrados.Item2.MotivoRegistro = dReader["mensaje"].ToString();

                    dReader.Dispose();
                    Command.Dispose();
                    Conn.Close();
                    Conn.Dispose();
                    return DatosRegistrados;
                }

                
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();
                return null;

                //3: el administrador decidió, aceptado o rechazado
                //2: la solicitud ya había sido validada por correo
                //1: la solicitud es validada con éxito
            }
            catch
            {
                return null; //Ocurrió algún error...
            }
        }
        public static int GetMiIdProveedor(string RunProv)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetMiIdProveedor");

            OracleParameter p_Run = Command.Parameters.Add("p_run_prov", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Run.Value = RunProv;

            OracleParameter p_IdUserProv = Command.Parameters.Add("p_id_proveedor", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_IdUserProv.Size = 100;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();

                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                return Convert.ToInt32(p_IdUserProv.Value.ToString());
            }
            catch
            {
                return -1;
            }
        }
        public static UsuarioProveedorEntity GetTipoRazón(int idProveedor)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetTipoRazon");

            OracleParameter p_idProve = Command.Parameters.Add("idProveedor", OracleDbType.Int16, ParameterDirection.Input);
            p_idProve.Value = idProveedor;
            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;
            Conn.Open();
            OracleDataReader dReader = Command.ExecuteReader();

            if (dReader.Read())
            {
                UsuarioProveedorEntity UsuarioProveedor = new UsuarioProveedorEntity();
                UsuarioProveedor.descripcionTipo = Convert.ToString(dReader["CODIGO_TIPO_RAZON"].ToString());
      
                dReader.Dispose();
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();
                return UsuarioProveedor;
            }
            else
            {
                UsuarioProveedorEntity UsuarioProveedor = new UsuarioProveedorEntity();
                UsuarioProveedor.descripcionTipo = "P";
                dReader.Dispose();
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();
                return UsuarioProveedor;
            }
        }

        public static UsuarioProveedorEntity GetUsuarioProveedor(string RunUsuarioProv)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetUsuarioProveedor");

            OracleParameter p_RutUsuarioProv = Command.Parameters.Add("p_run_proveedor", OracleDbType.Varchar2, ParameterDirection.Input);
            p_RutUsuarioProv.Value = RunUsuarioProv;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            Conn.Open();
            OracleDataReader dReader = Command.ExecuteReader();

            if (dReader.Read())
            {
                UsuarioProveedorEntity UsuarioProveedor = new UsuarioProveedorEntity();
                UsuarioProveedor.IdUserProv = Convert.ToInt32(dReader["id_usuario_proveedor"].ToString());
                UsuarioProveedor.Run = dReader["rut_usuario_proveedor"].ToString(); //run***
                UsuarioProveedor.Nombre = dReader["nombre"].ToString();
                UsuarioProveedor.ApellidoPat = dReader["apellido_paterno"].ToString();
                UsuarioProveedor.ApellidoMat = dReader["apellido_materno"].ToString();
                UsuarioProveedor.Email = dReader["email"].ToString();
                UsuarioProveedor.Fono = Convert.ToInt32(dReader["telefono"].ToString());
                UsuarioProveedor.FonoMovil = Convert.ToInt32(dReader["telefono_movil"].ToString());
                UsuarioProveedor.AccesoPortal = Convert.ToInt32(dReader["acceso_a_sistema"].ToString());
                UsuarioProveedor.Vigente = Convert.ToInt32(dReader["vigente"].ToString());
                dReader.Dispose();
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();
                return UsuarioProveedor;
            }
            else
            {
                dReader.Dispose();
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();
                return null;
            }
        }

        public static UsuarioProveedorEntity ValidaEmailRun(string Email, string RunUserProv)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.ValidaEmailRun");

            OracleParameter p_Email = Command.Parameters.Add("p_email", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Email.Value = Email;

            OracleParameter p_RunUsuarioProv = Command.Parameters.Add("p_run_proveedor", OracleDbType.Varchar2, ParameterDirection.Input);
            p_RunUsuarioProv.Value = RunUserProv;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            Conn.Open();
            OracleDataReader dReader = Command.ExecuteReader();

            if (dReader.Read())
            {
                UsuarioProveedorEntity userProv = new UsuarioProveedorEntity();
                userProv.Run = RunUserProv;
                userProv.Email = Email;
                userProv.AccesoPortal = Convert.ToInt32(dReader["ACCESO_A_SISTEMA"].ToString());
                userProv.Vigente = Convert.ToInt32(dReader["vigente"].ToString());
                dReader.Dispose();
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();
                return userProv;
            }
            else
            {
                dReader.Dispose();
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();
                return null;
            }
        }

        public static int SetPassword(string RunUserProv, string Password)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.SetPassword");

            OracleParameter p_RunUsuarioProv = Command.Parameters.Add("p_run_proveedor", OracleDbType.Varchar2, ParameterDirection.Input);
            p_RunUsuarioProv.Value = RunUserProv;

            OracleParameter p_Password = Command.Parameters.Add("p_password", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Password.Value = Password;

            OracleParameter p_Respuesta = Command.Parameters.Add("p_respuesta", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_Respuesta.Size = 100;
            p_Respuesta.Value = 0;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                int Respuesta = Convert.ToInt32(p_Respuesta.Value.ToString());

                // p_respuesta:
                // 1: Actualizada
                // 2: Error

                return Respuesta;
            }
            catch
            {
                //Error interno (conexión bd, etc...)
                return -1;
            }
        }

        public static int InsertConsulta(string Nombre, string ApellidoPat, string ApellidoMat, string Email, int FonoFijo, int FonoMovil, string Mensaje, DateTime Fecha)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.InsertConsulta");

            OracleParameter p_Nombre = Command.Parameters.Add("p_nombre", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Nombre.Value = Nombre;

            OracleParameter p_ApellidoPa = Command.Parameters.Add("p_apellido_pa", OracleDbType.Varchar2, ParameterDirection.Input);
            p_ApellidoPa.Value = ApellidoPat;

            OracleParameter p_ApellidoMa = Command.Parameters.Add("p_apellido_ma", OracleDbType.Varchar2, ParameterDirection.Input);
            p_ApellidoMa.Value = ApellidoMat;

            OracleParameter p_Email = Command.Parameters.Add("p_email", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Email.Value = Email;

            OracleParameter p_TelefonoFijo = Command.Parameters.Add("p_telefono", OracleDbType.Int32, ParameterDirection.Input);
            p_TelefonoFijo.Value = FonoFijo;

            OracleParameter p_TelefonoMovil = Command.Parameters.Add("p_telefono_movil", OracleDbType.Int32, ParameterDirection.Input);
            p_TelefonoMovil.Value = FonoMovil;

            OracleParameter p_Mensaje = Command.Parameters.Add("p_mensaje", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Mensaje.Value = Mensaje;

            OracleParameter p_Fecha = Command.Parameters.Add("p_fecha", OracleDbType.Date, ParameterDirection.Input);
            p_Fecha.Value = Fecha;

            OracleParameter p_Respuesta = Command.Parameters.Add("p_respuesta", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_Respuesta.Size = 100;
            p_Respuesta.Value = 0;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();

                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                int Respuesta = Convert.ToInt32(p_Respuesta.Value.ToString());
                return Respuesta;
            }
            catch
            {
                return -1;
            }
        }

        public static int InsertRegistroCambioPass(string RunUserProv, DateTime FechaCreacion)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.InsertRegistroCambioPass");

            OracleParameter p_RunUsuarioProv = Command.Parameters.Add("p_run_proveedor", OracleDbType.Varchar2, ParameterDirection.Input);
            p_RunUsuarioProv.Value = RunUserProv;

            OracleParameter p_FechaCreacion = Command.Parameters.Add("p_fecha_creacion", OracleDbType.Date, ParameterDirection.Input);
            p_FechaCreacion.Value = FechaCreacion;

            OracleParameter p_Respuesta = Command.Parameters.Add("p_respuesta", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_Respuesta.Size = 100;
            p_Respuesta.Value = 0;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();

                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                int Respuesta = Convert.ToInt32(p_Respuesta.Value.ToString());

                // p_respuesta:
                // 1: Actualizada
                // 2: Error

                return Respuesta;
            }
            catch
            {
                //Error interno (conexión bd, etc...)
                return -1;
            }
        }

        public static void SetRegistroCambioPass(int IdSolicitud, DateTime FechaActualizacion)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.SetRegistroCambioPass");

            OracleParameter p_IdSolicitud = Command.Parameters.Add("p_id_solicitud", OracleDbType.Int32, ParameterDirection.Input);
            p_IdSolicitud.Value = IdSolicitud;

            OracleParameter p_FechaActualizacion = Command.Parameters.Add("p_fecha_actualizacion", OracleDbType.Date, ParameterDirection.Input);
            p_FechaActualizacion.Value = FechaActualizacion;

            Conn.Open();
            Command.ExecuteNonQuery();

            Command.Dispose();
            Conn.Close();
            Conn.Dispose();
        }

        public static Tuple<ProveedorEntity, UsuarioProveedorEntity> GetProveedorExistente(string Rut)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetProveedorExistente");

            OracleParameter p_Rut = Command.Parameters.Add("p_rut_proveedor", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Rut.Value = Rut;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            try
            {
                Conn.Open();
                OracleDataReader dReader = Command.ExecuteReader();

                if (dReader.Read())
                {
                    Tuple<ProveedorEntity, UsuarioProveedorEntity> Proveedor = new Tuple<ProveedorEntity, UsuarioProveedorEntity>(new ProveedorEntity(), new UsuarioProveedorEntity());
                    Proveedor.Item1.Rut = dReader["rut_proveedor"].ToString();
                    Proveedor.Item1.RazonSocial = dReader["razon_social"].ToString();

                    Proveedor.Item2.Nombre = dReader["nombre"].ToString();
                    Proveedor.Item2.ApellidoPat = dReader["apellido_paterno"].ToString();
                    Proveedor.Item2.ApellidoMat = dReader["apellido_materno"].ToString();
                    Proveedor.Item2.Email = dReader["email"].ToString();
                    Proveedor.Item2.Fono = Convert.ToInt32(dReader["telefono"].ToString());
                    Proveedor.Item2.FonoMovil = Convert.ToInt32(dReader["telefono_movil"].ToString());

                    dReader.Dispose();
                    Command.Dispose();
                    Conn.Close();
                    Conn.Dispose();

                    return Proveedor;
                }
                else
                    return null;

            }
            catch
            {
                return null;
            }
        }

        //Valida a usuarios kaufmann y a proveedores
        public static UsuarioProveedorEntity.UsuarioKaufmann getValidaLogin(string Email, string Pass, int TipoUser)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.ValidaLogin");

            OracleParameter p_Email = Command.Parameters.Add("p_Email", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Email.Value = Email;

            OracleParameter p_Pass = Command.Parameters.Add("p_password", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Pass.Value = Pass;

            OracleParameter p_TipoUser = Command.Parameters.Add("p_tipo_user", OracleDbType.Int32, ParameterDirection.Input);
            p_TipoUser.Value = TipoUser;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            Conn.Open();
            //Command.ExecuteNonQuery();
            OracleDataReader dReader = Command.ExecuteReader(); /*Util.InstanciarCursor(p_Cursor, 1);*/

            if (dReader.Read())
            {
                //Falta capturar el rol y acceso a sistemas de cada usuario
                UsuarioProveedorEntity.UsuarioKaufmann User;

                if (TipoUser == 10)
                {
                    User = new UsuarioProveedorEntity.UsuarioKaufmann(Convert.ToInt32(dReader["ID_USUARIO_PROVEEDOR"].ToString()), dReader["rut_usuario_proveedor"].ToString(), dReader["nombre"].ToString(), dReader["apellido_paterno"].ToString(), dReader["apellido_materno"].ToString(), Convert.ToInt16(dReader["acceso_a_sistema"].ToString()), Convert.ToInt32(dReader["id_proveedor"].ToString()), Convert.ToInt16(dReader["tipo_usuario_prov"].ToString()));
                }
                else
                    User = new UsuarioProveedorEntity.UsuarioKaufmann(dReader["username"].ToString());

                dReader.Dispose();
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();
                return User;
            }
            else
                return null; //Datos inválidos
        }

        public static int ValidaLinkPass(int IdSolicitud)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.ValidaLinkPass");

            OracleParameter p_IdSolicitud = Command.Parameters.Add("p_id_solicitud", OracleDbType.Int32, ParameterDirection.Input);
            p_IdSolicitud.Value = IdSolicitud;

            OracleParameter p_Resp = Command.Parameters.Add("p_resp", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_Resp.Value = 0;

            Conn.Open();
            Command.ExecuteNonQuery();

            Command.Dispose();
            Conn.Close();
            Conn.Dispose();

            int Resp = Convert.ToInt32(p_Resp.Value.ToString());

            return Resp;
        }

        // Obtiene el administrador de una razón social (proveedor)
        public static List<UsuarioProveedorEntity> GetAdmRS(int IdUserProv)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetAdmRS");

            OracleParameter p_IDProv = Command.Parameters.Add("p_id_rs", OracleDbType.Int32, ParameterDirection.Input);
            p_IDProv.Value = IdUserProv;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            Conn.Open();
            OracleDataReader dReader = Command.ExecuteReader();
            
            List<UsuarioProveedorEntity> listAdmins = new List<UsuarioProveedorEntity>();

            while (dReader.Read())
                listAdmins.Add(new UsuarioProveedorEntity(Convert.ToInt32(dReader["id_usuario_proveedor"].ToString()),
                                                                     dReader["rut_usuario_proveedor"].ToString(),
                                                                     dReader["nombre"].ToString(),
                                                                     dReader["apellido_paterno"].ToString(),
                                                                     dReader["apellido_materno"].ToString(),
                                                                     dReader["email"].ToString(),
                                                                     Convert.ToInt32(dReader["telefono"].ToString()),
                                                                     Convert.ToInt32(dReader["telefono_movil"].ToString()),
                                                                     Convert.ToInt32(dReader["acceso_a_sistema"].ToString())));

            dReader.Dispose();
            Command.Dispose();
            Conn.Close();
            Conn.Dispose();
            return listAdmins;
        }

        // Obtiene los usuarios de una razón social (proveedor)
        public static List<UsuarioProveedorEntity> GetUsersRS(int IdUserProv)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetUsersRS");

            OracleParameter p_IDProv = Command.Parameters.Add("p_id_rs", OracleDbType.Int32, ParameterDirection.Input);
            p_IDProv.Value = IdUserProv;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            Conn.Open();
            OracleDataReader dReader = Command.ExecuteReader();

            List<UsuarioProveedorEntity> listAllUsers = new List<UsuarioProveedorEntity>();

            while (dReader.Read())
                listAllUsers.Add(new UsuarioProveedorEntity(Convert.ToInt32(dReader["id_usuario_proveedor"].ToString()),
                                                                     dReader["rut_usuario_proveedor"].ToString(),
                                                                     dReader["nombre"].ToString(),
                                                                     dReader["apellido_paterno"].ToString(),
                                                                     dReader["apellido_materno"].ToString(),
                                                                     dReader["email"].ToString(),
                                                                     Convert.ToInt32(dReader["telefono"].ToString()),
                                                                     Convert.ToInt32(dReader["telefono_movil"].ToString()),
                                                                     Convert.ToInt32(dReader["acceso_a_sistema"].ToString()),
                                                                     Convert.ToInt32(dReader["tipo_usuario_prov"].ToString())));


            dReader.Dispose();
            Command.Dispose();
            Conn.Close();
            Conn.Dispose();
            return listAllUsers;
        }


        //Obtienes los datos de la empresa (proveedor)
        public static ProveedorEntity GetProveedor(int IdProv)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetProveedor");

            OracleParameter p_IDProv = Command.Parameters.Add("p_id_prov", OracleDbType.Int32, ParameterDirection.Input);
            p_IDProv.Value = IdProv;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            Conn.Open();
            OracleDataReader dReader = Command.ExecuteReader();

            if (dReader.Read())
            {
                ProveedorEntity proveedor = new ProveedorEntity(Convert.ToInt32(dReader["id_proveedor"].ToString()), dReader["rut_proveedor"].ToString(), dReader["razon_social"].ToString());
                dReader.Dispose();
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();
                return proveedor;
            }
            else
            {
                dReader.Dispose();
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();
                return null;
            }
        }

        public static int InsertSolicitudRegUsuario(int IdProv, int IdUsuarioProveedor, DateTime FechaSolicitudRegistro)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.InsertSolicitudRegUsuario");

            OracleParameter p_IdProv = Command.Parameters.Add("p_id_prov", OracleDbType.Int32, ParameterDirection.Input);
            p_IdProv.Value = IdProv;

            OracleParameter p_IdUsuarioProv = Command.Parameters.Add("p_id_user_prov", OracleDbType.Int32, ParameterDirection.Input);

            if (IdUsuarioProveedor == 0)
                p_IdUsuarioProv.Value = DBNull.Value;
            else
                p_IdUsuarioProv.Value = IdUsuarioProveedor;

            OracleParameter p_FechaCreacion = Command.Parameters.Add("p_fech_sol_user", OracleDbType.Date, ParameterDirection.Input);
            p_FechaCreacion.Value = FechaSolicitudRegistro;

            OracleParameter p_IdRegistro = Command.Parameters.Add("p_respuesta", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_IdRegistro.Size = 100;
            p_IdRegistro.Value = 0;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                int IdRegistro = Convert.ToInt32(p_IdRegistro.Value.ToString()); //id de solicitud

                // p_respuesta:
                // 1: Actualizada
                // 2: Error

                return IdRegistro;
            }
            catch
            {
                //Error interno (conexión bd, etc...)
                return -1;
            }
        }

        public static void InsertUserProveedorSolicitada(int IdRegistro, string RutProveedor, string Nombre, string AP, string AM, string Email, int FonoFijo, int FonoMovil, string Password, DateTime FechaCreacion)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.InsertUserProveedorSolicitada");

            OracleParameter p_IdReg = Command.Parameters.Add("p_id_registro", OracleDbType.Int32, ParameterDirection.Input);
            p_IdReg.Value = IdRegistro;

            OracleParameter p_RutProv = Command.Parameters.Add("p_run_user_prov", OracleDbType.Varchar2, ParameterDirection.Input);
            p_RutProv.Value = RutProveedor;

            OracleParameter p_Nombre = Command.Parameters.Add("p_nombre", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Nombre.Value = Nombre;

            OracleParameter p_AP = Command.Parameters.Add("p_ap", OracleDbType.Varchar2, ParameterDirection.Input);
            p_AP.Value = AP;

            OracleParameter p_AM = Command.Parameters.Add("p_am", OracleDbType.Varchar2, ParameterDirection.Input);
            p_AM.Value = AM;

            OracleParameter p_Email = Command.Parameters.Add("p_email", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Email.Value = Email;

            OracleParameter p_FonoF = Command.Parameters.Add("p_fono", OracleDbType.Int32, ParameterDirection.Input);
            p_FonoF.Value = FonoFijo;

            OracleParameter p_FonoM = Command.Parameters.Add("p_fono_movil", OracleDbType.Int32, ParameterDirection.Input);
            p_FonoM.Value = FonoMovil;

            OracleParameter p_Pass = Command.Parameters.Add("p_pass", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Pass.Value = Password;

            OracleParameter p_FechaCreacion = Command.Parameters.Add("p_fecha_creacion", OracleDbType.Date, ParameterDirection.Input);
            p_FechaCreacion.Value = FechaCreacion;

            Conn.Open();
            Command.ExecuteNonQuery();
            
            Command.Dispose();
            Conn.Close();
            Conn.Dispose();
        }

        public static Tuple<ProveedorEntity, UsuarioProveedorEntity, int> GetSolicitudRegUsuario(int IdRegistro, int TipoSolicitud)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetSolicitudRegUsuario");

            OracleParameter p_IdReg = Command.Parameters.Add("p_id_prov", OracleDbType.Int32, ParameterDirection.Input);
            p_IdReg.Value = IdRegistro;

            OracleParameter p_TipoSolicitud = Command.Parameters.Add("p_tipo_solicitud", OracleDbType.Int32, ParameterDirection.Input);
            p_TipoSolicitud.Value = TipoSolicitud;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            Conn.Open();
            OracleDataReader dReader = Command.ExecuteReader();

            if (dReader.Read())
            {
                int Registrado = dReader["fecha_creacion"].ToString() != string.Empty ? 1 : 2; //1: Si está registrado | 2: No se ha registrado
                Tuple<ProveedorEntity, UsuarioProveedorEntity, int> datosSolicitud = null;

                switch(TipoSolicitud)
                {
                    case 10:
                        datosSolicitud = new Tuple<ProveedorEntity, UsuarioProveedorEntity, int>(new ProveedorEntity(Convert.ToInt32(dReader["id_proveedor"].ToString()), dReader["rut_proveedor"].ToString(), dReader["razon_social"].ToString()), null, Registrado);
                        break;
                    case 20:
                        datosSolicitud = new Tuple<ProveedorEntity, UsuarioProveedorEntity, int>(new ProveedorEntity(Convert.ToInt32(dReader["id_proveedor"].ToString()), dReader["rut_proveedor"].ToString(), dReader["razon_social"].ToString()), new UsuarioProveedorEntity(Convert.ToInt32(dReader["id_usuario_proveedor"].ToString()), dReader["nombre"].ToString(), dReader["apellido_paterno"].ToString(), dReader["apellido_materno"].ToString()), Registrado);
                        break;
                }

                dReader.Dispose();
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();
                return datosSolicitud;
            }
            else
            {
                dReader.Dispose();
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();
                return null;
            }
        }

        public static int VerificaSiEmailExiste(string Email)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.VerificaSiEmailExiste");

            OracleParameter p_Email = Command.Parameters.Add("p_email", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Email.Value = Email;

            OracleParameter p_Respuesta = Command.Parameters.Add("p_respuesta", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_Respuesta.Size = 100;
            p_Respuesta.Value = 0;

            Conn.Open();
            Command.ExecuteNonQuery();

            Command.Dispose();
            Conn.Close();
            Conn.Dispose();

            int Respuesta = Convert.ToInt32(p_Respuesta.Value.ToString());
            p_Respuesta.Dispose();

            return Respuesta;
        }

        public static int SetVigenciaUsuarioPortal(int IdUserProv, int EstadoVigencia, DateTime FechaModificacion)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.SetVigenciaUsuarioPortal");

            OracleParameter p_IdUserProv = Command.Parameters.Add("p_id_user_prov", OracleDbType.Int32, ParameterDirection.Input);
            p_IdUserProv.Value = IdUserProv;

            OracleParameter p_EstadoVigencia = Command.Parameters.Add("p_estado_vigencia", OracleDbType.Int32, ParameterDirection.Input);
            p_EstadoVigencia.Value = EstadoVigencia;

            OracleParameter p_FechaMod = Command.Parameters.Add("p_fecha_modificacion", OracleDbType.Date, ParameterDirection.Input);
            p_FechaMod.Value = FechaModificacion;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();
                
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();
                return 1;
            }
            catch
            {
                return -1;
            }
        }

        public static int SetDatosContactoProv(string RunProv, string Nombre, string ApellidoPaterno, string ApellidoMaterno, string Email, int Fono, int FonoMovil)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.SetDatosContactoProv");

            OracleParameter p_Run = Command.Parameters.Add("p_run_prov", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Run.Value = RunProv;

            OracleParameter p_Nombre = Command.Parameters.Add("p_nombre", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Nombre.Value = Nombre;

            OracleParameter p_AP = Command.Parameters.Add("p_apellido_pa", OracleDbType.Varchar2, ParameterDirection.Input);
            p_AP.Value = ApellidoPaterno;

            OracleParameter p_AM = Command.Parameters.Add("p_apellido_ma", OracleDbType.Varchar2, ParameterDirection.Input);
            p_AM.Value = ApellidoMaterno;

            OracleParameter p_Email = Command.Parameters.Add("p_email", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Email.Value = Email;

            OracleParameter p_Fono = Command.Parameters.Add("p_fono", OracleDbType.Int32, ParameterDirection.Input);
            p_Fono.Value = Fono;

            OracleParameter p_FonoMovil = Command.Parameters.Add("p_fono_movil", OracleDbType.Int32, ParameterDirection.Input);
            p_FonoMovil.Value = FonoMovil;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();

                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                return 1;
            }
            catch
            {
                return -1;
            }
        }

        public static int GetMiRazonSocial(string RunProv)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetMiRazonSocial");

            OracleParameter p_Run = Command.Parameters.Add("p_run_prov", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Run.Value = RunProv;

            OracleParameter p_IdUserProv = Command.Parameters.Add("p_id_proveedor", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_IdUserProv.Value = -1;
            p_IdUserProv.Size = 100;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();
                
                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                return Convert.ToInt32(p_IdUserProv.Value.ToString());
            }
            catch
            {
                return -1;
            }
        }
        public static int GetMiRazonSocial_ParaUsuarioProveedor(string RunProv)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetMiRazonSocial");

            OracleParameter p_Run = Command.Parameters.Add("p_run_prov", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Run.Value = RunProv;

            OracleParameter p_IdUserProv = Command.Parameters.Add("p_id_proveedor", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_IdUserProv.Size = 100;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();

                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                return Convert.ToInt32(p_IdUserProv.Value.ToString());
            }
            catch
            {
                return -1;
            }
        }
       

        public static int InsertSolicitudAccesoSistema(int IdSistema, int IdUsuarioProv, DateTime FechaSoli)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.InsertSolicitudAccesoSistema");

            OracleParameter p_IdSis = Command.Parameters.Add("p_id_sistema", OracleDbType.Int32, ParameterDirection.Input);
            p_IdSis.Value = IdSistema;

            OracleParameter p_IdUsuarioProv = Command.Parameters.Add("p_id_usuario_prov", OracleDbType.Int32, ParameterDirection.Input);
            p_IdUsuarioProv.Value = IdUsuarioProv;

            OracleParameter p_Fecha = Command.Parameters.Add("p_fecha", OracleDbType.Date, ParameterDirection.Input);
            p_Fecha.Value = FechaSoli;

            OracleParameter p_Resp = Command.Parameters.Add("p_respuesta", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_Resp.Value = -1;
            p_Resp.Size = 100;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();

                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                return Convert.ToInt16(p_Resp.Value.ToString());
            }
            catch
            {
                return -1;
            }
        }

        public static List<SistemaEntity> GetSistemasParaUsersProv(string RunUserProv)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetSistemasParaUsersProv");

            OracleParameter p_Run = Command.Parameters.Add("p_run_usuario", OracleDbType.Varchar2, ParameterDirection.Input);
            p_Run.Value = RunUserProv;

            Command.Parameters.Add(new OracleParameter("IO_CURSOR", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;

            Conn.Open();
            OracleDataReader dReader = Command.ExecuteReader();

            List<SistemaEntity> Sistemas = null;

            while (dReader.Read())
            {
                Sistemas = new List<SistemaEntity>();
                Sistemas.Add(new SistemaEntity(Convert.ToInt32(dReader["id_sistema"].ToString()), dReader["nombre"].ToString(), dReader["url"].ToString(), dReader["nombre_img"].ToString(), Convert.ToInt32(dReader["estado"].ToString())));
            }

            return Sistemas;
        }

        public static int GetSistemaAcceso(int IdUsuarioProv)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.GetSistemaAcceso");

            OracleParameter p_IdUsuarioProv = Command.Parameters.Add("p_id_usuario_prov", OracleDbType.Int32, ParameterDirection.Input);
            p_IdUsuarioProv.Value = IdUsuarioProv;

            OracleParameter p_Resp = Command.Parameters.Add("p_respuesta", OracleDbType.Int32, ParameterDirection.InputOutput);
            p_Resp.Value = -1;
            p_Resp.Size = 100;

            try
            {
                Conn.Open();
                Command.ExecuteNonQuery();

                Command.Dispose();
                Conn.Close();
                Conn.Dispose();

                return Convert.ToInt16(p_Resp.Value.ToString());
            }
            catch
            {
                return -1;
            }
        }

        public static void SetReasignarAdmEnRS(int IdRegistro, DateTime FechaEliminacion)
        {
            DAO_Conexion objConexion = new DAO_Conexion();
            OracleConnection Conn = objConexion.getConexion();
            OracleCommand Command = objConexion.getCommand("PCK_PORTAL_PROVEEDORES.SetReasignarAdmEnRS");

            OracleParameter p_IdRegistro = Command.Parameters.Add("p_id_registro", OracleDbType.Int32, ParameterDirection.Input);
            p_IdRegistro.Value = IdRegistro;

            OracleParameter p_FechaEliminacion = Command.Parameters.Add("p_fecha_eliminacion", OracleDbType.Date, ParameterDirection.Input);
            p_FechaEliminacion.Value = FechaEliminacion;

            Conn.Open();
            Command.ExecuteNonQuery();
            
            Command.Dispose();
            Conn.Close();
            Conn.Dispose();
        }
    }
}
