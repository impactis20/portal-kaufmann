﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="contactenos.aspx.cs" Inherits="PortalProveedores.App.UI.contactenos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title></title>
    <link type="text/css" href="Css/kf_style.css" rel="stylesheet" />    
    <link type="text/css" href="Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="Css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link type="text/css" href="Css/preview.min.css" rel="stylesheet" />    
    <link type="text/css" href="Css/font-awesome.min.css" rel="stylesheet" />

    <%--<script src="http://code.jquery.com/jquery-1.5.js"></script>--%>

    <script type="text/javascript">
        function countChar(val) {
            var len = val.value.length;
            var lblContador = document.getElementById("lblContador");
            if (len >= 501) {
                val.value = val.value.substring(0, 500);
            } else {
                //$('#lblContador').text("Caracteres restantes permitidos: " + (500 - len));
                lblContador.innerText = "Caracteres restantes permitidos: " + (500 - len);
            }
        };
    </script>

    <script>
        function validate(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>

    <style type="text/css">
        .html, body{
            background-color: rgba(255, 255, 255, 0);
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="tabbable custom-tabs fadeInUp">
            <div class="tab-content"> 
                <div class="row-fluid">
                    <div class="span5">
                        <h3><i class="glyphicon glyphicon-envelope"></i>&nbsp;&nbsp; Contáctenos</h3>
                        <p>Envíenos todas sus dudas o consultas llenando este formulario. Considere que los campos con asterisco (*) son obligatorios.</p>
                        <br />
                        <div id="cNombres" runat="server" class="form-group">
                            <span class="text-danger text-bold">*&nbsp;</span><label>Nombres</label>
                            <asp:TextBox ID="txtNombres" runat="server" placeholder="Ingrese sus nombres..." CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <div class="span12">
                                <div class="span6">
                                    <div id="cApellidoPat" runat="server" class="form-group">
                                        <span class="text-danger text-bold">*&nbsp;</span><label>Apellido Paterno</label>
                                        <asp:TextBox ID="txtApellidoPat" runat="server" placeholder="Ingrese apellido paterno..." CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="span6">
                                    <div id="cApellidoMat" runat="server" class="form-group">
                                        <span class="text-danger text-bold">*&nbsp;</span><label>Apellido Materno</label>
                                        <asp:TextBox ID="txtApellidoMat" runat="server" placeholder="Ingrese apellido materno..." CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="cEmail" runat="server" class="form-group">
                            <span class="text-danger text-bold">*&nbsp;</span><label>Correo Electrónico</label>
                            <asp:TextBox ID="txtEmail" runat="server" placeholder="Ingrese su correo electrónico" CssClass="form-control" TextMode="Email"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <div class="span12">
                                <div class="span6">
                                    <div id="cFonoFijo" runat="server" class="form-group">
                                        <span class="text-danger text-bold">*&nbsp;</span><label>Télefono Fijo</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">+56</div>
                                            <asp:TextBox ID="txtFonoFijo" runat="server" placeholder="Red fija..." MaxLength="9" CssClass="form-control" onkeypress='validate(event)'></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="span6">
                                    <div id="cFonoMovil" runat="server" class="form-group">
                                        <span class="text-danger text-bold">*&nbsp;</span><label>Teléfono Móvil</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">+56 9</div>
                                            <asp:TextBox ID="txtFonoMovil" runat="server" placeholder="Celular..." MaxLength="8" CssClass="form-control" onkeypress='validate(event)'></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="cMensaje" runat="server" class="form-group">
                            <span class="text-danger text-bold">*&nbsp;</span><label>Mensaje</label>
                            <asp:TextBox ID="txtMensaje" runat="server" placeholder="Escriba el mensaje a enviar..." CssClass="form-control max-width-txtarea" onkeyup="countChar(this)" TextMode="MultiLine" Height="180"></asp:TextBox>
                            <asp:Label ID="lblContador" runat="server" CssClass="help-block"></asp:Label>
                        </div>
                        <br />
                        <asp:Button ID="btnEnviarMsj" runat="server" CssClass="btn btn-primary" Text="Enviar" OnClick="btnEnviarMsj_Click"></asp:Button>
                    </div>
                    <div class="span7">
                        <div class="row-fluid">
                            <div class="span12">
                                <h3><i class="glyphicon glyphicon-pushpin"></i>&nbsp;&nbsp;Localícenos</h3>
                                <div class="map"></div>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span6">
                                <h4><i class="glyphicon glyphicon-arrow-down"></i>&nbsp;&nbsp;Contactos</h4>
                                <address>
                                    <span><i class="glyphicon glyphicon-envelope"></i>&nbsp;&nbsp;<a href="mailto:#">consultaproveedores@kaufmann.cl</a></span>
                                </address>
                            </div>
                            <div class="span6">
                                <h4><i class="glyphicon glyphicon-pushpin"></i>&nbsp;&nbsp;Nuestra Dirección</h4>

                                <address>
                                    <strong>Kaufmann</strong> Casa Matriz.<br />
                                    Av. Gladys Marín 5830, Estación Central, Santiago de Chile.
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="Js/iframeResizer.contentWindow.min.js"></script>
    </form>
</body>
</html>