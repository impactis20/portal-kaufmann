﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI
{
    public partial class olvidepassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRestablecerContrasena_Click(object sender, EventArgs e)
        {
            string RunUserProv = txtRunUserProv.Text;
            string Email = txtEmailRecuperar.Text.ToLower().Trim();

            bool EstadoTxtRun = string.IsNullOrEmpty(RunUserProv);
            bool EstadoTxtEmail = string.IsNullOrEmpty(Email);

            //Si están vacíos, que queden de color rojo
            NormalizaFormularioRegistro();

            if (EstadoTxtRun || EstadoTxtEmail)
            {
                MostrarMensaje("Datos incompletos", "Estimado proveedor, ambos campos son obligatorios.", "text-danger"); //Mensaje de error al dejar campos obligatorios vacíos.
            }
            else
            {
                //Validar datos ingresados
                #region Email
                bool EmailValido = App_Code.Util.isValidEmail(Email);

                if (!EmailValido)
                {
                    MostrarMensaje("Error", "Su Email es inválido, por favor corríjalo.", "text-danger");
                    SetColorTxT(oEmail, "form-group has-error");
                    return;
                }
                #endregion

                #region RUN
                bool RunValido = App_Code.Util.RutValido(RunUserProv);

                if (!RunValido)
                {
                    MostrarMensaje("Rut inválido", string.Format("Su RUN \"{0}\" es inválido, corríjalo.", RunUserProv), "text-danger");
                    SetColorTxT(oRun, "form-group has-error");
                    return;
                }
                #endregion

                try
                {
                    UsuarioProveedorEntity RespuestaRestablecerPass = PortalProveedorBL.ValidaEmailRun(Email, RunUserProv);

                    if (RespuestaRestablecerPass == null) //Datos inválidos (usuario no encontrado)
                    {
                        MostrarMensaje("Datos incorrectos", "El Rut o Email no son correctos, ya que estos datos no fueron encontrados. Corríjalos y vuelva a intentarlo.", "text-danger");
                        SetColorTxT(oRun, "form-group has-error");
                        SetColorTxT(oEmail, "form-group has-error");
                    }
                    else if (RespuestaRestablecerPass.AccesoPortal == 2) //usuario bloqueado: no puede restablecer su contraseña
                    {
                        MostrarMensaje("Usuario bloqueado", "Estimado usuario, usted no podrá restablecer su contraseña, ya que el estado de su cuenta es <b>bloqueado</b>. Para mayor información contacte al <b>administrador de la razón social</b> que usted pertenece.", "text-danger");
                        SetColorTxT(oRun, "form-group has-error");
                        SetColorTxT(oEmail, "form-group has-error");
                    }
                    else if (RespuestaRestablecerPass.Vigente == 1 && RespuestaRestablecerPass.AccesoPortal == 1)
                    {
                        //Guarda registro del evento en la base de datos
                        int IdCambioPass = PortalProveedorBL.InsertRegistroCambioPass(RunUserProv, DateTime.Now); //Devuelve el id del cambio de pass. -1 indica que hubo error

                        if (IdCambioPass != -1)
                        {
                            string PaginaLogin = Request.Url.AbsoluteUri.ToLower().Replace("olvidepassword", "proveedores");
                            string PaginaRestablecerPass = Request.Url.AbsoluteUri.ToLower().Replace("olvidepassword", "restablecerpassword");
                            string Valores = string.Format("id={0}&run_prov={1}&max_time_restablece={2}", IdCambioPass, RunUserProv, DateTime.Now.AddHours(1));
                            string ValoresEncriptados = App_Code.Util.Encriptar(Valores);
                            string URLRestablecerPass = string.Format("{0}?v={1}", PaginaRestablecerPass, ValoresEncriptados);

                            string Mensaje = string.Format("Estimado proveedor, usted podrá restablecer su contraseña accediendo al link que está a continuación. Para una mayor seguridad, el enlace tiene una validez de una hora.<br><br><a href=\"{0}\">{0}</a><br><br>Si el link ha expirado, vuelva a nuestro portal de proveedores y nuevamente haga el procedimiento de restablecer la contraseña en el módulo <b>Olvidé mi contraseña.</b><br><a href='{1}'>Ir al Portal de Proveedores</a>.<br><br><b>Si usted no generó este correo electrónico, favor de ignorar</b>.", URLRestablecerPass, PaginaLogin);

                            //Envia correo electrónico con link para restablecer contraseña
                            WebServices.EnviaCorreo(Email, "Restablecer contraseña - Kaufmann Portal de Proveedores", Mensaje);

                            MostrarMensaje("¡Datos correctos y válidos!", string.Format("Estimado proveedor, le hemos enviado un correo electrónico a <b>{0}</b> con un link para restablecer su contraseña.", Email), "text-success");

                            txtRunUserProv.Text = string.Empty;
                            txtEmailRecuperar.Text = string.Empty;
                        }
                        else
                        {
                            MostrarMensaje("Error temporal (211)", "Ha ocurrido un error interno en nuestro sistema. Por favor inténtelo más tarde.", "text-success");
                            SetColorTxT(oRun, "form-group has-error");
                            SetColorTxT(oEmail, "form-group has-error");
                        }
                    }
                }
                catch
                {
                    MostrarMensaje("Error temporal (210)", "Ha ocurrido un error interno en nuestro sistema. Por favor inténtelo más tarde.", "text-danger");
                }
            }
        }

        private void NormalizaFormularioRegistro()
        {
            ColorEstadoTxT(txtRunUserProv, oRun);
            ColorEstadoTxT(txtEmailRecuperar, oEmail);
        }

        //Es llamado cuando hay algún dato ingresado con error en el txt
        private void SetColorTxT(HtmlGenericControl div, string Estilo)
        {
            div.Attributes["class"] = Estilo;
        }

        //Valida que los txt no estén vacíos
        private void ColorEstadoTxT(TextBox txt, HtmlGenericControl div)
        {
            if (txt.Text == string.Empty)
                div.Attributes["class"] = "form-group has-error";
            else
                div.Attributes["class"] = "form-group";
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("parent.abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }
    }
}