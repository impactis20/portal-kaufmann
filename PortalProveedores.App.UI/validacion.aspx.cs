﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI
{
    public partial class validacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string ValorURL = Request.QueryString["v"];
                string ValoresDesencriptados = App_Code.Util.Desencriptar(ValorURL);

                string[] Separador = { "&" };
                string[] Valores = ValoresDesencriptados.Split(Separador, StringSplitOptions.RemoveEmptyEntries);

                int IdSolicitud = Convert.ToInt32(Valores[0].Split('=')[1]);
                int ValidaCorreo = Convert.ToInt32(Valores[1].Split('=')[1]);
                DateTime MaximaFechaVigencia = Convert.ToDateTime(Valores[2].Split('=')[1]);

                if (DateTime.Now <= MaximaFechaVigencia)
                {
                    Tuple<ProveedorEntity, UsuarioProveedorEntity> vc = PortalProveedorBL.ValidaEmail(IdSolicitud, ValidaCorreo);

                    switch (vc.Item1.IdSolicitud) //Respuesta
                    {
                        case 1:
                            titulo.InnerText = "¡Solicitud de registro enviada!";
                            msj.InnerHtml = string.Format("La solicitud de registro ha sido enviada a los administradores de este sistema. Cuando se tome una decisión se lo <b>notificaremos a su correo electrónico</b>.<br>Le recordamos nuevamente que si es <u>aceptado</u>, será usted el <b>usuario administrador de {0}</b> en este sistema.", vc.Item1.RazonSocial);
                            GetDatosRegistrados(vc.Item1.Rut, vc.Item1.RazonSocial, vc.Item1.Password, vc.Item1.Fecha, vc.Item2.MotivoRegistro, vc.Item2.Run, vc.Item2.Nombre, vc.Item2.ApellidoPat, vc.Item2.ApellidoMat, vc.Item2.Email, vc.Item2.Fono, vc.Item2.FonoMovil);
                            break;

                        case 2:
                            //titulo.InnerText = "Solicitud validada...";
                            //msj.InnerText = "La solicitud de registro ya fue validada.";
                            //GetDatosRegistrados(vc.Item1.Rut, vc.Item1.RutDigVer, vc.Item1.RazonSocial, vc.Item1.Password, vc.Item1.Fecha, vc.Item2.MotivoRegistro, vc.Item2.Run, vc.Item2.DigVer, vc.Item2.Nombre, vc.Item2.ApellidoPat, vc.Item2.ApellidoMat, vc.Item2.Email, vc.Item2.Fono, vc.Item2.FonoMovil);
                            GetPaginaInvalida();
                            break;

                        case 3:
                            GetPaginaInvalida();
                            break;
                    }
                }
                else
                {
                    //URL no vigente
                    GetPaginaInvalida();
                }
            }
            catch
            {
                GetPaginaInvalida();
            }
        }

        private void GetDatosRegistrados(string RutEmpresa, string RazonSocial, string Password, DateTime FechaSolicitud, string Mensaje, string RunUSer, string Nombre, string ApellidoPat, string ApellidoMat, string Email, int Fono, int FonoMovil)
        {
            lblRut.Text = RutEmpresa;
            lblRazonSocial.Text = RazonSocial;
            lblPassword.Text = PortalProveedorBL.PasswordOculta(Password);
            lblFechaSolicitud.Text = FechaSolicitud.ToShortDateString();
            lblMensaje.Text = Mensaje;

            lblRun.Text = RunUSer;
            lblNombre.Text = string.Format("{0} {1} {2}", Nombre, ApellidoPat, ApellidoMat);
            lblEmail.Text = Email;
            lblFono.Text = string.Format("+56 {0}", Fono);
            lblFonoMovil.Text = string.Format("+56 9 {0}", FonoMovil);
        }

        private void GetPaginaInvalida()
        {
            titulo.InnerHtml = "<i class='glyphicon glyphicon-remove'></i>&nbsp;&nbsp;Página inválida o incorrecta";
            msj.InnerHtml = "Lo sentimos, pero esta página es incorrecta o ya dejó de ser válida.<br><br>Para registrarse haga click <a href=\"proveedores.aspx#registrarse\">aquí</a>.";
            form.Visible = false;
        }
    }
}