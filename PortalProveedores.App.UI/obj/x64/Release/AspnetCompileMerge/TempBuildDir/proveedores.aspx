﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="proveedores.aspx.cs" Inherits="PortalProveedores.App.UI.proveedores" Title="Portal Proveedores Kaufmann" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="Css/kf_style.css" rel="stylesheet" />    
    <link type="text/css" href="Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="Css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link type="text/css" href="Css/preview.min.css" rel="stylesheet" />
    <%--<link href="http://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet" type="text/css">--%>
    <%--<link type="text/css" href="Css/font-awesome.min.css" rel="stylesheet" />--%>

    <script type="text/javascript" src="Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="Js/bootstrap.min.js"></script>
    <script type="text/javascript" src="Js/tabs-addon.js"></script>
    
    <script type="text/javascript">
        function abrePopUp(title, msj, estilo) {

            alertTitle.innerHTML = title;
            alertMsj.innerHTML = msj;
            alertMsj.className = estilo;

            $('#AlertPopUp').modal({
                backdrop: 'static',
                keyboard: true
            });
        }

        function regUrl(pag) {
            window.history.pushState('page', '', pag.getAttribute("href"));
        }

        function termCondi() {
            $('#TerminosCondiciones').modal({
                backdrop: 'static',
                keyboard: true
            });
        }
    </script>

    <style type="text/css">
        html, body, form{
            background-color: lightgrey;
            height: 99.83%;
            margin: 0
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }

        ul span{
            font-size:14px;
            font-weight: bold
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        
        <div class="wrap">
            <div class="row-fluid">
                <%--<div class="container" style="margin: 0 auto;">--%>
                <div class="container" style="margin: 0 auto; max-width: 1049px;">
                <%--<div class="container">--%>
                    <div class="header navbar" style="min-width:567px; border-bottom: 1px solid silver;">
                        <div class="logo-kaufmann">
                            <img src="Img/logo.kaufmann.png" class="img-logo-corp"/>
                        </div>
                        <div class="logo-marcas">
                            <img src="Img/mb_logo.png" class="img-marcas"/>
                            <img src="Img/freightliner_logo.png" class="img-marcas"/>
                            <img src="Img/fuso_logo.png" class="img-marcas"/>
                            <img src="Img/western_logo.png" class="img-marcas"/>
                        </div>
                    </div>
                    <h1>Portal de Proveedores</h1>

                    <div class="tabbable custom-tabs tabs-animated flat flat-all hide-label-980 shadow track-url auto-scroll">
                        <ul id="myTab" class="nav nav-tabs">
                            <li class="active"><a href="#login" data-toggle="tab" onclick="regUrl(this)"><i class="glyphicon glyphicon-lock"></i>&nbsp;<span>Acceder / Login</span></a></li>
                            <li class=""><a href="#registrarse" data-toggle="tab" onclick="regUrl(this)"><i class="glyphicon glyphicon-user"></i>&nbsp;<span>Registrarse</span></a></li>
                            <li class=""><a href="#olvidemipassword" data-toggle="tab" onclick="regUrl(this)"><i class="glyphicon glyphicon-asterisk"></i>&nbsp;<span>Olvidé mi contraseña</span></a></li>
                            <li class=""><a href="#contactenos" data-toggle="tab" onclick="regUrl(this)"><i class="glyphicon glyphicon-envelope"></i>&nbsp;<span>Contáctenos</span></a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="login" class="tab-pane active">
                                <div class="tab-content" style="padding: 10px 17px 10px 15px">
                                    <div class="row-fluid">
                                        <div class="span4">
                                            <h3><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp; Acceder</h3>
                                            <div id="lEmail" runat="server" class="form-group">
                                                <label>Email</label>
                                                <asp:TextBox ID="txtEmail" runat="server" class="form-control" TextMode="Email" placeholder="Ingrese su email..."></asp:TextBox>
                                            </div>
                                            <div id="lPass" runat="server" class="form-group">
                                                <label>Contraseña</label>
                                                <asp:TextBox ID="txtPassword" runat="server" class="form-control" placeholder="Ingrese su contraseña..." TextMode="Password" MaxLength="35"></asp:TextBox>
                                            </div>
                                            <%--<div class="checkbox">
                                                <asp:CheckBox ID="chbNoCerrarS" runat="server" Text="&nbsp;&nbsp;No cerrar sesión"/>
                                            </div>--%>
                                            <br />
                                            <asp:Button id="btnEntrar" runat="server" Text="Acceder" CssClass="btn btn-primary" OnClick="btnEntrar_Click"></asp:Button>
                                        </div>
                                        <div class="span4">
                                            <h3><i class="glyphicon glyphicon-info-sign"></i>&nbsp;&nbsp;Importante</h3>
                                            <div class="box">
                                                <p>Es importante que usted se registre en nuestro portal para que así pueda acceder a los sistemas de proveedores de <b>Kaufmann</b>. De esta forma, podrá obtener la información que necesite <b>en línea</b>.</p>
                                            </div>
                                        </div>
                                        <div class="span4">
                                            <h3><i class="glyphicon glyphicon-globe"></i>&nbsp;&nbsp;Nuestra página oficial</h3>
                                            <a href="http://www.kaufmann.cl">Kaufmann Chile</a>
                                            <br />
                                            <br />
                                            <a href="http://www.kaufmann.cl"><img src="Img/vuela.jpg" height="110" class="img-thumbnail"/></a>
                                            <%--<div class="socials clearfix">
                                                <a class="facebook" href="https://www.facebook.com/KaufmannChile"><img src="Img/xd.png" height="36" /></a>
                                                <a class="twitter" href="https://twitter.com/mbchile"><img src="Img/tw.png" height="36" /></a>
                                                <a class="pinterest" href="http://www.youtube.com/user/KaufmannChile"><img src="Img/yt.png" height="36" /></a>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="registrarse" class="tab-pane">
                                <iframe src="Registrarse.aspx"></iframe>
                            </div>

                            <div id="olvidemipassword" class="tab-pane">
                                <iframe src="OlvidePassword.aspx"></iframe>
                            </div>

                            <div id="contactenos" class="tab-pane">
                                <iframe src="Contactenos.aspx"></iframe>
                                <script type="text/javascript" src="Js/iframeResizer.contentWindow.min.js"></script> 
		                        <script type="text/javascript" src="Js/iframeResizer.min.js"></script> 
		                        <script type="text/javascript">
		                            iFrameResize({
		                                log: false, // Enable console logging
		                                resizedCallback: function (messageData) { // Callback fn when message is received
		                                    //setTimeout(function () { parentIFrame.sendMessage('nested') }, 50);
		                                }
		                            });
		                        </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="container" style="max-width: 1080px">
                <div class="pull-left" style="background: url(img/bg.footer.logo.png) no-repeat;padding: 35px 55px">
                    <img src="img/footer.logo.png" style="width:148px;height:24px;" />
                </div>

                <div class="col-lg-9" style="float: left; margin-top: 5px">
                    <div class="col-lg-2" style="margin-bottom:10px">
                        <div class="col-lg-12">
                            <span><b>Sitio</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;">
                            <a href="http://www.kaufmann.cl" target="_blank">Kaufmann</a>
                        </div>
                    </div>
                    <div class="col-lg-3" style="margin-bottom:10px">
                        <div class="col-lg-12">
                            <span><b>Portal Proveedores</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;">
                            <a href="policies/privacidad.aspx" target="_blank">Políticas de privacidad</a>
                            <br />
                            <a href="policies/terminos.aspx" target="_blank">Términos y condiciones</a>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="col-lg-12">
                            <span><b>Contacto Mesa de Ayuda</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;color: rgba(255, 255, 255, 0.72);">
                            <i class="glyphicon glyphicon-envelope"></i> <a href="mailto:#">consultaproveedores@kaufmann.cl</a>
                            <br />
                            <i class="glyphicon glyphicon-pushpin"></i> Av. Gladys Marín 5830, Estación Central, Santiago de Chile.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade bs-example-modal-sm" id="AlertPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="modal-title" id="alertTitle" runat="server"></h3>
                    </div>
                    <div class="modal-body">
                        <p id="alertMsj" runat="server"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <%--<button type="button" class="btn btn-primary">Save changes</button>--%>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>