﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="registrousuario.aspx.cs" Inherits="PortalProveedores.App.UI.registrousuario" Title="Registro de usuario" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="Css/kf_style.css" rel="stylesheet" />    
    <link type="text/css" href="Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="Css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link type="text/css" href="Css/preview.min.css" rel="stylesheet" />    
    <link type="text/css" href="Css/font-awesome.min.css" rel="stylesheet" />

    <script type="text/javascript" src="Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="Js/bootstrap.min.js"></script>

    <script type="text/javascript">
        function abrePopUp(title, msj, estilo) {

            alertTitle.innerHTML = title;
            alertMsj.innerHTML = msj;
            alertMsj.className = estilo;

            $('#AlertPopUp').modal({
                backdrop: 'static',
                keyboard: true
            });
        }
    </script>

    <script>
        function validate(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>

    <style type="text/css">
        .html, body{
            background-color: lightgrey;
            height: 99.85%;
            margin: 0
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="Ss" runat="server"></asp:ScriptManager>

        <div class="wrap">
            <div class="row-fluid">
                <div style="margin: 0 auto; max-width: 1000px;">
                    <div class="header navbar" style="min-width:767px; border-bottom: 1px solid silver;">
                        <div style="margin-top: 10px; float: left;">
                            <img src="Img/logo.kaufmann.png" height="40"/>
                        </div>
                        <div class="logo-marcas">
                            <img src="Img/mb_logo.png" class="img-marcas"/>
                            <img src="Img/freightliner_logo.png" class="img-marcas"/>
                            <img src="Img/fuso_logo.png" class="img-marcas"/>
                            <img src="Img/western_logo.png" class="img-marcas"/>
                        </div>
                    </div>
                    <h1>Portal de Proveedores</h1>

                    <div class="tabbable custom-tabs tabs-animated flat flat-all hide-label-980 shadow track-url auto-scroll">
                        <div class="tab-content">
                            <div class="margin-modulo">
                                <h2 id="titulo" runat="server" class="page-header"></h2>
                                <p id="msj" runat="server"></p>
                                <p id="campos" runat="server" visible="false">Todos los campos con asterisco son obligatorios (*).</p>
                                <div id="form" runat="server" style="margin-top:35px">
                                    <h2>Formulario de Registro</h2>
                                    <div class="span12" style="margin:15px 0px">
                                        <asp:UpdatePanel ID="up1" runat="server">
                                            <ContentTemplate>
                                                <div class="span6">
                                                    <h4>Datos personales</h4>
                                                    <p>Ingreses sus datos personales en el formulario.</p>
                                                    <div class="form-horizontal">
                                                        <div id="dRun" runat="server" class="form-group">
                                                            <label for="lblRun" class="col-sm-2 control-label"><span class="text-danger text-bold">*&nbsp;</span>RUN</label>
                                                            <div class="col-sm-8">
                                                                <asp:Textbox ID="txtRun" runat="server" CssClass="form-control" OnTextChanged="txtRun_TextChanged" AutoPostBack="true" MaxLength="10"></asp:Textbox>
                                                            </div>
                                                        </div>
                                                        <div id="dNombre" runat="server" class="form-group">
                                                            <label for="txtNombre" class="col-sm-2 control-label"><span class="text-danger text-bold">*&nbsp;</span>Nombres</label>
                                                            <div class="col-sm-8">
                                                                <asp:Textbox ID="txtNombre" runat="server" CssClass="form-control" MaxLength="35"></asp:Textbox>
                                                            </div>
                                                        </div>
                                                        <div id="dAP" runat="server" class="form-group">
                                                            <label for="txtAP" class="col-sm-2 control-label"><span class="text-danger text-bold">*&nbsp;</span>Apellido paterno</label>
                                                            <div class="col-sm-8">
                                                                <asp:Textbox ID="txtAP" runat="server" CssClass="form-control" MaxLength="35"></asp:Textbox>
                                                            </div>
                                                        </div>
                                                        <div id="dAM" runat="server" class="form-group">
                                                            <label for="txtAM" class="col-sm-2 control-label"><span class="text-danger text-bold">*&nbsp;</span>Apellido materno</label>
                                                            <div class="col-sm-8">
                                                                <asp:Textbox ID="txtAM" runat="server" CssClass="form-control" MaxLength="35"></asp:Textbox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6">
                                                    <h4>Datos de contacto</h4>                                        
                                                    <p>Ingrese sus datos de contacto para contactarlo si es necesario.</p>
                                                    <div class="form-horizontal">
                                                        <div class="form-group">
                                                            <label for="txtEmail" class="col-sm-2 control-label">Email</label>
                                                            <div class="col-sm-8">
                                                                <asp:Textbox ID="txtEmail" runat="server" CssClass="form-control" TextMode="Email" Enabled="false"></asp:Textbox>
                                                            </div>
                                                        </div>
                                                        <div id="dTF" runat="server" class="form-group">
                                                            <label for="txtTF" class="col-sm-2 control-label"><span class="text-danger text-bold">*&nbsp;</span>Teléfono fijo</label>
                                                            <div class="col-sm-8">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">+56</div>
                                                                    <asp:Textbox ID="txtTF" runat="server" CssClass="form-control" onkeypress='validate(event)' MaxLength="9"></asp:Textbox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="dTM" runat="server" class="form-group">
                                                            <label for="txtTM" class="col-sm-2 control-label"><span class="text-danger text-bold">*&nbsp;</span>Teléfono móvil</label>
                                                            <div class="col-sm-8">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">+56 9</div>
                                                                    <asp:Textbox ID="txtTM" runat="server" CssClass="form-control" onkeypress='validate(event)' MaxLength="8"></asp:Textbox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </div>

                                    <div class="span12" style="margin:15px 0px">
                                        <div class="span6">
                                            <h4>Password de seguridad</h4>
                                            <p>Ingrese la password de su cuenta, debe contener entre 8 y 35 caracteres. Recuerde que esto debe ser secreto.</p>
                                            <div class="form-horizontal">
                                                <div id="dPass" runat="server" class="form-group">
                                                    <label for="txtEmail" class="col-sm-2 control-label"><span class="text-danger text-bold">*&nbsp;</span>Password</label>
                                                    <div class="col-sm-8">
                                                        <asp:Textbox ID="txtPass" runat="server" CssClass="form-control" TextMode="Password" MaxLength="35"></asp:Textbox>
                                                    </div>
                                                </div>
                                                <div id="dPassRe" runat="server" class="form-group">
                                                    <label for="txtT" class="col-sm-2 control-label"><span class="text-danger text-bold">*&nbsp;</span>Repetir Password</label>
                                                    <div class="col-sm-8">
                                                        <asp:Textbox ID="txtPassRe" runat="server" CssClass="form-control" TextMode="Password" MaxLength="35"></asp:Textbox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6">
                                            <h4>Términos y condiciones</h4>
                                            <p>Léa aquí los <a href="policies/terminos.aspx" target="_blank">términos & condiciones</a> y acéptelos para registrarse. También, infórmese acerca de nuestras <a href="policies/privacidad.aspx">políticas de privacidad</a>.</p>
                                            <div id="dTerminos" runat="server" class="form-horizontal">
                                                <div class="checkbox">
                                                    <asp:CheckBox ID="chbAceptaTerminos" runat="server" Text="&nbsp;&nbsp;Acepto los <a href='policies/terminos.aspx' target='_blank'>términos & condiciones.</a>"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="span12" style="margin:25px 0px">
                                        <div class="text-center">
                                            <asp:Button ID="btnReg" runat="server" Text="Registrarse" CssClass="btn btn-primary btn-lg" OnClick="btnReg_Click" />
                                        </div>
                                    </div>
                                </div>
                                <div style="clear:both">
                                    <div class="line-top">
                                        <div class="margin-content-modulo" style="margin-bottom:0px;">
                                            <div class="text-center">
                                                <a class="btn btn-default" href="Proveedores.aspx"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;Ir al inicio</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="container" style="max-width: 1080px">
                <div class="pull-left" style="background: url(img/bg.footer.logo.png) no-repeat;padding: 35px 55px">
                    <img src="img/footer.logo.png" style="width:148px;height:24px;" />
                </div>

                <div class="col-lg-9" style="float: left; margin-top: 5px">
                    <div class="col-lg-2" style="margin-bottom:10px">
                        <div class="col-lg-12">
                            <span><b>Sitio</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;">
                            <a href="http://www.kaufmann.cl" target="_blank">Kaufmann</a>
                        </div>
                    </div>
                    <div class="col-lg-3" style="margin-bottom:10px">
                        <div class="col-lg-12">
                            <span><b>Portal Proveedores</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;">
                            <a href="policies/privacidad.aspx" target="_blank">Políticas de privacidad</a>
                            <br />
                            <a href="policies/terminos.aspx" target="_blank">Términos y condiciones</a>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="col-lg-12">
                            <span><b>Contacto Mesa de Ayuda</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;color: rgba(255, 255, 255, 0.72);">
                            <i class="glyphicon glyphicon-envelope"></i> <a href="mailto:#">consultaproveedores@kaufmann.cl</a>
                            <br />
                            <i class="glyphicon glyphicon-pushpin"></i> Av. Gladys Marín 5830, Estación Central, Santiago de Chile.
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bs-example-modal-sm" id="AlertPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="modal-title" id="alertTitle" runat="server"></h3>
                    </div>
                    <div class="modal-body">
                        <p id="alertMsj" runat="server"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>