﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="registrarse.aspx.cs" Inherits="PortalProveedores.App.UI.registrarse" Title="Políticas de Privacidad" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="Css/kf_style.css" rel="stylesheet" />
    <link type="text/css" href="Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="Css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link type="text/css" href="Css/preview.min.css" rel="stylesheet" />
    <link type="text/css" href="Css/font-awesome.min.css" rel="stylesheet" />

    <script type="text/javascript">
        function validate(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function countChar(val) {
            var len = val.value.length;
            var lblContador = document.getElementById("lblContador");
            if (len >= 501) {
                val.value = val.value.substring(0, 500);
            } else {
                lblContador.innerText = "Caracteres restantes permitidos: " + (500 - len);
            }
        };

        function bloq()
        {

        }
    </script>

    <style type="text/css">
        .html, body{
            background-color: rgba(255, 255, 255, 0);
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>
</head>
<body>    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ss" runat="server"></asp:ScriptManager>
        <div class="tabbable custom-tabs">
            <div class="tab-content">
                <div class="row-fluid">
                    <div class="span8">
                        <h3><i class="glyphicon glyphicon-arrow-down"></i>&nbsp;&nbsp;Regístrese aquí</h3>
                        <p>Para registrarse debe completar todos los campos con asterisco (*).<br /><br />Para su información en el proceso de registro, una vez ingresado el <b>RUT Razón Social</b>, el sistema validará si corresponde a un proveedor de Kaufmann y automáticamente rellenará el campo <b>Razón Social</b>.</p>
                        <div class="span6" style="margin:0px">
                            <h4>Datos de la Empresa</h4>
                            <div class="span10">
                                <asp:UpdatePanel ID="up1" runat="server">
                                    <ContentTemplate>
                                        <div id="rRut" runat="server" class="form-group">
                                            <span class="text-danger">*&nbsp;</span><label>RUT Razón Social</label>&nbsp;<span>(Ej: 12345678-K)</span>
                                            <asp:TextBox ID="txtRutProveedorReg" runat="server" placeholder="Ingrese el RUT de razón social..." CssClass="form-control" AutoPostBack="true" OnTextChanged="txtRutProveedorReg_TextChanged" MaxLength="10"></asp:TextBox>
                                            <%--<asp:TextBox ID="txtRutProveedorReg" runat="server" placeholder="Ingrese el RUT de su empresa..." CssClass="form-control"></asp:TextBox>--%>
                                        </div>

                                        <asp:UpdateProgress ID="up_pro" runat="server" AssociatedUpdatePanelID="up1">
                                            <ProgressTemplate>
                                                Validando... <img src="img/loading.gif" />
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>

                                        <div id="rRazonS" runat="server" class="form-group">
                                            <span class="text-danger"></span><label>Razón Social</label>
                                            <asp:TextBox ID="txtRazonSocial" runat="server" CssClass="form-control" ViewStateMode="Enabled" disabled></asp:TextBox>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                
                                <div id="rPAss" runat="server" class="form-group">
                                    <span class="text-danger">*&nbsp;</span><label>Contraseña</label>&nbsp;&nbsp;Entre 8 y 35 caracteres
                                    <asp:TextBox ID="txtPasswordReg" runat="server" placeholder="Ingrese contraseña..." CssClass="form-control" TextMode="Password" MaxLength="35"></asp:TextBox>
                                </div>
                                <div id="rConfPass" runat="server" class="form-group">
                                    <span class="text-danger">*&nbsp;</span><label>Repetir Contraseña</label>
                                    <asp:TextBox ID="txtConfirmarPasswordReg" runat="server" placeholder="Repita su contraseña..." CssClass="form-control" TextMode="Password" MaxLength="35"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <h4>Persona encargada de la cuenta</h4>
                            <div class="span10">
                                <asp:UpdatePanel ID="up2" runat="server">
                                    <ContentTemplate>
                                        <div id="rRun" runat="server" class="form-group">
                                            <span class="text-danger">*&nbsp;</span><label>RUN Usuario</label>&nbsp;<span>(Ej: 12345678-K)</span>
                                            <asp:TextBox ID="txtRunUser" runat="server" placeholder="Ingrese su RUN..." AutoPostBack="true" CssClass="form-control" OnTextChanged="txtRunUser_TextChanged" MaxLength="10"></asp:TextBox>
                                            <%--<asp:TextBox ID="txtRunUser" runat="server" placeholder="Ingrese su RUN..." CssClass="form-control"></asp:TextBox>--%>
                                        </div>
                                        <div id="rNombre" runat="server" class="form-group">
                                            <span class="text-danger">*&nbsp;</span><label>Nombre</label>
                                            <asp:TextBox ID="txtNombre" runat="server" placeholder="Ingrese su nombre..." CssClass="form-control" MaxLength="35"></asp:TextBox>
                                        </div>
                                        <div id="rApellidoP" runat="server" class="form-group">
                                            <span class="text-danger">*&nbsp;</span><label>Apellido Paterno</label>
                                            <asp:TextBox ID="txtApellidoPat" runat="server" placeholder="Ingrese su apellido paterno..." CssClass="form-control" MaxLength="35"></asp:TextBox>
                                        </div>
                                        <div id="rApellidoM" runat="server" class="form-group">
                                            <span class="text-danger">*&nbsp;</span><label>Apellido Materno</label>
                                            <asp:TextBox ID="txtApellidoMat" runat="server" placeholder="Ingrese su apellido materno..." CssClass="form-control" MaxLength="35"></asp:TextBox>
                                        </div>
                                        <div id="rEmail" runat="server" class="form-group">
                                            <span class="text-danger">*&nbsp;</span><label>Correo Electrónico</label>
                                            <asp:TextBox ID="txtE" runat="server" placeholder="Ingrese su correo electrónico..." CssClass="form-control" TextMode="Email" MaxLength="50"></asp:TextBox>
                                        </div>
                                        <div id="rTelefonoFijo" runat="server" class="form-group">
                                            <span class="text-danger">*&nbsp;</span><label>Télefono Fijo</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">+56</div>
                                                <asp:TextBox ID="txtFono" runat="server" placeholder="Red Fija..." MaxLength="9" CssClass="form-control" onkeypress='validate(event)'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div id="rTelefonoMovil" runat="server" class="form-group">
                                            <span class="text-danger">*&nbsp;</span><label>Teléfono Móvil</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">+56 9</div>
                                                <asp:TextBox ID="txtFonoMovil" runat="server" placeholder="Celular..." MaxLength="8" CssClass="form-control" onkeypress='validate(event)'></asp:TextBox>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        
                        <div class="span12" style="margin-left: 8px">
                            <div id="rMotivo" runat="server" class="form-group">
                                <span class="text-danger">*&nbsp;</span><label>Cuéntenos el motivo de su registro</label>
                                <asp:TextBox ID="txtMotivoReg" runat="server" placeholder="Escriba texto..." CssClass="form-control max-width-txtarea" TextMode="MultiLine" onkeyup="countChar(this)" Height="180" Width="100%"></asp:TextBox>
                                <asp:Label ID="lblContador" runat="server" CssClass="help-block"></asp:Label>
                            </div>
                            <div class="checkbox">
                                <asp:CheckBox ID="chbAceptaTerminos" runat="server" Text="&nbsp;&nbsp;Acepto los <a target='_blank' href='policies/terminos.aspx'>términos y condiciones</a>."/>
                            </div>
                            <br />
                            <asp:Button ID="btnRegistrarse" runat="server" Text="Registrarme Ahora" CssClass="btn btn-primary" OnClick="btnRegistrarse_Click"></asp:Button>
                        </div>
                    </div>
                    <div class="span4">
                        <h3><i class="glyphicon glyphicon-info-sign"></i>&nbsp;&nbsp;Informaciones</h3>
                        <div class="box">
                            <h5>¡IMPORTANTE!</h5>
                            <p>Cuando usted registre una razón social con sus datos, quedará automáticamente como administrador de dicha razón social en este portal. Dentro del sistema, usted tendrá que crear usuarios, los que estime conveniente para dar acceso a los distintos sistemas de proveedores que ofrece Kaufmann, con el fin de mantener un orden con su personal encargado de entrar a nuestros sitios.</p>
                            <br />
                            <h5>TENGA PRESENTE...</h5>
                            <p>Recuerde que una <b>contraseña segura</b> evita el acceso no autorizado de su cuenta en esta aplicación Web.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="Js/iframeResizer.contentWindow.min.js"></script>
    </form>
</body>
</html>