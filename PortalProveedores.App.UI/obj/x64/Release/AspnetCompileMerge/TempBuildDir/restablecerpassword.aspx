﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="restablecerpassword.aspx.cs" Inherits="PortalProveedores.App.UI.restablecerpassword" Title="Restablecer contraseña" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="Css/kf_style.css" rel="stylesheet" />    
    <link type="text/css" href="Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="Css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link type="text/css" href="Css/preview.min.css" rel="stylesheet" />    
    <link type="text/css" href="Css/font-awesome.min.css" rel="stylesheet" />

    <script type="text/javascript" src="Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="Js/bootstrap.min.js"></script>

    <script type="text/javascript">
        function abrePopUp(title, msj, estilo) {

            alertTitle.innerHTML = title;
            alertMsj.innerHTML = msj;
            alertMsj.className = estilo;

            $('#AlertPopUp').modal({
                backdrop: 'static',
                keyboard: true
            });
        }
    </script>

    <style type="text/css">
        html, body, form{
            background-color: lightgrey;
            height: 99.85%;
            margin: 0;
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrap">
            <div class="row-fluid">
                <div style="margin: 0 auto; max-width: 1000px;">
                    <div class="header navbar" style="min-width:767px; border-bottom: 1px solid silver;">
                        <div style="margin-top: 10px; float: left;">
                            <img src="Img/logo.kaufmann.png" height="40"/>
                        </div>
                        <div class="logo-marcas">
                            <img src="Img/mb_logo.png" class="img-marcas"/>
                            <img src="Img/freightliner_logo.png" class="img-marcas"/>
                            <img src="Img/fuso_logo.png" class="img-marcas"/>
                            <img src="Img/western_logo.png" class="img-marcas"/>
                        </div>
                    </div>
                    <h1>Portal de Proveedores</h1>

                    <div class="tabbable custom-tabs tabs-animated flat flat-all hide-label-980 shadow track-url auto-scroll">
                        <div class="tab-content">
                            <div class="margin-modulo">
                                <h2 id="titulo" runat="server" class="page-header">Restablecer Contraseña</h2>
                                <p id="msj" runat="server"></p>                            
                                <div id="form" runat="server">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label for="lblNuevaPass" class="col-sm-3 control-label">Contraseña nueva</label>
                                            <div class="col-sm-4">
                                                <asp:Textbox ID="txtNuevaPass" runat="server" CssClass="form-control" placeholder="Ingrese una contraseña nueva" TextMode="Password" MaxLength="35"></asp:Textbox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="lblConfirmaNuevaPass" class="col-sm-3 control-label">Confirmar contraseña nueva</label>
                                            <div class="col-sm-4">
                                                <asp:Textbox ID="txtConfirmaNuevaPass" runat="server" CssClass="form-control" placeholder="Repita su nueva contraseña" TextMode="Password" MaxLength="35"></asp:Textbox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="lblConfirmaNuevaPass" class="col-sm-3 control-label"></label>
                                            <div class="col-sm-4">
                                                <asp:LinkButton ID="lnkRestablecerPass" runat="server" Text="<i class='glyphicon glyphicon-refresh'></i>&nbsp;&nbsp;Restablecer mi contraseña" CssClass="btn btn-primary" OnClick="lnkRestablecerPass_Click"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="clear:both">
                                    <div class="line-top">
                                        <div class="margin-content-modulo" style="margin-bottom:0px;">
                                            <a class="btn btn-default" href="Proveedores.aspx"><i class="glyphicon glyphicon-home"></i>&nbsp;Ir al Portal Web</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade bs-example-modal-sm" id="AlertPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h3 class="modal-title" id="alertTitle" runat="server"></h3>
                                </div>
                                <div class="modal-body">
                                    <span id="alertMsj" runat="server"></span>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    <%--<button type="button" class="btn btn-primary">Save changes</button>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="container" style="max-width: 1080px">
                <div class="pull-left" style="background: url(img/bg.footer.logo.png) no-repeat;padding: 35px 55px">
                    <img src="img/footer.logo.png" style="width:148px;height:24px;" />
                </div>

                <div class="col-lg-9" style="float: left; margin-top: 5px">
                    <div class="col-lg-2" style="margin-bottom:10px">
                        <div class="col-lg-12">
                            <span><b>Sitio</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;">
                            <a href="http://www.kaufmann.cl" target="_blank">Kaufmann</a>
                        </div>
                    </div>
                    <div class="col-lg-3" style="margin-bottom:10px">
                        <div class="col-lg-12">
                            <span><b>Portal Proveedores</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;">
                            <a href="policies/privacidad.aspx" target="_blank">Políticas de privacidad</a>
                            <br />
                            <a href="policies/terminos.aspx" target="_blank">Términos y condiciones</a>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="col-lg-12">
                            <span><b>Contacto Mesa de Ayuda</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;color: rgba(255, 255, 255, 0.72);">
                            <i class="glyphicon glyphicon-envelope"></i> <a href="mailto:#">consultaproveedores@kaufmann.cl</a>
                            <br />
                            <i class="glyphicon glyphicon-pushpin"></i> Av. Gladys Marín 5830, Estación Central, Santiago de Chile.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>