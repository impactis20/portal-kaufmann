﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="principal.aspx.cs" Inherits="PortalProveedores.App.UI.private.principal" Title="Portal de Proveedores" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../Css/kf_style.css" rel="stylesheet" />  
    <link type="text/css" href="../Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../Css/bootstrap.min.original.css" rel="stylesheet" />
    <link type="text/css" href="../Css/font-awesome.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="../Js/bootstrap.min.js"></script>

    <script type="text/javascript">
        function abrePopUp(title, msj, estilo) {

            alertTitle.innerHTML = title;
            alertMsj.innerHTML = msj;
            alertMsj.className = estilo;

            $('#AlertPopUp').modal({
                backdrop: 'static',
                keyboard: true
            });
        }


        <%-- Mantiene la url estática
        window.history.pushState("object or string", "Portal Proveedores", "/private/principal.aspx"); 
        http://stackoverflow.com/questions/824349/modify-the-url-without-reloading-the-page
        --%>
        
    </script>


    <script type="text/javascript">

        $(document).ready(function () {

            $('li').click(function () {
                var url = $(this).attr('rel');
                $('#frame').attr('src', url);
            });

            //$('#frame').load(function () {
            //    //var absolutePath = $('#frame').contents().get(0).location.pathname + window.location.search;
            //    var propertyIframe = $('#frame').contents().get(0);
            //    var absolutePath = propertyIframe.location.pathname + propertyIframe.location.search;
            //    history.pushState('', "", "?val=" + absolutePath) //.split("?")[0]
            //});

        });

    </script>


    <style type="text/css">
        html, body, form{
            height: 99.83%;
            margin: 0;
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrap">
            <div class="navbar navbar-default" role="navigation">
                <div class="container">
                    <div style="min-width: 567px;border-bottom: 1px solid silver;margin-bottom: 8px;margin-top: 0px;height: 50px;">
                        <div class="logo-kaufmann">
                            <img src="../Img/logo.kaufmann.png" height="30"/>
                        </div>
                        <div class="logo-marcas">
                            <img src="../Img/mb_logo.png" class="img-banner-menu"/>
                            <img src="../Img/freightliner_logo.png" class="img-banner-menu"/>
                            <img src="../Img/fuso_logo.png" height="20" class="img-banner-menu"/>
                            <img src="../Img/western_logo.png" height="20" class="img-banner-menu"/>
                        </div>
                    </div>

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#" style="pointer-events:none">Portal de Proveedores</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li rel="Index.aspx"><a href="#">Sistemas</a></li>                        
                            <li id="razonSocial" runat="server" rel="user/RazonSocial.aspx"><a href="#">Mi razón social</a></li>
                            <li id="inicio" runat="server" rel="admin/PanelAdministrador.aspx"><a href="#">Panel adm.</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a id="aUser" runat="server" href="#" class="dropdown-toggle" data-toggle="dropdown" style="white-space:nowrap;width:100%;overflow:hidden;text-overflow:ellipsis;"></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li class="dropdown-header"><i class="glyphicon glyphicon-envelope"></i>&nbsp;&nbsp;<span id="correo" runat="server"></span></li>
                                    <li id="d1" runat="server" class="divider"></li>                                
                                    <li id="miPerfil" runat="server" rel="user/miperfil.aspx"><a href="#"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;Mi perfil</a></li>
                                    <li id="cambiarPass" runat="server" rel="cambiarpassword.aspx"><a href="#"><i class="glyphicon glyphicon-asterisk"></i>&nbsp;&nbsp;Cambiar contraseña</a></li>
                                    <li id="d2" runat="server" class="divider"></li>
                                    <li><asp:LinkButton ID="btnCerrarSesion" runat="server" Text="<i class='glyphicon glyphicon-off'></i>&nbsp;&nbsp;Cerrar Sesión" OnClick="btnCerrarSesion_Click"></asp:LinkButton></li>
                                </ul>
                            </li>                        
                        </ul>
                    </div>
                </div>
            </div>
            <div id="content" class="container">
                <iframe id="frame" runat="server"></iframe>
                <script type="text/javascript" src="../Js/iframeResizer.contentWindow.min.js"></script> 
		        <script type="text/javascript" src="../Js/iframeResizer.min.js"></script> 
		        <script type="text/javascript">
		            iFrameResize({
		                log: false, // Enable console logging
		                resizedCallback: function (messageData) { // Callback fn when message is received
		                    //setTimeout(function () { parentIFrame.sendMessage('nested') }, 50);
		                }
		            });
		        </script>
            </div>
        </div>


        <div class="footer">
            <div class="container" style="color:white">
                <div class="pull-left" style="background: url(../img/bg.footer.logo.png) no-repeat;padding: 35px 55px">
                    <img src="../img/footer.logo.png" style="width:148px;height:24px;" />
                </div>

                <div class="col-lg-9" style="float: left; margin-top: 5px;">
                    <div class="col-lg-2" style="margin-bottom:10px">
                        <div class="col-lg-12">
                            <span><b>Sitio</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;">
                            <a href="http://www.kaufmann.cl" target="_blank">Kaufmann</a>
                        </div>
                    </div>
                    <div class="col-lg-4" style="margin-bottom:10px">
                        <div class="col-lg-12">
                            <span><b>Portal Proveedores</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;">
                            <a href="../policies/privacidad.aspx" target="_blank">Políticas de privacidad</a>
                            <br />
                            <a href="../policies/terminos.aspx" target="_blank">Términos y condiciones</a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="col-lg-12">
                            <span><b>Contacto Mesa de Ayuda</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px; color: rgba(255, 255, 255, 0.72);">
                            <i class="glyphicon glyphicon-envelope"></i> <a href="mailto:#">consultaproveedores@kaufmann.cl</a>
                            <br />
                            <i class="glyphicon glyphicon-pushpin"></i> Av. Gladys Marín 5830, Estación Central, Santiago de Chile.
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bs-example-modal-sm" id="AlertPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="modal-title" id="alertTitle" runat="server"></h3>
                    </div>
                    <div class="modal-body">
                        <p id="alertMsj" runat="server"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <%--<button type="button" class="btn btn-primary">Save changes</button>--%>
                    </div>
                </div>
            </div>
        </div>
    </form>


    <%--$('#frame').load(function () {
        //var absolutePath = $('#frame').contents().get(0).location.pathname + window.location.search;
        var propertyIframe = $('#frame').contents().get(0);
        var absolutePath = propertyIframe.location.pathname + propertyIframe.location.search;
        history.pushState('', "", "?val=" + absolutePath) //.split("?")[0]
    });--%>

    <%--<script>
        var contentIframe = document.getElementById('frame');

        // Switcheroo!
        // función que cambia el contenido
        function updateContent(data) {
            if (data == null)
                return;

            contentIframe.src = '?val=' + propertyIframe.location.pathname + propertyIframe.location.search;
        }

        // Load some mock JSON data into the page
        // Carga algunos datos simulados (JSON) en la página
        function clickHandler(event) {
            //var cat = event.target.getAttribute('href').split('/').pop(), data = cats[cat] || null; // In reality this could be an AJAX request .............. En realidad, esto podría ser una petición AJAX

            updateContent(data);

            // Add an item to the history log
            // Agrega un elemento al registro de la historia
            history.pushState(data, event.target.textContent, '?val=' + propertyIframe.location.pathname + propertyIframe.location.search);

            return event.preventDefault();
        }

        // Attach event listeners
        // Coloca detectores de eventos
        for (var i = 0, l = contentIframe.length; i < l; i++) {
            contentIframe.addEventListener('onload', clickHandler, true);
        }

        // Revert to a previously saved state
        // Retorna a un estado guardado previamente
        window.addEventListener('popstate', function (event) {
            console.log('popstate fired!');

            updateContent(event.state);
        });

        // Store the initial content so we can revisit it later
        // Almacenar el contenido inicial para que podamos volver más tarde
        history.replaceState({
            content: contentEl.textContent,
            photo: photoEl.src
        }, document.title, document.location.href);
    </script>--%>

</body>
</html>