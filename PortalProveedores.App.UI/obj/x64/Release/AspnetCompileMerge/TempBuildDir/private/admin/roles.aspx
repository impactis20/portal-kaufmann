﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="roles.aspx.cs" Inherits="PortalProveedores.App.UI.private.admin.roles" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../../Css/kf_style.css" rel="stylesheet" />  
    <link type="text/css" href="../../Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/bootstrap.min.original.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/font-awesome.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="../../Js/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="../../Js/bs.pagination.js"></script>--%>

    <script type='text/javascript'>//<![CDATA[ 
        $(window).load(function () {
            $(function () {
                var new_checkbox_button = function (name, id) {
                    return "<li><span class='button-checkbox'><button type='button' class='btn btn-sm btn-primary active' data-color='primary'><i class='state-icon glyphicon glyphicon-check'></i>&nbsp;" + name + "</button><input type='checkbox' class='hidden' checked=''></span></li>";
                };

                generateCheckbox();

                $("#add").click(function () {
                    $("#selected_targets").append(new_checkbox_button("lallalala", "1111"));
                    generateCheckbox($("#selected_targets").find('.button-checkbox').last());
                });
            });

            function generateCheckbox($ele) {
                if (!$ele) { $ele = $('.button-checkbox'); }
                $ele.each(function () {

                    // Settings
                    var $widget = $(this),
                        $button = $widget.find('button'),
                        $checkbox = $widget.find('input:checkbox'),
                        color = $button.data('color'),
                        settings = {
                            on: {
                                icon: 'glyphicon glyphicon-check'
                            },
                            off: {
                                icon: 'glyphicon glyphicon-unchecked'
                            }
                        };

                    // Event Handlers
                    $button.on('click', function () {
                        $checkbox.prop('checked', !$checkbox.is(':checked'));
                        $checkbox.triggerHandler('change');
                        updateDisplay();
                    });
                    $checkbox.on('change', function () {
                        updateDisplay();
                    });

                    // Actions
                    function updateDisplay() {
                        var isChecked = $checkbox.is(':checked');

                        // Set the button's state
                        $button.data('state', (isChecked) ? "on" : "off");

                        // Set the button's icon
                        $button.find('.state-icon')
                            .removeClass()
                            .addClass('state-icon ' + settings[$button.data('state')].icon);

                        // Update the button's color
                        if (isChecked) {
                            $button
                                .removeClass('btn-default')
                                .addClass('btn-' + color + ' active');
                        }
                        else {
                            $button
                                .removeClass('btn-' + color + ' active')
                                .addClass('btn-default');
                        }
                    }

                    // Initialization
                    function init() {

                        updateDisplay();

                        // Inject the icon if applicable
                        if ($button.find('.state-icon').length === 0) {
                            $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
                        }
                    }
                    init();
                });
            }


        });//]]>  

</script>


    <style>
        body{
            overflow:hidden;
            margin: 0px 15px
        }

        alert, panel, .alert, .panel{
            margin:0px
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="s1" runat="server"></asp:ScriptManager>
        <asp:SiteMapPath ID="SiteMap1"
                runat="server"  
                PathSeparator=" > " 
                ParentLevelsDisplayed="10" 
                PathDirection="RootToCurrent" 
                RenderCurrentNodeAsLink="false" 
                ShowToolTips="true"
                CssClass="breadcrumb">
        </asp:SiteMapPath>

        <div id="admin" runat="server" class="row">
            <div class="navbar navbar-default mx-wd">
                <div class="mrgn-page">
                    <h2 class="page-header" style="margin-top:25px">Roles del Sistema</h2>
                    <div class="col-xs-12 margin-btn-footer">
                        <div class="col-md-12">
                            <p>Agregue los roles de usuario del sistema <b id="nombre_sistema" runat="server"></b>. Una vez que usted cree el rol, deberá asignar los módulos (páginas) a los que tendrá permisos de acceso.</p>

                            <span class="button-checkbox">
                                <button type="button" class="btn" data-color="warning">Warning</button>
                                <input type="checkbox" class="hidden" checked />
                            </span>
                            <span class="button-checkbox">
                                <button type="button" class="btn" data-color="primary">Danger</button>
                                <asp:CheckBox ID="ch" runat="server" CssClass="hidden" />
                            </span>

                            <asp:Button ID="btnTestear" runat="server" CssClass="btn btn-default" Text="Testear" OnClick="btnTestear_Click"/>                            


                                <span class="button-checkbox">
                                  <button type="button" class="btn" data-color="primary">Danger</button>                                  
                                  <asp:CheckBox ID="CheckBox1" runat="server" CssClass="hidden" />
                                </span>
                                <span class="button-checkbox">
                                  <button type="button" class="btn" data-color="primary">Danger</button>                                  
                                  <asp:CheckBox ID="CheckBox2" runat="server" CssClass="hidden" />
                                </span>
                                <span class="button-checkbox">
                                  <button type="button" class="btn" data-color="primary">Danger</button>                                  
                                  <asp:CheckBox ID="CheckBox3" runat="server" CssClass="hidden" />
                                </span>
                                <span class="button-checkbox">
                                  <button type="button" class="btn" data-color="primary">Danger</button>                                  
                                  <asp:CheckBox ID="CheckBox4" runat="server" CssClass="hidden" />
                                </span>
                                <span class="button-checkbox">
                                  <button type="button" class="btn" data-color="primary">Danger</button>                                  
                                  <asp:CheckBox ID="CheckBox5" runat="server" CssClass="hidden" />
                                </span>
                        </div>

                        <div class="col-md-12 col-md-offset-4">
                            <div class="col-md-6">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label for="txtBU" class="col-sm-1 control-label">Rol</label>
                                        <div class="col-lg-7">
                                            <div class="input-group">
                                                <asp:TextBox ID="txtEmail" runat="server" placeholder="Ingrese un rol..." CssClass="form-control"></asp:TextBox>                                                    
                                                <span class="input-group-btn">
                                                    <asp:Button ID="btnAddRol" runat="server" Text="Aceptar" CssClass="btn btn-default" OnClick="btnAddRol_Click" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 margin-btn-footer">
                            <div class="col-xs-12">
                                <p id="cont_roles" runat="server">asd</p>
                                <label class="label label-info">Tenga en cuenta que</label><br /><i class="text-info">Usted podrá ver sólamente los sistemas a los cuales está catalogado como <b>Administrador</b>.</i>
                            </div>
                        </div>


                        <div class="col-md-5">
                            <asp:GridView ID="gvRolesSistema" runat="server" AutoGenerateColumns="false" ShowHeader="false" CssClass="table table-bordered"
                                OnRowDataBound="gvRolesSistema_RowDataBound"
                                OnRowCommand="gvRolesSistema_RowCommand">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-7 text-overflow">
                                                            <i class="glyphicon glyphicon-comment">&nbsp;</i><asp:Label ID="lblD" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                                                        </div>
                                                        
                                                        <div class="col-md-5">
                                                            <asp:LinkButton ID="btnAddPag" runat="server" Text="<i class='glyphicon glyphicon-plus'>&nbsp;</i>Agregar módulos (páginas)" CommandArgument='<%# Eval("IdSistema") %>' CommandName="AddPag"></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>