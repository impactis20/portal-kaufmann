﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="modulos.aspx.cs" Inherits="PortalProveedores.App.UI.private.admin.modulos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>

    <link type="text/css" href="../../Css/kf_style.css" rel="stylesheet" />  
    <link type="text/css" href="../../Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/bootstrap.min.original.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/font-awesome.min.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/bootstrap-switch.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="../../Js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Js/bootstrap-switch.js"></script>
    
    <script type="text/javascript">
        function countCharMod(val) {
            var len = val.value.length;
            if (len >= 201) {
                val.value = val.value.substring(0, 200);
            } else {
                $('#lblCont1').text("Caracteres restantes permitidos: " + (200 - len));
            }
        };

        function countCharModEditing(val) {
            var len = val.value.length;
            if (len >= 201) {
                val.value = val.value.substring(0, 200);
            } else {
                $('.help-block').text("Caracteres restantes permitidos: " + (200 - len));
            }
        };
    </script>

    <style>
        body{
            overflow:hidden;
            margin: 0px 15px
        }

        alert, panel, .alert, .panel{
            margin:0px
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="s1" runat="server"></asp:ScriptManager>
        <asp:SiteMapPath ID="SiteMap1"
                runat="server"  
                PathSeparator=" > " 
                ParentLevelsDisplayed="10" 
                PathDirection="RootToCurrent" 
                RenderCurrentNodeAsLink="false" 
                ShowToolTips="true"
                CssClass="breadcrumb">
        </asp:SiteMapPath>
        <div id="admin" runat="server" class="row">
            <div class="navbar navbar-default mx-wd">
                <div class="mrgn-page">
                    <h2 class="page-header" style="margin-top:25px">Administrador del Sistema</h2>
                    <h3><i class="glyphicon glyphicon-chevron-right" style="width:32px;height:42px;">&nbsp;</i>Páginas del Sistema</h3>
                    <div class="col-md-12 margin-btn-footer">
                        <p>Administre los módulos del sistema <b id="nombreSis" runat="server"></b>.</p>                        

                        <div class="panel-body" style="margin-bottom:15px">
                            <div class="col-sm-12" style="margin-bottom:15px">
                                <p>Agregue todas las páginas que contenga este sistema.</p>
                            </div>
                            <div class="col-sm-5">
                                <div id="dNombreMod" runat="server">
                                    <span class="text-danger">*&nbsp;</span><label>Nombre</label>
                                    <asp:Textbox ID="txtNombrePag" runat="server" CssClass="form-control input-sm"></asp:Textbox>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div id="dPaginaMod" runat="server">
                                    <span class="text-danger">*&nbsp;</span><label>Nombre real página</label>
                                    <asp:Textbox ID="txtPag" runat="server" CssClass="form-control input-sm"></asp:Textbox>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <label>Estado Inicial</label>
                                <div id="animated-switch" class="make-switch" data-animated="true">
                                    <asp:CheckBox ID="chEstadoSis" runat="server" Checked="false"/>                                        
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label>Descripción página</label>
                                <asp:Textbox ID="txtDescPag" runat="server" CssClass="form-control" TextMode="MultiLine" onkeyup="countCharMod(this)"></asp:Textbox>
                                <asp:Label ID="lblCont1" runat="server" CssClass="help-block"></asp:Label>
                            </div>
                            <div class="col-sm-12">
                                <div style="margin-bottom:0">
                                    <asp:LinkButton ID="btnAddMod" runat="server" Text="<i class='glyphicon glyphicon-plus'></i>&nbsp;Agregar Módulo" CssClass="btn btn-success rgth" OnClick="btnAddMod_Click"></asp:LinkButton>
                                </div>
                            </div>                                                        
                        </div>

                        <asp:GridView ID="gvMods" runat="server" AutoGenerateColumns="false" ShowHeader="false" CssClass="table table-bordered" 
                                OnRowCommand="gvMods_RowCommand"
                                OnRowDataBound="gvMods_RowDataBound"
                                OnRowEditing="gvMods_RowEditing"
                                OnRowCancelingEdit="gvMods_RowCancelingEdit"
                                OnRowUpdating="gvMods_RowUpdating">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <div class="col-sm-3">
                                            <i class='glyphicon glyphicon-file'>&nbsp;</i><span>Nombre</span>
                                        </div>
                                        <div class="col-sm-2">
                                            <i class='glyphicon glyphicon-file'>&nbsp;</i><span>Página</span>
                                        </div>
                                        <div class="col-sm-5">
                                            <i class='glyphicon glyphicon-comment'>&nbsp;</i><span>Descripción</span>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="col-sm-10">
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <label><i class='glyphicon glyphicon-tag'>&nbsp;</i>Nombre Módulo</label>
                                                            <asp:TextBox ID="lblE" runat="server" CssClass="form-control input-sm" disabled="disabled" Text='<%# Eval("Nombre") %>'></asp:TextBox>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label><i class='glyphicon glyphicon-file'>&nbsp;</i>Página</label>
                                                            <asp:TextBox ID="lblP" runat="server" CssClass="form-control input-sm" disabled="disabled" Text='<%# Eval("Pagina") %>'></asp:TextBox>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label><i class='glyphicon glyphicon-off'>&nbsp;</i>Estado Módulo</label>
                                                            <br />
                                                            <div id="estado" class="make-switch" data-animated="true">
                                                                <input id="chEstadoSis" runat="server" type="checkbox" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <label><i class='glyphicon glyphicon-comment'>&nbsp;</i>Descripción</label>
                                                            <asp:TextBox ID="lblD" runat="server" CssClass="form-control input-sm" disabled="disabled" TextMode="MultiLine" Text='<%# Eval("Descripcion") %>'></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1" style="margin-top: 25px;">
                                                    <asp:LinkButton ID="btnE" runat="server" Text="<i class='glyphicon glyphicon-remove'>&nbsp;</i>Editar" CssClass="btn btn-info btn-sm" CommandName="Edit" CommandArgument='<%# Eval("IdModulo") %>'></asp:LinkButton>
                                                </div>
                                                <div class="col-sm-1" style="margin-top: 25px;">
                                                    <asp:LinkButton ID="btnQ" runat="server" Text="<i class='glyphicon glyphicon-remove'>&nbsp;</i>Quitar" CssClass="btn btn-danger btn-sm"></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <div id="row_editing" runat="server" class="panel panel-primary">
                                            <div class="panel-body">
                                                <div class="col-sm-10">
                                                    <div class="row">
                                                        <div id="dNombre" runat="server" class="col-sm-4">
                                                            <asp:HiddenField ID="h_mod" runat="server" Value='<%# Eval("IdModulo") %>' />
                                                            <label><i class='glyphicon glyphicon-tag'>&nbsp;</i>Nombre Módulo</label>
                                                            <asp:TextBox ID="txtN" runat="server" CssClass="form-control input-sm" Text='<%# Eval("Nombre") %>'></asp:TextBox>
                                                        </div>
                                                        <div id="dPagina" runat="server" class="col-sm-4">
                                                            <label><i class='glyphicon glyphicon-file'>&nbsp;</i>Página</label>
                                                            <asp:TextBox ID="txtP" runat="server" CssClass="form-control input-sm" Text='<%# Eval("Pagina") %>'></asp:TextBox>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label><i class='glyphicon glyphicon-file'>&nbsp;</i>Estado Módulo</label>
                                                            <br />
                                                            <div id="animated-switch" class="make-switch" data-animated="true">
                                                                <asp:CheckBox ID="chEstadoSis" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <label><i class='glyphicon glyphicon-comment'>&nbsp;</i>Descripción</label>
                                                            <asp:TextBox ID="txtD" runat="server" CssClass="form-control input-sm" TextMode="MultiLine" onkeyup="countCharModEditing(this)" Text='<%# Eval("Descripcion") %>'></asp:TextBox>
                                                            <asp:Label ID="lblCont2" runat="server" CssClass="help-block"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1" style="margin-top: 25px;">
                                                    <asp:LinkButton ID="btnU" runat="server" Text="<i class='glyphicon glyphicon-ok'></i> Guardar" CssClass="btn btn-primary btn-sm" CommandName="Update"></asp:LinkButton>
                                                </div>
                                                <div class="col-sm-1" style="margin-top: 25px;">
                                                    <asp:LinkButton ID="btnC" runat="server" Text="<i class='glyphicon glyphicon-remove'></i> Cancelar" CssClass="btn btn-danger btn-sm" CommandName="Cancel"></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>                    
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../Js/iframeResizer.contentWindow.min.js"></script>
    </form>
</body>
</html>