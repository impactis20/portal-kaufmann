﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="sistemas.aspx.cs" Inherits="PortalProveedores.App.UI.private.admin.sistemas" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../../Css/kf_style.css" rel="stylesheet" />  
    <link type="text/css" href="../../Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/bootstrap.min.original.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/font-awesome.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="../../Js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Js/bs.pagination.js"></script>

    <style>
        body{
            overflow:hidden;
            margin: 0px 15px
        }

        alert, panel, .alert, .panel{
            margin:0px
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="s1" runat="server"></asp:ScriptManager>
        <asp:SiteMapPath ID="SiteMap1"
                runat="server"  
                PathSeparator=" > " 
                ParentLevelsDisplayed="10" 
                PathDirection="RootToCurrent" 
                RenderCurrentNodeAsLink="false" 
                ShowToolTips="true"
                CssClass="breadcrumb">
        </asp:SiteMapPath>

        <div id="admin" runat="server" class="row">
            <div class="navbar navbar-default mx-wd">
                <div class="mrgn-page">
                    <h2 class="page-header" style="margin-top:25px">Administrador de Sistemas de Proveedores</h2>
                    <div class="col-xs-12 margin-btn-footer">
                        <div class="col-xs-12">
                            <p>En este módulo usted podrá ver el status de un sistema, dar de alta o de baja si es necesario. También, puede <a href="AgregarSistema.aspx">agregar un nuevo sistema</a> de proveedores para que sea visualizado en el módulo principal de este portal (Sistemas de Proveedores). Si desea administrar un sistema en específico, sólo debe acceder haciendo click en el sistema.</p>                            
                        </div>
                    </div>
                    <asp:GridView ID="gvSistemas" runat="server" AutoGenerateColumns="false" ShowHeader="false" CssClass="table table-bordered"
                        OnRowDataBound="gvSistemas_RowDataBound"
                        OnRowCommand="gvSistemas_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-3 text-overflow">
                                                    <i class="glyphicon glyphicon-tag">&nbsp;</i><asp:LinkButton ID="btnGo" runat="server" Text='<%# Eval("nombre") %>' CommandName="Go" CommandArgument='<%# Eval("url") %>'></asp:LinkButton><%--<asp:Label ID="lblN" runat="server" Text='<%# Eval("nombre") %>'></asp:Label>--%>
                                                </div>
                                                <div class="col-md-5 text-overflow">
                                                    <i class="glyphicon glyphicon-comment">&nbsp;</i><asp:Label ID="lblD" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>
                                                </div>
                                                <div class="col-md-2 text-overflow">
                                                    <i id="eIcon" runat="server">&nbsp;</i><asp:Label ID="lblE" runat="server" Text='<%# Eval("estado") %>'></asp:Label>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="btn-group">
                                                        <a class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                                            Opciones <span class="caret"></span>
                                                        </a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><asp:LinkButton ID="btnSolis" runat="server"  Text="<i class='glyphicon glyphicon-transfer'>&nbsp;</i>Solicitudes de acceso" PostBackUrl='<%# string.Format("aceptasolicitud.aspx?is={0}", Eval("IdSistema")) %>'></asp:LinkButton></li>
                                                            <%--<li class="disabled"><a><i class='glyphicon glyphicon-eye-open'>&nbsp;</i>Ver visitas</a></li>--%>
                                                            <li class="divider"></li>
                                                            <li class="dropdown-header">Administración</li>
                                                            <li><asp:LinkButton ID="btnCG" runat="server" Text="<i class='glyphicon glyphicon-cog'>&nbsp;</i>Configuraciones" PostBackUrl='<%# string.Format("administrarsistema.aspx?is={0}", Eval("IdSistema")) %>'></asp:LinkButton></li>
                                                            <li><asp:LinkButton ID="btnMS" runat="server" Text="<i class='glyphicon glyphicon-file'>&nbsp;</i>Módulos del sistema" PostBackUrl='<%# string.Format("modulos.aspx?is={0}", Eval("IdSistema")) %>'></asp:LinkButton></li>
                                                            <li><asp:LinkButton ID="btnR" runat="server" Text="<i class='glyphicon glyphicon-user'>&nbsp;</i>Roles" PostBackUrl='<%# string.Format("Roles.aspx?is={0}", Eval("IdSistema")) %>'></asp:LinkButton></li>                                                            
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div class="col-xs-12 margin-btn-footer">
                        <div class="col-xs-12">
                            <p>Resultados encontrados: <b id="contSis" runat="server"></b>.</p>
                            <br />
                            <label class="label label-info">Tenga en cuenta que</label><br /><i class="text-info">Usted podrá ver sólamente los sistemas a los cuales está catalogado como <b>Administrador</b>.</i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../Js/iframeResizer.contentWindow.min.js"></script>
    </form>
</body>
</html>