﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="razonessociales.aspx.cs" Inherits="PortalProveedores.App.UI.private.admin.razonessociales" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../../Css/kf_style.css" rel="stylesheet" />  
    <link type="text/css" href="../../Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/bootstrap.min.original.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/font-awesome.min.css" rel="stylesheet" />    

    <script type="text/javascript" src="../../Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="../../Js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Js/bs.pagination.js"></script>

    <style>
        body{
            overflow:hidden;
            margin: 0px 15px
        }

        alert, panel, .alert, .panel{
            margin:0px
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="s1" runat="server"></asp:ScriptManager>
        <asp:SiteMapPath ID="SiteMap1"
                runat="server"
                PathSeparator=" > " 
                ParentLevelsDisplayed="10" 
                PathDirection="RootToCurrent" 
                RenderCurrentNodeAsLink="false" 
                ShowToolTips="true"
                CssClass="breadcrumb">
        </asp:SiteMapPath>

        <div id="admin" runat="server" class="row">
            <div class="navbar navbar-default mx-wd">
                <div class="mrgn-page">
                    <h2 class="page-header" style="margin-top:25px">Razones Sociales</h2>
                    <div class="col-md-12" style="margin-bottom:15px">
                        <p>Véa las razones sociales registradas en este portal Web de proveedores. Si desea ver el detalle de una razón social (administrador de cuenta, usuarios de cuenta, datos, etc...) haga click en el botón <b>"Entrar"</b> según corresponda.</p>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="txtBU" class="col-sm-3 control-label">Razón Social</label>
                                    <div class="col-sm-6">
                                        <div id="dBU" runat="server" class="input-group">
                                            <asp:TextBox ID="txtBU" runat="server" placeholder="Ingrese razón social..." CssClass="form-control" ></asp:TextBox>                                                    
                                            <span class="input-group-btn">
                                                <asp:Button ID="btnBU" runat="server" Text="Buscar" CssClass="btn btn-default" OnClick="btnBU_Click" />
                                            </span>
                                        </div>                                        
                                    </div>  
                                </div>
                            </div>
                        </div>

                        <div id="filtro" runat="server" class="col-md-6" visible="false">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="ddlPg" class="col-sm-3 control-label">Items por página</label>                            
                                    <div class="col-sm-2">
                                        <asp:DropDownList ID="ddlPg" runat="server" CssClass="form-control" Width="65px" AutoPostBack="true" OnSelectedIndexChanged="ddlPg_SelectedIndexChanged">
                                            <asp:ListItem Value="5" Text="5" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                            <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                            <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                            <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                            <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                            <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                            <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                            <asp:ListItem Value="13" Text="13"></asp:ListItem>
                                            <asp:ListItem Value="14" Text="14"></asp:ListItem>
                                            <asp:ListItem Value="15" Text="15"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <span id="lblMsj" runat="server" class="help-block" style="margin-bottom:0px" ></span>
                    </div>

                    <div id="dProvs" runat="server" class="row" visible="false" style="margin-bottom:15px">

                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <asp:GridView ID="gvProvs" runat="server" AutoGenerateColumns="false" ShowHeader="false" CssClass="table table-bordered" AllowPaging="True" PageSize="5"
                                        PagerStyle-CssClass="bs-pagination text-center" PagerSettings-Mode="Numeric" PagerSettings-Position="TopAndBottom"
                                        OnPageIndexChanging="gvProvs_PageIndexChanging"
                                        OnRowDataBound="gvProvs_RowDataBound"
                                        OnRowCommand="gvProvs_RowCommand">
                                        <Columns>                                            
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <div class="panel panel-default">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-3 text-overflow">
                                                                    <i class="glyphicon glyphicon-barcode">&nbsp;</i><asp:Label ID="lblR" runat="server" Text='<%# Eval("rut") %>' ToolTip='<%# Eval("rut") %>'></asp:Label>
                                                                </div>
                                                                <div class="col-md-3 text-overflow">
                                                                    <i class="glyphicon glyphicon-tag">&nbsp;</i><asp:Label ID="lblN" runat="server" Text='<%# Eval("RazonSocial") %>'></asp:Label>
                                                                </div>
                                                                <div class="col-md-5 text-overflow">
                                                                    <span id="lblAcceso" runat="server" style="font-size:13px"></span>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <asp:LinkButton ID="btnVer" runat="server" Text="<i class='glyphicon glyphicon-eye-open'>&nbsp;</i>Entrar" CssClass="btn btn-success btn-xs" CommandName="GoToRS" CommandArgument='<%# Eval("IdProv") %>'></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>                                           
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../Js/iframeResizer.contentWindow.min.js"></script>
    </form>
</body>
</html>