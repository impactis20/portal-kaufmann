﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="agregarsistema.aspx.cs" Inherits="PortalProveedores.App.UI.private.admin.agregarsistema" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../../Css/kf_style.css" rel="stylesheet" />  
    <link type="text/css" href="../../Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/bootstrap.min.original.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/font-awesome.min.css" rel="stylesheet" />
    <%--<link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" />--%>
    <link href="../../Css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="../../Js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Js/bs.pagination.js"></script>
    <script src="../../Js/fileinput.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        function countCharSis(val) {
            var len = val.value.length;
            if (len >= 251) {
                val.value = val.value.substring(0, 250);
            } else {
                $('#lblCont2').text("Caracteres restantes permitidos: " + (250 - len));
            }
        };
    </script>

    <style>
        body{
            overflow:hidden;
            margin: 0px 15px
        }

        alert, panel, .alert, .panel{
            margin:0px
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="s1" runat="server"></asp:ScriptManager>
        <asp:SiteMapPath ID="SiteMap1"
                runat="server"  
                PathSeparator=" > " 
                ParentLevelsDisplayed="10" 
                PathDirection="RootToCurrent" 
                RenderCurrentNodeAsLink="false" 
                ShowToolTips="true"
                CssClass="breadcrumb">
        </asp:SiteMapPath>

        <div id="admin" runat="server" class="row">
            <div class="navbar navbar-default mx-wd">
                <div class="mrgn-page">
                    <h2 class="page-header" style="margin-top:25px">Agregar un sistema para proveedores</h2>
                    <div class="col-md-12 margin-btn-footer">
                        <div class="col-xs-12" style="margin-bottom:15px">
                            <p>Agregue un sistema completando el siguiente formulario. Recuerde que los campos con asterisco son obligatorios.</p>
                        </div>                        
                        <div class="form-horizontal">
                            <div id="dNombre" runat="server" class="form-group">
                                <label for="txtNombreSis" class="col-sm-2 control-label"><span class="text-danger text-bold">*&nbsp;</span>Nombre sistema</label>
                                <div class="col-sm-6">
                                    <asp:Textbox ID="txtNombreSis" runat="server" CssClass="form-control"></asp:Textbox>
                                </div>
                            </div>
                            <div id="dUrl" runat="server" class="form-group">
                                <label for="txtUrlSis" class="col-sm-2 control-label"><span class="text-danger text-bold">*&nbsp;</span>Url del sistema</label>
                                <div class="col-sm-6">
                                    <asp:Textbox ID="txtUrlSis" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtUrl_TextChanged"></asp:Textbox>
                                    <asp:Label id="lblUrlMsj" runat="server" CssClass="help-block"></asp:Label>
                                </div>
                            </div>
                            <asp:UpdatePanel ID="up1" runat="server">
                            <ContentTemplate>
                            <div id="dUD" runat="server" class="form-group">
                                <label for="txtUDSis" class="col-sm-2 control-label"><span class="text-danger text-bold">*&nbsp;</span>Usuario desarrollador</label>
                                <div class="col-sm-6">
                                    <asp:Textbox ID="txtUDSis" runat="server" CssClass="form-control" OnTextChanged="txtUDSis_TextChanged" AutoPostBack="true"></asp:Textbox>
                                    <span id="lblValUD" runat="server" class="help-block"></span>
                                </div>
                            </div>                            
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="up3" runat="server">
                            <ContentTemplate>
                            <div id="dUS" runat="server" class="form-group">
                                <label for="txtUSSis" class="col-sm-2 control-label"><span class="text-danger text-bold">*&nbsp;</span>Usuario solicitante</label>                                
                                <div class="col-sm-6">
                                    <asp:Textbox ID="txtUSSis" runat="server" CssClass="form-control" OnTextChanged="txtUSSis_TextChanged" AutoPostBack="true"></asp:Textbox>
                                    <span id="lblValUS" runat="server" class="help-block"></span>
                                </div>                                
                            </div>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <div id="dDesc" runat="server" class="form-group">
                                <label for="txtDescSis" class="col-sm-2 control-label"><span class="text-danger text-bold">*&nbsp;</span>Descripción</label>
                                <div class="col-sm-6">
                                    <asp:Textbox ID="txtDescSis" runat="server" CssClass="form-control" onkeyup="countCharSis(this)" Height="160px" TextMode="MultiLine"></asp:Textbox>
                                    <asp:Label ID="lblCont2" runat="server" CssClass="help-block"></asp:Label>
                                </div>
                            </div>
                            <div id="dImg" runat="server" class="form-group">
                                <label for="fArchivo" class="col-sm-2 control-label"><span class="text-danger text-bold">*&nbsp;</span>Imagen del sistema</label>
                                <div class="col-sm-6">
                                    <asp:FileUpload ID="fArchivo" runat="server" class="file" multiple="true" data-show-upload="false" data-show-caption="true" />
                                    <span class="help-block"><b>Si no carga una imagen al sistema, se le asignará una imagen por defecto.</b></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8">
                                    <asp:LinkButton ID="btnAgregar" runat="server" Text="Agregar" CssClass="btn btn-primary rgth" OnClick="btnAgregar_Click"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../../Js/iframeResizer.contentWindow.min.js"></script>
    </form>
</body>
</html>