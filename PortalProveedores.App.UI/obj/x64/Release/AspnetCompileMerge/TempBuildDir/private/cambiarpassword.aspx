﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cambiarpassword.aspx.cs" Inherits="PortalProveedores.App.UI.private.cambiarpassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../Css/kf_style.css" rel="stylesheet" />  
    <link type="text/css" href="../Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../Css/bootstrap.min.original.css" rel="stylesheet" />
    <link type="text/css" href="../Css/font-awesome.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="../Js/bootstrap.min.js"></script>

    <style>
        body{
            margin: 0px 15px;
            overflow:hidden;
        }

        alert, panel, .alert, .panel{
            margin:0px
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="s1" runat="server"></asp:ScriptManager>
        <asp:SiteMapPath ID="SiteMap1" runat="server" PathSeparator=" > " ParentLevelsDisplayed="10" PathDirection="RootToCurrent" RenderCurrentNodeAsLink="false" ShowToolTips="true" CssClass="breadcrumb">
        </asp:SiteMapPath>

        <div class="row">
            <div class="navbar navbar-default mx-wd">
                <div class="mrgn-page">
                    <h2 class="page-header" style="margin-top:25px">Cambiar contraseña</h2>
                    <div class="col-md-12">
                        <p id="msj" runat="server">Por su seguridad, cambie su contraseña cada cierto tiempo. Una contraseña segura evita el acceso no autorizado de su cuenta en este sitio Web.<br /><br />Su Contraseña debe tener entre 8 y 35 caracteres.</p>

                        <div id="formulario" runat="server">
                            <div class="col-md-12">
                                <div class="col-md-7" style="margin-top:15px">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div id="dPassAtc" runat="server">
                                                <label for="txtPassAct" class="col-sm-4 control-label">Contraseña actual</label>
                                                <div class="col-sm-6">
                                                    <asp:Textbox ID="txtPassAct" runat="server" CssClass="form-control" TextMode="Password" MaxLength="35"></asp:Textbox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div id="dNewPass" runat="server">
                                                <label for="txtNewPass" class="col-sm-4 control-label">Nueva contraseña</label>
                                                <div class="col-sm-6">
                                                    <asp:Textbox ID="txtNewPass" runat="server" CssClass="form-control" TextMode="Password" MaxLength="35"></asp:Textbox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div id="dNewPassRe" runat="server">
                                                <label for="txtNewPassRe" class="col-sm-4 control-label">Repita nueva contraseña</label>
                                                <div class="col-sm-6">
                                                    <asp:Textbox ID="txtNewPassRe" runat="server" CssClass="form-control" TextMode="Password" MaxLength="35"></asp:Textbox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom:15px">
                                <div class="col-md-7">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label for="txtPassAct" class="col-sm-4 control-label"></label>
                                            <div class="col-sm-6">
                                                <div class="rgth">
                                                    <asp:Button ID="btnCambiarPass" runat="server" Text="Cambiar contraseña" CssClass="btn btn-primary" OnClick="btnCambiarPass_Click" />
                                                </div>                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../Js/iframeResizer.contentWindow.min.js"></script>
    </form>
</body>
</html>