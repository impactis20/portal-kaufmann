﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="estadodcto.aspx.cs" Inherits="PortalProveedores.App.UI.private.sistema.estadopagos.estadodcto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../../../Css/kf_style.css" rel="stylesheet" />
    <link type="text/css" href="../../../Css/bootstrap.css" rel="stylesheet" />

    <script type="text/javascript" src="../../../Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="../../../Js/bootstrap.min.js"></script>

    <style type="text/css"> /*Para mozilla firefox*/
        @-moz-document url-prefix() {
            .of {
                overflow:hidden;
                }
            }

        body{
            margin: 0px;
            overflow:hidden;
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }

        td, h4{
            margin-bottom: 0px;
            margin-top: 0px;
        }

        .pagada{
            color: #63a945
        }

        .porpag{
            color: #e8c400
        }

        .pend{
            color: #e8201a
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm" runat="server"></asp:ScriptManager>
        <div class="navbar navbar-default" style="padding-left: 15px;padding-bottom: 15px;padding-right: 15px; border:0px">
            <p>Para buscar un documento en específico, ingrese el número y presione el botón "Buscar".</p>
            <%--<asp:updatepanel id="upa2" runat="server">
                <contenttemplate>--%>
                    <script type="text/javascript">
                        function validate(evt) {
                            var theEvent = evt || window.event;
                            var key = theEvent.keyCode || theEvent.which;
                            key = String.fromCharCode(key);
                            var regex = /[0-9]|\./;
                            if (!regex.test(key)) {
                                theEvent.returnValue = false;
                                if (theEvent.preventDefault) theEvent.preventDefault();
                            }
                        }
                    </script>

                    <div class="row" style="margin-bottom:15px;">
                        <div class="col-md-12">
                            <div class="col-md-2" style="padding-top: 8px;">
                                <h4>Nro. de documento</h4>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                  <asp:TextBox ID="txtNroFactura" runat="server" CssClass="form-control" onkeypress='validate(event)' MaxLength="10" Height="34" TextMode="Search"></asp:TextBox>
                                  <span class="input-group-btn">
                                    <asp:LinkButton ID="btnBuscarDcto" runat="server" Text="<span class='glyphicon glyphicon-search'></span> Buscar" CssClass="btn btn-primary" OnClick="btnBuscarDcto_Click"></asp:LinkButton>
                                  </span>
                                </div>
                                <%--<asp:UpdateProgress ID="up_prog2" runat="server" AssociatedUpdatePanelID="upa2">
                                    <ProgressTemplate>
                                        Cargando...
                                        <img src="../../../Img/loading.gif"/>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>--%>
                            </div>
                            <div class="col-md-4">
                                <div id="p2Error" runat="server" role="alert" visible="false">
                                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                    <strong id="p2TituloError" runat="server"></strong> <span id="p2MsjError" runat="server" style="margin-right: 15px;"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div id="dBusquedaEspecifica" runat="server" visible="false">
                        <div class="col-md-10">
                            <h2>Resultado de la búsqueda</h2>
                        </div>
                        <div class="col-md-2">
                            <h2></h2>
                            <asp:LinkButton ID="btnExportToExcel" runat="server" Text="<span class='glyphicon glyphicon-download-alt'></span> Exportar a Excel" CssClass="btn btn-success rgth" OnClick="btnExportToExcel_Click"></asp:LinkButton>
                        </div>
                        <table class="table" style="border: 2px solid #ddd;border-right:  0px;border-left: 0px;">
                            <tr>
                                <td>
                                    <div class="row-centered">
                                        <div class="col-md-3 col-centered">
                                            <h4>Estado documento:</h4>
                                        </div>
                                        <div class="col-md-2 col-centered">
                                            <h4><i class="glyphicon glyphicon-ok-sign pagada">&nbsp;</i><small>Pagada</small></h4>
                                        </div>
                                        <div class="col-md-2 col-centered">
                                            <h4><i class="glyphicon glyphicon-info-sign porpag">&nbsp;</i><small>Próximo a pagar</small></h4>
                                        </div>
                                        <div class="col-md-2 col-centered">
                                            <h4><i class="glyphicon glyphicon-question-sign pend">&nbsp;</i><small>Pendiente de pago</small></h4>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="panel panel-default" style="margin-top:15px">                                                    
                            <div class="panel-body" style="padding: 0;">                                                
                                <div style="margin:25px">
                                    <h3 id="RSPagadora" runat="server"></h3>
                                    <h4 id="RSRut" runat="server"></h4>
                                    <br />
                                    <asp:GridView ID="gvBusquedaEspecifica" runat="server" AutoGenerateColumns="false" Font-Size="8" CssClass="table table-bordered"
                                        OnRowDataBound="gvBusquedaEspecifica_RowDataBound">
                                        <HeaderStyle CssClass="gv-header-row gv-no-border" />
                                        <RowStyle CssClass="gv-no-border"/>
                                        <AlternatingRowStyle CssClass="gv-no-border" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Nro. Documento" >
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNroDocumento" runat="server" Text='<%# string.Format("<b>{0}</b>", Eval("NroDocumento")) %>'></asp:Label>                                                                                                
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Monto">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMonto" runat="server" Text='<%# string.Format("<b>{1}</b>: {0:n0}", Eval("Monto"), Eval("Moneda")) %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Forma de Pago">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFormaPago" runat="server" Text='<%# Eval("FormaPago") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Nro. Cheque">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNroCheque" runat="server" Text='<%# Eval("NroCheque") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Fecha Posible Pago">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFechaPosiblePago" runat="server" Text='<%# string.Format("{0:dd/MM/yyy}", Eval("FechaCompensa")) %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Fecha Pago">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFechaPago" runat="server" Text='<%# string.Format("{0:dd/MM/yyy}", Eval("FechaPago")) %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                            
                                     <asp:TemplateField HeaderText="Estado">
                                            <ItemTemplate >
                                                <asp:Label ID="lblEstado" runat="server" Text='<%# Eval("estado") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Razón Social">
                                            <ItemTemplate >
                                                <asp:Label ID="lblrazon" runat="server" Text='<%# Eval("razon") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField >
                                            <asp:TemplateField HeaderText="Rut">
                                            <ItemTemplate>
                                                <asp:Label ID="lblrut" runat="server" Text='<%# Eval("rut") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                             <%--  <asp:TemplateField HeaderText="Doc. Pago">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblpago" runat="server" Text='<%# Eval("noTransa") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                             <asp:TemplateField HeaderText="Monto Total" >
                                                <ItemTemplate>
                                                    <asp:Label ID="lbltotal" runat="server" Text='<%#  string.Format("<b>{1}</b>: {0:n0}",  Eval("total"), Eval("Moneda")) %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        <asp:TemplateField>
                                        <ItemTemplate>
                                            <h4 id="icon" runat="server" aria-hidden="true"></h4>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                <%--</ContentTemplate>
                </asp:updatepanel>--%>
        </div>
        <script type="text/javascript" src="../../../Js/iframeResizer.contentWindow.min.js"></script>
    </form>
</body>
</html>