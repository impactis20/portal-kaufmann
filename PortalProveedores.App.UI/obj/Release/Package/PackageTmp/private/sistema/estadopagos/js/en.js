//Calendar.LANG("en", "English", {

//        fdow: 1,                // first day of week for this locale; 0 = Sunday, 1 = Monday, etc.

//        goToday: "Go Today",

//        today: "Today",         // appears in bottom bar

//        wk: "wk",

//        weekend: "0,6",         // 0 = Sunday, 1 = Monday, etc.

//        AM: "am",

//        PM: "pm",

//        mn : [ "January",
//               "February",
//               "March",
//               "April",
//               "May",
//               "June",
//               "July",
//               "August",
//               "September",
//               "October",
//               "November",
//               "December" ],

//        smn : [ "Jan",
//                "Feb",
//                "Mar",
//                "Apr",
//                "May",
//                "Jun",
//                "Jul",
//                "Aug",
//                "Sep",
//                "Oct",
//                "Nov",
//                "Dec" ],

//        dn : [ "Sunday",
//               "Monday",
//               "Tuesday",
//               "Wednesday",
//               "Thursday",
//               "Friday",
//               "Saturday",
//               "Sunday" ],

//        sdn : [ "Su",
//                "Mo",
//                "Tu",
//                "We",
//                "Th",
//                "Fr",
//                "Sa",
//                "Su" ]

//});

Calendar.LANG("es", "Espa�ol", {

    fdow: 1,                // first day of week for this locale; 0 = Sunday, 1 = Monday, etc.

    goToday: "Ir a Hoy",

    today: "Hoy",         // appears in bottom bar

    wk: "wk",

    weekend: "0,6",         // 0 = Sunday, 1 = Monday, etc.

    AM: "am",

    PM: "pm",

    mn: ["Enero",
           "Febrero",
           "Marzo",
           "Abril",
           "Mayo",
           "Junio",
           "Julio",
           "Agosto",
           "Septiembre",
           "Octubre",
           "Noviembre",
           "Diciembre"],

    smn: ["Ene",
            "Feb",
            "Mar",
            "Abr",
            "May",
            "Jun",
            "Jul",
            "Ago",
            "Sep",
            "Oct",
            "Nov",
            "Dic"],

    dn: ["Domingo",
           "Lunes",
           "Martes",
           "Miercoles",
           "Jueves",
           "Viernes",
           "Sabado",
           "Domingo"],

    sdn: ["Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa",
            "Do"]

});
