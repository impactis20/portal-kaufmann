﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="registrosproveedores.aspx.cs" Inherits="PortalProveedores.App.UI.private.admin.registrosproveedores" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../../Css/kf_style.css" rel="stylesheet" />  
    <link type="text/css" href="../../Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/bootstrap.min.original.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/font-awesome.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="../../Js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Js/bs.pagination.js"></script>

    <style>
        body{
            overflow:hidden;
            margin: 0px 15px
        }

        alert, panel, .alert, .panel{
            margin:0px
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="s1" runat="server"></asp:ScriptManager>
        <asp:SiteMapPath ID="SiteMap1"
                runat="server"  
                PathSeparator=" > " 
                ParentLevelsDisplayed="10" 
                PathDirection="RootToCurrent" 
                RenderCurrentNodeAsLink="false" 
                ShowToolTips="true"
                CssClass="breadcrumb">
        </asp:SiteMapPath>

        <div id="admin" runat="server" class="row">
            <div class="navbar navbar-default mx-wd">
                <div class="mrgn-page">
                    <h2 class="page-header" style="margin-top:25px">Registros de proveedores</h2>
                    <div class="col-md-12 margin-btn-footer">
                        <div class="col-xs-8">
                            <p>Aquí podrá visualizar todas solicitudes enviadas de registros de usuarios del portal Web.</p>
                            <p><b>Resultados encontrados: <span id="cont" runat="server" class="text-danger"></span>.</b></p>

                            <div class="left">
                                <asp:LinkButton ID="btnRefresh" runat="server" CssClass="btn btn-default btn-lg" Text="<i class='glyphicon glyphicon-refresh'></i>" OnClick="btnRefresh_Click"></asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            
                            <div class="form-group rgth">
                                <label for="ddlFiltro" class="control-label lft">Filtrar por :</label>                            
                                <div class="col-sm-8">
                                    <asp:DropDownList ID="ddlFiltro" runat="server" CssClass="col-xs-3 form-control" AutoPostBack="true" OnTextChanged="ddlFiltro_TextChanged">
                                        <asp:ListItem Value="0" Text="Pendientes"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="Aceptadas"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Rechazadas"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Todas" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group rgth">
                                <label for="ddlFiltro" class="control-label lft">Registros por página:</label>                            
                                <div class="col-sm-1" style="width: 97px;">
                                    <asp:DropDownList ID="ddlPg" runat="server" CssClass="col-xs-3 form-control" AutoPostBack="true" OnTextChanged="ddlPg_TextChanged">
                                        <asp:ListItem Value="5" Text="5" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                        <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                        <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                        <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                        <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                        <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                        <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                        <asp:ListItem Value="13" Text="13"></asp:ListItem>
                                        <asp:ListItem Value="14" Text="14"></asp:ListItem>
                                        <asp:ListItem Value="15" Text="15"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="up" runat="server">            
                    <ContentTemplate>
                        <%-- Corrige la paginación cuando selecciona una página --%>
                        <script type="text/javascript">
                            function pageLoad() {
                                $('.bs-pagination td table').each(function (index, obj) {
                                    convertToPagination(obj)
                                });
                            }
                        </script>

                        <%--<asp:Timer ID="timer" runat="server" OnTick="timer_Tick"></asp:Timer>--%>
                        <div class="col-md-12">
                            <div class="row">
                                <asp:GridView ID="gvRegistros" runat="server" AutoGenerateColumns="false" ShowHeader="false" CssClass="table table-bordered" AllowPaging="True" PageSize="5"
                                    PagerStyle-CssClass="bs-pagination text-center" PagerSettings-Mode="Numeric" PagerSettings-Position="TopAndBottom"
                                    OnPageIndexChanging="gvRegistros_PageIndexChanging"
                                    OnRowDataBound="gvRegistros_RowDataBound"
                                    OnRowCommand="gvRegistros_RowCommand" >
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div id="state" runat="server">
                                                            <div class="row">
                                                                <div class="col-md-1">
                                                                    <asp:LinkButton ID="btnV" runat="server" CssClass="btn btn-primary btn-xs" Text="<i class='glyphicon glyphicon-share-alt'>&nbsp;</i>Visualizar" CommandName="V" CommandArgument='<%# Eval("Item1.IdSolicitud") %>'></asp:LinkButton>
                                                                </div>
                                                                <div class="col-md-2 text-overflow">
                                                                    <i class="glyphicon glyphicon-tag">&nbsp;</i><asp:Label ID="lblRS" runat="server" Text='<%# Eval("Item1.RazonSocial") %>'></asp:Label>
                                                                </div>
                                                                <div class="col-md-2 text-overflow">
                                                                    <i class="glyphicon glyphicon-user">&nbsp;</i><asp:Label ID="lblNom" runat="server" Text='<%# string.Format("{0} {1}", Eval("Item2.Nombre"), Eval("Item2.ApellidoPat")) %>'></asp:Label>
                                                                </div>
                                                                <div class="col-md-5 text-overflow">
                                                                    <i class="glyphicon glyphicon-comment">&nbsp;</i><asp:Label ID="lblM" runat="server" Text='<%# Eval("Item2.MotivoRegistro") %>'></asp:Label>
                                                                </div>
                                                                <div class="col-md-2" style="text-align:right">
                                                                    <asp:Label ID="lblFec" runat="server" CssClass="" Text='<%# string.Format("{0}", Eval("Item1.Fecha")) %>'></asp:Label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="ConfirmDeci" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h3 class="modal-title" id="alertTitle" runat="server"></h3>
                            </div>
                            <div class="modal-body">
                                <span id="alertMsj" runat="server"></span>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                <%--<button type="button" class="btn btn-primary">Save changes</button>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../Js/iframeResizer.contentWindow.min.js"></script>
    </form>
</body>
</html>