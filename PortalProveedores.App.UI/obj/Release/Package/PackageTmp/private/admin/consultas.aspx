﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="consultas.aspx.cs" Inherits="PortalProveedores.App.UI.private.admin.consultas" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../../Css/kf_style.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/bootstrap.min.original.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/font-awesome.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="../../Js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Js/bs.pagination.js"></script>

    <style>
        body{
            overflow:hidden;
            margin: 0px 15px
        }

        alert, panel, .alert, .panel{
            margin:0px
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }

        .resp{
            color: #63a945
        }

        .pend{
            color: #e8201a
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="s1" runat="server"></asp:ScriptManager>
        <asp:SiteMapPath ID="SiteMap1"
                runat="server"
                PathSeparator=" > " 
                ParentLevelsDisplayed="10" 
                PathDirection="RootToCurrent" 
                RenderCurrentNodeAsLink="false" 
                ShowToolTips="true"
                CssClass="breadcrumb" >
            <%--<NodeTemplate>
                <li>
                    <asp:HyperLink id="lnkPage" Text='<%# Eval("Title") %>' NavigateUrl='<%# Eval("Url") %>' ToolTip='<%# Eval("Description") %>' Runat="server" />
                </li>
            </NodeTemplate>--%>
        </asp:SiteMapPath>
        <div id="admin" runat="server" class="row">
            <div class="navbar navbar-default mx-wd">
                <div class="mrgn-page">
                    <h2 class="page-header" style="margin-top:25px">Consultas Portal</h2>
                    <div class="col-xs-12 margin-btn-footer">
                        <div class="col-xs-8">
                            <p>Véa las dudas y consultas que realizaron los proveedores en el portal. Sea cortés y responda todas las inquietudes.</p>
                            <p><b>Resultados encontrados: <span id="cont" runat="server" class="text-danger"></span>.</b></p>

                            <div class="left">
                                <asp:LinkButton ID="btnRefresh" runat="server" CssClass="btn btn-default btn-lg" Text="<i class='glyphicon glyphicon-refresh'></i>" OnClick="btnRefresh_Click"></asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group rgth">
                                <label for="ddlFiltro" class="control-label lft">Filtrar por :</label>                            
                                <div class="col-sm-8">
                                    <asp:DropDownList ID="ddlFiltro" runat="server" CssClass="col-xs-3 form-control" AutoPostBack="true" OnTextChanged="ddlFiltro_TextChanged">
                                        <asp:ListItem Value="0" Text="Pendientes"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="Respondidas"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Todas" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group rgth">
                                <label for="ddlFiltro" class="control-label lft">Registros por página:</label>                            
                                <div class="col-sm-1" style="width: 97px;">
                                    <asp:DropDownList ID="ddlPg" runat="server" CssClass="col-xs-3 form-control" AutoPostBack="true" OnTextChanged="ddlPg_TextChanged">
                                        <asp:ListItem Value="5" Text="5" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                        <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                        <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                        <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                        <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                        <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                        <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                        <asp:ListItem Value="13" Text="13"></asp:ListItem>
                                        <asp:ListItem Value="14" Text="14"></asp:ListItem>
                                        <asp:ListItem Value="15" Text="15"></asp:ListItem>
                                        <asp:ListItem Value="16" Text="16"></asp:ListItem>
                                        <asp:ListItem Value="17" Text="17"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="up" runat="server">
                    <ContentTemplate>
                        <%-- Corrige la paginación cuando selecciona una página --%>
                        <script type="text/javascript">
                            function pageLoad() {
                                $('.bs-pagination td table').each(function (index, obj) {
                                    convertToPagination(obj)
                                });
                            }
                        </script>
                        <div class="col-md-12">
                        <%--<asp:Timer ID="timer" runat="server" OnTick="timer_Tick"></asp:Timer>--%>
                            <div class="row">
                                <asp:GridView ID="gvConsultas" runat="server" AutoGenerateColumns="false" ShowHeader="true" ShowFooter="true" CssClass="table table-bordered" AllowPaging="True" PageSize="5"
                                    PagerStyle-CssClass="bs-pagination text-center" PagerSettings-Mode="Numeric" PagerSettings-Position="TopAndBottom"
                                    OnPageIndexChanging="gvConsultas_PageIndexChanging" 
                                    OnRowDataBound="gvConsultas_RowDataBound"
                                    OnRowCommand="gvConsultas_RowCommand">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <div class="col-md-offset-3">
                                                    <div class="col-md-11">
                                                        <div class="col-md-2">
                                                            <h4>Estado documento:</h4>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <h4><i class="glyphicon glyphicon-ok-sign resp">&nbsp;</i><small>Respondida</small></h4>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <h4><i class="glyphicon glyphicon-question-sign pend">&nbsp;</i><small>Sin responder</small></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <%--<div class="col-md-12">--%>
                                                            <div class="row">
                                                                <div class="col-md-1">
                                                                    <asp:LinkButton ID="btnV" runat="server" CssClass="btn btn-primary btn-xs" Text="<i class='glyphicon glyphicon-share-alt'>&nbsp;</i>Visualizar" CommandName="VC" CommandArgument='<%# Eval("IdConsulta") %>'></asp:LinkButton>
                                                                </div>
                                                                <div class="col-md-2 text-overflow">
                                                                    <i class="glyphicon glyphicon-user">&nbsp;</i><asp:Label ID="lblNom" runat="server" Text='<%# string.Format("{0} {1}", Eval("Nombre"), Eval("ApellidoPat"), Eval("ApellidoMat"))%>'></asp:Label>
                                                                </div>
                                                                <div class="col-md-3 text-overflow">
                                                                    <i class="glyphicon glyphicon-envelope">&nbsp;</i><asp:Label ID="lblE" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                                                                </div>
                                                                <div class="col-md-4 text-overflow">
                                                                    <i class="glyphicon glyphicon-comment">&nbsp;</i><asp:Label ID="lblM" runat="server" CssClass="text-overflow" Text='<%# Eval("Mensaje") + "..." %>'></asp:Label>
                                                                </div>
                                                                <div class="col-md-2" style="text-align:right">
                                                                    <asp:Label ID="lblFec" runat="server" CssClass="" Text='<%# string.Format("{0}", Eval("Fecha")) %>'></asp:Label>
                                                                    <%--<h4 id="gvBusquedaFiltradaH_gvBusquedaFiltradaB_0_icon_1614" aria-hidden="true" class="glyphicon glyphicon-ok-sign pagada" style="margin:0px; padding:0px;"></h4>--%>
                                                                    <h4 id="icon" runat="server" aria-hidden="true" style="margin:0px"></h4>
                                                                </div>
                                                            </div>
                                                        <%--</div>--%>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            
                        </div>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../Js/iframeResizer.contentWindow.min.js"></script>
    </form>
</body>
</html>