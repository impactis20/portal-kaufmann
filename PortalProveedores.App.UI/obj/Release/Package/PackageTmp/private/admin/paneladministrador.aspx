﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="paneladministrador.aspx.cs" Inherits="PortalProveedores.App.UI.private.admin.paneladministrador" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../../Css/kf_style.css" rel="stylesheet" />  
    <link type="text/css" href="../../Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/bootstrap.min.original.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/font-awesome.min.css" rel="stylesheet" />
    
    <script type="text/javascript" src="../../Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="../../Js/bootstrap.min.js"></script>
    
    <script type="text/javascript">
        //function loadIfrm() {
        $(document).ready(function () {
            $('li').click(function () {
                var url = $(this).attr('rel');
                $('#frame').attr('src', url);
            });
        });
    </script>
    
    <style>
        body {
            overflow:hidden;
            margin: 0px 15px;
        }
    
    
        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:SiteMapPath ID="sm"
                runat="server"
                PathSeparator=" > " 
                ParentLevelsDisplayed="10" 
                PathDirection="RootToCurrent" 
                RenderCurrentNodeAsLink="false" 
                ShowToolTips="true"
                CssClass="breadcrumb">
        </asp:SiteMapPath>
        <div id="admin" runat="server" class="row">
            <div class="navbar navbar-default mx-wd">
                <div class="mrgn-page">
                    <h2 class="page-header" style="margin-top:25px">Panel Administrador</h2>
                    <div class="margin-modulo text-center">
                        <div class="col-lg-4">
                            <div class="well">
                                <img class="img-thumbnail img-first" src="../../Img/register.png" />
                                <h3>Registros de Proveedores</h3>
                                <p>Podrá ver todos los registros de proveedores que quieran ser partícipe de este sistema. Usted puede aprobar o rechazar estas solicitudes.</p>
                                <br />
                                <p><a class="btn btn-primary btn-lg btn-block" href="RegistrosProveedores.aspx" role="button">Acceder</a></p>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="well">
                                <img class="img-thumbnail img-first" src="../../Img/usuarios.png" />
                                <h3>Usuarios Corporativos</h3>
                                <p>Busque, otorgue permisos de acceso o bloquee usuarios corporativos en este Portal Web.</p>
                                <%--<p>Visualice los usuarios que pertenecen a este portal. Además podrá ver a qué sistemas de proveedores tienen permisos de acceso.</p>--%>
                                <br />
                                <br />
                                <p><a class="btn btn-primary btn-lg btn-block" href="usuariocorporativo.aspx" role="button">Acceder</a></p>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="well">
                                <img class="img-thumbnail img-first" src="../../Img/soportetecno.png" />
                                <h3>Consultas de Proveedores</h3>
                                <p>Véa y responda amablemente todas las dudas o consultas enviadas a este portal web de proveedores.</p>
                                <br />
                                <p><a class="btn btn-primary btn-lg btn-block" href="Consultas.aspx" role="button">Acceder</a></p>
                            </div>
                        </div>
                        <%--<div class="col-lg-4">
                            <div class="well">
                                <img class="img-thumbnail img-first" src="../../Img/switch_system.png" />
                                <h3>Administrador de Sistemas para Proveedores</h3>
                                <p>Administre los sistemas para proveedores que están incorporados a este portal.</p>
                                <br />
                                <p><a class="btn btn-primary btn-lg btn-block" href="Sistemas.aspx" role="button">Acceder</a></p>
                                <p><%--<a class="btn btn-primary btn-lg btn-block disabled" style="" role="button">En construcción...</a></p>
                            </div>
                        </div>--%>
                        <div class="col-lg-4">
                            <div class="well">
                                <img class="img-thumbnail img-first" src="../../Img/user_prov.png" />
                                <h3>Usuarios Proveedores</h3>
                                <p>Véa la lista de usuarios proveedores registrados en este Portal Web. Puede ver también todos los usuarios asociados a una razón social.</p>
                                <br />
                                <p><a class="btn btn-primary btn-lg btn-block" href="UsuariosProveedores.aspx" role="button">Acceder</a></p>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="well">
                                <img class="img-thumbnail img-first" src="../../Img/dcto_razonsocial.png" />
                                <h3>Buscar una Razón Social</h3>
                                <p>Busque una razón social y véa los usuarios que están ligados a dicha razón social.</p>
                                <br />
                                <br />
                                <p><a class="btn btn-primary btn-lg btn-block" href="RazonesSociales.aspx" role="button">Acceder</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../Js/iframeResizer.contentWindow.min.js"></script>
    </form>
</body>
</html>