﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="consulta.aspx.cs" Inherits="PortalProveedores.App.UI.private.admin.consulta" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../../Css/kf_style.css" rel="stylesheet" />  
    <link type="text/css" href="../../Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/bootstrap.min.original.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/font-awesome.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="../../Js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Js/bs.pagination.js"></script>

    <style>
        body{
            overflow:hidden;
            margin: 0px 15px
        }

        alert, panel, .alert, .panel{
            margin:0px
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6{
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="s1" runat="server"></asp:ScriptManager>
        <asp:SiteMapPath ID="SiteMap1"
                runat="server"
                PathSeparator=" > " 
                ParentLevelsDisplayed="10" 
                PathDirection="RootToCurrent" 
                RenderCurrentNodeAsLink="false" 
                ShowToolTips="true"
                CssClass="breadcrumb">
        </asp:SiteMapPath>

        <div id="admin" runat="server" class="row">
            <div class="navbar navbar-default mx-wd">
                <div class="mrgn-page">
                    <h2 class="page-header" style="margin-top:25px">Consultas Portal</h2>
                    <div class="margin-btn-footer">                        
                        <div class="col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row margin-btn-footer">
                                        <div class="col-md-12">
                                            <div class="col-md-1">
                                                <asp:Label ID="lblID" runat="server" CssClass="label label-danger"></asp:Label>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:Label ID="lblFec" runat="server" CssClass="label label-warning"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row margin-btn-footer">                                                            
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <label for="lblNom"><i class="glyphicon glyphicon-user"></i></label>
                                                        <asp:Label ID="lblNom" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <label for="lblE"><i class="glyphicon glyphicon-envelope"></i></label>
                                                        <asp:Label ID="lblE" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <label for="lblF"><i class="glyphicon glyphicon-phone-alt"></i></label>
                                                        <asp:Label ID="lblF" runat="server" ></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <label for="lblFM"><i class="glyphicon glyphicon-phone"></i></label>
                                                        <asp:Label ID="lblFM" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">                                            
                                            <div class="well" style="margin-bottom:0px">
                                                <span id="txtM" runat="server"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center" style="margin-top:15px">
                                <asp:LinkButton id="btnA" runat="server"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../Js/iframeResizer.contentWindow.min.js"></script>
    </form>
</body>
</html>