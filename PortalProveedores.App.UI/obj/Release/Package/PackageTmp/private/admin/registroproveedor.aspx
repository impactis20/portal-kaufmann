﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="registroproveedor.aspx.cs" Inherits="PortalProveedores.App.UI.private.admin.registroproveedor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../../Css/kf_style.css" rel="stylesheet" />  
    <link type="text/css" href="../../Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/bootstrap.min.original.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/font-awesome.min.css" rel="stylesheet" />    

    <script type="text/javascript" src="../../Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="../../Js/bootstrap.min.js"></script>

    <script type="text/javascript">
        function countChar(val) {
            var len = val.value.length;
            if (len >= 501) {
                val.value = val.value.substring(0, 500);
            } else {
                $('#lblContador').text("Caracteres restantes permitidos: " + (500 - len));
            }
        };
    </script>

    <style>
        body{
            overflow:hidden;
            margin: 0px 15px
        }

        alert, panel, .alert, .panel{
            margin:0px
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="s1" runat="server"></asp:ScriptManager>
        <asp:SiteMapPath ID="SiteMap1"
                runat="server"
                PathSeparator=" > " 
                ParentLevelsDisplayed="10" 
                PathDirection="RootToCurrent" 
                RenderCurrentNodeAsLink="false" 
                ShowToolTips="true"
                CssClass="breadcrumb">
        </asp:SiteMapPath>
        <div id="admin" runat="server" class="row">
            <div class="navbar navbar-default mx-wd" style="margin-bottom: 37px;">
                <div class="mrgn-page">
                    <h2 class="page-header" style="margin-top:25px">Registros de proveedores</h2>
                    <div class="margin-btn-footer">
                        <div class="col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="col-md-8"> <%--70%--%>
                                        <div class="row margin-btn-footer">
                                            <div class="col-md-2">
                                                <asp:Label ID="lblID" runat="server" CssClass="label label-danger" Text='<%# string.Format("ID Solicitud: {0}", Eval("Item1.IdSolicitud")) %>'></asp:Label>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:Label ID="lblFech" runat="server" CssClass="label label-warning" Text='<%# string.Format("Fecha Solicitud: {0}", Eval("Item1.Fecha")) %>'></asp:Label>
                                            </div>                                                        
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <h4>Datos empresa</h4>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="margin-btn-footer">
                                                <div class="margin-btn-footer">
                                                    <div class="col-md-11">
                                                        <div class="col-md-3">
                                                            <div class="form-inline">
                                                                <div class="form-group">
                                                                    <label for="lblRut"><i class="glyphicon glyphicon-barcode"></i></label>
                                                                    <asp:Label ID="lblRut" runat="server" Text='<%# string.Format("{0}-{1}", Eval("Item1.Rut"), Eval("Item1.RutDigVer")) %>'></asp:Label>&nbsp;&nbsp;
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <div class="form-inline">
                                                                <div class="form-group">
                                                                    <label for="lblRS"><i class="glyphicon glyphicon-home"></i></label>
                                                                    <asp:Label ID="lblRS" runat="server" Text='<%# Eval("Item1.RazonSocial") %>'></asp:Label>                                                                            
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12"> <%--100%--%>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h4>Responsable cuenta</h4>
                                            </div>
                                            <div class="margin-btn-footer">
                                                <div class="col-md-12">
                                                    <div class="col-md-2" style="padding-right: 0;">
                                                        <div class="form-inline">
                                                            <div class="form-group">
                                                                <label for="lblRun"><i class="glyphicon glyphicon-barcode"></i></label>
                                                                <asp:Label ID="lblRun" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3" style="padding-left: 0px;">
                                                        <div class="form-inline">
                                                            <div class="form-group">
                                                                <label for="lblNom"><i class="glyphicon glyphicon-user"></i></label>
                                                                <asp:Label ID="lblNom" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3" style="padding: 0;">
                                                        <div class="form-inline">
                                                            <div class="form-group">
                                                                <label for="lblE"><i class="glyphicon glyphicon-envelope"></i></label>
                                                                <asp:Label ID="lblE" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-inline">
                                                            <div class="form-group">
                                                                <label for="lblF"><i class="glyphicon glyphicon-phone-alt"></i></label>
                                                                <asp:Label ID="lblF" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-inline">
                                                            <div class="form-group">
                                                                <label for="lblFM"><i class="glyphicon glyphicon-phone"></i></label>
                                                                <asp:Label ID="lblFM" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12"> <%--100%--%>
                                        <div class="row">
                                            <div class="well" style="margin-bottom:0px">
                                                <span id="txtM" runat="server"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="text-center" style="margin-top:15px">
                                <div id="state" runat="server" role="alert">
                            
                                </div>
                                <div id="dDecision" runat="server" class="form-horizontal" style="margin-top:25px;margin-bottom:25px;">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <asp:Label ID="lblAccionDec" runat="server" Text="Justifique su decisión" CssClass="label label-default lft" Font-Size="10"></asp:Label>
                                            <span id="responsable" runat="server" class="lft" style="margin-left:15px"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <asp:TextBox ID="txtMotivo" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Escriba texto..." onkeyup="countChar(this)" Height="150"></asp:TextBox>
                                            <asp:Label ID="lblContador" runat="server" CssClass="help-block"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div id="buttons" runat="server" class="margin-panel text-center">
                                    <div class="btn-group">
                                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                                        <i class='glyphicon glyphicon-thumbs-up'></i>&nbsp;Aceptar&nbsp;<span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><asp:LinkButton ID="btnA" runat="server" Text="¡Estoy seguro!" CssClass="active" OnClick="btnA_Click"></asp:LinkButton></li>
                                        </ul>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <i class='glyphicon glyphicon-thumbs-down'></i>&nbsp;Rechazar&nbsp;<span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><asp:LinkButton ID="btnR" runat="server" Text="¡Estoy seguro!" OnClick="btnR_Click"></asp:LinkButton></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../Js/iframeResizer.contentWindow.min.js"></script>
    </form>
</body>
</html>