﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="razonsocial.aspx.cs" Inherits="PortalProveedores.App.UI.private.user.razonsocial" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../../Css/kf_style.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/bootstrap.min.original.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/font-awesome.min.css" rel="stylesheet" />

    <script src="../../Js/jquery-2.1.1.js"></script>
    <script src="../../Js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Js/bs.pagination.js"></script>

    <style>
        body{
            margin: 0px 15px;
            overflow:hidden;
        }

        alert, panel, .alert, .panel{
            margin:0px
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }

        #gvAdm{
            margin: 1px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="s1" runat="server"></asp:ScriptManager>
        <asp:SiteMapPath ID="SiteMap1"
                runat="server"
                PathSeparator=" > " 
                ParentLevelsDisplayed="10" 
                PathDirection="RootToCurrent" 
                RenderCurrentNodeAsLink="true" 
                ShowToolTips="true" 
                CssClass="breadcrumb">
        </asp:SiteMapPath>

        <div class="row">
            <div class="navbar navbar-default mx-wd">
                <div class="mrgn-page">
                    <h2 class="page-header" style="margin-top:25px">Proveedor</h2>
                    <div class="col-md-12">
                        <p id="msj" runat="server">Este módulo obtiene los datos del proveedor, el administrador del proveedor y sus usuarios relacionados a éste mismo. Si desea agregar un nuevo usuario a esta cuenta, haga click en <asp:LinkButton ID="lbAgregarUsuario" runat="server" Text="agregar un nuevo usuario" OnClick="lbAgregarUsuario_Click"></asp:LinkButton>.</p>

                        <div id="formulario" runat="server">
                            <h3><i class="glyphicon glyphicon-chevron-right" style="width:32px;height:42px;margin-top:15px">&nbsp;</i>Datos del proveedor</h3>
                            <div class="col-md-12">
                               <div class="col-md-2">
                                   <div id="cApellidoPat" runat="server" class="form-group">
                                       <label>RUT</label>
                                       <asp:TextBox ID="txtRut" runat="server" CssClass="form-control" disabled></asp:TextBox>
                                   </div>
                               </div>
                               <div class="col-md-10">
                                   <div id="cApellidoMat" runat="server" class="form-group">
                                       <label>Razón Social</label>
                                       <asp:TextBox ID="txtRS" runat="server" CssClass="form-control" disabled></asp:TextBox>
                                   </div>
                               </div>
                            </div>

                            <h3><i class="glyphicon glyphicon-chevron-right" style="width:32px;height:42px;margin-top:15px">&nbsp;</i>Administrador</h3>
                            <div class="row" style="margin-bottom:15px">
                                <div class="col-md-12">
                                    <asp:Button ID="btnReAsignarADM" runat="server" CssClass="btn btn-primary btn-lg btn-block" OnClick="btnReAsignarADM_Click" />
                                    <asp:GridView ID="gvAdm" runat="server" AutoGenerateColumns="false" ShowHeader="false" CssClass="table table-bordered"
                                        OnRowDataBound="gvAdm_RowDataBound"
                                        OnRowCommand="gvAdm_RowCommand">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <div class="panel panel-default">
                                                        <div class="panel-body">
                                                            <div class="row" style="margin-bottom:10px">
                                                                <div class="col-md-12 text-overflow" style="height:23px">
                                                                  <span id="lblEstadoAdm" runat="server" style="font-size:13px"></span>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-2 text-overflow">
                                                                    <i class="glyphicon glyphicon-barcode">&nbsp;</i><asp:Label ID="lblRAdm" runat="server" Text='<%# Eval("Run") %>' ToolTip='<%# Eval("Run") %>'></asp:Label>
                                                                </div>
                                                                <div class="col-md-2 text-overflow">
                                                                    <i class="glyphicon glyphicon-tag">&nbsp;</i><asp:Label ID="lblNAdm" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                                                                </div>
                                                                <div class="col-md-2 text-overflow">
                                                                    <i class="glyphicon glyphicon-tags">&nbsp;</i><asp:Label ID="lblAPAdm" runat="server" Text='<%# string.Format("{0} {1}", Eval("ApellidoPat"), Eval("ApellidoMat")) %>'></asp:Label>
                                                                </div>
                                                                <div class="col-md-4 text-overflow">
                                                                    <i class="glyphicon glyphicon-envelope">&nbsp;</i><asp:Label ID="lblE" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                                                                </div>
                                                                <div id="op_adm" runat="server" class="col-md-2">
                                                                    <div class="btn-group">
                                                                        <a class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                                                            Opciones<span class="caret"></span>
                                                                        </a>
                                                                        <ul class="dropdown-menu" role="menu">
                                                                            <li class="dropdown-header">Administrador Portal</li>
                                                                            <li><asp:LinkButton ID="btnBloquearAdm" runat="server" Text="<i class='glyphicon glyphicon-ban-circle'></i>&nbsp;Bloquear" CommandName="Bloq" CommandArgument='<%# string.Format("{0}|{1} {2} {3}", Eval("IdUserProv"), Eval("Nombre"), Eval("ApellidoPat"), Eval("ApellidoMat")) %>'></asp:LinkButton></li>
                                                                            <li><asp:LinkButton ID="btnDesbloquearAdm" runat="server" Text="<i class='glyphicon glyphicon-ok-circle'></i>&nbsp;Desbloquear" CommandName="Desbloq" CommandArgument='<%# string.Format("{0}|{1} {2} {3}", Eval("IdUserProv"), Eval("Nombre"), Eval("ApellidoPat"), Eval("ApellidoMat")) %>'></asp:LinkButton></li>
                                                                            <li><asp:LinkButton ID="btnReasignarAdm" runat="server" Text="<i class='glyphicon glyphicon-transfer'></i>&nbsp;Re-asignar admin." CommandName="Re-Asign" CommandArgument='<%# string.Format("{0}|{1} {2} {3}", Eval("IdUserProv"), Eval("Nombre"), Eval("ApellidoPat"), Eval("ApellidoMat")) %>'></asp:LinkButton></li>
                                                                            <li><asp:LinkButton ID="btnBorrar" runat="server" Text="<i class='glyphicon glyphicon-remove'></i>&nbsp;Borrar" CommandName="DelUser" CommandArgument='<%# string.Format("{0}|{1} {2} {3}", Eval("IdUserProv"), Eval("Nombre"), Eval("ApellidoPat"), Eval("ApellidoMat")) %>'></asp:LinkButton></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br />
                                                            <div class="row">
                                                                <div class="col-md-2 text-overflow">
                                                                    <i class="glyphicon glyphicon-phone-alt">&nbsp;</i><asp:Label ID="lblT" runat="server" Text='<%# Eval("Fono", "+56 {0}") %>'></asp:Label>
                                                                </div>
                                                                <div class="col-md-2 text-overflow">
                                                                    <i class="glyphicon glyphicon-phone">&nbsp;</i><asp:Label ID="lblTM" runat="server" Text='<%# Eval("FonoMovil", "+56 9 {0}") %>'></asp:Label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <span class="label label-warning">Recuerde</span> que la opción <b>Re-asignar admin.</b> permite cambiar de administrador por otro. Cuando el nuevo administrador es validado y registrado, el administrador actual de la razón social es <b class="text-danger">eliminado</b> del portal sin previo aviso.
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView> 
                                </div>
                            </div>

                            <h3><i class="glyphicon glyphicon-chevron-right" style="width:32px;height:42px;margin-top:15px">&nbsp;</i>Usuarios</h3>
                            <div class="row" style="margin-bottom:15px">
                                <div id="dUsers" runat="server" class="panel-body">
                                    <asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="false" ShowHeader="false" CssClass="table table-bordered" AllowPaging="True" PageSize="5"
                                        PagerStyle-CssClass="bs-pagination text-center" PagerSettings-Mode="Numeric" PagerSettings-Position="TopAndBottom"
                                        OnRowDataBound="gvUsers_RowDataBound"
                                        OnRowCommand="gvUsers_RowCommand">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <div class="panel panel-default">
                                                        <div class="panel-body">
                                                            <div class="row" style="margin-bottom:10px">
                                                                <div class="col-md-12 text-overflow" style="height:23px">
                                                                  <span id="lblEstadoUsuario" runat="server" style="font-size:13px"></span>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-2 text-overflow">
                                                                    <i class="glyphicon glyphicon-barcode">&nbsp;</i><asp:Label ID="lblR" runat="server" Text='<%# Eval("Run") %>' ToolTip='<%# Eval("Run") %>'></asp:Label>
                                                                </div>
                                                                <div class="col-md-2 text-overflow">
                                                                    <i class="glyphicon glyphicon-tag">&nbsp;</i><asp:Label ID="lblN" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                                                                </div>
                                                                <div class="col-md-2 text-overflow">
                                                                    <i class="glyphicon glyphicon-tags">&nbsp;</i><asp:Label ID="lblAP" runat="server" Text='<%# string.Format("{0} {1}", Eval("ApellidoPat"), Eval("ApellidoMat")) %>'></asp:Label>
                                                                </div>
                                                                <div class="col-md-4 text-overflow">
                                                                    <i class="glyphicon glyphicon-envelope">&nbsp;</i><asp:Label ID="lblE" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                                                                </div>
                                                                <div id="op_users" runat="server" class="col-md-2">
                                                                    <div class="btn-group">
                                                                        <a class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                                                            Opciones <span class="caret"></span>
                                                                        </a>
                                                                        <ul class="dropdown-menu" role="menu">
                                                                            <li class="dropdown-header">Administrador Razón Social</li>
                                                                            <li><asp:LinkButton ID="btnBloquearUsuario" runat="server" Text="<i class='glyphicon glyphicon-ban-circle'></i>&nbsp;Bloquear" CommandName="Bloq" CommandArgument='<%# string.Format("{0}|{1} {2} {3}", Eval("IdUserProv"), Eval("Nombre"), Eval("ApellidoPat"), Eval("ApellidoMat")) %>'></asp:LinkButton></li>
                                                                            <li><asp:LinkButton ID="btnDesbloquearUsuario" runat="server" Text="<i class='glyphicon glyphicon-ok-circle'></i>&nbsp;Desbloquear" CommandName="Desbloq" CommandArgument='<%# string.Format("{0}|{1} {2} {3}", Eval("IdUserProv"), Eval("Nombre"), Eval("ApellidoPat"), Eval("ApellidoMat")) %>'></asp:LinkButton></li>
                                                                            <li><asp:LinkButton ID="btnBorrar" runat="server" Text="<i class='glyphicon glyphicon-remove'></i>&nbsp;Borrar usuario" CommandName="DelUser" CommandArgument='<%# string.Format("{0}|{1} {2} {3}*{4}", Eval("IdUserProv"), Eval("Nombre"), Eval("ApellidoPat"), Eval("ApellidoMat"), Eval("Email")) %>'></asp:LinkButton></li>
                                                                            <li><asp:LinkButton ID="btnDejarComoADM" runat="server" Text="<i class='glyphicon glyphicon-star'></i>&nbsp;Dejar como Admin." CommandName="EsAdmin" CommandArgument='<%# string.Format("{0}|{1} {2} {3}*{4}", Eval("IdUserProv"), Eval("Nombre"), Eval("ApellidoPat"), Eval("ApellidoMat"), Eval("Email")) %>'></asp:LinkButton></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>

                        <div style="margin-bottom:15px;">
                            <span class="label label-warning">Tenga en cuenta</span> que las acciones realizadas en los usuarios, se verán reflejadas la próxima vez que inicien sesión.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../Js/iframeResizer.contentWindow.min.js"></script>
    </form>
</body>
</html>