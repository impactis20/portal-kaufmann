﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="miperfil.aspx.cs" Inherits="PortalProveedores.App.UI.private.user.miperfil" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../../Css/kf_style.css" rel="stylesheet" />  
    <link type="text/css" href="../../Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/bootstrap.min.original.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/font-awesome.min.css" rel="stylesheet" />    

    <script type="text/javascript" src="../../Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="../../Js/bootstrap.min.js"></script>

    <script>
        function validate(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>

    <style>
        body{
            margin: 0px 15px;
            overflow:hidden;
        }

        alert, panel, .alert, .panel{
            margin:0px
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="s1" runat="server"></asp:ScriptManager>
        <asp:SiteMapPath ID="SiteMap1" runat="server" PathSeparator=" > " ParentLevelsDisplayed="10" PathDirection="RootToCurrent" RenderCurrentNodeAsLink="false" ShowToolTips="true" CssClass="breadcrumb">
        </asp:SiteMapPath>

        <div class="row">
            <div class="navbar navbar-default mx-wd">
                <div class="mrgn-page">
                    <h2 class="page-header" style="margin-top:25px">Mis datos</h2>
                    <div class="col-md-12">
                        <p id="msj" runat="server">Visualice sus datos personales y mantenga sus datos de contacto actualizados.</p> 

                        <div id="formulario" runat="server">
                            
                            <asp:UpdatePanel ID="up" runat="server">
                            <ContentTemplate>
                                <div class="col-md-6">
                                    <h3>Datos personales</h3>
                                    <div class="form-horizontal">
                                        <div id="dRun" runat="server" class="form-group">
                                            <label for="lblRun" class="col-sm-2 control-label">RUN</label>
                                            <div class="col-sm-6">
                                                <asp:Textbox ID="txtRun" runat="server" CssClass="form-control" Enabled="false"></asp:Textbox>
                                            </div>
                                        </div>
                                        <div id="dNombre" runat="server" class="form-group">
                                            <label for="txtNombre" class="col-sm-2 control-label">Nombres</label>
                                            <div class="col-sm-6">
                                                <asp:Textbox ID="txtNombre" runat="server" CssClass="form-control" Enabled="false"></asp:Textbox>
                                            </div>
                                        </div>
                                        <div id="dAP" runat="server" class="form-group">
                                            <label for="txtAP" class="col-sm-2 control-label">Apellido paterno</label>
                                            <div class="col-sm-6">
                                                <asp:Textbox ID="txtAP" runat="server" CssClass="form-control" Enabled="false"></asp:Textbox>
                                            </div>
                                        </div>
                                        <div id="dAM" runat="server" class="form-group">
                                            <label for="txtAM" class="col-sm-2 control-label">Apellido materno</label>
                                            <div class="col-sm-6">
                                                <asp:Textbox ID="txtAM" runat="server" CssClass="form-control" Enabled="false"></asp:Textbox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <h3>Datos de contacto</h3>
                                    <div class="form-horizontal">
                                        <div id="dEmail" runat="server" class="form-group">
                                            <label for="txtEmail" class="col-sm-2 control-label">Email</label>
                                            <div class="col-sm-6">
                                                <asp:Textbox ID="txtEmail" runat="server" CssClass="form-control" Enabled="false"></asp:Textbox>
                                            </div>
                                        </div>
                                        <div id="dTF" runat="server" class="form-group">
                                            <label for="txtTF" class="col-sm-2 control-label">Teléfono fijo</label>
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon">+56</span>
                                                    <asp:Textbox ID="txtTF" runat="server" CssClass="form-control" MaxLength="9" TextMode="Phone" Enabled="false" onkeypress='validate(event)'></asp:Textbox>                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div id="dTM" runat="server" class="form-group">
                                            <label for="txtTM" class="col-sm-2 control-label">Teléfono móvil</label>
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon">+56 9</span>
                                                    <asp:Textbox ID="txtTM" runat="server" CssClass="form-control" MaxLength="8" TextMode="Phone" Enabled="false" onkeypress='validate(event)'></asp:Textbox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtTM" class="col-sm-2 control-label"></label>
                                            <div class="col-sm-6">
                                                <div class="rgth">
                                                    <asp:LinkButton ID="btnEditar" runat="server" CssClass="btn btn-primary" Text="Editar" OnClick="btnEditar_Click"></asp:LinkButton>
                                                    <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-primary" Text="Guardar" OnClick="btnGuardar_Click" Visible="false"/>
                                                    <asp:Button ID="btnCancelar" runat="server" CssClass="btn btn-default" Text="Cancelar" OnClick="btnCancelar_Click" Visible="false"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../Js/iframeResizer.contentWindow.min.js"></script>
    </form>
</body>
</html>