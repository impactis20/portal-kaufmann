﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="agregaruserproveedor.aspx.cs" Inherits="PortalProveedores.App.UI.private.user.agregaruserproveedor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../../Css/kf_style.css" rel="stylesheet" />  
    <link type="text/css" href="../../Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/bootstrap.min.original.css" rel="stylesheet" />
    <link type="text/css" href="../../Css/font-awesome.min.css" rel="stylesheet" />    

    <script type="text/javascript" src="../../Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="../../Js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Js/bs.pagination.js"></script>

    <style>
        body{
            margin: 0px 15px;
            overflow:hidden;
        }

        alert, panel, .alert, .panel{
            margin:0px
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }

        #gvAdm{
            margin: 1px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="s1" runat="server"></asp:ScriptManager>
        <asp:SiteMapPath ID="SiteMap1"
                runat="server"
                PathSeparator=" > " 
                ParentLevelsDisplayed="10" 
                PathDirection="RootToCurrent" 
                RenderCurrentNodeAsLink="false" 
                ShowToolTips="true"
                CssClass="breadcrumb">
        </asp:SiteMapPath>

        <div class="row">
            <div class="navbar navbar-default mx-wd">
                <div class="mrgn-page">
                    <h2 id="tituloMod" runat="server" class="page-header" style="margin-top:25px">Agregar un usuario proveedor</h2>
                    <div class="col-md-12" style="margin-bottom:15px">
                        <p>A continuación, ingrese el email del nuevo usuario para agregarlo a la cuenta de proveedor <b id="rs" runat="server"></b>. Una vez que usted ingrese el email y presione el botón "Aceptar", se le enviará automáticamente al nuevo usuario un correo electrónico con una URL de validación para su registro en este portal, donde posteriormente tendrá que ingresar sus datos.</p>
                    </div>

                    <div class="col-md-12 col-md-offset-4">
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="txtBU" class="col-sm-1 control-label">Email</label>
                                    <div class="col-lg-7">
                                        <div class="input-group">
                                            <asp:TextBox ID="txtEmail" runat="server" placeholder="Ingrese email..." CssClass="form-control" TextMode="Email" ></asp:TextBox>                                                    
                                            <span class="input-group-btn">
                                                <asp:Button ID="btnEn" runat="server" Text="Aceptar" CssClass="btn btn-default" OnClick="btnEn_Click" />
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-bottom:15px">
                        <label class="label label-info">Importante</label>&nbsp;<i class="text-info">La <b>URL de validación de usuario</b> tiene un tiempo vigente de 24 hrs.</i>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../Js/iframeResizer.contentWindow.min.js"></script>
    </form>
</body>
</html>