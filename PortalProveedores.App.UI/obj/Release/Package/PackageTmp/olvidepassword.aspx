﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="olvidepassword.aspx.cs" Inherits="PortalProveedores.App.UI.olvidepassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title></title>
    <link type="text/css" href="Css/kf_style.css" rel="stylesheet" />    
    <link type="text/css" href="Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="Css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link type="text/css" href="Css/preview.min.css" rel="stylesheet" />    
    <link type="text/css" href="Css/font-awesome.min.css" rel="stylesheet" />

    <style type="text/css">
        .html, body{
            background-color: rgba(255, 255, 255, 0);
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="tabbable custom-tabs">
            <div class="tab-content">
                <div class="row-fluid">
                    <div class="span5">
                        <h3><i class="glyphicon glyphicon-asterisk"></i>&nbsp;&nbsp; Restablecer contraseña</h3>
                        <p>Ingrese su RUN (persona natural) y correo electrónico asociado a su cuenta.</p>
                        <div id="oRun" runat="server" class="form-group">
                            <span class="text-danger">*&nbsp;</span><label>RUN</label>&nbsp;<span>(Ej: 12345678-K)</span>
                            <asp:TextBox ID="txtRunUserProv" runat="server" placeholder="Ingrese su RUN..." CssClass="form-control"></asp:TextBox>
                        </div>
                        <div id="oEmail" runat="server" class="form-group">
                            <span class="text-danger">*&nbsp;</span><label>Email</label>
                            <asp:TextBox ID="txtEmailRecuperar" runat="server" placeholder="Ingrese email..." CssClass="form-control" TextMode="Email"></asp:TextBox>
                        </div>
                        <br />
                        <asp:Button ID="btnRestablecerContrasena" runat="server" Text="Restablecer mi contraseña" CssClass="btn btn-primary" OnClick="btnRestablecerContrasena_Click"></asp:Button>
                    </div>
                    <div class="span7">
                        <h3><i class="glyphicon glyphicon-info-sign"></i>&nbsp;&nbsp;Informaciones</h3>
                        <div class="box">
                            <p>Si olvido la contraseña de su cuenta, rellene el formulario ingresando su <b>RUN</b> y el <b>Email</b> perteneciente a su cuenta para restablecer la contraseña.</p>
                        </div>
                        <div class="box">
                            <p>Para dudas o consultas usted puede enviarnos un correo electrónico a:</p>
                            <div class="well">
                                <span><i class="glyphicon glyphicon-envelope"></i>&nbsp;&nbsp;<a href="mailto:#">consultaproveedores@kaufmann.cl</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="Js/iframeResizer.contentWindow.min.js"></script>
    </form>
</body>
</html>