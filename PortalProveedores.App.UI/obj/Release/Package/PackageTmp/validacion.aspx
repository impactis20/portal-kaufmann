﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="validacion.aspx.cs" Inherits="PortalProveedores.App.UI.validacion" Title="Validación correo electrónico" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="Css/kf_style.css" rel="stylesheet" />
    <link type="text/css" href="Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="Css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link type="text/css" href="Css/preview.min.css" rel="stylesheet" />
    <link type="text/css" href="Css/font-awesome.min.css" rel="stylesheet" />

    <style type="text/css">
        html, body, form{
            background-color: lightgrey;
            height: 99.85%;
            margin: 0
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div class="wrap">
            <div class="row-fluid">
                <div style="margin: 0 auto; max-width: 1000px;">
                    <div class="header navbar" style="min-width:767px; border-bottom: 1px solid silver;">
                        <div style="margin-top: 10px; float: left;">
                            <img src="Img/logo.kaufmann.png" height="40"/>
                        </div>
                        <div class="logo-marcas">
                            <img src="Img/mb_logo.png" class="img-marcas"/>
                            <img src="Img/freightliner_logo.png" class="img-marcas"/>
                            <img src="Img/fuso_logo.png" class="img-marcas"/>
                            <img src="Img/western_logo.png" class="img-marcas"/>
                        </div>
                    </div>
                    <h1>Portal de Proveedores</h1>

                    <div class="tabbable custom-tabs tabs-animated flat flat-all hide-label-980 shadow track-url auto-scroll">
                        <div class="tab-content">
                            <div class="margin-modulo">
                                <h2 id="titulo" runat="server" class="page-header"></h2>
                                <p id="msj" runat="server"></p>
                                <div id="form" runat="server">
                                    <h3>Datos Registrados</h3>
                                    <div class="span12" style="margin:0px">
                                        <div class="span6">
                                            <h4>Datos empresa</h4>
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <label for="lblRut" class="col-sm-2 control-label">RUT</label>
                                                    <div class="col-sm-6">
                                                        <asp:Textbox ID="lblRut" runat="server" CssClass="form-control" disabled></asp:Textbox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="lblRazonSocial" class="col-sm-2 control-label">Razón Social</label>
                                                    <div class="col-sm-6">
                                                        <asp:Textbox ID="lblRazonSocial" runat="server" CssClass="form-control" disabled></asp:Textbox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="lblPassword" class="col-sm-2 control-label">Password</label>
                                                    <div class="col-sm-6">
                                                        <asp:Textbox ID="lblPassword" runat="server" CssClass="form-control" disabled></asp:Textbox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="lblFechaSolicitud" class="col-sm-2 control-label">Fecha Solicitud</label>
                                                    <div class="col-sm-6">
                                                        <asp:Textbox ID="lblFechaSolicitud" runat="server" CssClass="form-control" disabled></asp:Textbox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6">
                                            <h4>Sus datos</h4>
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <label for="lblRun" class="col-sm-2 control-label">RUN</label>
                                                    <div class="col-sm-6">
                                                        <asp:Textbox ID="lblRun" runat="server" CssClass="form-control" disabled></asp:Textbox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="lblNombre" class="col-sm-2 control-label">Nombre Completo</label>
                                                    <div class="col-sm-6">
                                                        <asp:Textbox ID="lblNombre" runat="server" CssClass="form-control" disabled></asp:Textbox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="lblEmail" class="col-sm-2 control-label">Email</label>
                                                    <div class="col-sm-6">
                                                        <asp:Textbox ID="lblEmail" runat="server" CssClass="form-control" disabled></asp:Textbox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="lblFono" class="col-sm-2 control-label">Teléfono</label>
                                                    <div class="col-sm-6">
                                                        <asp:Textbox ID="lblFono" runat="server" CssClass="form-control" disabled></asp:Textbox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="lblFonoMovil" class="col-sm-2 control-label">Teléfono Móvil</label>
                                                    <div class="col-sm-6">
                                                        <asp:Textbox ID="lblFonoMovil" runat="server" CssClass="form-control" disabled></asp:Textbox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                
                                    </div>
                                    <div class="span12" style="margin:0px">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label for="lblMensaje" class="col-sm-2 control-label">Motivo de registro</label>
                                                <div class="col-sm-9">                                                
                                                    <asp:Textbox id="lblMensaje" runat="server" CssClass="form-control" TextMode="MultiLine" disabled Height="150"></asp:Textbox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="margin-content-modulo">
                                        <p>¡Agradecemos que usted quiera ser partícipe de este sistema web <b>Portal Proveedores de Kaufmann</b>!.</p>                                    
                                    </div>
                                </div>
                                <div style="clear:both">
                                    <div class="line-top">
                                        <div class="margin-content-modulo" style="margin-bottom:0px;">
                                            <a class="btn btn-default" href="Proveedores.aspx"><i class="glyphicon glyphicon-home"></i>&nbsp;Ir al Portal Web</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="container" style="max-width: 1080px">
                <div class="pull-left" style="background: url(img/bg.footer.logo.png) no-repeat;padding: 35px 55px">
                    <img src="img/footer.logo.png" style="width:148px;height:24px;" />
                </div>

                <div class="col-lg-9" style="float: left; margin-top: 5px">
                    <div class="col-lg-2" style="margin-bottom:10px">
                        <div class="col-lg-12">
                            <span><b>Sitio</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;">
                            <a href="http://www.kaufmann.cl" target="_blank">Kaufmann</a>
                        </div>
                    </div>
                    <div class="col-lg-3" style="margin-bottom:10px">
                        <div class="col-lg-12">
                            <span><b>Portal Proveedores</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;">
                            <a href="policies/privacidad.aspx" target="_blank">Políticas de privacidad</a>
                            <br />
                            <a href="policies/terminos.aspx" target="_blank">Términos y condiciones</a>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="col-lg-12">
                            <span><b>Contacto Mesa de Ayuda</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;color: rgba(255, 255, 255, 0.72);">
                            <i class="glyphicon glyphicon-envelope"></i> <a href="mailto:#">consultaproveedores@kaufmann.cl</a>
                            <br />
                            <i class="glyphicon glyphicon-pushpin"></i> Av. Gladys Marín 5830, Estación Central, Santiago de Chile.
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
</body>
</html>