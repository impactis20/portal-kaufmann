﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="privacidad.aspx.cs" Inherits="PortalProveedores.App.UI.policies.privacidad" Title="Políticas de Privacidad" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../Css/kf_style.css" rel="stylesheet" />
    <link type="text/css" href="../Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../Css/preview.min.css" rel="stylesheet" />
    <link type="text/css" href="../Css/font-awesome.min.css" rel="stylesheet" />

    <style type="text/css">
        html, body, form{
            background-color: lightgrey;
            height: 99.511%;
            margin: 0;
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div class="wrap">
            <div class="row-fluid">
                <div style="margin: 0 auto; max-width: 1000px;">
                    <div class="header navbar" style="min-width:767px; border-bottom: 1px solid silver;">
                        <div style="margin-top: 10px; float: left;">
                            <img src="../Img/logo.kaufmann.png" height="40"/>
                        </div>
                        <div class="logo-marcas">
                            <img src="../Img/mb_logo.png" class="img-marcas"/>
                            <img src="../Img/freightliner_logo.png" class="img-marcas"/>
                            <img src="../Img/fuso_logo.png" class="img-marcas"/>
                            <img src="../Img/western_logo.png" class="img-marcas"/>
                        </div>
                    </div>
                    <h1>Portal de Proveedores</h1>

                    <div class="tabbable custom-tabs tabs-animated flat flat-all hide-label-980 shadow track-url auto-scroll">
                        <div class="tab-content">
                            <div class="margin-modulo">
                                <h2 id="titulo" runat="server" class="page-header">Políticas de Privacidad</h2>
                                <p>Por medio del presente documento, Kaufmann informa a sus proveedores que éste no transmitirá, bajo ninguna modalidad y a ninguna empresa que no pertenezca al grupo de empresas Kaufmann, los datos personales de sus proveedores y les asegura que éstos serán manejados en forma absolutamente confidencial, atendido que Kaufmann toma muy en serio la seguridad de los datos y la privacidad de la información personal de sus proveedores.</p>
                                <dl>
                                    <dt>Compromiso con la Seguridad</dt>
                                    <dd>En relación a esta aplicación Web para proveedores, Kaufmann hace esta declaración de seguridad y privacidad con el objeto de dar a conocer y comunicar su compromiso con una práctica de negocios de alto nivel ético, dotada de los controles internos apropiados y respetuosos de los derechos de nuestros clientes.</dd>
                                </dl>

                                <dl>
                                    <dt>Información requerida del Usuario</dt>
                                    <dd>El formulario <a href="../proveedores.aspx#registrarse" target="_blank">Registrarse</a> de nuestro portal, pide a los usuarios información como cédula de identidad, nombre, apellidos, dirección electrónica, dirección física y números de contacto telefónicos. Esta información será usada para responder las consultas acerca de nuestros servicios, como también para mantener contacto con nuestros proveedores. Bajo ninguna circunstancia, esta información será compartida con otras empresas ajenas al grupo de empresas Kaufmann o con terceros.</dd>
                                </dl>

                                <dl>
                                    <dt>Captura de información de usuarios</dt>
                                    <dd>Hacemos presente que nuestro sitio web no captura información referente a actividades específicas de un usuario particular. Sin perjuicio de lo anterior, se generan reportes que nos permiten ver las actividades a nivel genérico en el portal.</dd>
                                </dl>

                                <dl>
                                    <dt>Cookies</dt>
                                    <dd>Nuestro sistema Portal de Proveedores puede agregar “cookies” en su ordenador. Las cookies son pequeños archivos de texto que colocamos en el navegador Web de su computadora o dispositivo móvil para almacenar sus preferencias, datos del usuario o configuraciones del sistema. Una cookie asigna un identificador numérico único a su navegador web o dispositivo, por lo que nos permite reconocerlo a usted como un usuario proveedor que utiliza nuestro portal, y relaciona su uso a otra información sobre usted, como por ejemplo, datos personales.
                                        <br />
                                        <br />
                                        La mayoría de los navegadores Web aceptan las cookies en forma automática. Puede configurar sus opciones del navegador Web para no recibir cookies y también puede eliminar las cookies existentes. No obstante, puede que algunas partes del sitio no funcionen adecuadamente si ha rechazado las cookies o las tecnologías de seguimiento similares, y debe tomar en cuenta que el deshabilitar las cookies o las tecnologías de seguimiento similares puede impedirle acceder a algunos de nuestros contenidos, o puede afectar su visualización del contenido o funcionamiento de la aplicación Web.
                                    </dd>
                                </dl>

                                <dl id="term_sis_cuenta_usuario">
                                    <dt>Vínculos con otros Sitios</dt>
                                    <dd>Este sitio puede contener vínculos a otros sitios. Sin perjuicio de ello, Kaufmann no es responsable de las prácticas de seguridad o privacidad, o el contenido de esos sitios. Asimismo, Kaufmann no avala ningún producto o servicio ofrecido en dichos sitios.
                                        <br />
                                        <br />
                                        SNuestro sitio está protegido con una amplia variedad de medidas de seguridad. También empleamos otros mecanismos para asegurar que los datos que usted proporciona no sean extraviados, mal utilizados o modificados inapropiadamente por personal de Kaufmann. Esos controles incluyen políticas de confidencialidad y respaldos según las políticas de Kaufmann.
                                    </dd>
                                </dl>

                                <dl>
                                    <dt>Datos Personales</dt>
                                    <dd>El usuario dispondrá en todo momento de los derechos de información y rectificación de los datos personales que le correspondan en conformidad a la Ley Nº 19.628 sobre protección de datos de carácter personal. Asimismo, si desea la cancelación o modificación de su información personal, lo puede hacer utilizando algunos de los siguientes canales de comunicación:
                                        <ul>
                                            <li>Enviando un correo electrónico a <a href="mailto:#">consultaproveedores@kaufmann.cl</a>.</li>
                                            <li>llamando al teléfono (+56 2) 2 720 2434 en horario de oficina.</li>
                                        </ul>
                                    </dd>
                                </dl>

                                <dl>
                                    <dt>Productos</dt>
                                    <dd>Todos los productos y servicios que aparecen en este sitio, están sujetos a disponibilidad y a modificaciones sin previo aviso respecto del proveedor.
                                        <br />
                                        <br />
                                        La información publicada en este sitio tiene derechos reservados y no puede ser reproducida sin autorización previa de Kaufmann.
                                    </dd>
                                </dl>

                                <dl>
                                    <dt>Marcas Comerciales</dt>
                                    <dd>Salvo indicación contraria, todas las marcas exhibidas en la página Web de Kaufmann están amparadas por los derechos de la marca Mercedes-Benz, Freightliner y Western Star incluyendo todas las placas identificativas de modelo, logotipos y emblemas corporativos, cuyos derechos de marca pertenecen a DaimlerChrysler AG.</dd>
                                </dl>
                            </div>

                            <div style="clear:both">
                                <div class="line-top">
                                    <div class="margin-content-modulo" style="margin-bottom:0px;">
                                        <div class="text-center">
                                            <a class="btn btn-default" href="../proveedores.aspx"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;Ir al inicio</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="container" style="max-width: 1080px">
                <div class="pull-left" style="background: url(../img/bg.footer.logo.png) no-repeat;padding: 35px 55px">
                    <img src="../img/footer.logo.png" style="width:148px;height:24px;" />
                </div>

                <div class="col-lg-9" style="float: left; margin-top: 5px">
                    <div class="col-lg-2" style="margin-bottom:10px">
                        <div class="col-lg-12">
                            <span><b>Sitio</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;">
                            <a href="http://www.kaufmann.cl" target="_blank">Kaufmann</a>
                        </div>
                    </div>
                    <div class="col-lg-3" style="margin-bottom:10px">
                        <div class="col-lg-12">
                            <span><b>Portal Proveedores</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;">
                            <a href="privacidad.aspx" target="_blank">Políticas de privacidad</a>
                            <br />
                            <a href="terminos.aspx" target="_blank">Términos y condiciones</a>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="col-lg-12">
                            <span><b>Contacto Mesa de Ayuda</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;color: rgba(255, 255, 255, 0.72);">
                            <i class="glyphicon glyphicon-phone-alt"></i> (+56 2) 2 720 2434
                            <br />
                            <i class="glyphicon glyphicon-envelope"></i> <a href="mailto:#">consultaproveedores@kaufmann.cl</a>
                            <br />
                            <i class="glyphicon glyphicon-pushpin"></i> Av. Gladys Marín 5830, Estación Central, Santiago de Chile.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
