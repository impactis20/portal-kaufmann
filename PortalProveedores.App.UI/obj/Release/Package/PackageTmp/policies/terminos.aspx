﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="terminos.aspx.cs" Inherits="PortalProveedores.App.UI.policies.terminos" Title="Términos y condiciones" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../Css/kf_style.css" rel="stylesheet" />
    <link type="text/css" href="../Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../Css/preview.min.css" rel="stylesheet" />
    <link type="text/css" href="../Css/font-awesome.min.css" rel="stylesheet" />

    <style type="text/css">
        html, body, form{
            background-color: lightgrey;
            height: 99.511%;
            margin: 0;
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div class="wrap">
            <div class="row-fluid">
                <div style="margin: 0 auto; max-width: 1000px;">
                    <div class="header navbar" style="min-width:767px; border-bottom: 1px solid silver;">
                        <div style="margin-top: 10px; float: left;">
                            <img src="../Img/logo.kaufmann.png" height="40"/>
                        </div>
                        <div class="logo-marcas">
                            <img src="../Img/mb_logo.png" class="img-marcas"/>
                            <img src="../Img/freightliner_logo.png" class="img-marcas"/>
                            <img src="../Img/fuso_logo.png" class="img-marcas"/>
                            <img src="../Img/western_logo.png" class="img-marcas"/>
                        </div>
                    </div>
                    <h1>Portal de Proveedores</h1>

                    <div class="tabbable custom-tabs tabs-animated flat flat-all hide-label-980 shadow track-url auto-scroll">
                        <div class="tab-content">
                            <div class="margin-modulo">
                                <h2 id="titulo" runat="server" class="page-header">Términos y condiciones</h2>
                                <p>Nuestros términos y condiciones rigen el uso que usted puede dar a este sistema Web y a cualquiera de los contenidos disponibles que éste presente. Nosotros podemos cambiar los términos y condiciones de vez en cuando sin previo aviso hacia los usuarios de este sistema.
                                    <br />
                                    <br />
                                    Tenga en cuenta que al momento de registrarse en este sistema Web, usted está <b>aceptando los términos y condiciones</b> en lo que se refiere a su uso. Si usted no está de acuerdo con estos términos y condiciones, entonces no puede usar ni podrá tener acceso a este sistema.
                                </p>
                                <dl>
                                    <dt>Términos de acceso y Licencia Limitada</dt>
                                    <dd>Usted puede acceder a la aplicación Web cuando estime conveniente desde su computadora o dispositivo móvil, ingresando su email y contraseña una vez ya registrado, en el módulo <a href="../proveedores.aspx#login">Login</a> del sistema, y podrá ver su contenido libremente, a no ser que el sistema se encuentre en mantenimiento, el servidor esté fuera de servicio, problemas de red o con Internet, el administrador del sistema o el administrador de la razón social a la cual usted pertenece lo haya bloqueado temporalmente o eliminado del sistema (más información en la cláusula <a href="#terms_sis_cuenta_us">Término de cuenta de usuario</a>). El uso del sistema y de las funcionalidades que éste posee, son sólo de carácter personal por cada cuenta registrada y sin fines comerciales. Sacar copias o impresiones del contenido del sistema es válido sólo para uso personal (interno únicamente) y no comercial.</dd>
                                </dl>

                                <dl>
                                    <dt>Uso prohibido</dt>
                                    <dd>Cualquier distribución, publicación o explotación comercial o promocional del sistema, o de cualquiera de los contenidos, códigos, datos o materiales del sistema, está estrictamente prohibida, a menos de que usted haya recibido el previo permiso expreso por escrito del personal autorizado de Kaufmann y/o del propietario de la información en cuestión. Asimismo, está  prohibido descargar, informar, exponer, publicar, copiar, reproducir, distribuir, transmitir, modificar, ejecutar, difundir, transferir, crear trabajos derivados de, vender o de cualquier otra manera explotar cualquiera de los contenidos, códigos, datos o materiales disponibles a través de este sistema.
                                        <br />
                                        <br />
                                        Además, se obliga a no alterar, editar, borrar, quitar, o de otra manera cambiar el significado o la apariencia del sistema, o cambiar el propósito de cualquiera de los contenidos, códigos, datos o materiales disponibles a través del sistema, incluyendo, la alteración o retiro de cualquier marca comercial, marca registrada, logo, marca de servicios o cualquier otro contenido de propiedad de Kaufmann o notificación de derechos de propiedad.
                                        <br />
                                        <br />
                                        También, reconoce que no adquiere ningún derecho de propiedad al descargar algún material con derechos de autor a través del sistema. La infracción a esta disposición puede constituir una violación de las leyes que protegen los derechos de autor (Ley N° 17.336 sobre Propiedad Intelectual) y de otras leyes que se aplican para estos casos, pudiendo ser sujeto a responsabilidad legal por dicho uso no autorizado.
                                    </dd>
                                </dl>

                                <dl>
                                    <dt>Datos e información</dt>
                                    <dd>Con motivo del uso que haga del sistema o de las funcionalidades puestas a su disposición, se le puede pedir que nos proporcione sus datos personales y datos de contacto al momento de registrarse o una vez ya registrado. Las políticas de uso y recopilación de información de Kaufmann con respecto a la privacidad de dicha información del usuario, se establecen en el vínculo <a href="privacidad.aspx">Políticas de privacidad</a> de nuestro sistema. Usted reconoce y acepta ser el único responsable de la exactitud del contenido de la información del usuario.</dd>
                                </dl>

                                <dl>
                                    <dt>Enlace de Sitios Web hacia terceros</dt>
                                    <dd>Este sistema puede contener vínculos hacia otros sitios Web de terceros. Usted reconoce y está de acuerdo en que Kaufmann no es responsable de las prácticas de seguridad o privacidad, productos, servicios, anuncios, códigos u otros materiales que contienen esos sitios. Asimismo, Kaufmann no garantiza ningún producto o servicio ofrecido en dichos sitios.
                                        <br />
                                        <br />
                                        Se deja constancia que este portal de proveedores, no constituye enlaces de tipo anuncios o publicidades en que usted pueda ser redireccionado.
                                    </dd>
                                </dl>

                                <dl id="terms_sis_cuenta_us">
                                    <dt>Término del sistema Web y cuenta de usuario</dt>
                                    <dd>Se puede llegar a terminar, cambiar, interrumpir, suspender o descontinuar cualquier aspecto del sistema Web o de los servicios que brinda en cualquier momento sin previo aviso.
                                        <br />
                                        <br />
                                        Los administradores de Kaufmann o de algún otro poseedor de derechos aplicable, pueden restringir, suspender o terminar su acceso del sistema y/o a sus servicios en caso que usted se encuentre en incumplimiento de nuestros términos y condiciones o de la ley aplicable, o por cualquier otra razón sin notificación.
                                    </dd>
                                </dl>
                            </div>
                            <div style="clear:both">
                                <div class="line-top">
                                    <div class="margin-content-modulo" style="margin-bottom:0px;">
                                        <div class="text-center">
                                            <a class="btn btn-default" href="../proveedores.aspx"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;Ir al inicio</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="container" style="max-width: 1080px">
                <div class="pull-left" style="background: url(../img/bg.footer.logo.png) no-repeat;padding: 35px 55px">
                    <img src="../img/footer.logo.png" style="width:148px;height:24px;" />
                </div>

                <div class="col-lg-9" style="float: left; margin-top: 5px">
                    <div class="col-lg-2" style="margin-bottom:10px">
                        <div class="col-lg-12">
                            <span><b>Sitio</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;">
                            <a href="http://www.kaufmann.cl" target="_blank">Kaufmann</a>
                        </div>
                    </div>
                    <div class="col-lg-3" style="margin-bottom:10px">
                        <div class="col-lg-12">
                            <span><b>Portal Proveedores</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;">
                            <a href="privacidad.aspx" target="_blank">Políticas de privacidad</a>
                            <br />
                            <a href="terminos.aspx" target="_blank">Términos y condiciones</a>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="col-lg-12">
                            <span><b>Contacto Mesa de Ayuda</b></span>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;color: rgba(255, 255, 255, 0.72);">
                            <i class="glyphicon glyphicon-phone-alt"></i> (+56 2) 2 720 2434
                            <br />
                            <i class="glyphicon glyphicon-envelope"></i> <a href="mailto:#">consultaproveedores@kaufmann.cl</a>
                            <br />
                            <i class="glyphicon glyphicon-pushpin"></i> Av. Gladys Marín 5830, Estación Central, Santiago de Chile.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
