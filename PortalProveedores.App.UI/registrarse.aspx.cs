﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI
{
    public partial class registrarse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ViewState["rs_encontrada"] != null)
                txtRazonSocial.Text = ViewState["rs_encontrada"].ToString();
            BindLstTipoUsuario();
        }
        public void BindLstTipoUsuario()
        {
            lstTipoUsuario1.Items.Add(new ListItem("",""));
            lstTipoUsuario1.Items.Add(new ListItem("Proveedor", "P"));
            lstTipoUsuario1.Items.Add(new ListItem("Factoring", "F"));
            lstTipoUsuario1.DataBind();

        }

        protected void btnRegistrarse_Click(object sender, EventArgs e)
        {
            string RutProveedor = txtRutProveedorReg.Text;
            string RazonSocial = ViewState["rs_encontrada"] == null ? string.Empty : ViewState["rs_encontrada"].ToString();
            string Password = txtPasswordReg.Text;
            string ConfirmaPassword = txtConfirmarPasswordReg.Text;
            string MotivoReg = txtMotivoReg.Text;
            string TipoUsuario = lstTipoUsuario1.SelectedValue.ToString();

            string RunUser = txtRunUser.Text;
            string Nombre = txtNombre.Text;
            string ApellidoPat = txtApellidoPat.Text;
            string ApellidoMat = txtApellidoMat.Text;
            string Email = txtE.Text.ToLower().Trim();
            string Fono = txtFono.Text;
            string FonoMovil = txtFonoMovil.Text;


            bool EstadoTxtRut = string.IsNullOrEmpty(RutProveedor);
            bool EstadoTxtRazonS = string.IsNullOrEmpty(RazonSocial);
            bool EstadoTxtPassword = string.IsNullOrEmpty(Password);
            bool EstadoTxtConfPassword = string.IsNullOrEmpty(ConfirmaPassword);

            bool EstadoTxtRun = string.IsNullOrEmpty(RunUser);
            bool EstadoTxtNombre = string.IsNullOrEmpty(Nombre);
            bool EstadoTxtApellidoPat = string.IsNullOrEmpty(ApellidoPat);
            bool EstadoTxtApellidoMat = string.IsNullOrEmpty(ApellidoMat);
            bool EstadotxtE = string.IsNullOrEmpty(Email);
            bool EstadoTxtFono = string.IsNullOrEmpty(Fono);
            bool EstadoTxtFonoMovil = string.IsNullOrEmpty(FonoMovil);

            bool EstadoTxtMotivoReg = string.IsNullOrEmpty(MotivoReg);

            //Si están vacíos, que queden de color rojo
            NormalizaFormularioRegistro();

            //Mantengo la contraseña escrita en los txts
            MantienePasswordTxT();

            //Campos con asterisco son obligatorios
            if (EstadoTxtRut || EstadoTxtRazonS || EstadoTxtPassword || EstadoTxtConfPassword || EstadoTxtRun || EstadoTxtNombre || EstadoTxtApellidoPat || EstadoTxtApellidoMat || EstadotxtE || EstadoTxtFono || EstadoTxtFonoMovil || EstadoTxtMotivoReg)
            {
                MostrarMensaje("Datos incompletos", "Estimado proveedor, todos los campos con asterisco son obligatorios.", "text-danger"); //Mensaje de error al dejar campos obligatorios vacíos.
            }
            else
            {
                //Validar algunos datos ingresados
                if (!chbAceptaTerminos.Checked) //Acepta términos&condiciones
                {
                    MostrarMensaje("Acepte los términos y condiciones", "Para registrarse en nuestro sistema de proveedores, ustedes debe aceptar los <b>términos y condiciones</b>.", "text-danger");
                    return;
                }

                //Datos Empresa
                #region Rut
                if (!ValidaRutProveedor())
                    return;
                #endregion

                #region Password
                if (Password.Length < 8 || Password.Length > 35) //Valida largo mínimo
                {
                    MostrarMensaje("Contraseña Inválida", "Su contraseña debe contener entre 8 y 35 caracteres.", "text-danger");
                    ErrorRegistroPassword();
                    return;
                }
                else if (Password != ConfirmaPassword) //Valida que la repetición de la contraseña sea correcta
                {
                    MostrarMensaje("Contraseña no coincide", "La repetición de contraseña es distinta a la ingresada, favor corregir.", "text-danger");
                    ErrorRegistroPassword();
                    return;
                }
                #endregion

                //Datos Persona
                #region Rut
                if (!ValidaRunUsuario())
                    return;
                #endregion

                #region Email
                bool EmailValido = App_Code.Util.isValidEmail(Email); //Valida si el correo tiene el formato correcto

                if (!EmailValido)
                {
                    MostrarMensaje("Email incorrecto", "Su Email es incorrecto, por favor corríjalo.", "text-danger"); //Mensaje de error al dejar campos obligatorios vacíos.
                    SetColorTxT(rEmail, "form-group has-error");
                    return;
                }
                else
                {
                    try
                    {
                        int EmailExiste = PortalProveedorBL.VerificaSiEmailExiste(Email);

                        if (EmailExiste > 0 || Email.Contains("kaufmann.cl")) //Valida si el email ya fue registrado
                        {
                            string UrlRecuperarPass = string.Format("{0}proveedores.aspx#olvidemipassword", HttpContext.Current.Request.Url.Host);
                            MostrarMensaje("Email inválido", string.Format("El email ingresado está siendo utilizado por otro usuario en el sistema.<br><br><label class=\"label label-info\">Recuerde que</label><i class=\"text-info\"> si usted olvida la contraseña de una cuenta, puede recuperarla en el módulo <u><a href=\"{0}\" target=\"_blank\">restablecer contraseña</a></u></i>.", UrlRecuperarPass), "text-danger");
                            SetColorTxT(rEmail, "form-group has-error");
                            return;
                        }
                    }
                    catch
                    {
                        MostrarMensaje("Error temporal 89", string.Format("El Email \"{0}\" no se pudo validar el formato del correo electrónico. Aparentemente ocurrió un problema interno. Se le solicita por favor, registrarse más tarde.", Email), "text-danger");
                    }
                }
                #endregion

                #region Teléfonos de contacto
                //Red fija
                int FonoNumeric = 0;
                if (App_Code.Util.isIntNumeric(Fono))
                {
                    FonoNumeric = Convert.ToInt32(Fono);

                    if (Fono.Length <= 8)
                    {
                        MostrarMensaje("Teléfono fijo inválido", "Su número de teléfono es incorrecto. Hoy en día los números de teléfono red fija contienen 9 dígitos.", "text-danger");
                        return;
                    }
                }
                else
                {
                    MostrarMensaje("Teléfono fijo inválido", "Su número de teléfono debe ser numérico.", "text-danger");
                    return;
                }

                //Móvil
                int FonoMovilNumeric = 0;
                if (App_Code.Util.isIntNumeric(FonoMovil))
                {
                    FonoMovilNumeric = Convert.ToInt32(FonoMovil);

                    if (FonoMovil.Length <= 7)
                    {
                        MostrarMensaje("Teléfono móvil inválido", "Su número de teléfono móvil es incorrecto. Los teléfonos móviles contienen 8 dígitos.", "text-danger");
                        return;
                    }
                }
                else
                {
                    MostrarMensaje("Teléfono móvil inválido", "Su número de teléfono móvil debe ser numérico.", "text-danger");
                    return;
                }
                #endregion

                //Inserta Registro
                int IdSolicitud = 0;
                IdSolicitud = PortalProveedorBL.InsertSolicitudRegistro(RutProveedor, RazonSocial, Password, DateTime.Now, MotivoReg, RunUser, Nombre, ApellidoPat, ApellidoMat, Email, FonoNumeric, FonoMovilNumeric, TipoUsuario);

                //-2 = el correo ya existe en la bd.
                //-1 = el rut ingresado ya existe en la base de datos
                // 0 = error, solicitud no insertada 
                
                if (IdSolicitud != 0 && IdSolicitud != -1)
                {
                    //Genera link
                    string PaginaValidacion = Request.Url.AbsoluteUri.ToLower().Replace("registrarse", "validacion");
                    string Valores = string.Format("id={0}&validacorreo=1&max_fecha_vigencia={1}", IdSolicitud, DateTime.Now.AddHours(24));
                    string ValoresEncriptados = App_Code.Util.Encriptar(Valores);
                    string URLValidacion = string.Format("{0}?v={1}", PaginaValidacion, ValoresEncriptados);

                    //Envía correo electrónico con link de validación
                    bool EnviaCorreo = WebServices.EnviaCorreo(Email, "Validación registro de usuario - Kaufmann Portal de Proveedores", string.Format("Estimado {0}, haga click en <a href='{2}'>este enlace</a> para validar su email ingresado en el formulario de registro del Portal de Proveedores de Kaufmann. De esta forma nos aseguramos de que el correo electrónico ingresado corresponda a usted.<br><br>Le recordamos que una vez validado su correo electrónico (link anterior), se enviará su solicitud de registro a los administradores del portal para que decidan aceptar o no. Si es <u>aceptada</u> podrá acceder al portal y será usted <b>el administrador de <u>{1}</u></b>. En caso que su solicitud de registro sea <u>rechazada</u>, le haremos llegar el motivo.<br><br><b style=\"color:red\">IMPORTANTE: Si usted no se ha registrado en el Portal de Proveedores de Kaufmann, favor de ignorar este correo, gracias.</b><br><br>Saludos cordiales,", Nombre, RazonSocial, URLValidacion));

                    //Deja en blanco el formulario
                    EstadoTextBoxs(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, true, true, true, true);
                    
                    txtRunUser.Text = string.Empty;
                    txtRutProveedorReg.Text = string.Empty;
                    txtRazonSocial.Text = string.Empty;
                    txtPasswordReg.Attributes["value"] = string.Empty;
                    txtConfirmarPasswordReg.Attributes["value"] = string.Empty;
                    txtMotivoReg.Text = string.Empty;

                    chbAceptaTerminos.Checked = false;

                    MostrarMensaje("¡Valide su solicitud!", string.Format("Estimado proveedor, le hemos enviado un link de validación por correo electrónico a <b>{0}</b>. Acceda al link para culminar la solicitud de registro en nuestro sistema.<br><br><label class=\"label label-info\">¡Importante!</label>&nbsp;<i class=\"text-info\">El <b>link de validación</b> tiene una vigencia de 24 hrs. En caso que el link expire, vuelva a registrar sus datos en este formulario.</i>", Email), "text-success");
                }
                else
                {
                    MostrarMensaje("Error temporal (113)", "Estimado proveedor, hubo un error interno al generar la solicitud de registro, por favor inténtelo más tarde.<br><br>Cualquier duda o consulta contacte a nuestra mesa de ayuda.", "text-danger");
                }
            }
        }

        private void NormalizaFormularioRegistro()
        {
            //Empresa Proveedor
            ColorEstadoTxT(txtRutProveedorReg, rRut);
            //ColorEstadoTxT(txtRazonSocial, rRazonS);
            ColorEstadoTxT(txtPasswordReg, rPAss);
            ColorEstadoTxT(txtConfirmarPasswordReg, rConfPass);
            ColorEstadoTxT(txtMotivoReg, rMotivo);

            //Usuario Proveedor
            ColorEstadoTxT(txtRunUser, rRun);
            ColorEstadoTxT(txtNombre, rNombre);
            ColorEstadoTxT(txtApellidoPat, rApellidoP);
            ColorEstadoTxT(txtApellidoMat, rApellidoM);
            ColorEstadoTxT(txtE, rEmail);
            ColorEstadoTxT(txtFono, rTelefonoFijo);
            ColorEstadoTxT(txtFonoMovil, rTelefonoMovil);
        }

        private void ErrorRegistroPassword()
        {
            txtPasswordReg.Attributes["value"] = string.Empty;
            txtConfirmarPasswordReg.Attributes["value"] = string.Empty;
            SetColorTxT(rPAss, "form-group has-error");
            SetColorTxT(rConfPass, "form-group has-error");
        }

        //Es llamado cuando hay algún dato ingresado con error en el txt
        private void SetColorTxT(HtmlGenericControl div, string Estilo)
        {
            div.Attributes["class"] = Estilo;
        }

        //Valida que los txt no estén vacíos
        private void ColorEstadoTxT(TextBox txt, HtmlGenericControl div)
        {
            if (txt.Text == string.Empty)
                div.Attributes["class"] = "form-group has-error";
            else
                div.Attributes["class"] = "form-group";
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("parent.abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }

        //Verifica si la empresa proveedora ya existe en el sistema
        protected void txtRutProveedorReg_TextChanged(object sender, EventArgs e)
        {
            ValidaRutProveedor();
        }

        private bool ValidaRutProveedor()
        {
            string Rut = txtRutProveedorReg.Text.Trim();
            string TipoUsuario = lstTipoUsuario1.SelectedValue.ToString();
            if (!string.IsNullOrEmpty(Rut))
            {
                if (Rut.Contains("-"))
                {
                    try
                    {
                        Rut = App_Code.Util.FormatoRut(Rut);
                        bool RutValido = App_Code.Util.RutValido(Rut);

                        //HabilitaFormUser(true); //Habilito el formulario de usuario
                        MantienePasswordTxT();
                        txtRazonSocial.Focus(); //Establesco el foco del siguiente TextBox

                        if (RutValido)
                        {
                            string RespRazonSocial = PagosBL.GetExisteRazonSocialSAP(Rut, TipoUsuario);

                            if (RespRazonSocial == "NE")
                            {
                                MostrarMensaje("Rut no fue encontrado", string.Format("El Rut de la razón social ingresado \"<b>{0}</b>\", no fue encontrado en nuestros sistemas como proveedor de <b>Kaufmann</b>. Por lo que usted no podrá registrarse en este portal.<br><br><br><span class=\"label label-danger\"><i class=\"glyphicon glyphicon-exclamation-sign\"></i></span> Si usted es un proveedor reciente, favor contactarse con quien le solicitó los servicios.", Rut), "text-danger");
                                SetColorTxT(rRut, "form-group has-error");
                            }
                            else if (RespRazonSocial == "Error")
                            {
                                MostrarMensaje("Error temporal 427", string.Format("Ha ocurrido un error interno en nuestros sistemas. Favor de intentarlo más tarde.", Rut), "text-danger");
                                SetColorTxT(rRut, "form-group has-error");
                            }
                            else
                            {
                                txtRazonSocial.Text = App_Code.Util.Initcap(RespRazonSocial);
                                ViewState.Add("rs_encontrada", RespRazonSocial);

                                Tuple<ProveedorEntity, UsuarioProveedorEntity> Proveedor = PortalProveedorBL.GetProveedorExistente(Rut);

                                if (Proveedor != null) //Si entra es porque la razón social ya está registrada en el portal
                                {
                                    string DatosEmpresa = string.Format("<div class=\"panel panel-default\">" +
                                                                            "<div class=\"panel-heading\"><b>Datos Empresa</b></div>" +
                                                                            "<div class=\"panel-body\">" +

                                                                                "<div class=\"form-horizontal\" role=\"form\" style=\"margin-top:15px\">" +
                                                                                    "<div class=\"form-group\">" +
                                                                                        "<div class=\"col-sm-12\">" +
                                                                                            "<label for=\"rs\" class=\"col-sm-3 control-label\">Razón Social</label>" +
                                                                                            "<span class=\"label label-default\" id=\"rs\">{0}</span>" +
                                                                                        "</div>" +
                                                                                    "</div>" +
                                                                                "</div>" +

                                                                                "<div class=\"form-horizontal\" role=\"form\">" +
                                                                                    "<div class=\"form-group\">" +
                                                                                        "<div class=\"col-sm-12\">" +
                                                                                            "<label for=\"contacto\" class=\"col-sm-3 control-label\">Contacto</label>" +
                                                                                            "<span class=\"label label-default\" id=\"contacto\">{1} {2}</span>" +
                                                                                        "</div>" +
                                                                                    "</div>" +
                                                                                "</div>" +

                                                                                "<div class=\"form-horizontal\" role=\"form\">" +
                                                                                    "<div class=\"form-group\">" +
                                                                                        "<div class=\"col-sm-12\">" +
                                                                                            "<label for=\"Email\" class=\"col-sm-3 control-label\">Email</label>" +
                                                                                            "<span class=\"label label-default\" id=\"Email\">{3}</span>" +
                                                                                        "</div>" +
                                                                                    "</div>" +
                                                                                "</div>" +

                                                                            "</div>" +
                                                                        "</div>", Proveedor.Item1.RazonSocial, Proveedor.Item2.Nombre, Proveedor.Item2.ApellidoPat, Proveedor.Item2.Email);

                                    MostrarMensaje("Rut ya existe en el sistema", string.Format("La razón social ya está registrada. Si usted necesita ser un usuario proveedor de esta empresa, por favor contactarse con los siguientes datos.<br><br>{0}", DatosEmpresa), "text-danger");
                                    SetColorTxT(rRut, "form-group has-error");
                                }
                                else
                                {
                                    SetColorTxT(rRut, "form-group ");

                                    return true;
                                }
                            }
                        }
                        else
                        {
                            txtRazonSocial.Text = string.Empty;

                            MostrarMensaje("RUT Razón Social inválido", string.Format("El RUT de la razón social \"{0}\" es inválido, corríjalo.", txtRutProveedorReg.Text.Trim()), "text-danger");
                            SetColorTxT(rRut, "form-group has-error");
                        }
                    }
                    catch(Exception ex)
                    {
                        //Rut inválido
                        //MostrarMensaje("RUT inválido", string.Format("El RUT de la razón social \"{0}\" no se pudo validar. Aparentemente ocurrió un problema interno en el sistema. Se le solicita por favor, registrarse más tarde.", txtRutProveedorReg.Text.Trim()), "text-danger");
                        MostrarMensaje("RUT inválido", string.Format("El RUT de la razón social \"{0}\" no se pudo validar. ERROR: {1}.", txtRutProveedorReg.Text.Trim(), ex.Message), "text-danger");
                    }
                }
                else
                {
                    MostrarMensaje("RUT Razón Social incorrecto ", "El RUT de la razón social lo debe ingresar con un guión.<br><br>Ejemplo: 12345678-K", "text-danger");
                }
            }
            else
            {
                MostrarMensaje("Campo RUT Razón Social vacío", "Para registrarse debe ingresar el RUT de la empresa que quiere registrar en el portal.", "text-danger");
            }

            return false;
        }

        //Verifica si el usuario ya existe en el sistema
        protected void txtRunUser_TextChanged(object sender, EventArgs e)
        {
            ValidaRunUsuario();
        }

        private bool ValidaRunUsuario()
        {
            string Run = txtRunUser.Text.Trim();

            if (!string.IsNullOrEmpty(Run))
            {
                if (Run.Contains("-"))
                {
                    try
                    {
                        Run = App_Code.Util.FormatoRut(Run);
                        bool RutValido = App_Code.Util.RutValido(Run);

                        //HabilitaFormUser(true); //Habilito el formulario de usuario
                        MantienePasswordTxT();
                        txtNombre.Focus(); //Establesco el foco del siguiente TextBox
                        SetColorTxT(rRun, "form-group");

                        if (RutValido)
                        {
                            UsuarioProveedorEntity UsuarioProveedor = PortalProveedorBL.GetUsuarioProveedor(Run);
                            if (UsuarioProveedor == null)
                            {
                                txtNombre.Focus();
                            }
                            else if (UsuarioProveedor.Vigente == 1)
                            {
                                EstadoTextBoxs(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, true, true, true, true);

                                MostrarMensaje("Run ya existe", string.Format("El Run \"{0}\" ya existe en nuestro sistema y está siendo utilizado.", Run), "text-danger");
                                txtRunUser.Focus();
                            }
                            else if (UsuarioProveedor.Vigente == 2)
                            {
                                EstadoTextBoxs(UsuarioProveedor.Nombre, UsuarioProveedor.ApellidoPat, UsuarioProveedor.ApellidoMat, UsuarioProveedor.Email, UsuarioProveedor.Fono.ToString(), UsuarioProveedor.FonoMovil.ToString(), false, false, false, true);

                                if (ViewState["UsuarioYaRegistrado"] == null || Convert.ToInt32(ViewState["UsuarioYaRegistrado"]) != UsuarioProveedor.IdUserProv)
                                {
                                    ViewState.Add("UsuarioYaRegistrado", UsuarioProveedor.IdUserProv); //indico que es un usuario que ya estaba registrado pero almaceno el id del usuario proveedor
                                    MostrarMensaje("Tenga presente que...", string.Format("El Run \"{0}\" en algún momento se registró en este portal, por lo que utilizaremos los datos personales que usted ya había ingresado (nombre, apellido paterno y apellido materno).<br><br>Los datos de contacto que puede modificar son: <b>teléfono fijo</b>, <b>teléfono móvil</b> y <b>correo electrónico</b>.", Run, UsuarioProveedor.Email), "text-warning");
                                }

                                txtRunUser.Focus();
                                return true;
                            }

                            if (ViewState["UsuarioYaRegistrado"] != null) //remueve el valor almacenado si cambia el run ingresado
                                ViewState.Remove("UsuarioYaRegistrado");

                            return true;
                        }
                        else
                        {
                            if (ViewState["UsuarioYaRegistrado"] != null) //remueve el valor almacenado si cambia el run ingresado
                                ViewState.Remove("UsuarioYaRegistrado");

                            EstadoTextBoxs(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, true, true, true, true);
                            SetColorTxT(rRun, "form-group has-error");
                            MostrarMensaje("Rut inválido", string.Format("Su RUN \"{0}\" es inválido, corríjalo.", Run), "text-danger");
                            txtRunUser.Focus();
                            return false;
                        }
                    }
                    catch
                    {
                        //Rut inválido
                        MostrarMensaje("Error al validar el Run", "Ha ocurrido un error al momento de validar el Run ingresado. Si el error persiste, favor de intentarlo más tarde.", "text-danger");
                        return false;
                    }
                }
                else
                {
                    MostrarMensaje("RUN usuario incorrecto ", "Su RUN lo debe ingresar con un guión.<br><br>Ejemplo: 12345678-K", "text-danger");
                    SetColorTxT(rRun, "form-group has-error");
                    txtRunUser.Focus();
                }
            }
            else
            {
                MostrarMensaje("Campo RUN Usuario vacío", "Para registrarse debe ingresar su RUN en el campo <b>RUN Usuario</b>.", "text-danger");
                SetColorTxT(rRun, "form-group has-error");
                txtRunUser.Focus();
            }

            return false;
        }

        private void EstadoTextBoxs(string ValorTxtNombre, string ValorTxtAP, string ValorTxtAM, string ValorTxtEmail, string ValorTxtTF, string ValorTxtTM, bool EnabledTxtNombre, bool EnabledTxtAP, bool EnabledTxtAM, bool EnabledTxtEmail)
        {
            //datos personales
            txtNombre.Text = ValorTxtNombre;
            txtApellidoPat.Text = ValorTxtAP;
            txtApellidoMat.Text = ValorTxtAM;

            txtNombre.Enabled = EnabledTxtNombre;
            txtApellidoPat.Enabled = EnabledTxtAP;
            txtApellidoMat.Enabled = EnabledTxtAM;

            //datos de contacto
            txtE.Text = ValorTxtEmail;
            txtFono.Text = ValorTxtTF;
            txtFonoMovil.Text = ValorTxtTM;

            txtE.Enabled = EnabledTxtEmail;
        }

        private void LimpiaFormUser()
        {
            txtNombre.Text = string.Empty;
            txtApellidoPat.Text = string.Empty;
            txtApellidoMat.Text = string.Empty;
            txtE.Text = string.Empty;
            txtFono.Text = string.Empty;
            txtFonoMovil.Text = string.Empty;
        }

        private void MantienePasswordTxT()
        {
            txtPasswordReg.Attributes["value"] = txtPasswordReg.Text;
            txtConfirmarPasswordReg.Attributes["value"] = txtConfirmarPasswordReg.Text;
        }
    }
}