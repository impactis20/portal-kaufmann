﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI
{
    public partial class registrousuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["v"] != null)
                {
                    string ValoresDesencriptados = App_Code.Util.Desencriptar(Request.QueryString["v"].ToString());

                    string[] Separador = { "&" };
                    string[] Valores = ValoresDesencriptados.Split(Separador, StringSplitOptions.RemoveEmptyEntries);

                    int IdRegistro = Convert.ToInt32(Valores[0].Split('=')[1]);
                    DateTime MaxFechaVigencia = Convert.ToDateTime(Valores[1].Split('=')[1]);

                    //10 = usuario proveedor solicita al administrador del portal web, una re-asignación de administrador en una determinada razón social (falso adm)
                    //20 = el usuario administrador de una determinada razón social, desea agregar un usuario proveedor

                    ViewState.Add("email", Valores[2].Split('=')[1]); //almacena email ingresado desde el módulo "agregaruserprov.aspx"

                    int TipoSolicitud = Convert.ToBoolean(Valores[3].Split('=')[1]) ? 10 : 20; //(reasignar_admin)
                    ViewState.Add("TipoSolicitud", TipoSolicitud);

                    Tuple<ProveedorEntity, UsuarioProveedorEntity, int> datosSolicitud = PortalProveedorBL.GetSolicitudRegUsuario(IdRegistro, TipoSolicitud);

                    if (datosSolicitud.Item3 == 1 || DateTime.Now > MaxFechaVigencia) //1: Está registrado | 2: No se ha registrado
                    {
                        PaginaInvalidaIncorrecta(datosSolicitud);
                        return;
                    }
                    else
                    {
                        ViewState.Add("id_registro", IdRegistro);
                        ViewState.Add("razon_social", datosSolicitud.Item1.RazonSocial);

                        switch (TipoSolicitud)
                        {
                            case 10:
                                titulo.InnerHtml = string.Format("Regístrese como usuario Administrador de \"{0}\"", datosSolicitud.Item1.RazonSocial);
                                msj.InnerHtml = string.Format("El administrador de este portal, le ha solicitado que se registre como usuario administrador de <b>{0}</b>. Esto se debe a que el administrador actual de dicha razón social, puede que sea un usuario inactivo o falso.<br><br>Para ser el administrador de <b>{0}</b>, complete el siguiente formulario.", datosSolicitud.Item1.RazonSocial);
                                break;
                            case 20:
                                titulo.InnerHtml = string.Format("Regístrese como usuario para \"{0}\"", datosSolicitud.Item1.RazonSocial);
                                msj.InnerHtml = string.Format("El administrador <b>{0} {1}</b> de la cuenta de proveedor <b>{2}</b>, le ha solicitado que se registre como usuario. Para ser un usuario de <b>{2}</b>, complete el siguiente formulario.", datosSolicitud.Item2.Nombre, datosSolicitud.Item2.ApellidoPat, datosSolicitud.Item1.RazonSocial);
                                break;
                        }
                        
                        campos.Visible = true;
                        txtEmail.Text = Valores[2].Split('=')[1];

                        ViewState.Add("email", Valores[2].Split('=')[1]);
                    }
                }
            }
            catch
            {
                titulo.InnerHtml = "<i class='glyphicon glyphicon-remove'></i>&nbsp;&nbsp;Página incorrecta";
                msj.InnerHtml = "El acceso a esta página es inválido.";
                form.Visible = false;
            }
        }

        private void PaginaInvalidaIncorrecta(Tuple<ProveedorEntity, UsuarioProveedorEntity, int> datosSolicitud)
        {
            //Establece título
            titulo.InnerHtml = "<i class='glyphicon glyphicon-remove'></i>&nbsp;&nbsp;Página inválida o incorrecta";

            //Obtiene administradores de la razón social
            List<UsuarioProveedorEntity> listAdmins = PortalProveedorBL.GetAdmRS(datosSolicitud.Item1.IdProv);
            string Administradores = string.Empty;

            foreach (UsuarioProveedorEntity admin in listAdmins)
                Administradores += string.Format(" {0} {1} (<b>{2}</b>),", admin.Nombre, admin.ApellidoPat, admin.Email);

            msj.InnerHtml = string.Format("Lo sentimos, pero este enlace ya fue utilizado para un registro o ya dejó de ser válida.<br><br>Si lo que desea es ser usuario de <b>{0}</b>, entonces contáctese y solicíteselo a un administrador de la cuenta: <b>{1}</b>.", datosSolicitud.Item1.RazonSocial, Administradores.TrimStart().Substring(0, Administradores.Length - 2));
            form.Visible = false;
        }

        protected void btnReg_Click(object sender, EventArgs e)
        {
            string Run = txtRun.Text.Trim();
            string Nombre = txtNombre.Text.Trim();
            string AP = txtAP.Text.Trim();
            string AM = txtAM.Text.Trim();
            string Email = ViewState["email"].ToString();
            string FonoFijo = txtTF.Text.Trim();
            string FonoMovil = txtTM.Text.Trim();
            
            string Pass = txtPass.Text.Trim();
            string PassRe = txtPassRe.Text.Trim();

            NormalizaFormulario();

            //Mantengo la contraseña escrita en los txts
            MantienePasswordTxT();

            if (string.IsNullOrEmpty(Run) || string.IsNullOrEmpty(Nombre) || string.IsNullOrEmpty(AP) || string.IsNullOrEmpty(AM) || string.IsNullOrEmpty(FonoFijo) || string.IsNullOrEmpty(FonoMovil) || string.IsNullOrEmpty(Pass) || string.IsNullOrEmpty(PassRe))
            {
                MostrarMensaje("Datos incompletos", "Estimado proveedor, todos los campos con asterisco son obligatorios.", "text-danger"); //Mensaje de error al dejar campos obligatorios vacíos.
            }
            else
            {
                //Validar algunos datos ingresados
                if (!chbAceptaTerminos.Checked) //Acepta términos&condiciones
                {
                    MostrarMensaje("Acepte los términos y condiciones", "Para registrarse en nuestro sistema de proveedores debe <b>aceptar los términos y condiciones</b>.", "text-danger");
                    return;
                }

                #region Run
                if (ValidaRun())
                {
                    #region teléfonos de contacto
                    //Red fija
                    if (App_Code.Util.isIntNumeric(FonoFijo))
                    {
                        if (FonoFijo.Length <= 8)
                        {
                            MostrarMensaje("Teléfono fijo inválido", "Su número de <b>teléfono fijo es incorrecto</b>. Hoy en día los números de teléfono red fija contienen 9 dígitos.", "text-danger");
                            SetColorTxT(dTF, "form-group has-error");
                            return;
                        }
                    }
                    else
                    {
                        SetColorTxT(dTF, "form-group has-error");
                        MostrarMensaje("Teléfono fijo inválido", "Su número de <b>teléfono fijo debe ser numérico</b>.", "text-danger");
                        return;
                    }

                    //Móvil
                    if (App_Code.Util.isIntNumeric(FonoMovil))
                    {
                        if (FonoMovil.Length <= 7)
                        {
                            SetColorTxT(dTM, "form-group has-error");
                            MostrarMensaje("Teléfono móvil inválido", "Su número de <b>teléfono móvil es incorrecto</b>. Los teléfonos móviles contienen 8 dígitos.", "text-danger");
                            return;
                        }
                    }
                    else
                    {
                        SetColorTxT(dTM, "form-group has-error");
                        MostrarMensaje("Teléfono móvil inválido", "Su número de teléfono móvil debe ser numérico.", "text-danger");
                        return;
                    }
                    #endregion

                    #region Password
                    if (Pass.Length < 8 || Pass.Length > 35) //Valida largo mínimo
                    {
                        MostrarMensaje("Contraseña Inválida", "Su contraseña debe contener entre 8 y 35 caracteres.", "text-danger");
                        ErrorRegistroPassword();
                        return;
                    }
                    else if (Pass != PassRe) //Valida que la repetición de la contraseña sea correcta
                    {
                        MostrarMensaje("Contraseña no coincide", "La repetición de contraseña es distinta a la ingresada, favor corregir.", "text-danger");
                        ErrorRegistroPassword();
                        return;
                    }
                    #endregion

                    try
                    {
                        if (ViewState["id_registro"] != null && ViewState["razon_social"] != null)
                        {
                            int IdRegistro = Convert.ToInt32(ViewState["id_registro"].ToString());
                            string RazonSocial = ViewState["razon_social"].ToString();
                            int iFonoFijo = Convert.ToInt32(FonoFijo);
                            int iFonoMovil = Convert.ToInt32(FonoMovil);

                            if (ViewState["UsuarioYaRegistrado"] != null) //Si el usuario ya estaba registrado pero eliminado del sistema, entonces sólo actualizar datos de contacto.
                            {
                                //int Resp1 = PortalProveedorBL.SetDatosContactoProv(Run, ViewState["emailAntiguo"].ToString(), iFonoFijo, iFonoMovil); //actualiza datos de contacto (El email, sigue siendo el mismo)
                                
                                Email = ViewState["emailAntiguo"].ToString(); // almacena el email antiguo. Actualmente no es posible cambiar el email.

                                //if(Resp1 == 1) //datos contacto válido
                                //{
                                    int Resp2 = PortalProveedorBL.SetPassword(Run, Pass); //actualiza la password

                                    if (Resp2 == 1)
                                    {
                                        int Resp3 = PortalProveedorBL.SetVigenciaUsuarioPortal(Convert.ToInt32(ViewState["UsuarioYaRegistrado"]), 1);

                                        if (Resp3 == -1) //actualizar vigencia usuario inválido
                                        {
                                            MostrarMensaje("Error temporal 323", "No se pudo realizar el registro. Por favor inténtelo más tarde.", "text-danger");
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        MostrarMensaje("Error temporal 322", "No se pudo realizar el registro. Por favor inténtelo más tarde.", "text-danger");
                                        return;
                                    }
                                //}
                                //else
                                //{
                                //    MostrarMensaje("Error temporal 321", "No se pudo realizar el registro. Por favor inténtelo más tarde.", "text-danger");
                                //    return;
                                //}
                            }
                            
                            //inserta al usuario (si el usuario que se está registrando ya se había registrado antes, entonces sólo actualizará los campos telefono fijo y teléfono móvil) [ lógica negocio está en el procedimiento ]
                            PortalProveedorBL.InsertUserProveedorSolicitada(IdRegistro, Run, Nombre, AP, AM, Email, iFonoFijo, iFonoMovil, Pass);
                            
                            string ParaUsuarioAdministrador = string.Empty;
                            string Motivo = string.Empty;
                            string TipoUsuario = string.Empty;
                            string UrlAcceso = string.Format("{0}/Proveedores.aspx?e={1}", HttpContext.Current.Request.Url.Authority, App_Code.Util.Encriptar(Email));

                            //si es re-asignación de administrador, entonces quita el adm actual y lo elimina sin reportarlo
                            if (ViewState["TipoSolicitud"] != null && ViewState["TipoSolicitud"].ToString() == "10")
                            {
                                PortalProveedorBL.SetReasignarAdmEnRS(IdRegistro);
                                TipoUsuario = "<b>el nuevo usuario administrador</b>";
                                Motivo = string.Format("Usuario administrador registrado en {0}", RazonSocial);
                            }
                            else
                            {
                                Motivo = string.Format("Usuario registrado en {0}", RazonSocial);
                                TipoUsuario = "un nuevo usuario";
                            }

                            Motivo += " - Kaufmann Portal de Proveedores";
                            string Msj = string.Format("{0}, su registro se ha realizado correctamente. Ahora usted es {3} de <b>{1}</b> en el portal de proveedores de Kaufmann.<br><br>Para ingresar al portal, haga click <a href=\"{2}\">en este enlace</a>.<br><br>Le agradecemos su participación en el <b>Portal de proveedores Kaufmann</b>.", Nombre, RazonSocial, UrlAcceso, TipoUsuario);
                            
                            //envia correo electrónico notificando el registro del usuario proveedor
                            WebServices.EnviaCorreo(Email, Motivo, Msj);

                            //entrega mensaje de éxito modificando el formulario
                            titulo.InnerHtml = "<i class='glyphicon glyphicon-ok'></i>&nbsp;&nbsp;Registro exitoso";
                            msj.InnerHtml = string.Format("Su registro se ha realizado correctamente por lo que le enviaremos un correo electrónico a <b>{0}</b> notificando su registro.<br><br>Agradecemos que se haya registrado en el <b>Portal Proveedores de Kaufmann</b> y que sea parte de este sistema.", Email);
                            campos.InnerHtml = string.Format("Para acceder al sistema haga click <a href=\"{0}\">en este enlace</a>.", UrlAcceso);
                            form.Visible = false;
                        }
                    }
                    catch
                    {
                        MostrarMensaje("Error temporal", "Estimado proveedor, hubo un error interno al generar el registro, por favor inténtelo más tarde.", "text-danger");
                    }
                }
                #endregion
            }
        }

        //Es llamado cuando hay algún dato ingresado con error en el txt
        private void SetColorTxT(HtmlGenericControl div, string Estilo)
        {
            div.Attributes["class"] = Estilo;
        }

        private void NormalizaFormulario()
        {
            ColorEstadoTxT(txtRun, dRun);
            ColorEstadoTxT(txtNombre, dNombre);
            ColorEstadoTxT(txtAP, dAP);
            ColorEstadoTxT(txtAM, dAM);
            ColorEstadoTxT(txtTF, dTF);
            ColorEstadoTxT(txtTM, dTM);
            ColorEstadoTxT(txtPass, dPass);
            ColorEstadoTxT(txtPassRe, dPassRe);
        }

        private void ErrorRegistroPassword()
        {
            txtPass.Attributes["value"] = string.Empty;
            txtPassRe.Attributes["value"] = string.Empty;
            SetColorTxT(dPass, "form-group has-error");
            SetColorTxT(dPassRe, "form-group has-error");
        }

        //Valida que los txt no estén vacíos
        private void ColorEstadoTxT(TextBox txt, HtmlGenericControl div)
        {
            if (txt.Text == string.Empty)
                div.Attributes["class"] = "form-group has-error";
            else
                div.Attributes["class"] = "form-group";
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }

        private void MantienePasswordTxT()
        {
            txtPass.Attributes["value"] = txtPass.Text;
            txtPassRe.Attributes["value"] = txtPassRe.Text;
        }

        protected void txtRun_TextChanged(object sender, EventArgs e)
        {
            ValidaRun();
        }

        private void EstadoTextBoxs(string ValorTxtNombre, string ValorTxtAP, string ValorTxtAM, string ValorTxtEmail, string ValorTxtTF, string ValorTxtTM, bool EnabledTxtNombre, bool EnabledTxtAP, bool EnabledTxtAM, bool EnabledTxtEmail)
        {
            //datos personales
            txtNombre.Text = ValorTxtNombre;
            txtAP.Text = ValorTxtAP;
            txtAM.Text = ValorTxtAM;

            txtNombre.Enabled = EnabledTxtNombre;
            txtAP.Enabled = EnabledTxtAP;
            txtAM.Enabled = EnabledTxtAM;

            //datos de contacto
            txtEmail.Text = ValorTxtEmail;
            txtTF.Text = ValorTxtTF;
            txtTM.Text = ValorTxtTM;

            txtEmail.Enabled = EnabledTxtEmail;
        }

        private bool ValidaRun()
        {
            if (!string.IsNullOrEmpty(txtRun.Text))
            {
                if (txtRun.Text.Contains("-"))
                {
                    try
                    {
                        string Run = App_Code.Util.FormatoRut(txtRun.Text.Trim());
                        bool RutValido = App_Code.Util.RutValido(Run);
                        SetColorTxT(dRun, "form-group");
                        txtNombre.Focus();

                        if (RutValido)
                        {
                            UsuarioProveedorEntity UsuarioProveedor = PortalProveedorBL.GetUsuarioProveedor(Run);

                            if (UsuarioProveedor == null)
                            {
                                EstadoTextBoxs(string.Empty, string.Empty, string.Empty, ViewState["email"].ToString(), string.Empty, string.Empty, true, true, true, false);
                                txtNombre.Focus();
                            }
                            else if (UsuarioProveedor.Vigente == 1)
                            {
                                EstadoTextBoxs(string.Empty, string.Empty, string.Empty, ViewState["email"].ToString(), string.Empty, string.Empty, true, true, true, false);

                                MostrarMensaje("Run ya existe", string.Format("El Run \"{0}\" ya existe en nuestro sistema y está siendo utilizado.", Run), "text-danger");
                                txtRun.Focus();
                            }
                            else if (UsuarioProveedor.Vigente == 2)
                            {
                                EstadoTextBoxs(UsuarioProveedor.Nombre, UsuarioProveedor.ApellidoPat, UsuarioProveedor.ApellidoMat, UsuarioProveedor.Email, UsuarioProveedor.Fono.ToString(), UsuarioProveedor.FonoMovil.ToString(), false, false, false, false);

                                //almaceno datos personales por seguridad
                                ViewState.Add("Nombre", UsuarioProveedor.Nombre);
                                ViewState.Add("AP", UsuarioProveedor.ApellidoPat);
                                ViewState.Add("AM", UsuarioProveedor.ApellidoMat);
                                ViewState.Add("emailAntiguo", UsuarioProveedor.Email);

                                if (ViewState["UsuarioYaRegistrado"] == null || Convert.ToInt32(ViewState["UsuarioYaRegistrado"]) != UsuarioProveedor.IdUserProv)
                                {
                                    ViewState.Add("UsuarioYaRegistrado", UsuarioProveedor.IdUserProv); //indico que es un usuario que ya estaba registrado pero almaceno el id del usuario proveedor
                                    MostrarMensaje("Tenga presente que...", string.Format("El Run \"{0}\" en algún momento se registró en este portal, por lo que utilizaremos los datos personales que usted ya había ingresado (nombre, apellido paterno y apellido materno).<br><br>Los datos que puede modificar son: <b>teléfono fijo</b> y <b>teléfono móvil</b>.<br><br><label class=\"label label-danger\">¡Importante!</label>&nbsp;<i class=\"text-danger\">Por el momento, conservaremos su dirección de correo electrónico con la que se registró antiguamente <b>({1})</b>.</i>", Run, UsuarioProveedor.Email), "text-warning");
                                }

                                txtRun.Focus();
                                return true;
                            }

                            if (ViewState["UsuarioYaRegistrado"] != null) //remueve el valor almacenado si cambia el run ingresado
                                ViewState.Remove("UsuarioYaRegistrado");

                            return true;
                        }
                        else
                        {
                            if (ViewState["UsuarioYaRegistrado"] != null) //remueve el valor almacenado si cambia el run ingresado
                                ViewState.Remove("UsuarioYaRegistrado");

                            EstadoTextBoxs(string.Empty, string.Empty, string.Empty, ViewState["email"].ToString(), string.Empty, string.Empty, true, true, true, false);
                            SetColorTxT(dRun, "form-group has-error");
                            MostrarMensaje("Rut inválido", string.Format("Su RUN \"{0}\" es inválido, corríjalo.", Run), "text-danger");
                            txtRun.Focus();
                        }
                    }
                    catch
                    {
                        MostrarMensaje("Error al validar el Run", "Ha ocurrido un error al momento de validar el Run ingresado. Si el error persiste, favor de intentarlo más tarde.", "text-danger");
                    }
                }
                else
                {
                    MostrarMensaje("RUN usuario incorrecto ", "Su RUN lo debe ingresar con un guión.<br><br>Ejemplo: 12345678-K", "text-danger");
                    SetColorTxT(dRun, "form-group has-error");
                }
            }
            else
            {
                MostrarMensaje("Ingrese su RUN", "Debe ingresar su <b>RUN</b> para registrarse en el portal de Proveedores.", "text-danger");
                SetColorTxT(dRun, "form-group has-error");
            }

            return false;
        }
    }
}