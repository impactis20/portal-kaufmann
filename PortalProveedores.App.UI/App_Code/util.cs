﻿using System;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace PortalProveedores.App.UI.App_Code
{
    public class Util
    {
        public static bool RutValido(string Rut)
        {
            string rutLimpio = string.Empty;
            string digitoVerificador = string.Empty;
            int suma = 0;
            int contador = 2;
            bool valida = true;

            rutLimpio = Rut.Replace(".", string.Empty);
            rutLimpio = rutLimpio.Replace("-", string.Empty);
            rutLimpio = rutLimpio.Replace(" ", string.Empty);
            rutLimpio = rutLimpio.Substring(0, (rutLimpio.Length - 1));

            digitoVerificador = Rut.Substring((Rut.Length - 1), 1).ToUpper();

            int i = 0;
            for (i = (rutLimpio.Length - 1); i >= 0; i = (i + -1))
            {
                if ((contador > 7))
                {
                    contador = 2;
                }

                try
                {
                    suma = (suma + (int.Parse(rutLimpio[i].ToString()) * contador));
                    contador++;
                }
                catch (Exception)
                {
                    valida = true;
                }
            }

            if (valida)
            {
                int dv = (11 - (suma % 11));
                string DigVer = dv.ToString();

                if ((DigVer == "10"))
                {
                    DigVer = "K";
                }
                if ((DigVer == "11"))
                {
                    DigVer = "0";
                }
                if ((DigVer == digitoVerificador))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool isValidEmail(string Email)
        {
            bool invalid = false;
            if (string.IsNullOrEmpty(Email))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            try
            {
                Email = Regex.Replace(Email, @"(@)(.+)$", DomainMapper, RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalid)
                return false;

            // Return true if strIn is in valid e-mail format.
            try
            {
                return Regex.IsMatch(Email,
                        @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                        @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                        RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public static bool isIntNumeric(string valor)
        {
            int n;
            return int.TryParse(valor, out n);
        }

        /// <summary>
        /// Retorna un RUT sin puntos, ejemplo: 12345689-K
        /// </summary>
        /// <param name="Rut"></param>
        /// <returns></returns>
        public static string FormatoRut(string Rut)
        {
            Rut = Rut.ToUpper();
            Rut = Rut.Replace(".", string.Empty);
            Rut = Rut.Replace("-", string.Empty);

            Rut = Rut.Insert(Rut.Length - 1, "-");
            return Rut;
        }

        public static string Initcap(string valor)
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            return textInfo.ToTitleCase(valor);
        }

        private static string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            //try
            //{
            domainName = idn.GetAscii(domainName);
            //}
            //catch (ArgumentException)
            //{
            //    invalid = true;
            //}
            return match.Groups[1].Value + domainName;
        }

        /// <summary>
        /// Método para encriptar un texto plano usando el algoritmo (Rijndael). Este es el mas simple posible, muchos de los datos necesarios los definimos como constantes.
        /// </summary>
        /// <param name="textoQueEncriptaremos">texto a encriptar</param>
        /// <returns>Texto encriptado</returns>
        public static string Encriptar(string textoQueEncriptaremos)
        {
            return Encriptar(textoQueEncriptaremos, "_K_A_U_F_M_A_N_N_", "1_Chile_", "MD5", 1, "@1B2c3D4e5F6g7H8", 128);
        }
        /// <summary>
        /// Método para encriptar un texto plano usando el algoritmo (Rijndael) x
        /// </summary>
        /// <returns>Texto encriptado</returns>
        private static string Encriptar(string textoQueEncriptaremos, string passBase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize)
        {
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(textoQueEncriptaremos);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passBase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged()
            {
                Mode = CipherMode.CBC
            };
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            string cipherText = Convert.ToBase64String(cipherTextBytes);
            return cipherText;
        }

        /// <summary>
        /// Método para desencriptar un texto encriptado.
        /// </summary>
        /// <returns>Texto desencriptado</returns>
        public static string Desencriptar(string textoEncriptado)
        {
            return Desencriptar(textoEncriptado, "_K_A_U_F_M_A_N_N_", "1_Chile_", "MD5", 1, "@1B2c3D4e5F6g7H8", 128);
        }

        /// <summary>
        /// Método para desencriptar un texto encriptado (Rijndael)
        /// </summary>
        /// <returns>Texto desencriptado</returns>
        private static string Desencriptar(string textoEncriptado, string passBase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize)
        {
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] cipherTextBytes = Convert.FromBase64String(textoEncriptado.Replace(" ", "+"));
            PasswordDeriveBytes password = new PasswordDeriveBytes(passBase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged()
            {
                Mode = CipherMode.CBC
            };

            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            string plainText = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
            return plainText;
        }

    }
}