﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO; 

namespace PortalProveedores.App.UI
{
    public partial class img : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["fn"] != null)
            {
                try
                {
                    string NomnbreArchivo = Request.QueryString["fn"];
                    // Read the file and convert it to Byte Array
                    string filePath = Server.MapPath("~/Ls/");
                    string contenttype = "image/" + Path.GetExtension(NomnbreArchivo.Replace(".", ""));
                    FileStream fs = new FileStream(filePath + NomnbreArchivo,
                    FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    br.Close();
                    fs.Close();

                    //Write the file to response Stream
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = contenttype;
                    Response.AddHeader("content-disposition", "attachment;filename=" + NomnbreArchivo);
                    Response.BinaryWrite(bytes);
                    Response.Flush();
                    Response.End();
                }
                catch
                {
                }
            }
        }
    }
}