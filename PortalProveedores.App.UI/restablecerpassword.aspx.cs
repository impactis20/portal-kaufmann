﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI
{
    public partial class restablecerpassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    string ValoresEncriptados = Request.QueryString["v"];
                    string ValoresDesencriptados = App_Code.Util.Desencriptar(ValoresEncriptados);

                    string[] Separador = { "&" };
                    string[] Valores = ValoresDesencriptados.Split(Separador, StringSplitOptions.RemoveEmptyEntries);

                    int IdCambioPass = Convert.ToInt32(Valores[0].Split('=')[1]);
                    string RunUserProv = Valores[1].Split('=')[1];
                    DateTime FechaMax = Convert.ToDateTime(Valores[2].Split('=')[1]);

                    //Valido si el link ya fue utilizado
                    // p_resp:
                    //       1: ya fue utilizado
                    //       0: aún no ha generado cambio de password
                    if (PortalProveedorBL.ValidaLinkPass(IdCambioPass) == 0)
                    {
                        //Valida que el enlace esté dentro del tiempo (1 hora) al momento de abrirlo
                        if (DateTime.Now <= FechaMax)
                        {
                            //Almaceno los valores de la URL en el viewstate de la página
                            ViewState.Add("runprov", RunUserProv);
                            ViewState.Add("fechamax", FechaMax);
                            ViewState.Add("idcambiopass", IdCambioPass);

                            UsuarioProveedorEntity UserProv = new UsuarioProveedorEntity();
                            UserProv = PortalProveedorBL.GetUsuarioProveedor(RunUserProv);

                            msj.InnerHtml = string.Format("<b>{0} {1} {2}</b>, restablesca su contraseña en el formulario de abajo, ingresando una nueva contraseña que contenga entre 8 y 35 caracteres. Tenga presente que una contraseña segura evita el acceso no autorizado de su cuenta en esta aplicación Web.<br>", UserProv.Nombre, UserProv.ApellidoPat, UserProv.ApellidoMat);
                        }
                        else
                        {
                            GetPaginaInvalida("Lo sentimos, este enlace dejó de ser válido y ha expirado. Si necesita restablecer su password, hágalo <a href='Proveedores.aspx#olvidemipassword'>aquí</a>.");
                        }
                    }
                    else
                    {
                        GetPaginaInvalida("Lo sentimos, esté enlace ya fue utilizado y dejó de ser válido. Si necesita restablecer su password, hágalo <a href='Proveedores.aspx#olvidemipassword'>aquí</a>.");
                    }
                }
                catch
                {
                    GetPaginaInvalida("Lo sentimos, pero esta página es incorrecta o ha generado algún error.");
                }
            }
        }

        protected void lnkRestablecerPass_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime FechaMax = Convert.ToDateTime(ViewState["fechamax"]);

                if (DateTime.Now <= FechaMax)//Compruebo de que esté dentro del rango de hora permitido
                {
                    string Password = txtNuevaPass.Text;
                    string ConfirmaPass = txtConfirmaNuevaPass.Text;

                    bool EstadoTxtPass = string.IsNullOrEmpty(Password);
                    bool EstadoTxtConfirmPass = string.IsNullOrEmpty(ConfirmaPass);

                    if (EstadoTxtPass || EstadoTxtConfirmPass)
                    {
                        MostrarMensaje("Datos incompletos", "Estimado proveedor, ambos campos son obligatorios.", "text-danger");
                        return;
                    }
                    else
                    {
                        if (Password.Length < 8 || Password.Length > 35) //Valida largo mínimo
                        {
                            MostrarMensaje("Contraseña Inválida", "Su contraseña debe contener entre 8 y 35 caracteres.", "text-danger");
                            return;
                        }

                        if (Password != ConfirmaPass)
                        {
                            MostrarMensaje("Password no coinciden", "Vuelva a ingresar su contraseña y repítala correctamente.", "text-danger");
                            return;
                        }

                        if (ViewState["runprov"] != null && ViewState["idcambiopass"] != null)
                        {
                            string RunUserProv = ViewState["runprov"].ToString();
                            int IdCambioPass = Convert.ToInt32(ViewState["idcambiopass"]);

                            //Almaceno el registro de actualización de contraseña (log)
                            PortalProveedorBL.SetRegistroCambioPass(IdCambioPass, DateTime.Now);

                            //Actualiza la contraseña
                            int RespuestaRestablecer = PortalProveedorBL.SetPassword(RunUserProv, Password);

                            switch (RespuestaRestablecer)
                            {
                                case 1:
                                    titulo.InnerHtml = "<i class='glyphicon glyphicon-ok'></i>&nbsp;&nbsp;¡Contraseña actualizada!";
                                    msj.InnerHtml = "La contraseña fue <b>actualizada correctamente</b>. Ahora ingrese al portal web con su nueva contraseña.";
                                    msj.Attributes["class"] = "text-success";
                                    form.Visible = false;
                                    return;

                                case 2: //Error base de datos
                                    ErrorEnPagina();
                                    return;

                                case -1: //Error al invocar el package
                                    ErrorEnPagina();
                                    return;
                            }
                        }
                        else
                        {
                            MostrarMensaje("Error temporal (311)", "Ha ocurrido un error interno en el sistema, por favor inténtelo más tarde.", "text-danger");
                            return;
                        }
                    }
                }
                else
                {
                    GetPaginaInvalida("Lo sentimos, este enlace dejó de ser válido y ha expirado. Si necesita restablecer su password, hágalo <a href='Proveedores.aspx#olvidemipassword'>aquí</a>.");
                }
            }
            catch
            {
                MostrarMensaje("Error temporal (310)", "Ha ocurrido un error interno en el sistema, por favor inténtelo más tarde.", "text-danger");
            }
        }

        private void LimpiaFormulario()
        {
            txtConfirmaNuevaPass.Text = string.Empty;
            txtNuevaPass.Text = string.Empty;
        }

        private void ErrorEnPagina()
        {
            titulo.InnerHtml = "<i class='glyphicon glyphicon-remove'></i>&nbsp;&nbsp;Error temporal (311)";
            msj.InnerText = "Ha ocurrido un error interno al momento de actualizar su contraseña, por favor inténtelo más tarde.";
            msj.Attributes["class"] = "text-error";
            form.Visible = false;
        }

        private void GetPaginaInvalida(string Msj)
        {
            titulo.InnerHtml = "<i class='glyphicon glyphicon-remove'></i>&nbsp;&nbsp;Página inválida o incorrecta";
            msj.InnerHtml = Msj;
            form.Visible = false;
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }
    }
}