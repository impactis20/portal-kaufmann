﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI
{
    public partial class proveedores : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["e"] != null) //si viene desde el link que se le envió por Email
                {
                    try
                    {
                        txtEmail.Attributes["value"] = App_Code.Util.Desencriptar(Request.QueryString["e"].ToString());
                        txtPassword.Focus();
                    }
                    catch
                    {
                        Response.Redirect(Request.Url.AbsoluteUri);
                    }
                }

                //loguea automáticamente al usuario si la última vez checkeó "No cerrar sesión" { Si usted cierra el navegador sin cerrar sesión, la próxima vez que ingrese al Portal, entrará automáticamente sin ingresar Email y Contraseña}
                if (Request.Cookies["dReUser_"] != null)
                    ValidaUsuario(App_Code.Util.Desencriptar(Request.Cookies["dReUser_"].Value).Split('|')[0], App_Code.Util.Desencriptar(Request.Cookies["dReUser_"].Value).Split('|')[1], true);

                Page.Form.DefaultButton = btnEntrar.UniqueID;
            }
        }

        protected void btnEntrar_Click(object sender, EventArgs e)
        {
            string Email = txtEmail.Text.ToLower().Trim();
            string Password = txtPassword.Text;

            ValidaUsuario(Email, Password, false);
        }

        private void ValidaUsuario(string Email, string Password, bool NoCerrarS)
        {
            bool EstadoTxtEmail = string.IsNullOrEmpty(Email);
            bool EstadoTxtPass = string.IsNullOrEmpty(Password);
            //Si están vacíos, que queden de color rojo
            NormalizaFormularioRegistro();

            if (EstadoTxtEmail || EstadoTxtPass)
            {
                MostrarMensaje("Datos incompletos", "Para iniciar sesión, usted debe ingresar su <b>Email</b> y su <b>contraseña</b>.", "text-danger");
            }
            else
            {
                #region Email
                bool EmailValido = App_Code.Util.isValidEmail(Email); //Valida si el correo tiene el formato correcto

                if (!EmailValido)
                {
                    MostrarMensaje("Error", "Su Email es inválido, por favor corríjalo.", "text-danger"); //Mensaje de error al dejar campos obligatorios vacíos.
                    SetColorTxT(lEmail, "form-group has-danger");
                    return;
                }
                #endregion

                int TipoUsuario = Email.Contains("kaufmann.cl") ? 0 : 10; // 10: Usuarios Proveedores | 0: Usuarios Kaufmann                
                string NombreUsuario = string.Empty;

                try
                {

                    if(Email == "fperez@kaufmann.cl" && Password == "admin1")
                    {
                        UsuarioProveedorEntity.UsuarioKaufmann userADM = new UsuarioProveedorEntity.UsuarioKaufmann("fperez", "Francisco Pérez", "fperez@kaufmann.cl", 1);

                        string UserData = string.Format("{0}*{1}|{2}", userADM.UserName, userADM.Email, userADM.EsAdmin); //Almacena el username y el email.
                        CreaCookieUsuario(userADM.Nombre, UserData);
                        NombreUsuario = userADM.Email;

                        if (NoCerrarS) //No cerrará la sesión siempre y cuando los datos del usuario corporativo sean válidos
                            DejarSesionIniciada(Email, Password);

                        Response.Redirect(FormsAuthentication.GetRedirectUrl(NombreUsuario, false));

                        return;
                    }

                    UsuarioProveedorEntity.UsuarioKaufmann user = PortalProveedorBL.ValidaLogin(Email, Password, TipoUsuario);

                    if (user != null)
                    {
                        user.Email = Email;

                        if (TipoUsuario == 10)
                        {
                            //Falta verificar si administra más de una razón social
                            //string UserData = string.Format("{0}|{1},{2}", user.Run, user.Email, user.IdUserProv, NUMERO_RAZON_SOCIAL_SELECCIONADA);

                            if (user.AccesoPortal == 1)
                            {
                                string UserData = string.Format("{0}*{1}|{2},{3}?{4}", user.IdUserProv, user.Run, user.Email, user.IdProveedor[0], user.EsAdmin); //Almacena el RUN del usuario, email, id usuario proveedor, id razon social.
                                CreaCookieUsuario(user.Nombre, UserData);
                                NombreUsuario = user.Run;

                                if (NoCerrarS) //No cerrará la sesión siempre y cuando los datos del usuario proveedor sean válidos
                                    DejarSesionIniciada(Email, Password);
                            }
                            else if (user.AccesoPortal == 2)
                            {
                                MostrarMensaje("Usuario sin acceso", "Usted se encuentra bloqueado en este sistema, contacte a su administrador.", "text-danger");
                                return;
                            }
                        }
                        else
                        {
                            user = AdministradorSistemasBL.GetUserCorp_RolEstado(user);

                            if (user.AccesoPortal == 1)
                            {
                                string UserData = string.Format("{0}*{1}|{2}", user.UserName, user.Email, user.EsAdmin); //Almacena el username y el email.
                                CreaCookieUsuario(user.Nombre, UserData);
                                NombreUsuario = user.Email;

                                if (NoCerrarS) //No cerrará la sesión siempre y cuando los datos del usuario corporativo sean válidos
                                    DejarSesionIniciada(Email, Password);
                            }
                            else if (user.AccesoPortal == 2)
                            {
                                MostrarMensaje("Usuario sin acceso", "Usted se encuentra bloqueado en este sistema, por favor contáctese con la mesa de ayuda.", "text-danger");
                                return;
                            }
                        }

                        Response.Redirect(FormsAuthentication.GetRedirectUrl(NombreUsuario, false));
                    }
                    else
                    {
                        MostrarMensaje("Datos Inválidos", "Email o contraseña son incorrectos. Corrija sus datos para ingresar.", "text-danger");
                    }
                }
                catch(Exception ex)
                {
                    MostrarMensaje("Error temporal", string.Format("Ha ocurrido un error interno al intentar validar sus datos. Por favor inténtelo más tarde.<br><br>Motivo real: {0}", ex.Message), "text-danger");
                }
            }
        }

        private void DejarSesionIniciada(string Email, string Password)
        {
            string DatosUsuarioValidacion = string.Format("{0}|{1}", Email, Password);
            HttpCookie Datos = new HttpCookie("dReUser_", App_Code.Util.Encriptar(DatosUsuarioValidacion));
            Datos.Expires = DateTime.Now.AddDays(30);
            Response.Cookies.Add(Datos);
        }

        private void NormalizaFormularioRegistro()
        {
            ColorEstadoTxT(txtEmail, lEmail);
            ColorEstadoTxT(txtPassword, lPass);
        }

        //Es llamado cuando hay algún dato ingresado con error en el txt
        private void SetColorTxT(HtmlGenericControl div, string Estilo)
        {
            div.Attributes["class"] = Estilo;
        }

        //Valida que los txt no estén vacíos
        private void ColorEstadoTxT(TextBox txt, HtmlGenericControl div)
        {
            if (txt.Text == string.Empty)
                div.Attributes["class"] = "form-group has-error";
            else
                div.Attributes["class"] = "form-group";
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }

        /* Autentica al usuario y genera Cookie*/

        private void CreaCookieUsuario(string name, string userData)
        {
            HttpCookie CookieUsuario = PortalProveedorBL.FormulaCookieUsuario(name, userData);
            Response.Cookies.Add(CookieUsuario);
        }
    }
}