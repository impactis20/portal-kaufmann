﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private
{
	public partial class index : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
        {   
            int TipoUsuario = PortalProveedorBL.GetTipoUsuarioLogueado();
            
            if(TipoUsuario == 10) //si es proveedor
            {
                UsuarioProveedorEntity user = PortalProveedorBL.GetUsuarioProveedorLogueado();

                //si es un usuario proveedor, se le listarán los sistemas a los que su administrador tiene permiso de acceso
                if(user.EsAdmin == 1)
                {
                    //int Permiso = PortalProveedorBL.GetSistemaAcceso(user.IdUserProv); //está preguntando por el sistema 1 (estado de pago)
                    //ViewState.Add("id_user_prov", user.IdUserProv);

                    //switch(Permiso)
                    //{
                    //    case 0: //En espera de la solicitud                            
                    //        accS.Visible = false;
                    //        envA.CssClass = "btn btn-default btn-lg btn-block disabled";
                    //        envA.Text = "Solicitud enviada...";
                    //        envA.Visible = true;
                    //        break;

                    //    case 1: //Sólo puede acceder (solicitud aceptada)
                    //        accS.Visible = true;
                    //        accS.Click += accS_Click;
                    //        envA.Visible = false;
                    //        break;

                    //    case 2: //Puede enviar solicitud
                    //        accS.Visible = false;
                    //        envA.Visible = true; 
                    //        envA.Click += envA_Click;
                    //        break;
                    //}

                    accS.Click += accS_Click;
                }
                else
                {
                    //todos los usuarios tienen acceso al sistema Estado Pago Proveedores
                    //if (PortalProveedorBL.GetSistemasParaUsersProv(user.Run).Count > 0)
                    //{
                    //    accS.Visible = true;
                        accS.Click += accS_Click;
                    //}
                }                
            }
            else if(TipoUsuario == 0) //si es usuario corporativo
            {
                //debiera mostrar un cuadro con proveedores para que pueda seleccionar a quien quiere consultar
                accS.Visible = true;
                accS.Click += accS_Click;
            }
        }

        protected void accS_Click(object sender, EventArgs e)
        {
            Response.Redirect("sistema/estadopagos/EstadoPagos.aspx", true);
            //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "myUniqueKey", "self.parent.location='principal.aspx?val=sistema/ep/estadopago.aspx';", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("parent.processAjaxData('{0}', '{1}');", this, "www.buenabuena.com"), true);
        }

        protected void envA_Click(object sender, EventArgs e)
        {
            PortalProveedorBL.InsertSolicitudAccesoSistema(1, Convert.ToInt32(ViewState["id_user_prov"].ToString()));
            Response.Redirect("index.aspx");
        }
	}
}