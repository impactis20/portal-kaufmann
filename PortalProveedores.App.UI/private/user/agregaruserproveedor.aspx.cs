﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private.user
{
	public partial class agregaruserproveedor : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ip"] != null) //si recibe un querystring ip: se desea agregar un usuario proveedor a una razon social
                {
                    CargaDatosInicial(Request.QueryString["ip"].ToString());
                }
                else if (Request.QueryString["changeAdm"] != null) //si recibe un querystring changeAdm: se desea agregar un usuario proveedor administrador a una razon social
                {
                    CargaDatosInicial(Request.QueryString["changeAdm"].ToString());
                    tituloMod.InnerText = "Re-asignar administrador";

                    ViewState.Add("changeAdm", "changeAdm");
                }
            }
        }

        private void CargaDatosInicial(string IdProveedor)
        {
            int IdProv = Convert.ToInt32(App_Code.Util.Desencriptar(IdProveedor));
            ViewState.Add("ip", IdProv);

            ProveedorEntity prov = PortalProveedorBL.GetProveedor(IdProv);
            rs.InnerText = prov.RazonSocial;
            ViewState.Add("razonsocial", prov.RazonSocial);
        }

        protected void btnEn_Click(object sender, EventArgs e)
        {
            string Email = txtEmail.Text.Trim();

            if (!string.IsNullOrEmpty(Email))
            {
                if (PortalProveedorBL.GetTipoUsuarioLogueado() == 10) //sólamente el usuario administrador de la razón social puede enviar una invitación a otros usuarios a unirse al portal
                {
                    EnviaCorreo(Email, false);
                }
                else if (PortalProveedorBL.GetTipoUsuarioLogueado() == 0 && ViewState["changeAdm"] != null)
                {
                    EnviaCorreo(Email, true);
                }
                else
                {
                    MostrarMensaje("Acción no autorizada", string.Format("Usted no puede enviar a los usuarios solicitudes de registro. El administrador de esta razón social es quien debe realizar esta acción.", Email), "text-danger");
                }
            }
            else
            {
                MostrarMensaje("Envío inválido", "El campo <b>email</b> está vacío, por favor ingrese el email del usuario a registrar.", "text-danger");
            }
        }

        private void EnviaCorreo(string Email, bool ReasignarADM)
        {
            //Verifica si el Email del usuario ya existe registrado en el sistema
            if (PortalProveedorBL.VerificaSiEmailExiste(Email) == 0)
            {
                //Inserta solicitud de registro en tabla "registro_usuarios_proveedores"
                int Responsable =  PortalProveedorBL.GetTipoUsuarioLogueado() == 10 ? PortalProveedorBL.GetUsuarioProveedorLogueado().IdUserProv : 0;

                int IdRegistro = PortalProveedorBL.InsertSolicitudRegUsuario(Convert.ToInt32(ViewState["ip"].ToString()), Responsable);

                if (IdRegistro == -1)
                {
                    MostrarMensaje("Error de envío", string.Format("Ha ocurrido un error interno al intentar enviar el correo electrónico <b>\"{0}\"</b>, por favor vuelva a intentarlo, de lo contrario inténtelo más tarde.", Email), "text-success");
                    return;
                }

                //envía email
                string Asunto = string.Empty;
                string Mensaje = string.Empty;
                string ParametrosUrl = string.Format("id_reg={0}&max_fecha_vigencia={1}&e={2}&reasignar_admin={3}", IdRegistro, DateTime.Now.AddHours(24), Email, ReasignarADM.ToString());

                string EncriptaValoresUrl = App_Code.Util.Encriptar(ParametrosUrl);
                string UrlRegistroUsuario = string.Format("{0}/RegistroUsuario.aspx?v={1}", HttpContext.Current.Request.Url.Authority, EncriptaValoresUrl);

                if(ReasignarADM)
                {
                    Asunto = string.Format("Regístrese como usuario administrador de {0} - Kaufmann Portal de Proveedores", ViewState["razonsocial"].ToString());
                    Mensaje = string.Format("El administrador del portal de Proveedores de {1}, le está solicitando que se registre como <b>usuario administrador</b> de la razón social <b>{0}</b>.<br><br>Para ello, haga click <a href=\"{2}\">en este enlace</a> (El link de validación tiene una vigencia de 24 hrs. Si esta invitación expira, vuelva a solicitarlo).<br><br>Este mensaje fue enviado automáticamente desde el portal Web de proveedores de {1}.<br><br><b style=\"color:red\">IMPORTANTE: Si usted desconoce la procedencia de este correo electrónico, favor de ignorar, gracias.</b>", ViewState["razonsocial"].ToString(), "Kaufmann", UrlRegistroUsuario);
                }
                else
                {
                    Asunto = string.Format("Regístrese como usuario de {0} - Kaufmann Portal de Proveedores", ViewState["razonsocial"].ToString());
                    Mensaje = string.Format("El proveedor <b>{0}</b> quiere agregarlo a su cuenta como usuario en el portal de proveedores de <b>{1}</b>.<br><br>Para registrarse como usuario de <b>{0}</b> en el portal, haga click <a href=\"{2}\">en este enlace</a> (El link de validación tiene una vigencia de 24 hrs. Si esta invitación expira, vuelva a solicitarlo).<br><br>Este mensaje fue enviado automáticamente desde el portal de proveedores de {1}, por el administrador de <b>{0}</b>.<br><br><b style=\"color:red\">IMPORTANTE: Si usted desconoce la procedencia de este correo electrónico, favor de ignorar, gracias.</b>", ViewState["razonsocial"].ToString(), "Kaufmann", UrlRegistroUsuario);
                }

                bool Respuesta = WebServices.EnviaCorreo(Email, Asunto, Mensaje);

                switch (Respuesta)
                {
                    case true:
                        MostrarMensaje("¡Email enviado!", string.Format("El email ha sido enviado correctamente a <b>{0}</b>.<br><br>Ahora sólo queda que el usuario se valide mediante el correo electrónico que le acabamos de enviar, para que posteriormente ingrese sus datos y se registre como usuario de <b>{1}</b>.<br><br><label class=\"label label-info\">¡Importante!</label>&nbsp;<i class=\"text-info\">El <b>link de validación</b> tiene una vigencia de 24 hrs. Si esta invitación expira, vuelva a realizar esta acción.</i>", Email, ViewState["razonsocial"].ToString()), "text-success");
                        txtEmail.Text = string.Empty;
                        break;

                    case false:
                        MostrarMensaje("Error de envío", string.Format("Ha ocurrido un error interno al intentar enviar el correo electrónico <b>\"{0}\"</b>, por favor vuelva a intentarlo, de lo contrario inténtelo más tarde.", Email), "text-danger");
                        break;
                }
            }
            else
            {
                MostrarMensaje("Email inválido", string.Format("La acción no se pudo realizar porque el email <b>{0}</b> ya está registrado en este sistema.", Email), "text-danger");
                return;
            }
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("parent.abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }
	}
}