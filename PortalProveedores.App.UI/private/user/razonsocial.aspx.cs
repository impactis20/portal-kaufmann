﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private.user
{
	public partial class razonsocial : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int TipoUsuario = PortalProveedorBL.GetTipoUsuarioLogueado();

                if (Request.Cookies["chrol"] != null && Request.Cookies["chrol"].Value.Split('#')[0] == "ye")
                {
                    string ValorCookie = Request.Cookies["chrol"].Value;
                    int IdRS = Convert.ToInt32(ValorCookie.Split('*')[1].Split('?')[1]);

                    string Msj = string.Format("El administrador ha sido cambiado correctamente para <b>{0}</b>.<br><br>El nuevo usuario administrador <b>{1}</b> reflejará el cambio la próxima vez que inicie sesión.", PortalProveedorBL.GetProveedor(IdRS).RazonSocial, ValorCookie.Split('*')[1].Split('?')[0]);
                    MostrarMensaje("Administrador cambiado", Msj, "text-success");
                    Request.Cookies["chrol"].Expires = DateTime.Now;
                    Response.Cookies.Add(Request.Cookies["chrol"]);
                }

                if (TipoUsuario == 10)
                {
                    int IdProv = PortalProveedorBL.GetMiRazonSocial(PortalProveedorBL.GetUsuarioProveedorLogueado().Run);
                    ViewState.Add("_ip", IdProv);
                    GetProveedor(IdProv);

                    if (PortalProveedorBL.GetUsuarioProveedorLogueado().EsAdmin != 1) //Si es proveedor y NO es prov admin
                        msj.InnerHtml = "Este módulo obtiene los datos del proveedor, el administrador del proveedor y sus usuarios relacionados a éste mismo.";
                }
                else if (TipoUsuario == 0 && Request.QueryString["ip"] != null) //usuarios administradores corporativos
                {
                    int IdProv = Convert.ToInt32(Request.QueryString["ip"].ToString());
                    ViewState.Add("_ip", IdProv);
                    GetProveedor(IdProv);
                }
                else
                {
                    msj.InnerHtml = "<b>Error</b> al ingresar a esta página.";
                    formulario.Visible = false;
                }
            }
        }

        //Obtiene todo, datos empresa, administrador y usuarios.
        private void GetProveedor(int IdPRov)
        {
            //int IdPRov = Convert.ToInt32(ViewState["_ip"].ToString());

            ProveedorEntity proveedor = PortalProveedorBL.GetProveedor(IdPRov);

            //Obtiene los datos de la empresa
            txtRut.Text = proveedor.Rut;
            txtRS.Text = proveedor.RazonSocial;



            //Obtiene sólamente al usuario administrador (posiblemente administradores)
            List<UsuarioProveedorEntity> listAdm = PortalProveedorBL.GetAdmRS(IdPRov);
            gvAdm.DataSource = listAdm;
            gvAdm.DataBind();

            if (listAdm.Count > 0)
                btnReAsignarADM.Visible = false;
            else
            {
                btnReAsignarADM.Visible = true;
                btnReAsignarADM.Text = string.Format("Re-asignar administrador de {0}", proveedor.RazonSocial);
            }



            //Obtiene los usuarios
            List<UsuarioProveedorEntity> listUsers = PortalProveedorBL.GetUsersRS(IdPRov);
            //De la lista de usuarios, quito al(los) adminitrador(es).
            listUsers = (from item in listUsers
                         where item.EsAdmin == 2 // 1: administrador | 2: usuarios
                         select item).ToList();

            if (listUsers.Count == 0)
                dUsers.InnerHtml = string.Format("<div class=\"alert alert-danger\" role=\"alert\">El proveedor <strong>{0}</strong> <u>no tiene</u> usuarios asociados a su cuenta. Si desea agregar un usuario haga click <a href=\"agregaruserproveedor.aspx?ip={1}\">aquí</a>.</div>", proveedor.RazonSocial, App_Code.Util.Encriptar(proveedor.IdProv.ToString()));
            else
            {
                gvUsers.DataSource = listUsers;
                gvUsers.DataBind();
            }
        }

        protected void gvAdm_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                UsuarioProveedorEntity prov = (UsuarioProveedorEntity)r.DataItem;

                HtmlGenericControl lblEstadoUsuario = ((HtmlGenericControl)r.Controls[0].FindControl("lblEstadoAdm"));

                LinkButton btnDesbloquearUsuario = ((LinkButton)r.Controls[0].FindControl("btnDesbloquearAdm"));
                LinkButton btnBloquearUsuario = ((LinkButton)r.Controls[0].FindControl("btnBloquearAdm"));

                EstadoUsuario(lblEstadoUsuario, btnBloquearUsuario, btnDesbloquearUsuario, prov.AccesoPortal, prov.Nombre);

                //verifica tipo de usuario para asignar funcionalidad "re-asignar administrador"
                if(PortalProveedorBL.GetTipoUsuarioLogueado() == 10)
                    ((HtmlGenericControl)r.Controls[0].FindControl("op_adm")).Visible = false;
            }

            if(r.RowType == DataControlRowType.Footer)
            {
                if (PortalProveedorBL.GetTipoUsuarioLogueado() == 0 && PortalProveedorBL.GetUsuarioCorporativoLogueado().EsAdmin == 1)
                    gvAdm.ShowFooter = true;
            }
        }

        protected void gvAdm_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.CommandName)) 
            {
                int IdUserProvAdm = Convert.ToInt16(e.CommandArgument.ToString().Split('|')[0]);
                string NombreCompletoUsuarioAdm = e.CommandArgument.ToString().Split('|')[1];

                switch(e.CommandName)
                {
                    case "Bloq":
                        SetPermisoUsuario(IdUserProvAdm, 2, string.Format("El usuario administrador <b>{0}</b> ha sido <u>bloqueado</u>, por lo que no podrá acceder al portal Web.", NombreCompletoUsuarioAdm));
                        break;

                    case "Desbloq":
                        SetPermisoUsuario(IdUserProvAdm, 1, string.Format("El usuario administrador <b>{0}</b> ha sido <u>desbloqueado</u>, por lo que se encuentra en condiciones de acceder al portal Web.", NombreCompletoUsuarioAdm));
                        break;

                    case "Re-Asign":
                        AgregaNuevoUsuarioAdmRS();
                        break;

                    case "DelUser":
                        DelUser(IdUserProvAdm, NombreCompletoUsuarioAdm);
                        break;
                }

                //Refresca todo
                GetProveedor(Convert.ToInt32(ViewState["_ip"].ToString()));
            }
        }

        private void AgregaNuevoUsuarioAdmRS()
        {
            string IdRazonSocial = App_Code.Util.Encriptar(ViewState["_ip"].ToString());
            Response.Redirect("agregaruserproveedor.aspx?changeAdm=" + IdRazonSocial);
        }

        protected void btnReAsignarADM_Click(object sender, EventArgs e)
        {
            AgregaNuevoUsuarioAdmRS();
        }

        protected void gvUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int IdUserProv = Convert.ToInt16(e.CommandArgument.ToString().Split('|')[0]);
            string Datos = e.CommandArgument.ToString().Split('|')[1];

            switch (e.CommandName)
            {
                case "Bloq":
                    SetPermisoUsuario(IdUserProv, 2, string.Format("El usuario <b>{0}</b> ha sido <u>bloqueado</u>, por lo que no podrá acceder al portal Web.", Datos));
                    break;

                case "Desbloq":
                    SetPermisoUsuario(IdUserProv, 1, string.Format("El usuario <b>{0}</b> ha sido <u>desbloqueado</u>, por lo que se encuentra en condiciones de acceder al portal Web.", Datos));
                    break;

                case "EsAdmin":
                    int IdRazonSocial = Convert.ToInt16(ViewState["_ip"].ToString());

                    int Respuesta = AdministradorSistemasBL.SetCambiarAdmRS(IdUserProv, IdRazonSocial);
                    

                    switch(Respuesta)
                    {
                        case 1:
                            //exitoso

                            //si es usuario proveedor, actualizar página padre para refrescar su rol de "usuario proveedor"
                            if(PortalProveedorBL.GetTipoUsuarioLogueado() == 10)
                            {
                                //quita la cookie actual
                                FormsAuthentication.SignOut(); 

                                //genera nueva cookie como usuario proveedor 
                                UsuarioProveedorEntity UserProv = PortalProveedorBL.GetUsuarioProveedorLogueado();
                                string UserData = string.Format("{0}*{1}|{2},{3}?{4}", UserProv.IdUserProv, UserProv.Run, UserProv.Email, UserProv.IdProveedor[0], 2); //2: No es admin (deja de serlo) | 1: es admin
                                HttpCookie CookieUsuario = PortalProveedorBL.FormulaCookieUsuario(UserProv.Nombre, UserData);
                                Response.Cookies.Add(CookieUsuario);
                                
                                //se crea cookie para desplegar mensaje de cambio de usuario adminstrador exitoso
                                HttpCookie CambioRol = new HttpCookie("chrol", "ye#" + Datos + "?" + IdRazonSocial);
                                Response.Cookies.Add(CambioRol);

                                ClientScript.RegisterStartupScript(GetType(), "Load", "<script type='text/javascript'>window.parent.location.href = '../principal.aspx?val=user/razonsocial.aspx'; </script>");
                            }
                            else //si es usuario kaufmann solo debe refrescar esta página (razonsocial) para que se aprecie el cambio de administrador en la razón social
                            {
                                Response.Redirect(Request.Url.PathAndQuery);
                            }

                            break;

                        case -1:
                            string Msj = "El usuario no se pudo dejar como administrador debido a un error interno. Favor de intentarlo más tarde.";
                            MostrarMensaje("Error temporal 287", Msj, "text-danger");
                            break;
                    }

                    break;

                case "DelUser":
                    DelUser(IdUserProv, Datos.Split('*')[0]);
                    break;
            }

            //Refresca todo
            GetProveedor(Convert.ToInt32(ViewState["_ip"].ToString()));
        }

        private void DelUser(int IdUserProv, string NombreUsuario)
        {
            int Resp = PortalProveedorBL.SetVigenciaUsuarioPortal(IdUserProv, 2);

            switch (Resp)
            {
                case 1:
                    string Msj = string.Format("El usuario <b>{0}</b> ha sido eliminado de la razón social y del portal.<br><br><label class=\"label label-info\">Para su información</label>&nbsp;usted puede volver a solicitar el registro a un usuario eliminado.", NombreUsuario);
                    MostrarMensaje("Usuario eliminado", Msj, "text-success");
                    break;

                case -1:
                    MostrarMensaje("Error temporal 288", "El usuario no se pudo eliminar debido a un error interno. Favor de intentarlo más tarde.", "text-danger");
                    break;
            }
        }

        private void SetPermisoUsuario(int IdUserProv, int Decision, string Mensaje)
        {
            AdministradorSistemasBL.SetUserProvPortal(IdUserProv, Decision);
            MostrarMensaje("¡Acción realizada!", Mensaje, "text-success");
        }

        protected void gvUsers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                UsuarioProveedorEntity prov = (UsuarioProveedorEntity)r.DataItem;
                
                HtmlGenericControl lblEstadoUsuario = ((HtmlGenericControl)r.Controls[0].FindControl("lblEstadoUsuario"));

                LinkButton btnDesbloquearUsuario = ((LinkButton)r.Controls[0].FindControl("btnDesbloquearUsuario"));
                LinkButton btnBloquearUsuario = ((LinkButton)r.Controls[0].FindControl("btnBloquearUsuario"));

                EstadoUsuario(lblEstadoUsuario, btnBloquearUsuario, btnDesbloquearUsuario, prov.AccesoPortal, prov.Nombre);

                //Si es usuario proveedor y no es admin, no mostrar botón de bloqueo
                if (PortalProveedorBL.GetTipoUsuarioLogueado() == 10 && PortalProveedorBL.GetUsuarioProveedorLogueado().EsAdmin != 1)
                    ((HtmlGenericControl)r.Controls[0].FindControl("op_users")).Visible = false;

                if (prov.AccesoPortal == 2) //si usuario proveedor no tiene acceso al portal, bloquear la opción "dejar como admin"
                    ((LinkButton)r.Controls[0].FindControl("btnDejarComoADM")).Visible = false;
            }
        }

        private void EstadoUsuario(HtmlGenericControl lblEstadoUsuario, LinkButton btnBloquear, LinkButton btnDesbloquear, int EstadoUsuario, string Nombre)
        {
            //Estado
            // 0: Usuario "nuevo" que no tiene acceso al portal Web
            // 1: Tiene permiso para acceder al portal Web
            // 2: Usuario bloqueado
            // -1: Error

            switch (EstadoUsuario)
            {
                case 1:
                    lblEstadoUsuario.InnerHtml = string.Format("<i class='glyphicon glyphicon-user'>&nbsp;</i><b>{0}:</b> Con acceso al Portal Web.", Nombre);
                    lblEstadoUsuario.Attributes["class"] = "label label-success";
                    btnBloquear.Visible = true;
                    btnDesbloquear.Visible = false;
                    return;
                case 2:
                    lblEstadoUsuario.InnerHtml = string.Format("<i class='glyphicon glyphicon-user'>&nbsp;</i><b>{0}:</b> Usuario Bloqueado.", Nombre);
                    lblEstadoUsuario.Attributes["class"] = "label label-warning";
                    btnBloquear.Visible = false;
                    btnDesbloquear.Visible = true;
                    return;
            }
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("parent.abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }

        protected void lbAgregarUsuario_Click(object sender, EventArgs e)
        {
            Response.Redirect("AgregarUserProveedor.aspx?ip=" + App_Code.Util.Encriptar(ViewState["_ip"].ToString()));
        }
	}
}