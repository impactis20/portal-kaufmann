﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private.user
{
	public partial class miperfil : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (PortalProveedorBL.GetTipoUsuarioLogueado() == 10)
                {
                    string Run = PortalProveedorBL.GetUsuarioProveedorLogueado().Run;
                    UsuarioProveedorEntity userProv = PortalProveedorBL.GetUsuarioProveedor(Run);

                    txtRun.Text = userProv.Run;
                    txtNombre.Text = userProv.Nombre;
                    txtAP.Text = userProv.ApellidoPat;
                    txtAM.Text = userProv.ApellidoMat;
                    txtEmail.Text = userProv.Email;
                    txtTF.Text = userProv.Fono.ToString();
                    txtTM.Text = userProv.FonoMovil.ToString();

                    GuardaDatosTemporales(userProv.Nombre, userProv.ApellidoPat, userProv.ApellidoMat, userProv.Email, userProv.Fono.ToString(), userProv.FonoMovil.ToString());
                    ViewState.Add("run", Run);
                }
                else
                {
                    msj.InnerHtml = "Usted no tiene acceso a esta página. El acceso a esta página sólo se le permite a los <b>usuarios proveedores</b> para que puedan visualizar los datos de su cuenta.";
                    formulario.Visible = false;
                }
            }
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            Edita(true);
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            txtNombre.Text = ViewState["nom"].ToString();
            txtAP.Text = ViewState["ap"].ToString();
            txtAM.Text = ViewState["am"].ToString();

            txtEmail.Text = ViewState["e"].ToString();
            txtTF.Text = ViewState["tf"].ToString();
            txtTM.Text = ViewState["tm"].ToString();

            Edita(false);
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            //guardar
            string Nombre = txtNombre.Text.Trim();
            string AP = txtAP.Text.Trim();
            string AM = txtAM.Text.Trim();
            string Email = txtEmail.Text.Trim();
            string TF = txtTF.Text.Trim();
            string TM = txtTM.Text.Trim();

            //validar campos en blanco
            //validar que fonos sean numeros y email sea un email válido            

            if (App_Code.Util.isValidEmail(Email))
            {
                if (App_Code.Util.isIntNumeric(TF) && TF.Length == 9)
                {
                    if (App_Code.Util.isIntNumeric(TM) && TM.Length == 8)
                    {
                        //validación cambio de correo
                        //if(Email != ViewState["e"].ToString())
                        //{
                        //    MostrarMensaje("Valide nuevo email", string.Format("Se ha detectado el cambio de su email y necesitamos validarlo. Le hemos enviado un correo electrónico a su nuevo email con un número de validación, ingréselo a continuación para actualizar.<br><br>{0}", validacorreo), "text-danger");
                        //}

                        int respuesta = PortalProveedorBL.SetDatosContactoProv(ViewState["run"].ToString(), Nombre, AP, AM, Email, Convert.ToInt32(TF), Convert.ToInt32(TM));

                        if (respuesta == 1)
                        {
                            GuardaDatosTemporales(Nombre, AP, AM, Email, TF, TM);
                            MostrarMensaje("Datos actualizados", "Los datos han sido actualizados correctamente", "text-success");
                        }
                        else
                            MostrarMensaje("Error temporal", "Ha ocurrido un error al intentar guardar los datos. Favor de intentarlo más tarde.", "text-danger");

                        Edita(false);
                    }
                    else
                    {
                        MostrarMensaje("Teléfono móvil inválido", "Su número de telefono móvil es inválido. Le recordamos que un número móvil tiene 8 dígitos.", "text-danger");
                    }
                }
                else
                {
                    MostrarMensaje("Teléfono inválido", "Su número de telefono es inválido. Le recordamos que ahora todos los números tienen 9 dígitos.", "text-danger");
                }
            }
            else
            {
                MostrarMensaje("Email inválido", "Su email no tiene un formato correcto. Favor de ingresar un correo electrónico válido.", "text-danger");
            }
        }

        void button_ServerClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        void a_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Edita(bool Edita)
        {
            btnCancelar.Visible = Edita;
            btnGuardar.Visible = Edita;
            btnEditar.Visible = !Edita;

            //txtEmail.Enabled = Edita;
            txtNombre.Enabled = Edita;
            txtAP.Enabled = Edita;
            txtAM.Enabled = Edita;
            txtTF.Enabled = Edita;
            txtTM.Enabled = Edita;
        }

        private void GuardaDatosTemporales(string Nombre, string AP, string AM, string Email, string TF, string TM)
        {
            ViewState.Add("nom", Nombre);
            ViewState.Add("ap", AP);
            ViewState.Add("am", AM);
            ViewState.Add("e", Email);
            ViewState.Add("tf", TF);
            ViewState.Add("tm", TM);
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("parent.abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }
	}
}