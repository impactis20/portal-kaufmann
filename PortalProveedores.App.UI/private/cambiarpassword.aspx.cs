﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private
{
	public partial class cambiarpassword : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int TipoUsuario = PortalProveedorBL.GetTipoUsuarioLogueado();
                if (TipoUsuario == 0)
                {
                    msj.InnerHtml = "Usted <b>no puede cambiar la contraseña</b>, debido a que es un usuario corporativo. Su contraseña en este portal, siempre será la de usuario Windows.";
                    formulario.Visible = false;
                }
            }
        }

        protected void btnCambiarPass_Click(object sender, EventArgs e)
        {
            NormalizaFormularioRegistro();

            string PassAct = txtPassAct.Text;
            string NewPass = txtNewPass.Text;
            string NewPassRe = txtNewPassRe.Text;

            if (string.IsNullOrEmpty(PassAct) || string.IsNullOrEmpty(NewPass) || string.IsNullOrEmpty(NewPassRe))
            {
                MostrarMensaje("Campos incompletos", "Para cambiar la contraseña, debe llenar los 3 campos solicitados. Vuelva a realizar la operación.", "text-danger");
            }
            else
            {
                if (NewPass == NewPassRe)
                {
                    if (NewPass.Length < 8)
                    {
                        MostrarMensaje("Contraseña nueva incorrecta", "Su contraseña no puede contener menos de 8 caracteres. Ingrese una contraseña más larga.", "text-danger");
                    }
                    else
                    {
                        //obtiene datos de usuario logueado
                        UsuarioProveedorEntity userProv = PortalProveedorBL.GetUsuarioProveedorLogueado();

                        if (PortalProveedorBL.ValidaLogin(userProv.Email, PassAct, 10) != null) //10: usuario proveedor
                        {
                            int Respuesta = PortalProveedorBL.SetPassword(userProv.Run, NewPass);

                            if (Respuesta == 1)
                            {
                                MostrarMensaje("¡Cambio exitoso!", "Su contraseña ha sido cambiada correctamente. Recuerde que desde ahora para entrar al portal de proveedores, deberá ingresar su nueva contraseña.", "text-success");
                            }
                            else
                            {
                                MostrarMensaje("Error temporal", "Ha ocurrido un error interno, por favor intente cambiar su contraseña más tarde.", "text-danger");
                            }
                        }
                        else
                        {
                            MostrarMensaje("Contraseña actual incorrecta", "Su contraseña actual es incorrecta. Vuelva a intentar la operación.", "text-danger");
                        }
                    }
                }
                else
                {
                    SetColorTxT(dNewPass, "has-error");
                    SetColorTxT(dNewPassRe, "has-error");
                    MostrarMensaje("Nueva contraseña no coincide", "El campo <b>Nueva contraseña</b> debe ser igual al campo <b>Repita nueva contraseña</b>. Vuelva a intentar la operación", "text-danger");
                }
            }
        }

        private void NormalizaFormularioRegistro()
        {
            ColorEstadoTxT(txtPassAct, dPassAtc);
            ColorEstadoTxT(txtNewPass, dNewPass);
            ColorEstadoTxT(txtNewPassRe, dNewPassRe);
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("parent.abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }

        //Es llamado cuando hay algún dato ingresado con error en el txt
        private void SetColorTxT(HtmlGenericControl div, string Estilo)
        {
            div.Attributes["class"] = Estilo;
        }

        private void ColorEstadoTxT(TextBox txt, HtmlGenericControl div)
        {
            if (txt.Text == string.Empty)
                div.Attributes["class"] = "has-error";
            else
                div.Attributes["class"] = string.Empty;
        }
	}
}