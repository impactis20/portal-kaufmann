﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private
{
	public partial class principal : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int TipoUsuario = PortalProveedorBL.GetTipoUsuarioLogueado();
                GetDatosUsuario_MenuUsuario(TipoUsuario);
            }

            if (!string.IsNullOrEmpty(Request.QueryString["val"]))
                frame.Attributes["src"] = Request.QueryString["val"];
            else
                frame.Attributes["src"] = "Index.aspx";
        }

        private void GetDatosUsuario_MenuUsuario(int TipoUsuario)
        {
            if (TipoUsuario == 10) //Verifico qué tipo de usuario es y le asigno el nombre
            {
                UsuarioProveedorEntity userProv = PortalProveedorBL.GetUsuarioProveedorLogueado();

                if (userProv.EsAdmin == 1) //Si es admin, envío la razón social
                    InsertNombreUsuario(userProv.Nombre, PortalProveedorBL.GetProveedor(userProv.IdProveedor[0]).RazonSocial, 1);
                else
                    InsertNombreUsuario(userProv.Nombre, PortalProveedorBL.GetProveedor(userProv.IdProveedor[0]).RazonSocial, 2);

                correo.InnerText = userProv.Email;

                //bloquea
                inicio.Visible = false;
            }
            else if (TipoUsuario == 0)
            {
                UsuarioProveedorEntity.UsuarioKaufmann userCorp = PortalProveedorBL.GetUsuarioCorporativoLogueado();

                if (userCorp.EsAdmin == 1)
                {
                    inicio.Visible = true;
                    InsertNombreUsuario(userCorp.Nombre, string.Empty, 3);
                }
                else
                {
                    inicio.Visible = false;
                    InsertNombreUsuario(userCorp.Nombre, string.Empty, 4);
                }

                correo.InnerText = userCorp.Email;

                //bloquea
                razonSocial.Visible = false;
                miPerfil.Visible = false;
                cambiarPass.Visible = false;
                d1.Visible = false;
                d2.Visible = false;
            }
        }

        private void InsertNombreUsuario(string NombreUsuario, string RazonSocial, int TipoUsuario)
        {
            string user = string.Empty;

            if (NombreUsuario.Contains(" ")) //Inserto primer nombre
                user = NombreUsuario.Split(' ')[0];
            else
                user = NombreUsuario;

            switch (TipoUsuario) //Si viene con Razón Social, es porque es usuario proveedor y admin
            {
                case 3:
                    aUser.Title = "(Usuario Adm. Portal)";
                    aUser.InnerHtml = string.Format("<span>Adm. Portal:</span> <span><b>{0}</b></span> <span class=\"caret\"></span>", user);
                    break;

                case 4:
                    aUser.Title = "(Usuario Corporativo)";
                    aUser.InnerHtml = string.Format("<span>Usuario corporativo:</span> <span><b>{0}</b></span> <span class=\"caret\"></span>", user);
                    break;

                case 1:
                    aUser.Title = string.Format("(Administrador de {0})", RazonSocial);
                    aUser.InnerHtml = string.Format("<span>Adm. {0}:</span> <span><b>{1}</b></span> <span class=\"caret\"></span>", RazonSocial, user);
                    break;

                case 2:
                    aUser.Title = string.Format("(Usuario de {0})", RazonSocial);
                    aUser.InnerHtml = string.Format("<span>Usuario {0}:</span> <span><b>{1}</b></span> <span class=\"caret\"></span>", RazonSocial, user);
                    break;
            }

        }

        protected void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            if (Request.Cookies["dReUser_"] != null)
            {
                Request.Cookies["dReUser_"].Expires = DateTime.Now;
                Response.Cookies.Add(Request.Cookies["dReUser_"]);
            }

            Session.Clear();
            Session.RemoveAll();
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }
	}
}