﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="estadopagos.aspx.cs" Inherits="PortalProveedores.App.UI.private.sistema.estadopagos.estadopagos" ValidateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../../../Css/kf_style.css" rel="stylesheet" />
    <link type="text/css" href="../../../Css/bootstrap.css" rel="stylesheet" />
    <link href="../../../css/bootstrap-datetimepicker.css" rel="stylesheet" />
    <link href="../../../css/fileinput.min.css" rel="stylesheet" />
    <%--<link type="text/css" href="../../Css/bootstrap.min.original.css" rel="stylesheet" />--%>
<%--    <link type="text/css" href="../../Css/font-awesome.min.css" rel="stylesheet" />--%>

    <link type="text/css" href="Css/jscal2.css" rel="stylesheet" />
    <link type="text/css" href="Css/border-radius.css" rel="stylesheet" />

    <script type="text/javascript" src="../../../Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="../../../Js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../../Js/bs.pagination.js"></script>
    <script type="text/javascript" src="Js/jscal2.js"></script>
    <script type="text/javascript" src="Js/en.js"></script>
    <script src="../../../js/moment-with-locales.js"></script>
    <script src="../../../js/bootstrap-datetimepicker.js"></script>
    <script src="../../../js/fileinput.min.js"></script>
    <script type="text/javascript" >
        $('#TipoBusqueda a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });

        <%-- mantiene la misma pestaña después de un postback --%>
        $(function () {
            // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // save the latest tab; use cookies if you like 'em better:
                localStorage.setItem('lastTab', $(this).attr('href'));
            });

            // go to the latest tab, if it exists:
            var lastTab = localStorage.getItem('lastTab');
            if (lastTab) {
                $('[href="' + lastTab + '"]').tab('show');
            }
        });

        function validate(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        //Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);
    </script>
     <%--  <script>
           $(document).ready(function () {
               $("#<%= rbEstadoDocumento.ClientID %>").click(function () {
                var target = document.getElementById('<%=rbEstadoDocumento.ClientID %>');
                   var radioButtons = target.getElementsByTagName('input');
                   if (radioButtons[0].checked) {
                       window.location = '<%= Page.ResolveUrl("~/private/sistema/estadopagos/Detallepago.aspx") %>';
                       //window.location = "~/private/sistema/estadopagos/Detallepago.aspx";
                   }
               });
           });
    </script>--%>

    <style type="text/css"> /*Para mozilla firefox*/
        @-moz-document url-prefix() {
            .of {
                overflow:hidden;
                }
            }

        body{
            margin: 0px 15px;
            overflow:hidden;
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }

        td, h4{
            margin-bottom: 0px;
            margin-top: 0px;
        }

        .pagada{
            color: #63a945
        }

        .porpag{
            color: #e8c400
        }

        .pend{
            color: #e8201a
        }

        .highlight { color: #ff0000 !important; font-weight: bold; text-decoration:underline }
        .highlight2 { color: #090 !important; font-weight: bold }
        .birthday { background: #fff; font-weight: bold }
        .birthday.DynarchCalendar-day-selected { background: #89f; font-weight: bold }

        .row-dctos
        {
            min-height: 39.45px;
            height: 39.45px;
            max-height: 39.45px
        }
    </style>

    <%--Adapta el tamaño del calendario a esta página--%>
    <style type="text/css">
        #DynarchCalendar-topCont{
	        width: 210px;
            z-index: 999999999999999999;
        }

        #DynarchCalendar-dayNames, table{
	        width: 190px;
        }

        #DynarchCalendar-body, table{
	        width: 200px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ss" runat="server"></asp:ScriptManager>
        <asp:SiteMapPath ID="sm" runat="server" PathSeparator=" > " ParentLevelsDisplayed="10" PathDirection="RootToCurrent" RenderCurrentNodeAsLink="false" ShowToolTips="true" CssClass="breadcrumb">
        </asp:SiteMapPath>

        <div class="row">
            <div id="page-container" class="navbar navbar-default">
                <div class="margin-modulo">
                    <h2 class="page-header" style="margin-top:25px">Estado de pagos</h2>

                    <div id="adminPanel" runat="server" visible="false">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <b>Consultar documentos de un proveedor</b> (Sólo para administradores)
                            </div>
                            <div class="panel-body" style="padding-top:10px;padding-bottom:10px">
                                <div class="col-md-2" style="padding-top: 8px;">
                                    <h4>Rut del proveedor</h4>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <asp:TextBox ID="txtRutProveedor" runat="server" placeholder="78653920-K" Height="34" CssClass="form-control" TextMode="Search"></asp:TextBox>
                                      <span class="input-group-btn">
                                        <asp:LinkButton ID="btnBuscarProveedor" runat="server" Text="<span class='glyphicon glyphicon-search'></span> Proveedor" CssClass="btn btn-default" OnClick="btnBuscarProveedor_Click"></asp:LinkButton>
                                      </span>
                                    </div>
                                </div>
                                <div class="col-md-5" style="padding-top: 8px;">
                                    <b id="proveedor" runat="server"></b>
                                </div>
                            </div>
                        </div>                        
                    </div>

                    <p>En este módulo usted podrá ver sus facturas, especificando su condición, su fecha y forma de pago.</p>
                    <p>En la pestaña "<b>Buscar por N° de dcto</b>" puede buscar uno en específico ingresando el número del documento que desee encontrar. En la pestaña "<b>Buscar por rango de fecha</b>" podrá encontrar distintos registros con un rango de búsqueda de hasta 12 meses como máximo, filtrando además por empresa pagadora y el tipo de documento.</p>
                    
                    <div class="margin-content-modulo">
                        <ul id="TipoBusqueda" class="nav nav-tabs" role="tablist">
                            <li class="primer-tab active"><a href="#BusquedaEspecifica" role="tab" data-toggle="tab">Buscar por N° de dcto</a></li>
                            <li class=""><a href="#BusquedaGenerica" role="tab" data-toggle="tab">Buscar por rango de fecha</a></li>
                        </ul>

                        <div id="Pestanas" class="tab-content">
                            <div id="BusquedaEspecifica" runat="server" class="tab-pane active"> <%--action in--%>
                                <iframe id="iframe" runat="server" src="estadodcto.aspx"></iframe>
                                <script type="text/javascript" src="../../../Js/iframeResizer.contentWindow.min.js"></script> 
		                        <script type="text/javascript" src="../../../Js/iframeResizer.min.js"></script> 
		                        <script type="text/javascript">
		                            iFrameResize({
		                                log: false, // Enable console logging
		                                resizedCallback: function (messageData) { // Callback fn when message is received
		                                    //setTimeout(function () { parentIFrame.sendMessage('nested') }, 50);
		                                }
		                            });
		                        </script>
                            </div>

                            <div id="BusquedaGenerica" class="tab-pane fade">
                                <div class="col-md-12" style="margin-bottom: 15px;">
                                    <p style="text-align:justify">Puede buscar por intervalos de fechas sin superar los 12 meses como Máximo. <label id="ejemplo_hasta" runat="server"></label></p>
                                    <div class="col-md-6">
                                        <span>Información de búsqueda en calendarios:</span>
                                        <ul>
                                            <li>La fecha mínima, es apartir del 1 de enero del 2010.</li>
                                            <li>La fecha máxima, es la fecha actual + un mes a futuro (<span id="info_maxfecha" runat="server"></span>).</li>
                                            <li>Rangos de selección de fechas corresponden a la fecha de emisión de los documentos.</li>
                                        </ul>
                                    </div>
                                </div>
                                <%--<asp:UpdatePanel ID="upa1" runat="server">
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnExportToExcel" />
                                    </Triggers>
                                    <ContentTemplate>--%>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-3">
                                                    <div class="col-md-12">
                                                        <h4>Empresas</h4>
                            
                                                        <asp:UpdatePanel ID="uPanel1" runat="server">
                                                            <ContentTemplate>
                                                                <div style="clear: both; border-bottom: 1px solid silver">
                                                                    <asp:CheckBox ID="cbSeleccionarTodos" runat="server" Text="Seleccionar todas" CssClass="checkbox" OnCheckedChanged="cbSeleccionarTodos_CheckedChanged" AutoPostBack="true" Checked="false" />
                                                                </div>

                                                                <div style="clear:both; line-height:1.5">
                                                                    <asp:CheckBoxList id="cblEmpresa" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" TextAlign="Right" CssClass="checkbox" data-select-all="empresa" DataTextField="RazonSocial" DataValueField="IdSociedad">
                                                                    </asp:CheckBoxList>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <%--calendario antigüo--%>
                                                    <%--<div class="col-md-12">
                                                        <h4>Desde</h4>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtDesde" runat="server" CssClass="form-control" Enabled="false" TextMode="DateTime"></asp:TextBox>
                                                            <span id="calendario1" class="input-group-addon" style="background-color: #FFF;">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12" style="margin-top:15px; margin-bottom:0px">
                                                        <h4>Hasta</h4>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtHasta" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                            <span id="calendario2" class="input-group-addon" style="background-color: #FFF;">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>--%>

                                                    <div class="col-md-12">
                                                        <h4>Desde:</h4>(Fecha Emisión)
                                                        <div class="form-group">
                                                            <div class="input-group date input-group" id="datetimepicker6">
                                                                <asp:TextBox id="txtDesde" runat="server" CssClass="form-control input-sm disabled" placeholder="Seleccione..." AutoCompleteType="Disabled" autocomplete="off"></asp:TextBox>   
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </div>
                                                        <%--    <asp:RequiredFieldValidator ID="rfvFechaDesde" runat="server" ErrorMessage="Ingrese fecha Desde..." ControlToValidate="txtDesde" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="estado" />--%>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h4>Hasta:</h4>
                                                        <div class="form-group">
                                                            <div class="input-group date" id="datetimepicker7">
                                                                <asp:TextBox id="txtHasta" runat="server" CssClass="form-control input-sm disabled" placeholder="Seleccione..." AutoCompleteType="Disabled" autocomplete="off"></asp:TextBox>
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </div>
                                                             <%--<asp:RequiredFieldValidator ID="rfvFechaHasta" runat="server" ErrorMessage="Ingrese fecha Desde..." ControlToValidate="txtHasta" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="estado" />--%>
                                                        </div>
                                                    </div>

                                                    <script type="text/javascript">

                                                        function pageLoad(sender, args) {
                                                            $('#datetimepicker6').datetimepicker({ pickDate: false });
                                                            $('#datetimepicker7').datetimepicker({ pickDate: false });
                                                        }

                                                        $(function () {
                                                            $('#datetimepicker6').datetimepicker({
                                                                format: 'YYYY/MM/DD',
                                                                ignoreReadonly: true,
                                                                useCurrent: false
                                                            });
                                                            $('#datetimepicker7').datetimepicker({
                                                                format: 'YYYY/MM/DD',                                                                
                                                                ignoreReadonly: true,
                                                                useCurrent: false //Important! See issue #1075
                                                            });
                                                            $("#datetimepicker6").on("dp.change", function (e) {
                                                                $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
                                                            });
                                                            $("#datetimepicker7").on("dp.change", function (e) {
                                                                $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
                                                            });
                                                        });
                                                    </script>

                                                    <div class="col-md-12" style="margin-top:15px; margin-bottom:15px">
                                                        <h4>Seleccione estado documento</h4>
                                                        <div style="clear:both; line-height:1.5">
                                                            <asp:CheckBoxList id="cblEstadoDocumento" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" TextAlign="Right" CssClass="checkbox">
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="col-md-12" style="margin-bottom:15px;height:18px;">
                                                        <div class="pull-right">
                                                            <asp:LinkButton ID="btnBuscar" runat="server" Text="<span class='glyphicon glyphicon-search'></span> Buscar" CssClass="btn btn-primary rgth"  OnClick="btnBuscar_Click"></asp:LinkButton>
                                                            
                                                            <%--<asp:UpdateProgress ID="up_prog1" runat="server" AssociatedUpdatePanelID="upa1">
                                                                <ProgressTemplate>
                                                                    Cargando...
                                                                    <img src="../../../Img/loading.gif"/>
                                                                </ProgressTemplate>
                                                            </asp:UpdateProgress>--%>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12" style="padding-top:15px; padding-bottom:0px">
                                                        <div id="p1Error" runat="server" role="alert" visible="false">
                                                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                                            <strong id="p1TituloError" runat="server"></strong> <span id="p1MsjError" runat="server"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div id="dBusquedaGenerica" runat="server" visible="false" class="col-md-12" style="padding-left:15px;padding-bottom:15px;padding-right:15px;">
                                                        
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <h2>Resultado de la búsqueda</h2>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <h2></h2>
                                                                    <asp:LinkButton ID="btnExportToExcel" runat="server" Text="<span class='glyphicon glyphicon-download-alt'></span> Exportar a Excel" CssClass="btn btn-success rgth" OnClick="btnExportToExcel_Click"></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="table">
                                                                <asp:GridView ID="gvBusquedaFiltradaH" runat="server" AutoGenerateColumns="False" ShowHeader="true" ShowFooter="true" CssClass="table table-bordered" 
                                                                    OnRowDataBound="gvBusquedaFiltradaH_RowDataBound" >
                                                                    <HeaderStyle CssClass="gv-header-row gv-no-border" />
                                                                    <FooterStyle CssClass="gv-header-row gv-no-border" />
                                                                    <AlternatingRowStyle CssClass="active" />
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                <div class="row-centered">
                                                                                    <div class="col-md-3 col-centered">
                                                                                        <h4>Estado documento:</h4>
                                                                                    </div>
                                                                                    <div class="col-md-2 col-centered">
                                                                                        <h4><i class="glyphicon glyphicon-ok-sign pagada">&nbsp;</i><small>Pagada</small></h4>
                                                                                    </div>
                                                                                    <div class="col-md-2 col-centered">
                                                                                        <h4><i class="glyphicon glyphicon-info-sign porpag">&nbsp;</i><small>Próximo a pagar</small></h4>
                                                                                    </div>
                                                                                    <div class="col-md-2 col-centered">
                                                                                        <h4><i class="glyphicon glyphicon-question-sign pend">&nbsp;</i><small>Pendiente de pago</small></h4>
                                                                                    </div>
                                                                                </div>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <div style="margin:0px">
                                                                                    <div class="col-md-12" style="border-bottom: 1px solid silver">
                                                                                        <h3 id="RSPagadora" runat="server" ></h3>
                                                                                        <h4 id="RSRut" runat="server"></h4>
                                                                                        <br />
                                                                                        <div id="sin_reg" runat="server" class="alert alert-danger" role="alert" visible="false"></div>

                                                                                        <asp:GridView ID="gvBusquedaFiltradaB" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered" Font-Size="8" AllowPaging="True"
                                                                                            PageSize="10" PagerStyle-CssClass="bs-pagination text-center" PagerSettings-Mode="Numeric" PagerSettings-Position="TopAndBottom"
                                                                                            OnPageIndexChanging="gvBusquedaFiltradaB_PageIndexChanging"
                                                                                            OnRowDataBound="gvBusquedaFiltradaB_RowDataBound" >
                                                                                           <%-- <HeaderStyle CssClass="gv-header-row gv-no-border"  />--%>
                                                                                          <%--  <RowStyle CssClass="gv-no-border"/>--%>
                                                                                            <%--<AlternatingRowStyle CssClass="gv-no-border" />--%>
                                                                                            <Columns>
                                                                                                <asp:TemplateField HeaderText="Nro. Dcto." >
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblNroDocumento" runat="server" Text='<%# string.Format("<b>{0}</b>", Eval("NroDocumento")) %>'></asp:Label>                                                                                                
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Monto" >
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblMonto" runat="server" Text='<%# string.Format("<b>{1}</b>: {0:n0}", Eval("Monto"), Eval("Moneda")) %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Forma de Pago">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblFormaPago" runat="server" Text='<%# Eval("FormaPago") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Nro. Cheque">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblNroCheque" runat="server" Text='<%# Eval("NroCheque") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Fecha Posible Pago">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblFechaPosiblePago" runat="server" Text='<%# string.Format("{0:dd/MM/yyy}", Eval("FechaCompensa")) %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Fecha Pago">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblFechaPago" runat="server" Text='<%# string.Format("{0:dd/MM/yyy}", Eval("FechaPago")) %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Estado">
                                                                                                        <ItemTemplate >
                                                                                                            <asp:Label ID="lblEstado" runat="server" Text='<%# Eval("estado") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                        <asp:TemplateField HeaderText="Razón Social">
                                                                                                        <ItemTemplate >
                                                                                                            <asp:Label ID="lblrazon" runat="server" Text='<%# Eval("razon") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField >
                                                                                                        <asp:TemplateField HeaderText="Rut">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblrut" runat="server" Text='<%# Eval("rut") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                               <%--  <asp:TemplateField HeaderText="Doc. Pago">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblpago" runat="server"  Text='<%# Eval("noTransa") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>--%>
                                                                                                <asp:TemplateField HeaderText="Monto Total">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lbltotal" runat="server" Text='<%#  string.Format("<b>{1}</b>: {0:n0}",  Eval("total"), Eval("Moneda"))%>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                 <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <h4 id="icon" runat="server" aria-hidden="true"></h4>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <div class="row-centered">
                                                                                    <div class="col-md-3 col-centered">
                                                                                        <h4>Estado documento:</h4>
                                                                                    </div>
                                                                                    <div class="col-md-2 col-centered">
                                                                                        <h4><i class="glyphicon glyphicon-ok-sign pagada">&nbsp;</i><small>Pagada</small></h4>
                                                                                    </div>
                                                                                    <div class="col-md-2 col-centered">
                                                                                        <h4><i class="glyphicon glyphicon-info-sign porpag">&nbsp;</i><small>Próximo a pagar</small></h4>
                                                                                    </div>
                                                                                    <div class="col-md-2 col-centered">
                                                                                        <h4><i class="glyphicon glyphicon-question-sign pend">&nbsp;</i><small>Pendiente de pago</small></h4>
                                                                                    </div>
                                                                                </div>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <%--</ContentTemplate>
                                </asp:UpdatePanel>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../../Js/iframeResizer.contentWindow.min.js"></script>
    </form >
</body >
</html >