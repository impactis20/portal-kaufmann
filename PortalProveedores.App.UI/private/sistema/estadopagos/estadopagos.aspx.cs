﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Drawing;
using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private.sistema.estadopagos
{
    public partial class estadopagos : System.Web.UI.Page
    {
        //variables usadas para el calendario
        DateTime FechaMinima = new DateTime(2010, 01, 01);
        DateTime FechaMaxima = DateTime.Now.AddMonths(1);
        public List<PagosEntity> listSolicitud2 = new List<PagosEntity>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "myUniqueKey", "if(!(window.self !== window.top)) self.parent.location='principal.aspx?val=sistema/estadopagos/" + Path.GetFileName(Request.Url.AbsolutePath) + "';", true);

                //almaceno el rut del proveedor

                int TipoUsuario = PortalProveedorBL.GetTipoUsuarioLogueado();
                if (TipoUsuario == 10)
                {
                    ViewState.Add("r__prov", PortalProveedorBL.GetProveedor(PortalProveedorBL.GetUsuarioProveedorLogueado().IdProveedor[0]).Rut);
                    ViewState.Add("id__prov", PortalProveedorBL.GetMiRazonSocial_ParaUsuarioProveedor(PortalProveedorBL.GetUsuarioProveedorLogueado().Run)).ToString();

                    adminPanel.Visible = false;
                }
                else
                {
                    bool EsAdmin = PortalProveedorBL.GetUsuarioCorporativoLogueado().EsAdmin == 1 ? true : false;

                    //Usuario administrador puede ingresar el rut de la razon social a consultar
                    if (EsAdmin)
                    {
                        adminPanel.Visible = true;
                        btnBuscar.Visible = false;
                    }
                    else
                    {
                        adminPanel.Visible = false;
                        btnBuscar.Visible = true;
                    }
                }

                InicializacionCalendarios();
            }
        }
        //private void GetTipoRazon()
        //{
        //    string validaRazon = ViewState.Add("id__prov", PortalProveedorBL.GetMiRazonSocial_ParaUsuarioProveedor(PortalProveedorBL.GetUsuarioProveedorLogueado().Run)).ToString();
        //    if (validaRazon == null)
        //    {
        //        validaRazon = "P";

        //    }

        //}
        private string GetValoresSeleccionados(CheckBoxList cbl)
        {
            //Obtengo las sociedades pagadoras seleccionadas
            string SociedadesSeleccionadas = string.Empty;
            foreach (ListItem item in cbl.Items)
                if (item.Selected)
                    SociedadesSeleccionadas += string.Format("{0},", item.Value);

            return SociedadesSeleccionadas.Substring(0, SociedadesSeleccionadas.Length - 1); //quito la última coma
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            if (cbSeleccionarTodos.Checked || GetSelectedCBs(cblEmpresa))
            {
                if ((txtDesde.Text != "" && txtHasta.Text == "") || (txtDesde.Text == "" && txtHasta.Text != "") || (txtDesde.Text == "" && txtHasta.Text == ""))
                {
                    {
                        CreateMsj(p1Error, false, "alert alert-warning fade in", p1TituloError, "Atención:", p1MsjError, "Debe seleccionar fecha desde y fecha hasta.");
                    }
                }
                else
                {
                    try
                    {

                        Entities.UsuarioProveedorEntity usuario = PortalProveedorBL.GetTipoRazón(Convert.ToInt16(ViewState["id__prov"].ToString()));
                        string razon = usuario.descripcionTipo;

                        string fe = txtDesde.Text.Trim();
                        string fe2 = txtHasta.Text.Trim();
                        string f = fe.Replace("/", string.Empty).ToString();
                        string f1 = fe2.Replace("/", string.Empty).ToString();
                        DateTime FechaDesde = Convert.ToDateTime(fe.ToString());
                        DateTime FechaHasta = Convert.ToDateTime(fe2.ToString());

                        if (FechaDesde <= FechaHasta)
                        {
                            if (FechaDesde > FechaMinima || FechaDesde < FechaMaxima || FechaHasta > FechaMinima || FechaHasta < FechaMaxima) //verifica si la fecha seleccionada está en el rango de fecha permitida (también está validado a nivel de UI)
                            {
                                int totalDias = FechaDesde.Subtract(FechaHasta).Days;

                                if (FechaDesde >= FechaHasta.AddMonths(-12)) //valido que el rango de fecha sea máximo entre 12 meses
                                {
                                    if (GetSelectedCBs(cblEstadoDocumento))
                                    {
                                        dBusquedaGenerica.Visible = true;
                                        string EstadoDocumentoABuscar = GetValoresSeleccionados(cblEstadoDocumento);

                                        List<SociedadPagadoraEntity> ListPagos = new List<SociedadPagadoraEntity>();

                                        //Toma el rut del proveedor dependiendo si es un usuario proveedor o administrador del portal
                                        //if(ViewState["prov_adm"] != null)
                                        //    ListPagos = PagosBL.GetPagos(ViewState["prov_adm"].ToString(), GetValoresSeleccionados(cblEmpresa), FechaDesde, FechaHasta, EstadoDocumentoABuscar);
                                        //else
                                        ListPagos = PagosBL.GetPagos(ViewState["r__prov"].ToString(), GetValoresSeleccionados(cblEmpresa), f.ToString(), f1.ToString(), EstadoDocumentoABuscar, razon);

                                        ViewState.Add("ListPagos", ListPagos);
                                        ViewState.Add("EstadoDocumentoABuscar", EstadoDocumentoABuscar);

                                        if (ListPagos != null && ListPagos.Count == 0)
                                        {
                                            CreateMsj(p1Error, false, "alert alert-danger fade in", p1TituloError, "Sin resultados:", p1MsjError, "No se han encontrado documentos con los filtros aplicados.");
                                            dBusquedaGenerica.Visible = false;
                                        }
                                        else
                                        {
                                            ViewState.Add("cont_docs", 0);      //contador total de documentos
                                            ViewState.Add("pend_de_pago", 0);   //contador de documentos con estado "pendiente de pago"
                                            ViewState.Add("prox_a_pagar", 0);   //contador de documentos con estado "próximo a pagar"
                                            ViewState.Add("pagado", 0);         //contador de documentos con estado "pagado"

                                            gvBusquedaFiltradaH.DataSource = ListPagos; //dentro de este evento RowDataBound se usa la sessión cont_docs
                                            gvBusquedaFiltradaH.DataBind();

                                            //obtiene el contador GENERAL por estado de documentos en variables ViewState
                                            GetContadorPorEstadoDctos(ListPagos);

                                            
                                            string Msj = CreaMensajeDeBusqueda();

                                            CreateMsj(p1Error, true, "alert alert-success fade in", p1TituloError, "Búsqueda:", p1MsjError, Msj);

                                            ViewState.Remove("cont_docs");

                                            //valores del rango de búsqueda para paginación en gridview que se encuentra dentro del otro del gridview gvBusquedaFiltradaH
                                            ViewState.Add("fec_min_seleccionada", FechaDesde);
                                            ViewState.Add("fec_max_seleccionada", FechaHasta);
                                        }
                                    }
                                    else
                                    {
                                        CreateMsj(p1Error, false, "alert alert-warning fade in", p1TituloError, "Atención:", p1MsjError, "Seleccione el estado de documentos a buscar.");
                                    }
                                }
                                else
                                {
                                    CreateMsj(p1Error, false, "alert alert-warning fade in", p1TituloError, "Error:", p1MsjError, "El rango máximo de búsqueda entre la fecha <b>Desde</b> y la fecha <b>Hasta</b> es de <span style='text-decoration: underline'>12 meses</span>.");
                                }
                            }
                            else
                            {
                                CreateMsj(p1Error, false, "alert alert-warning fade in", p1TituloError, "Error:", p1MsjError, string.Format("No puede seleccionar una fecha que esté fuera del rango de fechas válidas. Rango de fecha válidas, entre <b>{0}</b> y <b>{1}</b>.", FechaMinima, FechaMaxima));
                            }
                        }
                        else
                        {
                            CreateMsj(p1Error, false, "alert alert-warning fade in", p1TituloError, "Error:", p1MsjError, "La fecha \"Desde\" no puede ser mayor a la fecha \"Hasta\".");
                        }
                    }
#pragma warning disable CS0168 // La variable 'ex' se ha declarado pero nunca se usa
                    catch (Exception ex)
#pragma warning restore CS0168 // La variable 'ex' se ha declarado pero nunca se usa
                    {
                        CreateMsj(p1Error, false, "alert alert-danger fade in", p1TituloError, "Error 485:", p1MsjError, "Se generó un problema interno, inténtelo más tarde.");
                    }
                }
            }
            else
            {
                CreateMsj(p1Error, false, "alert alert-warning fade in", p1TituloError, "Atención:", p1MsjError, "Seleccione una empresa para realizar una búsqueda.");
            }
        }

        //1 fila
        protected void gvBusquedaFiltradaH_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                //obtengo la fila
                SociedadPagadoraEntity sociedad = (SociedadPagadoraEntity)r.DataItem;
                //obtengo id sociedad pagadora, rut, razón social
                ((HtmlGenericControl)r.Controls[0].FindControl("RSPagadora")).InnerHtml = sociedad.RazonSocial;
                ((HtmlGenericControl)r.Controls[0].FindControl("RSRut")).InnerHtml = formatearRut(sociedad.Rut);

                //Obtiene el detalle de pagos de una sociedad pagadora
                GridView gvBusquedaFiltradaB = ((GridView)r.Controls[0].FindControl("gvBusquedaFiltradaB"));
                gvBusquedaFiltradaB.DataMember = sociedad.IdSociedad.ToString();
                //veo si la sociedad obtiene pagos
                if (sociedad.Pagos == null || sociedad.Pagos.Count == 0)
                {
                    HtmlGenericControl sin_reg = ((HtmlGenericControl)r.Controls[0].FindControl("sin_reg"));
                    sin_reg.InnerHtml = "<strong>No se encontraron</strong> registros en esta sociedad pagadora.";
                    sin_reg.Visible = true;
                    sin_reg.Attributes["style"] = "margin-bottom:15px";
                }
                else
                {
                    gvBusquedaFiltradaB.DataSource = sociedad.Pagos.OrderBy(f => f.EstadoDocumento).ToList(); 
                    gvBusquedaFiltradaB.DataBind();

                    //cuenta cantidad de documenos encontrados
                    int Cont_docs = (int)ViewState["cont_docs"] + sociedad.Pagos.Count;
                    ViewState["cont_docs"] = Cont_docs;
                }
            }
        }
 
        protected void gvBusquedaFiltradaB_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            GridViewRow r = e.Row;
            
            if (r.RowType == DataControlRowType.DataRow)
            {
                GridView gvBusquedaFiltradaB = (GridView)sender;
                r.Cells[9].HorizontalAlign = HorizontalAlign.Right;
                r.Cells[1].HorizontalAlign = HorizontalAlign.Right;
                Entities.UsuarioProveedorEntity usuario = PortalProveedorBL.GetTipoRazón(Convert.ToInt16(ViewState["id__prov"].ToString()));
                string razon = usuario.descripcionTipo;
                
                if (razon == "P")
                {
                    gvBusquedaFiltradaB.Columns[9].Visible = false;
                   


                }
                else
                {
                    gvBusquedaFiltradaB.Columns[9].Visible = true;
           
                }
                PagosEntity Pago = (PagosEntity)r.DataItem;
                CreaEstadoDocumento(Pago, r);
           
            }
        }
  
        private string CreaMensajeDeBusqueda()
        {
            string Msj = string.Format("<b>{0}</b> documentos encontrados.<br><div style=\"border-top: 1px solid silver;margin-top: 7px;\">", ViewState["cont_docs"]);

            for(int x = 0; x < cblEstadoDocumento.Items.Count; x++)
            {
                if (cblEstadoDocumento.Items[x].Selected)
                    switch(cblEstadoDocumento.Items[x].Value)
                    {
                        case "PA":
                            Msj += string.Format("<br>{0} documentos están en estado <b>{1}</b>", ViewState["pend_de_pago"], cblEstadoDocumento.Items[x].Text);
                            break;

                        case "PP":
                            Msj += string.Format("<br>{0} documentos están en estado <b>{1}</b>", ViewState["prox_a_pagar"], cblEstadoDocumento.Items[x].Text);
                            break;

                        case "PC":
                            Msj += string.Format("<br>{0} documentos están en estado <b>{1}</b>", ViewState["pagado"], cblEstadoDocumento.Items[x].Text);
                            break;
                    }
            }

            Msj += "</div>";

            return Msj;
        }

        private void GetContadorPorEstadoDctos(List<SociedadPagadoraEntity> ListPagos)
        {
            foreach(SociedadPagadoraEntity sociedad in ListPagos)
            {
                foreach(PagosEntity pagos in sociedad.Pagos)
                {
                    switch (pagos.EstadoDocumento)
                    {
                        case "PA":
                            ViewState["pend_de_pago"] = Convert.ToInt32(ViewState["pend_de_pago"]) + 1;
                            break;
                        case "PP":
                            ViewState["prox_a_pagar"] = Convert.ToInt32(ViewState["prox_a_pagar"]) + 1;
                            break;
                        case "PC":
                            ViewState["pagado"] = Convert.ToInt32(ViewState["pagado"]) + 1;
                            break;
                    }
                }
            }
        }

        private void CreaEstadoDocumento(PagosEntity Pago, GridViewRow r)
        {
            if (Pago.NroCheque == 0)
                ((Label)r.Controls[0].FindControl("lblNroCheque")).Text = "-";
            if (Pago.total == 0)
            
                ((Label)r.Controls[0].FindControl("lbltotal")).Text = "";
            
            HtmlGenericControl icon = ((HtmlGenericControl)r.Controls[0].FindControl("icon"));

            //verifica si el documento está bloqueado
            if (Pago.DocumentoBloqueado)
            {
                r.Cells[3].Visible = false;
                r.Cells[4].Visible = false;
                r.Cells[5].Visible = false;
                r.Cells[2].Attributes.Add("colspan", "4");

                ((Label)r.Controls[0].FindControl("lblFormaPago")).Visible = false;

                HtmlGenericControl ErrorBloqueo = new HtmlGenericControl("div");
                //ErrorBloqueo.InnerHtml = string.Format("<div class=\"alert alert-warning\" role=\"alert\" style=\"padding-top: 1px;padding-bottom: 1px;\">{0}</div>", Pago.FormaPago);
                ErrorBloqueo.InnerHtml = Pago.FormaPago;
                r.Cells[2].Controls.Add(ErrorBloqueo);
                r.CssClass += " alert alert-warning row-dctos";
                ErrorBloqueo.Attributes["style"] = "text-align: center;";
            }
            else
            {
                //crea el ícono del estado del documento
                switch (Pago.EstadoDocumento)
                {
                    case "PA": //Partida abierta - Pendiente de pago
                        ((Label)r.Controls[0].FindControl("lblFechaPosiblePago")).Text = "-";
                        icon.Attributes["class"] = "glyphicon glyphicon-question-sign pend";
                        break;

                    case "PP": //Por pagar
                        icon.Attributes["class"] = "glyphicon glyphicon-info-sign porpag";
                        break;

                    case "PC": //Partida compensada - Pagadas
                        icon.Attributes["class"] = "glyphicon glyphicon-ok-sign pagada";
                        break;
                }
            }
        }

        public static void SetSelectAllCBL(CheckBoxList cbl, bool Seleccionar)
        {
            if (Seleccionar)
            {
                foreach (ListItem item in cbl.Items)
                    item.Selected = Seleccionar;

                cbl.Enabled = !Seleccionar;
            }
            else
                cbl.Enabled = !Seleccionar;
        }

        private static bool GetSelectedCBs(CheckBoxList cbl)
        {
            foreach (ListItem item in cbl.Items)
                if (item.Selected)
                    return true;

            return false;
        }

        protected void cbSeleccionarTodos_CheckedChanged(object sender, EventArgs e)
        {
            bool Checked = cbSeleccionarTodos.Checked;
            SetSelectAllCBL(cblEmpresa, Checked);
        }

        public string formatearRut(string rut)
        {
            int cont = 0;
            string format;
            if (rut.Length == 0)
            {
                return "";
            }
            else
            {
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                format = "-" + rut.Substring(rut.Length - 1);
                for (int i = rut.Length - 2; i >= 0; i--)
                {
                    format = rut.Substring(i, 1) + format;
                    cont++;
                    if (cont == 3 && i != 0)
                    {
                        format = "." + format;
                        cont = 0;
                    }
                }
                return format;
            }
        }

        private void CreateMsj(HtmlGenericControl div, bool visibleDivGrid, string classDiv, HtmlGenericControl titulo, string TextoTitulo, HtmlGenericControl mensaje, string TextoMensaje)
        {
            div.Visible = true;
            div.Attributes["class"] = classDiv;
            titulo.InnerText = TextoTitulo;
            mensaje.InnerHtml = TextoMensaje;
            dBusquedaGenerica.Visible = visibleDivGrid;
        }

        protected void gvBusquedaEspecifica_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                
                PagosEntity Pago = (PagosEntity)r.DataItem;
                CreaEstadoDocumento(Pago, r);
            }
        }
        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            try
            {
                Entities.UsuarioProveedorEntity usuario = PortalProveedorBL.GetTipoRazón(Convert.ToInt16(ViewState["id__prov"].ToString()));
                string razon = usuario.descripcionTipo;
                string NombreExcel = string.Format("Estado pago documentos - {0} al {1}", txtDesde.Text, txtHasta.Text);

                List<SociedadPagadoraEntity> ListData = new List<SociedadPagadoraEntity>();

                if (ViewState["ListPagos"] != null)
                    ListData = (List<SociedadPagadoraEntity>)ViewState["ListPagos"];
                else
                    ListData = PagosBL.GetPagos(ViewState["r__prov"].ToString(), GetValoresSeleccionados(cblEmpresa), ViewState["fec_min_seleccionada"].ToString(), ViewState["fec_max_seleccionada"].ToString(), ViewState["EstadoDocumentoABuscar"].ToString(), razon.ToString());
                 
                Export(NombreExcel, ListData);
            }
            catch
            {
                CreateMsj(p1Error, true, "alert alert-danger fade in", p1TituloError, "Error:", p1MsjError, "No se pudo exportar los datos a archivo excel. Vuelva a intentarlo y si el error persiste, inténtelo más tarde.");
            }
        }

        public void Export(string fileName, List<SociedadPagadoraEntity> List)
        {
            //The Clear method erases any buffered HTML output.
            HttpContext.Current.Response.Clear();
            //The AddHeader method adds a new HTML header and value to the response sent to the client.
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            //The ContentType property specifies the HTTP content type for the response.
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.Unicode;
            HttpContext.Current.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
            //Implements a TextWriter for writing information to a string. The information is stored in an underlying StringBuilder.
            using (StringWriter sw = new StringWriter())
            {
                //Writes markup characters and text to an ASP.NET server control output stream. This class provides formatting capabilities that ASP.NET server controls use when rendering markup to clients.
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    //  Create a form to contain the ListS
                    Table table = new Table();
                    TableRow row = new TableRow();

                    string[] header = new string[] { "Rut", "Razón Social", "Nro. Documento", "Moneda", "Monto", "Forma de Pago", "Nro. Cheque", "Fecha Posible de Pago", "Fecha Pago", "Estado", "Razon Social", "Rut",  "Monto Total" };

                    for (int x = 0; x < header.Length; x++)
                    {
                        TableHeaderCell hcell = new TableHeaderCell();
                        hcell.Text = header[x];
                        hcell.Attributes["style"] = "border: .5pt solid #000;font-size: 15px; background-color:silver";
                        row.Cells.Add(hcell);
                    }

                    table.Rows.Add(row);
                 
                        //  add each of the data item to the table
                        foreach (SociedadPagadoraEntity sociedad in List)
                    {
                        if (sociedad.Pagos.Count == 0) //sociedades en las que no se encontraron documentos
                        {
                            TableRow Fila_Sociedad = new TableRow();

                            InsertaRow(Fila_Sociedad, sociedad.Rut);
                            InsertaRow(Fila_Sociedad, sociedad.RazonSocial);
                            InsertaRow(Fila_Sociedad, string.Empty);
                            InsertaRow(Fila_Sociedad, string.Empty);
                            InsertaRow(Fila_Sociedad, string.Empty);
                            InsertaRow(Fila_Sociedad, string.Empty);
                            InsertaRow(Fila_Sociedad, string.Empty);
                            InsertaRow(Fila_Sociedad, string.Empty);
                            InsertaRow(Fila_Sociedad, string.Empty);
                            InsertaRow(Fila_Sociedad, string.Empty);
                            InsertaRow(Fila_Sociedad, string.Empty);
                            InsertaRow(Fila_Sociedad, string.Empty);
                            InsertaRow(Fila_Sociedad, string.Empty);
                            table.Rows.Add(Fila_Sociedad);
                        }
                        else
                        {
                            Entities.UsuarioProveedorEntity usuario = PortalProveedorBL.GetTipoRazón(Convert.ToInt16(ViewState["id__prov"].ToString()));
                            string razon = usuario.descripcionTipo;
                           
                                foreach (PagosEntity Pago in sociedad.Pagos)
                            {
                                TableRow Fila_Sociedad = new TableRow();

                                InsertaRow(Fila_Sociedad, sociedad.Rut);
                                InsertaRow(Fila_Sociedad, sociedad.RazonSocial);
                                InsertaRow(Fila_Sociedad, Pago.NroDocumento);
                                InsertaRow(Fila_Sociedad, Pago.Moneda);
                                InsertaRow(Fila_Sociedad, Pago.Monto.ToString());
                                InsertaRow(Fila_Sociedad, Pago.FormaPago);

                                if (Pago.DocumentoBloqueado)
                                {
                                    InsertaRow(Fila_Sociedad, string.Empty);
                                    InsertaRow(Fila_Sociedad, string.Empty);
                                    InsertaRow(Fila_Sociedad, string.Empty);
                                    InsertaRow(Fila_Sociedad, string.Empty);
                                    InsertaRow(Fila_Sociedad, string.Empty);
                                    InsertaRow(Fila_Sociedad, string.Empty);
                                    InsertaRow(Fila_Sociedad, string.Empty);
                                }
                                else
                                {
                                    InsertaRow(Fila_Sociedad, Pago.NroCheque.ToString() == "0" ? "-" : Pago.NroCheque.ToString());
                                    InsertaRow(Fila_Sociedad, Pago.FechaPago.ToString("dd-MM-yyyy"));
                                    InsertaRow(Fila_Sociedad, Pago.FechaCompensa == DateTime.MinValue ? "-" : Pago.FechaCompensa.ToShortDateString());
                                    InsertaRow(Fila_Sociedad, Pago.estado);
                                    InsertaRow(Fila_Sociedad, Pago.razon);
                                    InsertaRow(Fila_Sociedad, Pago.rut);
                                    if (razon == "F")
                                    {
                                        InsertaRow(Fila_Sociedad, Convert.ToString(string.Format("{0:n0}",Pago.total)));
                                    }
                                    else
                                    {
                                       
                                        InsertaRow(Fila_Sociedad, string.Empty);

                                    }
                                }
                                table.Rows.Add(Fila_Sociedad);
                            }
                        }
                    }

                    table.Attributes["style"] = "margin-bottom: 20px";

                    //  render the table into the htmlwriter
                    table.RenderControl(htw);
                    //  render the htmlwriter into the response
                    HttpContext.Current.Response.Write(sw.ToString());
                    HttpContext.Current.Response.End();
                }
            }
        }

        private void InsertaRow(TableRow Row, string Valor)
        {
            TableCell cell_FechaPago = new TableCell();
            cell_FechaPago.Text = Valor;
            Row.Cells.Add(cell_FechaPago);
            cell_FechaPago.Attributes["style"] = "border: .5pt solid #000;";
        }

        private void InicializacionCalendarios()
        {
            //ejemplo_desde.InnerText = DateTime.Now.AddMonths(-4).ToShortDateString();
            ejemplo_hasta.InnerText = FechaMaxima.ToShortDateString();
            info_maxfecha.InnerText = FechaMaxima.ToShortDateString();

            ////////////////txtDesde.Text = DateTime.Today.ToShortDateString();
            ////////////////txtHasta.Text = DateTime.Today.ToShortDateString();

            ////////////////if (!IsPostBack)
            ////////////////{
            ////////////////    //Crea calendario
            ////////////////    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "function pageLoad() {" +

            ////////////////                                    "$('.bs-pagination td table').each(function (index, obj) {" +
            ////////////////                                         "convertToPagination(obj)" +
            ////////////////                                     "});" +

            ////////////////                                    "var DATE_INFO = {" +
            ////////////////                                        string.Format("{0:yyyyMMdd}", FechaMinima) + ": { klass: \"highlight\", tooltip: \"Fecha mínima seleccionable.\" }," +
            ////////////////                                        string.Format("{0:yyyyMMdd}", FechaMaxima) + ": { klass: \"highlight\", tooltip: \"Fecha máxima seleccionable.\" }" +
            ////////////////                                    "};" +

            ////////////////                                    "function getDateInfo(date, wantsClassName) {" +
            ////////////////                                        "var as_number = Calendar.dateToInt(date);" +
            ////////////////                                        "return DATE_INFO[as_number];" +
            ////////////////                                    "};" +

            ////////////////                                    "Calendar.setup({" +
            ////////////////        //cont: "calendar-container",
            ////////////////                                        "inputField: \"txtDesde\"," +
            ////////////////                                        "trigger: \"calendario1\"," +
            ////////////////                                        "weekNumbers: false," +
            ////////////////        //selectionType: Calendar.SEL_MULTIPLE,
            ////////////////                                        "selection: Calendar.dateToInt(new Date())," +
            ////////////////                                        "min: " + string.Format("{0:yyyyMMdd}", FechaMinima) + "," +
            ////////////////                                        "max: " + string.Format("{0:yyyyMMdd}", FechaMaxima) + "," +
            ////////////////                                        "showtime: 12," +
            ////////////////                                        "dateFormat: \"%d-%m-%Y\"," +
            ////////////////                                        "onSelect: function () { this.hide() }," +
            ////////////////                                        "dateInfo: getDateInfo" +
            ////////////////                                    "});" +

            ////////////////                                    "Calendar.setup({" +
            ////////////////        //cont: "calendar-container",
            ////////////////                                        "inputField: \"txtHasta\"," +
            ////////////////                                        "trigger: \"calendario2\"," +
            ////////////////                                        "min: " + string.Format("{0:yyyyMMdd}", FechaMinima) + "," +
            ////////////////                                        "max: " + string.Format("{0:yyyyMMdd}", FechaMaxima) + "," +
            ////////////////                                        "weekNumbers: false," +
            ////////////////        //selectionType: Calendar.SEL_MULTIPLE,
            ////////////////                                        "selection: Calendar.dateToInt(new Date())," +
            ////////////////                                        "showtime: 12," +
            ////////////////                                        "dateFormat: \"%d-%m-%Y\"," +
            ////////////////                                        "onSelect: function () { this.hide() }," +
            ////////////////                                        "dateInfo: getDateInfo" +
            ////////////////                                    "});" +
            ////////////////                                "}", true);
            ////////////////}

            //obtiene sociedades pagadoras
            cblEmpresa.DataSource = SociedadPagadoraEntity.GetSociedadesPagadoras();
            cblEmpresa.DataBind();
            //inicializa el checkbox list, tickeadas o no tickeadas
            SetSelectAllCBL(cblEmpresa, false);
            CreaCheckBoxListEstadoDocumento();
         
        }

        public void CreaCheckBoxListEstadoDocumento()
        {
            cblEstadoDocumento.Items.Add(new ListItem("Pendiente de pago", "PA"));
            cblEstadoDocumento.Items.Add(new ListItem("Próximo a pagar", "PP"));
            cblEstadoDocumento.Items.Add(new ListItem("Pagada", "PC"));

            cblEstadoDocumento.DataBind();
           
        }
        
      
        protected void gvBusquedaFiltradaB_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Entities.UsuarioProveedorEntity usuario = PortalProveedorBL.GetTipoRazón(Convert.ToInt16(ViewState["id__prov"].ToString()));
            string razon = usuario.descripcionTipo;
            GridView gvBusquedaFiltradaB = (GridView)sender;
            gvBusquedaFiltradaB.PageIndex = e.NewPageIndex;
            string SociedadPagadora = gvBusquedaFiltradaB.DataMember;
            List<SociedadPagadoraEntity> ListPagos = new List<SociedadPagadoraEntity>();
            
            if (ViewState["ListPagos"] != null)
            {
                ListPagos = ((List<SociedadPagadoraEntity>)ViewState["ListPagos"]).Where(x => x.IdSociedad.ToString() == SociedadPagadora).ToList();

                //var items = ListPagos.Where(x => x.IdSociedad.ToString() == SociedadPagadora);

            }
            else
              
            ListPagos = PagosBL.GetPagos(ViewState["r__prov"].ToString(), SociedadPagadora, ViewState["fec_min_seleccionada"].ToString(), ViewState["fec_max_seleccionada"].ToString(), ViewState["EstadoDocumentoABuscar"].ToString(), razon.ToString());

            gvBusquedaFiltradaB.DataSource = ListPagos[0].Pagos;
            gvBusquedaFiltradaB.DataBind();
        }
        
        protected void btnBuscarProveedor_Click(object sender, EventArgs e)
        {
            
                string RutProveedor = txtRutProveedor.Text.Trim();

                if (string.IsNullOrEmpty(RutProveedor))
                {
                    proveedor.InnerText = "Ingrese el Rut de un proveedor para realizar una búsqueda.";
                }
                else
                {
                    RutProveedor = App_Code.Util.FormatoRut(RutProveedor);
                    bool RutValido = App_Code.Util.RutValido(RutProveedor);

                    if (RutValido)
                    {
                        int TipoUsuario = PortalProveedorBL.GetTipoUsuarioLogueado(); // 0 = usuario corporativo : 1 = usuario proveedor

                        if (TipoUsuario == 0)
                        {
                            bool EsAdmin = PortalProveedorBL.GetUsuarioCorporativoLogueado().EsAdmin == 1 ? true : false;

                            if (EsAdmin)
                            {
                                Tuple<ProveedorEntity, UsuarioProveedorEntity> Proveedor = PortalProveedorBL.GetProveedorExistente(txtRutProveedor.Text.Trim());

                                if (Proveedor == null)
                                    this.proveedor.InnerText = "Proveedor no existe";
                                else
                                {
                                    this.proveedor.InnerText = Proveedor.Item1.RazonSocial;
                                ViewState.Add("r__prov", Proveedor.Item1.Rut);
                                ViewState.Add("id__prov", PortalProveedorBL.GetMiIdProveedor(txtRutProveedor.Text));
                        
                                
                                btnBuscar.Visible = true;

                                    iframe.Src = "estadodcto.aspx?rut_proveedor=" + RutProveedor;
                                    iframe.DataBind();
                                }
                            }
                        }
                    }
                    else
                    {
                        proveedor.InnerText = "Rut inválido.";
                    }
                }
        }
	}
}