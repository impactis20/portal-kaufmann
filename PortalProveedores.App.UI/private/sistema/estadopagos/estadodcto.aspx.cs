﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private.sistema.estadopagos
{
	public partial class estadodcto : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string url = HttpContext.Current.Request.Url.AbsolutePath.Replace("/", string.Empty);
                //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "myUniqueKey", "if(!(window.self !== window.top)) self.parent.location='principal.aspx?val=sistema/ep/estadopago.aspx" + url + "';", true);

                //almaceno el rut del proveedor
                int TipoUsuario = PortalProveedorBL.GetTipoUsuarioLogueado();

                if (TipoUsuario == 10)
                {
                    ViewState.Add("prov_adm", PortalProveedorBL.GetProveedor(PortalProveedorBL.GetUsuarioProveedorLogueado().IdProveedor[0]).Rut);
                }
                else
                {
                    bool EsAdmin = PortalProveedorBL.GetUsuarioCorporativoLogueado().EsAdmin == 1 ? true : false;

                    //Si es administrador  y consulta un proveedor, almacena el rut ingresado
                    if (EsAdmin && Request.QueryString["rut_proveedor"] != null)
                    {
                        ViewState.Add("prov_adm", Request.QueryString["rut_proveedor"].ToString());
                    }
                }

                Page.Form.DefaultButton = btnBuscarDcto.UniqueID;
            }
        }

        protected void btnBuscarDcto_Click(object sender, EventArgs e)
        {
            string ValorIngresado = txtNroFactura.Text.Trim();

            try
            {
                if (ViewState["prov_adm"] == null)
                {
                    CreateMsj(p2Error, true, "alert alert-warning fade in", p2TituloError, "Atención:", p2MsjError, "Debe Ingresar el rut del proveedor.");

                }
                else
                {

                    if (App_Code.Util.isIntNumeric(ValorIngresado))
                    {
                        p2Error.Visible = false;
                        int NroDocumento = Convert.ToInt32(txtNroFactura.Text);

                        List<PagosEntity> Pago = new List<PagosEntity>();

                        if (ViewState["prov_adm"] != null)
                        {
                            ViewState.Add("id__prov", PortalProveedorBL.GetMiIdProveedor(ViewState["prov_adm"].ToString()));
                            Entities.UsuarioProveedorEntity usuario = PortalProveedorBL.GetTipoRazón(Convert.ToInt16(ViewState["id__prov"].ToString()));
                            string razon = usuario.descripcionTipo;
                            Pago = PagosBL.GetPago(ViewState["prov_adm"].ToString(), NroDocumento, razon.ToString());
                        }
                        else
                        {
                            ViewState.Add("id__prov", PortalProveedorBL.GetMiIdProveedor(ViewState["prov_adm"].ToString()));
                            Entities.UsuarioProveedorEntity usuario = PortalProveedorBL.GetTipoRazón(Convert.ToInt16(ViewState["id__prov"].ToString()));
                            string razon = usuario.descripcionTipo;
                            Pago = PagosBL.GetPago(ViewState["r__prov"].ToString(), NroDocumento, razon.ToString());
                        }
                        if (Pago != null && Pago.Count > 0)
                        {
                            //obtengo id sociedad pagadora, rut, razón social
                            SociedadPagadoraEntity SociedadPaga = SociedadPagadoraEntity.GetSociedadPagadora(Pago[0].SociedadPagadora);
                            RSPagadora.InnerText = SociedadPaga.RazonSocial;
                            RSRut.InnerText = SociedadPaga.Rut;

                            gvBusquedaEspecifica.DataSource = Pago;
                            gvBusquedaEspecifica.DataBind();

                            dBusquedaEspecifica.Visible = true;

                            ViewState.Add("doc_ingresado", NroDocumento);
                        }
                        else
                        {
                            CreateMsj(p2Error, true, "alert alert-danger fade in", p2TituloError, "No hay registros:", p2MsjError, string.Format("No se encontró ningún documento asociado al número \"{0}\".", NroDocumento));
                            dBusquedaEspecifica.Visible = false;
                        }
                    }
                    else
                    {
                        CreateMsj(p2Error, true, "alert alert-warning fade in", p2TituloError, "Atención:", p2MsjError, "El valor a ingresar debe ser numérico.");
                    }
                    }
                }
            catch
            {
                CreateMsj(p2Error, true, "alert alert-danger fade in", p2TituloError, "Error 486:", p2MsjError, "Se generó un problema interno, inténtelo más tarde.");
            }
        }

        public void CreaEstadoDocumento(PagosEntity Pago, GridViewRow r)
        {

            if (Pago.NroCheque == 0)
                ((Label)r.Controls[0].FindControl("lblNroCheque")).Text = "-";
            if (Pago.total == 0)

                ((Label)r.Controls[0].FindControl("lbltotal")).Text = "";
            HtmlGenericControl icon = ((HtmlGenericControl)r.Controls[0].FindControl("icon"));

            //verifica si el documento está bloqueado
            if (Pago.DocumentoBloqueado)
            {
                r.Cells[3].Visible = false;
                r.Cells[4].Visible = false;
                r.Cells[5].Visible = false;
                r.Cells[2].Attributes.Add("colspan", "4");

                ((Label)r.Controls[0].FindControl("lblFormaPago")).Visible = false;

                HtmlGenericControl ErrorBloqueo = new HtmlGenericControl("div");
                ErrorBloqueo.EnableViewState = true;
                ErrorBloqueo.ViewStateMode = System.Web.UI.ViewStateMode.Enabled;
                ErrorBloqueo.InnerHtml = string.Format("<div class=\"alert alert-danger\" role=\"alert\">{0}</div>", Pago.FormaPago);
                r.Cells[2].Controls.Add(ErrorBloqueo);
            }
            else
            {
                //crea el ícono del estado del documento
                switch (Pago.EstadoDocumento)
                {
                    case "PA": //Partida abierta - Pendiente de pago
                        ((Label)r.Controls[0].FindControl("lblFechaPosiblePago")).Text = "-";
                        icon.Attributes["class"] = "glyphicon glyphicon-question-sign pend";
                        break;

                    case "PP": //Por pagar
                        icon.Attributes["class"] = "glyphicon glyphicon-info-sign porpag";
                        break;

                    case "PC": //Partida compensada - Pagadas
                        icon.Attributes["class"] = "glyphicon glyphicon-ok-sign pagada";
                        break;
                }
            }
        }

        public string formatearRut(string rut)
        {
            int cont = 0;
            string format;
            if (rut.Length == 0)
            {
                return "";
            }
            else
            {
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                format = "-" + rut.Substring(rut.Length - 1);
                for (int i = rut.Length - 2; i >= 0; i--)
                {
                    format = rut.Substring(i, 1) + format;
                    cont++;
                    if (cont == 3 && i != 0)
                    {
                        format = "." + format;
                        cont = 0;
                    }
                }
                return format;
            }
        }

        private void CreateMsj(HtmlGenericControl div, bool visible, string classDiv, HtmlGenericControl titulo, string TextoTitulo, HtmlGenericControl mensaje, string TextoMensaje)
        {
            div.Visible = visible;
            div.Attributes["class"] = classDiv;
            titulo.InnerText = TextoTitulo;
            mensaje.InnerHtml = TextoMensaje;
            dBusquedaEspecifica.Visible = false;
        }

        protected void gvBusquedaEspecifica_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;
            Entities.UsuarioProveedorEntity usuario = PortalProveedorBL.GetTipoRazón(Convert.ToInt16(ViewState["id__prov"].ToString()));
            string razon = usuario.descripcionTipo;
            GridView gvBusquedaFiltradaB = (GridView)sender;
            if (razon == "P")
            {
                gvBusquedaFiltradaB.Columns[9].Visible = false;


            }
            else
            {
                gvBusquedaFiltradaB.Columns[9].Visible = true;
            }
            if (r.RowType == DataControlRowType.DataRow)
            {
                PagosEntity Pago = (PagosEntity)r.DataItem;
                CreaEstadoDocumento(Pago, r);
            }
        }

        public void Export(string fileName, List<PagosEntity> List)
        {
            //The Clear method erases any buffered HTML output.
            HttpContext.Current.Response.Clear();
            //The AddHeader method adds a new HTML header and value to the response sent to the client.
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            //The ContentType property specifies the HTTP content type for the response.
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.Unicode;
            HttpContext.Current.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
            //Implements a TextWriter for writing information to a string. The information is stored in an underlying StringBuilder.
            using (StringWriter sw = new StringWriter())
            {
                //Writes markup characters and text to an ASP.NET server control output stream. This class provides formatting capabilities that ASP.NET server controls use when rendering markup to clients.
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    //  Create a form to contain the ListS
                    Table table = new Table();
                    TableRow row = new TableRow();

                    string[] header = new string[] { "Rut", "Razón Social", "Nro. Documento", "Moneda", "Monto", "Forma de Pago", "Nro. Cheque", "Fecha Posible de Pago", "Fecha Pago", "Estado", "Razon Social", "Rut", "Monto Total" };

                    for (int x = 0; x < header.Length; x++)
                    {
                        TableHeaderCell hcell = new TableHeaderCell();
                        hcell.Text = header[x];
                        hcell.Attributes["style"] = "border: .5pt solid #000;font-size: 15px; background-color:silver";
                        row.Cells.Add(hcell);
                    }

                    table.Rows.Add(row);

                    //  add each of the data item to the table
                    foreach (PagosEntity Pago in List)
                    {
                        Entities.UsuarioProveedorEntity usuario = PortalProveedorBL.GetTipoRazón(Convert.ToInt16(ViewState["id__prov"].ToString()));
                        string razon = usuario.descripcionTipo;
                        if (Pago != null) //sociedades en las que no se encontraron documentos
                        {
                            TableRow Fila_Sociedad = new TableRow();
                            SociedadPagadoraEntity Sociedad = SociedadPagadoraEntity.GetSociedadPagadora(Pago.SociedadPagadora);
                            InsertaRow(Fila_Sociedad, Sociedad.Rut);
                            InsertaRow(Fila_Sociedad, Sociedad.RazonSocial);
                            InsertaRow(Fila_Sociedad, Pago.NroDocumento);
                            InsertaRow(Fila_Sociedad, Pago.Moneda);
                            InsertaRow(Fila_Sociedad, Pago.Monto.ToString());
                            InsertaRow(Fila_Sociedad, Pago.FormaPago);
                            if (Pago.DocumentoBloqueado)
                            {
                                InsertaRow(Fila_Sociedad, string.Empty);
                                InsertaRow(Fila_Sociedad, string.Empty);
                                InsertaRow(Fila_Sociedad, string.Empty);
                                InsertaRow(Fila_Sociedad, string.Empty);
                                InsertaRow(Fila_Sociedad, string.Empty);
                                InsertaRow(Fila_Sociedad, string.Empty);
                                InsertaRow(Fila_Sociedad, string.Empty);
                            }
                            else
                            {
                                //Establece FechaPago si es que la FechaCompensa es menor
                                Pago.FechaPago = Pago.FechaPago > Pago.FechaCompensa ? Pago.FechaCompensa : Pago.FechaPago;

                                InsertaRow(Fila_Sociedad, Pago.NroCheque.ToString() == "0" ? "-" : Pago.NroCheque.ToString());
                                InsertaRow(Fila_Sociedad, Pago.FechaPago.ToShortDateString());
                                InsertaRow(Fila_Sociedad, Pago.FechaCompensa == DateTime.MinValue ? "-" : Pago.FechaCompensa.ToShortDateString());
                                InsertaRow(Fila_Sociedad, Pago.estado);
                                InsertaRow(Fila_Sociedad, Pago.razon);
                                InsertaRow(Fila_Sociedad, Pago.rut);
                                if (razon == "F")
                                {
                                    InsertaRow(Fila_Sociedad, Convert.ToString(string.Format("{0:n0}", Pago.total)));
                                }
                                else
                                {
                                    
                                    InsertaRow(Fila_Sociedad, string.Empty);
                                }
                            }
                            table.Rows.Add(Fila_Sociedad);
                        }
                    }

                    table.Attributes["style"] = "margin-bottom: 20px";

                    //  render the table into the htmlwriter
                    table.RenderControl(htw);
                    //  render the htmlwriter into the response
                    HttpContext.Current.Response.Write(sw.ToString());
                    HttpContext.Current.Response.End();
                }
            }
        }

        private void InsertaRow(TableRow Row, string Valor)
        {
            TableCell cell_FechaPago = new TableCell();
            cell_FechaPago.Text = Valor;
            Row.Cells.Add(cell_FechaPago);
            cell_FechaPago.Attributes["style"] = "border: .5pt solid #000;";
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["prov_adm"] != null)
                {
                    ViewState.Add("id__prov", PortalProveedorBL.GetMiIdProveedor(ViewState["prov_adm"].ToString()));
                    Entities.UsuarioProveedorEntity usuario = PortalProveedorBL.GetTipoRazón(Convert.ToInt16(ViewState["id__prov"].ToString()));
                    string razon = usuario.descripcionTipo;
                    string NombreExcel = string.Format("Estado pago documento {0}", ViewState["doc_ingresado"].ToString());
                    List<PagosEntity> ListData = PagosBL.GetPago(ViewState["prov_adm"].ToString(), Convert.ToInt32(txtNroFactura.Text), razon.ToString());
                    Export(NombreExcel, ListData);
                }
                else
                {
                    ViewState.Add("id__prov", PortalProveedorBL.GetMiIdProveedor(ViewState["prov_adm"].ToString()));
                    Entities.UsuarioProveedorEntity usuario = PortalProveedorBL.GetTipoRazón(Convert.ToInt16(ViewState["id__prov"].ToString()));
                    string razon = usuario.descripcionTipo;
                    string NombreExcel = string.Format("Estado pago documento {0}", ViewState["doc_ingresado"].ToString());
                    List<PagosEntity> ListData = PagosBL.GetPago(ViewState["prov_adm"].ToString(), Convert.ToInt32(txtNroFactura.Text), razon.ToString());
                    Export(NombreExcel, ListData);
                }

                }
            
            catch
            {
                CreateMsj(p2Error, true, "alert alert-danger fade in", p2TituloError, "Error:", p2MsjError, "No se pudo exportar los datos a archivo excel. Vuelva a intentarlo y si el error persiste, inténtelo más tarde.");
            }
        }

    }
}
