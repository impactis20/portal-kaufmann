﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="PortalProveedores.App.UI.private.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link type="text/css" href="../Css/kf_style.css" rel="stylesheet" />  
    <link type="text/css" href="../Css/bootstrap.css" rel="stylesheet" />
    <link type="text/css" href="../Css/font-awesome.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="../Js/bootstrap.min.js"></script>
           
    <style>
        body{
            margin: 0px 15px;
            overflow:hidden;
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:SiteMapPath ID="sm" runat="server" PathSeparator=" > " ParentLevelsDisplayed="10" PathDirection="RootToCurrent" RenderCurrentNodeAsLink="false" ShowToolTips="true" CssClass="breadcrumb">            
        </asp:SiteMapPath>
        <div id="admin" runat="server" class="row">
            <div class="navbar navbar-default mx-wd">
                <div class="mrgn-page">
                    <h2 class="page-header" style="margin-top:25px">Sistemas para Proveedores</h2>
                    <div class="col-md-12" style="margin-bottom:15px">
                        <p id="msj" runat="server">Actualmente Kaufmann sólamente cuenta con un sistema para proveedores. Para su información, usted puede acceder las veces que lo necesite.</p>
                    </div>
                    <div class="col-md-12 col-md-offset-3">
                        <div class="col-md-6 well text-center">                            
                            <img class="img-thumbnail img-system" src="../Img/Sistemas/estado_pago_factura.png" />
                            <h3>Estado de Pagos</h3>
                            <p class="text-center">Véa el estado de pago de sus facturas.</p>
                            <br />
                            <div class="col-md-offset-3">
                                <div class="col-md-8">
                                    <asp:Button ID="accS" runat="server" CssClass="btn btn-primary btn-lg btn-block" Text="Acceder" Visible="true"/>
                                    <%--<asp:Button ID="Button1" runat="server" CssClass="btn btn-primary btn-lg btn-block" Text="Acceder" Visible="false"/>--%>
                                    <%--<asp:Button ID="envA" runat="server" CssClass="btn btn-success btn-lg btn-block" Text="Enviar Solicitud de Acceso" Visible="false"/>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../Js/iframeResizer.contentWindow.min.js"></script>
    </form>
</body>
</html>