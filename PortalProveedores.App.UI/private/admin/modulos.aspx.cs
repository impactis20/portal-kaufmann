﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private.admin
{
	public partial class modulos : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int IdSistema = 0;

                if (ViewState["id_sis"] == null)
                {
                    IdSistema = Convert.ToInt32(Request.QueryString["is"].ToString());
                    ViewState.Add("id_sis", IdSistema);
                }
                else
                {
                    IdSistema = Convert.ToInt32(ViewState["id_sis"].ToString());
                }

                ActualizaTodo(IdSistema);                
            }
        }

        private void ActualizaTodo(int IdSistema)
        {            
            List<SistemaEntity> Sistema = AdministradorSistemasBL.GetSistema(IdSistema);
            nombreSis.InnerText = Sistema[0].Nombre;

            List<SistemaEntity.Modulo> Mods = AdministradorSistemasBL.GetModulos(IdSistema);
            gvMods.DataSource = Mods;
            gvMods.DataBind();
        }

        //Todos los módulos se pueden editar (campos: Nombre, Página, Descripción)
        //Sólo los módulos permitidos (sin usuarios asociados) se pueden eliminar
        protected void btnAddMod_Click(object sender, EventArgs e)
        {
            NormalizaFormularioModulo();

            string NombreMod = txtNombrePag.Text.Trim();
            string PaginaMod = txtPag.Text.Trim();
            string DescMod = txtDescPag.Text.Trim();

            if (string.IsNullOrEmpty(NombreMod) || string.IsNullOrEmpty(PaginaMod))
            {
                MostrarMensaje("Campos incompletos", "Para agregar un módulo al sistema, debe llenar los campos con asterisco.", "text-danger");
                return;
            }
            else
            {
                if (!PaginaMod.Contains("."))
                {
                    SetColorTxT(dPaginaMod, "text-danger");
                    MostrarMensaje("Página inválida", "La página es incorrecta.", "text-danger");
                    return;
                }
                else
                {
                    //if (ViewState["modulos"] == null)
                    //{
                    //    ViewState.Add("idcont", 1);
                    //    List<SistemaEntity.Modulo> list = new List<SistemaEntity.Modulo>(); //crea lista inicial
                    //    list.Add(new SistemaEntity.Modulo(1, NombreMod, PaginaMod, DescMod)); //inserta el primer módulo
                    //    ViewState.Add("modulos", list); //guarda en viewstate
                    //}
                    //else
                    //{
                    //    int IdModulo = Convert.ToInt16(ViewState["idcont"].ToString()) + 1;
                    //    ((List<SistemaEntity.Modulo>)ViewState["modulos"]).Add(new SistemaEntity.Modulo(IdModulo, NombreMod, PaginaMod, DescMod)); //agrega modulo al viewstate
                    //    ViewState["idcont"] = IdModulo; //almaceno el contador de ID's
                    //}

                    int EstadoMod = chEstadoSis.Checked ? 1 : 2;

                    try
                    {
                        int IdSistema = Convert.ToInt32(ViewState["id_sis"].ToString());
                        AdministradorSistemasBL.AddModuloSistema(IdSistema, NombreMod, PaginaMod, DescMod, EstadoMod);
                        ActualizaTodo(IdSistema);
                    }
                    catch
                    {
                        MostrarMensaje("Error al agregar un módulo", "Ha ocurrido un error al agregar un módulo, favor de intentarlo más tarde.", "text-danger");
                    }

                    //Re-establece el formulario
                    txtDescPag.Text = string.Empty;
                    txtNombrePag.Text = string.Empty;
                    txtPag.Text = string.Empty;
                    chEstadoSis.Checked = false;
                }
            }

            ActualizaTodo(Convert.ToInt32(ViewState["id_sis"].ToString()));
        }

        private void NormalizaFormularioModulo()
        {
            ColorEstadoTxTMod(txtNombrePag, dNombreMod);
            ColorEstadoTxTMod(txtPag, dPaginaMod);
        }

        //Es llamado cuando hay algún dato ingresado con error en el txt
        private void SetColorTxT(HtmlGenericControl div, string Estilo)
        {
            div.Attributes["class"] = Estilo;
        }

        //Valida que los txt de la sessión "módulos del sistema" no estén vacíos
        private void ColorEstadoTxTMod(TextBox txt, HtmlGenericControl div)
        {
            if (txt.Text == string.Empty)
                div.Attributes["class"] = "has-error";
            else
                div.Attributes["class"] = string.Empty;
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("parent.abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }

        protected void gvMods_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow Row = e.Row;

            if (!(gvMods.EditIndex == e.Row.DataItemIndex)) //Entra cuando NO se trata del evento editar
            {
                if (Row.RowType == DataControlRowType.DataRow)
                {
                    SistemaEntity.Modulo modulo = (SistemaEntity.Modulo)Row.DataItem;

                    LinkButton btnQ = (LinkButton)(Row.Controls[0].FindControl("btnQ")); //btn Quitar

                    if (modulo.EsUsado == 2) // 2: No esta siendo utilizado, por ende es un módulo que se puede eliminar
                    {
                        btnQ.CommandName = "delRow";
                        btnQ.CommandArgument = modulo.IdModulo.ToString();
                    }
                    else
                    {
                        btnQ.Enabled = false;
                        btnQ.CssClass = "btn btn-default btn-xs disabled";
                    }

                    HtmlInputCheckBox chEstadoSis = (HtmlInputCheckBox)(Row.Controls[0].FindControl("chEstadoSis"));
                    if (modulo.Estado == 1)
                        chEstadoSis.Checked = true;
                    else
                        chEstadoSis.Checked = false;
                }
            }
            else
            {
                if (Row.RowType == DataControlRowType.DataRow)
                {
                    SistemaEntity.Modulo modulo = (SistemaEntity.Modulo)Row.DataItem;

                    CheckBox chEstadoSis = (CheckBox)(Row.Controls[0].FindControl("chEstadoSis"));
                    if (modulo.Estado == 1)
                        chEstadoSis.Checked = true;
                    else
                        chEstadoSis.Checked = false;
                }
            }
        }



        protected void gvMods_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "delRow")
            {
                int ID = Convert.ToInt16(e.CommandArgument);
                ((List<SistemaEntity.Modulo>)ViewState["modulos"]).RemoveAll(x => x.IdModulo == ID);

                gvMods.DataSource = ((List<SistemaEntity.Modulo>)ViewState["modulos"]);
                gvMods.DataBind();                
            }
        }

        protected void gvMods_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvMods.EditIndex = e.NewEditIndex;
            ActualizaTodo(Convert.ToInt32(ViewState["id_sis"]));
            btnAddMod.Attributes.Add("disabled", "disabled");
        }

        protected void gvMods_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                //Normaliza los textbox de la fila a editar
                HtmlGenericControl dNombre = (HtmlGenericControl)gvMods.Rows[e.RowIndex].Controls[0].FindControl("dNombre");
                TextBox txtNombre = (TextBox)gvMods.Rows[e.RowIndex].Controls[0].FindControl("txtN");

                btnAddMod.Attributes.Remove("disabled");

                if (string.IsNullOrEmpty(txtNombre.Text))
                {
                    //dNombre.Attributes["class"] += " has-error";
                    SetColorTxT(dNombre, "col-sm-4 has-error");
                    MostrarMensaje("Campo vacío", "El nombre del módulo no puede quedar en blanco. Escriba un nombre al módulo seleccionado o de lo contrario cancele esta operación.", "text-danger");
                    return;
                }
                else
                {
                    SetColorTxT(dNombre, "col-sm-4");
                }

                HtmlGenericControl dPagina = (HtmlGenericControl)gvMods.Rows[e.RowIndex].Controls[0].FindControl("dPagina");
                TextBox txtPagina = (TextBox)gvMods.Rows[e.RowIndex].Controls[0].FindControl("txtP");
                
                if (string.IsNullOrEmpty(txtPagina.Text))
                {
                    //dPagina.Attributes["class"] += " has-error";
                    SetColorTxT(dPagina, "col-sm-4 has-error");
                    MostrarMensaje("Campo vacío", "El página del módulo no puede quedar en blanco. Ingrese la página del módulo seleccionado o de lo contrario cancele esta operación.", "text-danger");
                    return;
                }
                else
                {
                    SetColorTxT(dPagina, "col-sm-4");
                }

                TextBox txtDesc = (TextBox)gvMods.Rows[e.RowIndex].Controls[0].FindControl("txtD"); //descripción
                TextBox txtPag = (TextBox)gvMods.Rows[e.RowIndex].Controls[0].FindControl("txtP"); //Página

                bool EstadoMod = ((CheckBox)gvMods.Rows[e.RowIndex].Controls[0].FindControl("chEstadoSis")).Checked;
                HiddenField IdModulo = (HiddenField)gvMods.Rows[e.RowIndex].Controls[0].FindControl("h_mod");


                AdministradorSistemasBL.SetModulo(Convert.ToInt32(IdModulo.Value), txtNombre.Text.Trim(), txtPag.Text.Trim(), txtDesc.Text.Trim(), (EstadoMod==true?1:2));
                MostrarMensaje("¡Operación exitosa!", "Los cambios han sido guardados.", "text-success");
                gvMods.EditIndex = -1;

                ActualizaTodo(Convert.ToInt32(ViewState["id_sis"].ToString()));
            }
            catch
            {
                MostrarMensaje("Error al guardar", "Los cambios no han sido guardados debido a un error interno, por favor inténtelo más tarde", "text.danger");
            }
        }

        protected void gvMods_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvMods.EditIndex = -1;
            ActualizaTodo(Convert.ToInt32(ViewState["id_sis"]));
            btnAddMod.Attributes.Remove("disabled");
        }
	}
}