﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private.admin
{
	public partial class agregarsistema : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            NormalizaFormularioRegistro();

            string NombreSis = txtNombreSis.Text.Trim();
            string URLSis = txtUrlSis.Text.Trim();
            string UserDesaSis = txtUDSis.Text.Trim();
            string UserSoli = txtUSSis.Text.Trim();
            string DescSis = txtDescSis.Text.Trim();

            if (string.IsNullOrEmpty(NombreSis) || string.IsNullOrEmpty(URLSis) || string.IsNullOrEmpty(UserDesaSis) || string.IsNullOrEmpty(UserSoli) || string.IsNullOrEmpty(DescSis))
            {
                MostrarMensaje("Campos incompletos", "Para agregar un sistema, debe llenar los campos con asterisco.<br><br><label class=\"label label-info\">Importante</label>&nbsp;<b>Vuelva a cargar la imagen del sistema si es que ya lo había hecho.<b>", "text-danger");
                return;
            }
            else
            {
                if (ValidaUrl())
                {
                    bool userDesaOk = Convert.ToBoolean(ViewState["user_desa"]);
                    bool userSoliOk = Convert.ToBoolean(ViewState["user_soli"]);

                    if (!userDesaOk)
                    {
                        SetColorTxT(dUD, "form-group has-error");
                        MostrarMensaje("Usuario desarrollador incorrecto", "El usuario desarrollador ingresado es inválido, corríjalo.<br><br><b>Vuelva a cargar la imagen del sistema si es que ya lo había hecho.</b>", "text-danger");
                        return;
                    }
                    else if (!userSoliOk)
                    {
                        SetColorTxT(dUS, "form-group has-error");
                        MostrarMensaje("Usuario solicitante incorrecto", "El usuario solicitate ingresado es inválido, corríjalo.<br><br><b>Vuelva a cargar la imagen del sistema si es que ya lo había hecho.<b>", "text-danger");
                        return;
                    }

                    int IdSistema = 0;

                    if (fArchivo.HasFile && fArchivo.PostedFile.ContentType.Contains("image")) //Si adjunto imagen, se almacena y se guarda en la bd
                    {
                        IdSistema = AdministradorSistemasBL.AddSistema(NombreSis, URLSis, UserDesaSis, UserSoli, DescSis, fArchivo.FileName);
                        DespuesDeGuardar(IdSistema, NombreSis);
                    }
                    else
                    {
                        IdSistema = AdministradorSistemasBL.AddSistema(NombreSis, URLSis, UserDesaSis, UserSoli, DescSis, "imagen_not_found.png");
                        DespuesDeGuardar(IdSistema, NombreSis);
                    }
                }
                else
                {
                    MostrarMensaje("URL Inválida", "La URL ingresa es incorrecta, corríjala.<br><br>Vuelva a cargar la imagen del sistema si es que ya lo había hecho.", "text-success");
                    return;
                }
            }
        }

        private void DespuesDeGuardar(int IdSistema, string NombreSis)
        {
            if (IdSistema == -1) //-1: Si el registro no se generó en la base de datos, entonces borro la imagen que se guardó en el servidor.
            {
                MostrarMensaje("Error en la base de datos", "Se generó un error interno en la base de datos al intentar guardar los datos ingresados. Por favor vuelva a intentarlo.", "text-danger");
                return;
            }
            else
            {
                string RutaDestino = string.Format("{0}/{1}_{2}", Server.MapPath("~/Ls"), IdSistema, fArchivo.FileName);
                fArchivo.SaveAs(RutaDestino);

                //Limpia formulario
                txtNombreSis.Text = string.Empty;
                txtUrlSis.Text = string.Empty;
                txtUDSis.Text = string.Empty;
                txtUSSis.Text = string.Empty;
                txtDescSis.Text = string.Empty;
                lblValUD.InnerHtml = string.Empty;
                lblValUS.InnerHtml = string.Empty;

                string msj = string.Format("El sistema <b>{0}</b> ha sido agregado correctamente al portal, pero queda automáticamente con estado <b>Offline</b>.<br><br>Para dejar el sistema en modalidad <b>Online</b>, debe agregar los módulos (páginas) del sistema y los roles de usuario. Para agregar los módulos y roles del sistema, vaya a la <a href=\"principal.aspx?val=admin/sistemas.aspx\">lista de sistemas</a> y busque el sistema recientemente agregado, posterior haga click en el botón <kbd>Opciones <span class=\"caret\"></span></kbd> y se le desplegarán las acciones: Módulos y Roles.", NombreSis, IdSistema);
                MostrarMensaje("Sistema Agregado", msj, "text-success");
                return;
            }
        }

        protected void txtUDSis_TextChanged(object sender, EventArgs e)
        {
            bool Respuesta = CreaBusquedaUsuario(dUD, lblValUD, txtUDSis.Text.Trim());
            ViewState.Add("user_desa", Respuesta);

            if (Respuesta)
                txtUSSis.Focus();
            else
                txtUDSis.Focus(); //Si el usuario es incorrecto, dejo el foco en el mismo textbox
        }

        protected void txtUSSis_TextChanged(object sender, EventArgs e)
        {
            bool Respuesta = CreaBusquedaUsuario(dUS, lblValUS, txtUSSis.Text.Trim());
            ViewState.Add("user_soli", Respuesta);

            if (Respuesta)
                txtDescSis.Focus();
            else
                txtUSSis.Focus(); //Si el usuario es incorrecto, dejo el foco en el mismo textbox
        }

        private bool CreaBusquedaUsuario(HtmlGenericControl div, HtmlGenericControl span, string UserName)
        {
            if (string.IsNullOrEmpty(UsuarioBL.GetUserName(UserName)))
            {
                span.InnerHtml = "<div class='alert alert-danger alert-dismissible' role='alert'>" +
                    //"<button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>" +
                                     "<div class='row'>" +
                                         "<div class='col-md-11 text-overflow'>" +
                                            "<i class='glyphicon glyphicon-exclamation-sign'>&nbsp;</i><span>El usuario \"" + UserName + "\" no fue encontrado</span>" +
                                         "</div>" +
                                     "</div>" +
                                 "</div>";

                SetColorTxT(div, "form-group has-error");

                return false;
            }
            else
            {
                string Email = UsuarioBL.GetEmail(UserName);
                string Nombre = UsuarioBL.GetNombre(UserName) + " " + UsuarioBL.GetApellido(UserName);

                span.InnerHtml = "<div class='alert alert-warning alert-dismissible' role='alert'>" +
                    //"<button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>" +                                     
                                     "<div class='row'>" +
                                         "<div class='col-md-11 text-overflow'>" +
                                             "<i class='glyphicon glyphicon-tag'>&nbsp;</i><span>" + Nombre + "</span>" +
                                         "</div>" +
                                         "<div class='col-md-11 text-overflow'>" +
                                             "<i class='glyphicon glyphicon-envelope'>&nbsp;</i><span>" + Email + "</span>" +
                                         "</div>" +
                                     "</div>" +
                                 "</div>";

                SetColorTxT(div, "form-group");

                return true;
            }
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("parent.abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }

        protected void txtUrl_TextChanged(object sender, EventArgs e)
        {
            ValidaUrl();
        }

        private bool ValidaUrl()
        {
            string Url = txtUrlSis.Text.Trim();

            if (string.IsNullOrWhiteSpace(Url))
            {
                lblUrlMsj.Text = "Campo vacío.";

                return false;
            }
            else if (Url.Contains(" "))
            {
                lblUrlMsj.Text = "La Url entre medio no puede tener espacios en blanco.";
                lblUrlMsj.CssClass = "text-danger";
                SetColorTxT(dUrl, "form-group has-error");
                return false;
            }
            else
            {
                Uri uriResult;
                bool result = Uri.TryCreate(txtUrlSis.Text.Trim(), UriKind.Absolute, out uriResult) && uriResult.Scheme == Uri.UriSchemeHttp;

                if (result)
                {
                    lblUrlMsj.Text = string.Empty;
                    SetColorTxT(dUrl, "form-group");
                    txtUDSis.Focus();
                    return true;
                }
                else
                {
                    lblUrlMsj.Text = "La Url es inválida.";
                    lblUrlMsj.CssClass = "text-danger";
                    return false;
                }
            }
        }

        //Es llamado cuando hay algún dato ingresado con error en el txt
        private void SetColorTxT(HtmlGenericControl div, string Estilo)
        {
            div.Attributes["class"] = Estilo;
        }

        private void NormalizaFormularioRegistro()
        {
            ColorEstadoTxT(txtNombreSis, dNombre);
            ColorEstadoTxT(txtUrlSis, dUrl);
            ColorEstadoTxT(txtUDSis, dUD);
            ColorEstadoTxT(txtUSSis, dUS);
            ColorEstadoTxT(txtDescSis, dDesc);
        }

        //Valida que los txt no estén vacíos
        private void ColorEstadoTxT(TextBox txt, HtmlGenericControl div)
        {
            if (txt.Text == string.Empty)
                div.Attributes["class"] = "form-group has-error";
            else
                div.Attributes["class"] = "form-group";
        }
	}
}