﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private.admin
{
	public partial class consultas : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetConsultas();
                //timer.Interval = Convert.ToInt32(ConfigurationManager.AppSettings["RefreshGrid"]);
            }
        }

        protected void gvConsultas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvConsultas.PageIndex = e.NewPageIndex;
            GetConsultas();
        }

        private void GetConsultas()
        {
            try
            {
                int Estado = Convert.ToInt32(ddlFiltro.SelectedItem.Value);
                List<UsuarioProveedorEntity.Consulta> Consultas = AdministradorSistemasBL.GetConsultas(Estado);
                gvConsultas.DataSource = Consultas;
                gvConsultas.DataBind();

                if (Consultas == null)
                    cont.InnerText = "0";
                else
                    cont.InnerText = Consultas.Count.ToString();
            }
            catch
            {
                MostrarMensaje("Error temporal (410)", "Error interno al intentar cargar las consultas desde la base de datos.", "text-danger");
            }
        }

        protected void timer_Tick(object sender, EventArgs e)
        {
            GetConsultas();
        }

        protected void gvConsultas_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow Row = e.Row;

            if (Row.RowType == DataControlRowType.DataRow)
            {
                UsuarioProveedorEntity.Consulta Fila = (UsuarioProveedorEntity.Consulta)Row.DataItem;

                //Verifica largo del mensaje
                if (Fila.Mensaje.Length < 70)
                    Fila.Mensaje = Fila.Mensaje.Substring(0, Fila.Mensaje.Length);
                else
                    Fila.Mensaje = Fila.Mensaje.Substring(0, 70);

                //Da formato a la fecha
                Label lblFec = ((Label)Row.FindControl("lblFec"));
                DateTime Fecha = Convert.ToDateTime(Fila.Fecha);

                if (Fecha.ToShortDateString() == DateTime.Today.ToShortDateString())
                    lblFec.Text = Fecha.ToShortTimeString(); //string.Format("{0: hh:mm", Fecha);
                else
                    lblFec.Text = Fecha.ToShortDateString();

                //icono 
                HtmlGenericControl icon = ((HtmlGenericControl)Row.Controls[0].FindControl("icon"));

                if (string.IsNullOrEmpty(Fila.RespuestaUsuario))
                    icon.Attributes["class"] = "glyphicon glyphicon-question-sign pend";
                else
                    icon.Attributes["class"] = "glyphicon glyphicon-ok-sign resp";
            }
        }

        protected void gvConsultas_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "VC")
            {
                int Id = Convert.ToInt32(e.CommandArgument);
                string Ruta = string.Format("Consulta.aspx?ic={0}", Id);
                Response.Redirect(Ruta);
            }
        }

        protected void ddlFiltro_TextChanged(object sender, EventArgs e)
        {
            GetConsultas();
        }

        protected void ddlPg_TextChanged(object sender, EventArgs e)
        {
            gvConsultas.PageSize = Convert.ToInt16(ddlPg.SelectedItem.Value);
            GetConsultas();
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("parent.abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            GetConsultas();
        }
	}
}