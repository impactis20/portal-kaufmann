﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private.admin
{
	public partial class registrosproveedores : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetRegistros();
                //timer.Interval = Convert.ToInt32(ConfigurationManager.AppSettings["RefreshGrid"]);
            }
        }

        protected void ddlFiltro_TextChanged(object sender, EventArgs e)
        {
            GetRegistros();
        }

        protected void gvRegistros_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName != "Page")
            {
                switch (e.CommandName)
                {
                    case "V":
                        string Url = string.Format("RegistroProveedor.aspx?is={0}", e.CommandArgument);
                        Response.Redirect(Url);
                        break;
                }

                //Actualizar GridView
                GetRegistros();
            }
        }

        protected void gvRegistros_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow Row = e.Row;

            if (Row.RowType == DataControlRowType.DataRow)
            {
                Tuple<ProveedorEntity, UsuarioProveedorEntity> Fila = (Tuple<ProveedorEntity, UsuarioProveedorEntity>)e.Row.DataItem;
                HtmlGenericControl state = ((HtmlGenericControl)Row.Controls[0].FindControl("state")); //div que cambia de color según el estado de la solicitud


                switch (Fila.Item2.EstadoSolicitud)
                {
                    case 0: //Pendiente
                        state.Attributes["class"] = string.Empty;
                        break;
                    case 1: //Aceptado
                        state.Attributes["class"] = "alert alert-success";
                        break;
                    case 2: //Rechazado
                        state.Attributes["class"] = "alert alert-danger";
                        break;
                }

                Label lblFech = ((Label)Row.Controls[0].FindControl("lblFec"));
                DateTime FechReg = Convert.ToDateTime(Fila.Item1.Fecha);

                if (FechReg.ToShortDateString() == DateTime.Now.ToShortDateString())
                    lblFech.Text = FechReg.ToShortTimeString();
                else
                    lblFech.Text = FechReg.ToShortDateString();
            }
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("confirm('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }

        protected void gvRegistros_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvRegistros.PageIndex = e.NewPageIndex;
            GetRegistros();
        }

        private void GetRegistros()
        {
            int Valor = Convert.ToInt32(ddlFiltro.SelectedItem.Value);
            List<Tuple<ProveedorEntity, UsuarioProveedorEntity>> List = AdministradorSistemasBL.GetRegistrosProveedores(Valor);

            if (List == null)
                cont.InnerText = "0";
            else
                cont.InnerText = List.Count.ToString();

            gvRegistros.DataSource = List;
            gvRegistros.DataBind();
        }

        protected void timer_Tick(object sender, EventArgs e)
        {
            GetRegistros();
        }

        protected void ddlPg_TextChanged(object sender, EventArgs e)
        {
            gvRegistros.PageSize = Convert.ToInt32(ddlPg.SelectedItem.Value);
            GetRegistros();
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            GetRegistros();
        }
	}
}