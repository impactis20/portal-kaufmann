﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private.admin
{
	public partial class registroproveedor : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    int IdSoli = Convert.ToInt32(Request.QueryString["is"]);

                    Tuple<ProveedorEntity, UsuarioProveedorEntity, UsuarioProveedorEntity.UsuarioKaufmann, string> UsuarioReg = AdministradorSistemasBL.GetRegistroProveedor(IdSoli);

                    lblID.Text = string.Format("ID Solicitud: {0}", UsuarioReg.Item1.IdSolicitud);
                    lblFech.Text = string.Format("Fecha Solicitud: {0}", UsuarioReg.Item1.Fecha);

                    lblRut.Text = UsuarioReg.Item1.Rut;
                    lblRS.Text = UsuarioReg.Item1.RazonSocial;

                    lblRun.Text = UsuarioReg.Item2.Run;
                    lblNom.Text = string.Format("{0} {1} {2}", UsuarioReg.Item2.Nombre, UsuarioReg.Item2.ApellidoPat, UsuarioReg.Item2.ApellidoMat);
                    lblE.Text = UsuarioReg.Item2.Email;
                    lblF.Text = string.Format("(+56) {0}", UsuarioReg.Item2.Fono);
                    lblFM.Text = string.Format("(+56) 9 {0}", UsuarioReg.Item2.FonoMovil);
                    txtM.InnerHtml = UsuarioReg.Item2.MotivoRegistro.Replace("\r\n", "<br>");

                    switch (UsuarioReg.Item2.EstadoSolicitud)
                    {
                        case 0:
                            state.Attributes["class"] = "alert alert-warning text-center";
                            state.InnerHtml = "<strong>¡PENDIENTE!</strong>";
                            break;
                        case 1:
                            state.Attributes["class"] = "alert alert-success text-center";
                            state.InnerHtml = string.Format("<strong>{0}</strong>", "¡ACEPTADA!");
                            txtMotivo.Text = UsuarioReg.Item4;
                            txtMotivo.Enabled = false;
                            lblAccionDec.Text = "Decisión por:";
                            responsable.InnerText = string.Format("{0} {1} ({2}) {3}", UsuarioReg.Item3.Nombre, UsuarioReg.Item3.ApellidoPat, UsuarioReg.Item3.UserName, UsuarioReg.Item3.Email); ;
                            buttons.Visible = false;
                            break;
                        case 2:
                            state.Attributes["class"] = "alert alert-danger text-center";
                            state.InnerHtml = string.Format("<strong>{0}</strong>", "¡RECHAZADO!");
                            txtMotivo.Text = UsuarioReg.Item4;
                            txtMotivo.Enabled = false;
                            lblAccionDec.Text = "Decisión por:";
                            responsable.InnerText = string.Format("{0} {1} ({2}) {3}", UsuarioReg.Item3.Nombre, UsuarioReg.Item3.ApellidoPat, UsuarioReg.Item3.UserName, UsuarioReg.Item3.Email); ;
                            buttons.Visible = false;
                            break;
                    }

                    ViewState.Add("is", IdSoli); //Guardo ID solicitud en viewstate
                    ViewState.Add("em", UsuarioReg.Item2.Email); //Guardo en viewstate para informar mediante correo electrónico la decisión
                    ViewState.Add("nup", string.Format("{0} {1}", UsuarioReg.Item2.Nombre, UsuarioReg.Item2.ApellidoPat));
                }
            }
            catch
            {
                MostrarMensaje("Error temporal (710)", "Error interno al intentar cargar la consulta.", "text-danger");
            }
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("parent.abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }

        protected void btnA_Click(object sender, EventArgs e)
        {
            InsertaDecision(1); //Acepta
        }

        protected void btnR_Click(object sender, EventArgs e)
        {
            //Agregar motivo del rechazo

            InsertaDecision(2); //Rechaza
        }

        private void InsertaDecision(int Decision)
        {
            int IdSoli = Convert.ToInt32(ViewState["is"]);

            if (txtMotivo.Text == string.Empty && Decision == 2)
            {
                MostrarMensaje("Justifique su decisión", "Ingrese el motivo de su decisión.", "text-danger");
                dDecision.Attributes["class"] += "form-group has-error";
            }
            else
            {
                int RespuestaAC = AdministradorSistemasBL.InsertRegistroProveedor(IdSoli, Decision, PortalProveedorBL.GetUsuarioCorporativoLogueado().UserName, txtMotivo.Text, DateTime.Now);

                if (RespuestaAC == 1 || RespuestaAC == 2) // 1= Decisión generada con éxito
                {
                    AvisaSolicitudRegistro(Decision); //Envía correo electrónico avisando
                    string URL = string.Format("RegistroProveedor.aspx?is={0}&em=2", IdSoli);
                    Response.Redirect(URL);
                }
                else
                {
                    MostrarMensaje("Error temporal (711)", "La decisión no fue guardada, por favor inténtelo más tarde o reporte su problema.", "text-danger");
                    return;
                }
            }
        }

        private void AvisaSolicitudRegistro(int Decision)
        {
            string Sig = Decision == 1 ? "aceptada" : "rechazada";

            string Email = ViewState["em"].ToString();
            string NombreUserProv = ViewState["nup"].ToString();
            string Asunto = Decision == 1 ? "Bienvenido al Portal de Proveedores - Kaufmann" : "Portal de Proveedores - Kaufmann";
            string Mensaje = string.Format("Estimado {0}, su solicitud de registro en nuestro Portal de Proveedores, fue <b>{1}</b>.", NombreUserProv, Sig);

            switch (Decision)
            {
                case 1://aceptada
                    string UrlLogin = string.Format("{0}/Proveedores.aspx?e={1}", HttpContext.Current.Request.Url.Authority, App_Code.Util.Encriptar(Email));
                    Mensaje += string.Format("<br><br>Si desea ingresar ahora, haga click <a href='{0}'>aquí</a>.<br>", UrlLogin);
                    break;

                case 2://rechazada
                    string UrlContactenos = string.Format("{0}/Proveedores.aspx#contactenos", HttpContext.Current.Request.Url.Host);
                    Mensaje += string.Format("El motivo es el siguiente: {0}<br><br>Si tiene dudas o consultas, haga click <a href='{1}'>aquí</a> y contáctese con nuestros administradores.", txtMotivo.Text, UrlContactenos);
                    break;
            }

            bool EnviaEmail = WebServices.EnviaCorreo(Email, Asunto, Mensaje);

            //bool EnviaEmail = WebServices.EnviaCorreo(Email, Asunto, Mensaje);

            if (!EnviaEmail)
                MostrarMensaje("Aviso no generado", string.Format("No se pudo avisar vía correo electrónico al usuario proveedor de esta decisión. <br><br>Favor de envíar el correo manualmente al usuario \"{0}\", avisando de que fue <b>{1} en este sistema.</b>.", Email, Decision), "text-danger");
        }
	}
}