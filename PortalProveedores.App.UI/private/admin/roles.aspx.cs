﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private.admin
{
	public partial class roles : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["is"] != null)
            {
                gvRolesSistema.DataSource = GetRolesSistema();
                gvRolesSistema.DataBind();
            }
            else
            {
                //página inválida
            }
        }

        private List<SistemaEntity.Roles> GetRolesSistema()
        {
            return AdministradorSistemasBL.GetRolesSistema(Convert.ToInt32(Request.QueryString["is"].ToString()));
        }

        protected void btnTestear_Click(object sender, EventArgs e)
        {

        }

        protected void btnAddRol_Click(object sender, EventArgs e)
        {

        }

        protected void gvRolesSistema_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gvRolesSistema_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }
	}
}