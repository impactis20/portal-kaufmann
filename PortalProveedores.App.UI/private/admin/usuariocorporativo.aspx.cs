﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private.admin
{
	public partial class usuariocorporativo : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                if (Request.QueryString["buscar"] != null)
                    GetUsuarios(Request.QueryString["buscar"]);
        }

        protected void btnBU_Click(object sender, EventArgs e)
        {
            string UserName = txtBU.Text.Trim().ToLower();

            if (UserName.Length > 2)
            {
                ColorEstadoTxT(txtBU, dBU);

                string Url = string.Format("usuariocorporativo.aspx?buscar={0}", UserName);
                Response.Redirect(Url);
            }
            else
            {
                MostrarMensaje("Búsqueda incorrecta", "Para buscar usuarios debe ingresar un mínimo de 3 caracteres.", "text-danger");
                return;
            }
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("parent.abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }

        private void ColorEstadoTxT(TextBox txt, HtmlGenericControl div)
        {
            if (txt.Text == string.Empty)
                div.Attributes["class"] = "input-group has-error";
            else
                div.Attributes["class"] = "input-group";
        }

        protected void gvUsers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow Row = e.Row;

            if (Row.RowType == DataControlRowType.DataRow)
            {
                UsuarioProveedorEntity.UsuarioKaufmann user = (UsuarioProveedorEntity.UsuarioKaufmann)Row.DataItem;
                
                HtmlGenericControl lblEstadoUsuario = ((HtmlGenericControl)Row.Controls[0].FindControl("lblEstadoUsuario"));

                HyperLink adm_div = ((HyperLink)Row.Controls[0].FindControl("adm"));

                LinkButton btnNoEs = ((LinkButton)Row.Controls[0].FindControl("btnNoEs"));
                LinkButton btnEsAdm = ((LinkButton)Row.Controls[0].FindControl("btnEsAdm"));

                LinkButton btnDarAccesoUsuario = ((LinkButton)Row.Controls[0].FindControl("btnDarAccesoUsuario"));
                LinkButton btnDesbloquearUsuario = ((LinkButton)Row.Controls[0].FindControl("btnDesbloquearUsuario"));
                LinkButton btnBloquearUsuario = ((LinkButton)Row.Controls[0].FindControl("btnBloquearUsuario"));

                EstadoUsuario(lblEstadoUsuario, adm_div, btnNoEs, btnEsAdm, btnDarAccesoUsuario, btnBloquearUsuario, btnDesbloquearUsuario, user.AccesoPortal, user.UserName, user.EsAdmin);
            }
        }

        private void EstadoUsuario(HtmlGenericControl lblEstadoUsuario, HyperLink adm_link, LinkButton btnNoEs, LinkButton btnEsAdm, LinkButton btnDarAccesoUsuario, LinkButton btnBloquearUsuario, LinkButton btnDesbloquearUsuario, int EstadoUsuario, string UserName, int EsAdmin)
        {
            //Estado
            // 0: Usuario "nuevo" que no tiene acceso al portal Web
            // 1: Tiene permiso para acceder al portal Web
            // 2: Usuario bloqueado
            // -1: Error

            string RolUsuario = string.Empty;

            if (EsAdmin == 1)
            {
                RolUsuario = " (Administrador)";
                btnNoEs.Visible = true;
                btnEsAdm.Visible = false;
            }
            else
            {
                btnNoEs.Visible = false;
                btnEsAdm.Visible = true;
            }

            switch (EstadoUsuario)
            {
                case 0:
                    lblEstadoUsuario.InnerHtml = string.Format("<i class='glyphicon glyphicon-user'>&nbsp;</i><b>{0}{1}:</b> Sin acceso al Portal Web.", UserName, RolUsuario);
                    lblEstadoUsuario.Attributes["class"] = "label label-default";
                    adm_link.Visible = false;
                    btnDarAccesoUsuario.Visible = true;
                    btnBloquearUsuario.Visible = false;
                    btnDesbloquearUsuario.Visible = false;
                    return;
                case 1:
                    lblEstadoUsuario.InnerHtml = string.Format("<i class='glyphicon glyphicon-user'>&nbsp;</i><b>{0}{1}:</b> Con acceso al Portal Web.", UserName, RolUsuario);
                    lblEstadoUsuario.Attributes["class"] = "label label-success";
                    adm_link.Visible = true;
                    btnBloquearUsuario.Visible = true;
                    btnDarAccesoUsuario.Visible = false;
                    btnDesbloquearUsuario.Visible = false;
                    return;
                case 2:
                    lblEstadoUsuario.InnerHtml = string.Format("<i class='glyphicon glyphicon-user'>&nbsp;</i><b>{0}{1}:</b> Usuario Bloqueado.", UserName, RolUsuario);
                    lblEstadoUsuario.Attributes["class"] = "label label-warning";
                    adm_link.Visible = true;
                    btnBloquearUsuario.Visible = false;
                    btnDarAccesoUsuario.Visible = false;
                    btnDesbloquearUsuario.Visible = true;
                    return;
            }
        }

        protected void gvUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (App_Code.Util.isIntNumeric(e.CommandName))
            {
                int Command = Convert.ToInt32(e.CommandName);

                switch (Command)
                {
                    case 0:
                        string UserName = e.CommandArgument.ToString().Split('|')[0];
                        string Email = e.CommandArgument.ToString().Split('|')[1];
                        int Respuesta = AdministradorSistemasBL.InsertUserCorpPortal(UserName, Email, DateTime.Now);
                        break;
                    case 1:
                        SetAccesoUsuarioCorp(e.CommandArgument.ToString(), 1, "El usuario ha sido <b>desbloqueado</b>, por lo que se encuentra en condiciones de acceder al portal Web."); // 1: desbloquea usuario
                        break;
                    case 2:
                        SetAccesoUsuarioCorp(e.CommandArgument.ToString(), 2, "El usuario ha sido <b>bloqueado</b>, por lo que no podrá acceder al portal Web.."); // 2: bloquea usuario
                        break;
                    case 3:
                        SetRolUsuarioCorp(e.CommandArgument.ToString()); //la lógica de true o false (usuario corp administrador), está hecha en el package
                        break;
                }

                GetUsuarios(ViewState["user"].ToString());
            }
        }

        private void SetRolUsuarioCorp(string UserName)
        {
            int Respuesta = AdministradorSistemasBL.SetUserCorpToAdm(UserName);

            switch (Respuesta)
            {
                case 1: //se asigna como administrador
                    MostrarMensaje("¡Acción realizada!", string.Format("El usuario <b>{0}</b> ahora es administrador de este portal.", UserName), "text-success");
                    break;
                case 2: //dejo de ser administrador
                    MostrarMensaje("¡Acción realizada!", string.Format("El usuario <b>{0}</b> ya no es administrador de este portal.", UserName), "text-success");
                    break;
                case -1: //error en la bd
                    MostrarMensaje("Error temporal 577", "No se pudo realizar esta acción. Favor de intentar más tarde.", "text-danger");
                    break;
            }
        }

        private void SetAccesoUsuarioCorp(string UserName, int Decision, string Mensaje)
        {
            int Respuesta = AdministradorSistemasBL.SetUserCorpPortal(UserName, Decision);

            switch (Respuesta)
            {
                case 1:
                    MostrarMensaje("¡Acción realizada!", Mensaje, "text-success");
                    break;

                case -1:
                    MostrarMensaje("Error temporal 537", "No se pudo realizar esta acción. Favor de intentar más tarde.", "text-danger");
                    break;
            }
        }

        protected void gvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvUsers.PageIndex = e.NewPageIndex;

            if (ViewState["Usuarios"] != null)
            {
                gvUsers.DataSource = (List<UsuarioProveedorEntity.UsuarioKaufmann>)ViewState["Usuarios"];
                gvUsers.DataBind();
            }
            else
            {
                GetUsuarios(ViewState["user"].ToString());
            }
        }

        private void GetUsuarios(string UserName)
        {
            txtBU.Text = UserName;
            ViewState.Add("user", UserName); //Almaceno valor para el evento gvUsers_PageIndexChanging de la grilla

            List<UsuarioProveedorEntity.UsuarioKaufmann> Usuarios = AdministradorSistemasBL.GetUsuarioCorporativo(UserName);

            ViewState.Add("Usuarios", Usuarios);

            gvUsers.DataSource = Usuarios;
            gvUsers.DataBind();

            if (Usuarios.Count > 0)
            {
                dUsers.Visible = true;
                lblMsj.InnerHtml = string.Format("Resultados encontrados: <b>{0}</b>", Usuarios.Count);
                lblMsj.Attributes["style"] = string.Empty;
            }
            else
            {
                dUsers.Visible = false;
                lblMsj.InnerHtml = string.Format("No se encontraron registros para <b>\"{0}\"</b>.", UserName);
                lblMsj.Attributes["style"] = "color:#a94442";
            }

            if (Usuarios.Count > 5)
                filtro.Visible = true;
            else
                filtro.Visible = false;
        }

        protected void ddlPg_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvUsers.PageSize = Convert.ToInt16(ddlPg.SelectedItem.Value);
            GetUsuarios(ViewState["user"].ToString());
        }
	}
}