﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private.admin
{
	public partial class administrarsistema : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState.Add("user_desa", true); //Viene válido desde otra página.
                    ViewState.Add("user_soli", true); //Viene válido desde otra página.
                    ActualizaContenido();
                }
                catch
                {
                    msjprincipal.InnerText = "Error al leer los parámetros de la URL.";
                    return;
                }
            }
        }

        private SistemaEntity BuscaSistema()
        {
            int IdSistema = 0;
            List<SistemaEntity> Sistema = new List<SistemaEntity>();

            if (ViewState["id_sistema"] == null)
            {
                IdSistema = Convert.ToInt32(Request.QueryString["is"]);
                ViewState.Add("id_sistema", IdSistema);
                Sistema = AdministradorSistemasBL.GetSistema(IdSistema); //obtengo siempre 1 sóla tupla (row)
            }
            else
                Sistema = AdministradorSistemasBL.GetSistema(Convert.ToInt32(ViewState["id_sistema"].ToString()));

            return Sistema[0];
        }

        private void ActualizaContenido()
        {
            SistemaEntity Sistema = BuscaSistema();

            namesis.InnerText = Sistema.Nombre;
            txtNombreSis.Text = Sistema.Nombre;
            txtUrlSis.Text = Sistema.Url;
            txtUDSis.Text = Sistema.UserDesarrollador;
            txtUSSis.Text = Sistema.UserSolicitante;
            txtDescSis.Text = Sistema.Descripcion;
            chEstadoSis.Checked = Sistema.Estado == 1 ? true : false;

            //Si la imagen es null o si no existe, que muestre la imagen por defecto
            if (string.IsNullOrEmpty(Sistema.NombreIMG) || !File.Exists(Server.MapPath("~/Ls/") + Sistema.NombreIMG))
                imgActual.ImageUrl = "~/Img.aspx?fn=imagen_not_found.png";
            else
                imgActual.ImageUrl = "~/Img.aspx?fn=" + Sistema.NombreIMG;

            ViewState.Add("id_sis", Sistema.IdSistema);
            ViewState.Add("nombre_sis", Sistema.Nombre);
            ViewState.Add("url_sis", Sistema.Url);
        }

        protected void txtUrl_TextChanged(object sender, EventArgs e)
        {
            if (ValidaUrl())
                SetColorTxT(dUrl, "form-group");
        }

        private bool ValidaUrl()
        {
            string Url = txtUrlSis.Text.Trim();

            if (string.IsNullOrWhiteSpace(Url))
            {
                lblUrlMsj.Text = "Campo vacío.";
                return false;
            }
            else if (Url.Contains(" "))
            {
                lblUrlMsj.Text = "La Url entre medio no puede tener espacios en blanco.";
                lblUrlMsj.CssClass = "text-danger";
                SetColorTxT(dUrl, "form-group has-error");
                return false;
            }
            else
            {
                Uri uriResult;
                bool result = Uri.TryCreate(txtUrlSis.Text.Trim(), UriKind.Absolute, out uriResult) && uriResult.Scheme == Uri.UriSchemeHttp;

                if (!result)
                {
                    MostrarMensaje("false", "false", "text-success");
                    return false;
                }
            }

            lblUrlMsj.Text = string.Empty;
            return true;
        }

        //Es llamado cuando hay algún dato ingresado con error en el txt
        private void SetColorTxT(HtmlGenericControl div, string Estilo)
        {
            div.Attributes["class"] = Estilo;
        }

        //Valida que los txt de la sessión "módulos del sistema" no estén vacíos
        private void ColorEstadoTxTMod(TextBox txt, HtmlGenericControl div)
        {
            if (txt.Text == string.Empty)
                div.Attributes["class"] = "has-error";
            else
                div.Attributes["class"] = string.Empty;
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("parent.abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                int IdSistema = Convert.ToInt32(ViewState["id_sistema"].ToString());

                //valida si el sistema tiene módulos, si no tiene, entonces el sistema no puede mantenerse online
                if (chEstadoSis.Checked && AdministradorSistemasBL.GetModulos(IdSistema).Count == 0)
                {
                    MostrarMensaje("Sistema sin módulos", "No es posible dejar el sistema <b>Online</b> porque <b>no</b> tiene módulos. Si desea agregar módulos a este sistema, haga click <a href=\"principal.aspx?val=admin/modulos.aspx?is=" + IdSistema + "\">aquí</a>.", "text-danger");
                    return;
                }
                else if (txtNombreSis.Text.Trim().Length <= 0)
                {
                    MostrarMensaje("Nombre del sistema vacío", "Ingrese un nombre del sistema para guardar los datos.", "text-danger");
                    return;
                }
                else if (!ValidaUrl())
                {
                    MostrarMensaje("Url inválida", "Ingrese una Url válida para este sistema.", "text-danger");
                    return;
                }
                else if (!Convert.ToBoolean(ViewState["user_desa"])) //Si es false, es porque modificó el usuario desarrollador y dejo un usuario inválido.
                {
                    MostrarMensaje("Usuario desarrollador inválido", "El usuario desarrollador ingresado es inválido, por favor corríjalo.", "text-danger");
                    return;
                }
                else if (!Convert.ToBoolean(ViewState["user_soli"])) //Si es false, es porque modificó el usuario solicitante y dejo un usuario inválido.
                {
                    MostrarMensaje("Usuario solicitante inválido", "El usuario solicitante ingresado es inválido, por favor corríjalo.", "text-danger");
                    return;
                }
                else if (txtDescSis.Text.Trim().Length <= 0)
                {
                    MostrarMensaje("Descripción vacía", "Ingrese una descripción del sistema para guardar los datos.", "text-danger");
                    return;
                }
                else
                {
                    GuardarDatos();
                }
            }
            catch (Exception ex)
            {
                MostrarMensaje("Error temporal", "No se pudieron guardar los cambios, por favor intentelo más tarde. Error:" + ex.Message, "text-danger");
            }
        }

        private void GuardarDatos()
        {
            SistemaEntity Sistema = BuscaSistema();
            int Estado = chEstadoSis.Checked ? 1 : 2;

            string NuevaImg = fArchivo.HasFile ? fArchivo.FileName : string.Empty;
            string RutaDestino = Server.MapPath("~/Ls");

            if (NuevaImg != string.Empty) //Si no es nulo, que elimine la imagen anterior y guarde la nueva imagen
            {
                File.Delete(string.Format("{0}\\{1}", RutaDestino, Sistema.NombreIMG)); //Elimina la imagen antigua
                fArchivo.SaveAs(string.Format("{0}\\{1}_{2}", RutaDestino, Sistema.IdSistema, NuevaImg)); //Guarda la nueva imagen
                AdministradorSistemasBL.SetSistema(Sistema.IdSistema, txtNombreSis.Text.Trim(), txtUrlSis.Text.Trim(), txtDescSis.Text.Trim(), txtUDSis.Text.Trim(), txtUSSis.Text.Trim(), string.Format("{0}_{1}", Sistema.IdSistema, NuevaImg), Estado);
            }
            else
                AdministradorSistemasBL.SetSistema(Sistema.IdSistema, txtNombreSis.Text.Trim(), txtUrlSis.Text.Trim(), txtDescSis.Text.Trim(), txtUDSis.Text.Trim(), txtUSSis.Text.Trim(), Sistema.NombreIMG, Estado);

            ActualizaContenido(); //Recargo el sistema
            MostrarMensaje("¡Operación exitosa!", "Los datos han sido guardados.", "text-success");
        }

        protected void txtUDSis_TextChanged(object sender, EventArgs e)
        {
            bool Respuesta = CreaBusquedaUsuario(dUD, lblValUD, txtUDSis.Text.Trim());
            ViewState["user_desa"] = Respuesta;
        }

        protected void txtUSSis_TextChanged(object sender, EventArgs e)
        {
            bool Respuesta = CreaBusquedaUsuario(dUS, lblValUS, txtUSSis.Text.Trim());
            ViewState["user_soli"] = Respuesta;
        }

        private bool CreaBusquedaUsuario(HtmlGenericControl div, HtmlGenericControl span, string UserName)
        {
            if (UserName == string.Empty)// || string.IsNullOrEmpty(UsuarioBL.GetUserName(UserName)))
            {
                span.InnerHtml = "<div class='alert alert-danger alert-dismissible' role='alert'>" +
                    //"<button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>" +
                                     "<div class='row'>" +
                                         "<div class='col-md-11 text-overflow'>" +
                                            "<i class='glyphicon glyphicon-exclamation-sign'>&nbsp;</i><span>El usuario \"" + UserName + "\" no fue encontrado</span>" +
                                         "</div>" +
                                     "</div>" +
                                 "</div>";

                SetColorTxT(div, "form-group has-error");

                return false;
            }
            else
            {
                string Email = UsuarioBL.GetEmail(UserName);
                string Nombre = UsuarioBL.GetNombre(UserName) + " " + UsuarioBL.GetApellido(UserName);

                span.InnerHtml = "<div class='alert alert-warning alert-dismissible' role='alert'>" +
                    //"<button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>" +                                     
                                     "<div class='row'>" +
                                         "<div class='col-md-11 text-overflow'>" +
                                             "<i class='glyphicon glyphicon-tag'>&nbsp;</i><span>" + Nombre + "</span>" +
                                         "</div>" +
                                         "<div class='col-md-11 text-overflow'>" +
                                             "<i class='glyphicon glyphicon-envelope'>&nbsp;</i><span>" + Email + "</span>" +
                                         "</div>" +
                                     "</div>" +
                                 "</div>";

                SetColorTxT(div, "form-group");

                return true;
            }
        }

        protected void chEstadoSis_CheckedChanged(object sender, EventArgs e)
        {
            //Verificar si el sistema tiene páginas y roles agregados
        }
	}
}