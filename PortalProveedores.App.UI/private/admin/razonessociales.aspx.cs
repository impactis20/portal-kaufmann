﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private.admin
{
	public partial class razonessociales : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
                if (Request.QueryString["buscar"] != null)
                    GetProveedores(Request.QueryString["buscar"]);
        }

        private void GetProveedores(string RS)
        {
            try
            {
                txtBU.Text = Request.QueryString["buscar"];
                gvProvs.DataSource = AdministradorSistemasBL.GetRazonesSociales(RS);
                gvProvs.DataBind();

                if (gvProvs.Rows.Count > 0)
                {
                    dProvs.Visible = true;
                    lblMsj.InnerHtml = string.Format("Resultados encontrados: <b>{0}</b>", gvProvs.Rows.Count);
                    lblMsj.Attributes["style"] = string.Empty;
                }
                else
                {
                    dProvs.Visible = false;
                    lblMsj.InnerHtml = string.Format("No se encontraron registros para <b>\"{0}\"</b>.", RS);
                    lblMsj.Attributes["style"] = "color:#a94442";
                }

                if (gvProvs.Rows.Count > 5)
                    filtro.Visible = true;
                else
                    filtro.Visible = false;
            }
            catch
            {
                MostrarMensaje("Error temporal", "Error interno al intentar cargar los proveedores desde la base de datos.", "text-danger");
            }
        }

        protected void btnBU_Click(object sender, EventArgs e)
        {
            string RS = txtBU.Text.Trim().ToLower();

            if (!string.IsNullOrEmpty(RS))
            {
                if (RS.Length > 2)
                {
                    ColorEstadoTxT(txtBU, dBU);
                    ViewState.Add("razons", RS); //Almaceno valor para el evento gvUsers_PageIndexChanging de la grilla

                    string Url = string.Format("razonessociales.aspx?buscar={0}", RS);
                    Response.Redirect(Url);
                }
                else
                {
                    MostrarMensaje("Búsqueda incorrecta", "Para buscar usuarios debe ingresar un mínimo de 3 caracteres.", "text-danger");
                    return;
                }
            }
            else
            {
                MostrarMensaje("Error en la búsqueda", "Para buscar una razón social, debe ingresar un texto.", "text-danger");
            }
        }

        protected void gvProvs_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "GoToRS")
            {
                int IdProv = Convert.ToInt32(e.CommandArgument);
                Response.Redirect("../user/razonsocial.aspx?ip=" + IdProv);
                //redirecciona al 
            }
        }

        protected void gvProvs_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                ProveedorEntity prov = (ProveedorEntity)r.DataItem; //<i class="glyphicon glyphicon-remove-circle"></i>&nbsp;No tiene acceso a ningún sistemas.

                HtmlGenericControl lblAcceso = (HtmlGenericControl)r.Controls[0].FindControl("lblAcceso");

                if (prov.AccesoEnSistema == 1)
                    lblAcceso.InnerHtml = "<i class='glyphicon glyphicon-ok'>&nbsp;</i>Con acceso al portal Web.";
                else
                    lblAcceso.InnerHtml = "<i class='glyphicon glyphicon-ban-circle'></i>&nbsp;No tiene acceso al portal Web.";
            }
        }

        protected void ddlPg_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvProvs.PageSize = Convert.ToInt16(ddlPg.SelectedItem.Value);
            GetProveedores(ViewState["razons"].ToString());
        }

        protected void gvProvs_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProvs.PageIndex = e.NewPageIndex;
            GetProveedores(ViewState["razons"].ToString());
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("parent.abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }

        private void ColorEstadoTxT(TextBox txt, HtmlGenericControl div)
        {
            if (txt.Text == string.Empty)
                div.Attributes["class"] = "input-group has-error";
            else
                div.Attributes["class"] = "input-group";
        }
	}
}