﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private.admin
{
	public partial class consulta : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int IdConsulta = Convert.ToInt32(Request.QueryString["ic"]);
                ViewState.Add("id_consulta", IdConsulta);

                UsuarioProveedorEntity.Consulta Consulta = AdministradorSistemasBL.GetConsulta(IdConsulta);

                lblID.Text = string.Format("ID consulta: {0}", Consulta.IdConsulta);
                lblFec.Text = string.Format("Fecha consulta: {0}", Consulta.Fecha);

                lblNom.Text = string.Format("{0} {1} {2}", Consulta.Nombre, Consulta.ApellidoPat, Consulta.ApellidoMat);
                lblE.Text = Consulta.Email;
                lblF.Text = string.Format("(+56) {0}", Consulta.Fono);
                lblFM.Text = string.Format("(+56) 9 {0}", Consulta.FonoMovil);
                txtM.InnerHtml = Consulta.Mensaje.Replace("\r\n", "<br>");

                //Establece el botón según situación                

                if (string.IsNullOrEmpty(Consulta.RespuestaUsuario))
                {
                    btnA.Click += btnA_Click;
                    btnA.Visible = true;
                    btnA.Text = "<i class='glyphicon glyphicon-thumbs-up'></i>&nbsp;&nbsp;He leído y respondido esta duda";
                    btnA.CssClass = "btn btn-primary btn-lg";
                }
                else
                {
                    btnA.Text = string.Format("<i class='glyphicon glyphicon-ok'></i>&nbsp;&nbsp; Esta consulta la atendió el usuario \"{0}\"", Consulta.RespuestaUsuario);
                    btnA.CssClass = "btn btn-link btn-lg disabled";
                    //btnA.Attributes[""] = "";
                    btnA.Enabled = false;
                    //btnA.CssClass += " disabled";
                }
            }
            catch
            {
                MostrarMensaje("Error temporal (610)", "Error interno al intentar cargar la consulta.", "text-danger");
            }
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("parent.abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }

        protected void btnA_Click(object sender, EventArgs e)
        {
            int IdConsulta = Convert.ToInt32(ViewState["id_consulta"].ToString());
            string Responsable = PortalProveedorBL.GetUsuarioCorporativoLogueado().UserName;

            int Respuesta = AdministradorSistemasBL.InsertResponsableConsulta(IdConsulta, Responsable);

            if (Respuesta > 0)
            {
                string PagActual = Path.GetFileName(Request.Url.AbsolutePath);
                Response.Redirect(string.Format("{0}?ic={1}", PagActual, IdConsulta));
            }
            else
            {
                MostrarMensaje("Error temporal 611", "No se pudo guardar el responsable que respondió esta consulta. Por favor inténtelo más tarde.", "text-danger");
            }
        }
	}
}