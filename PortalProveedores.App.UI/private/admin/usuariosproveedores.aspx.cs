﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private.admin
{
	public partial class usuariosproveedores : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                if (Request.QueryString["buscar"] != null)
                    GetUsuarios(Request.QueryString["buscar"]);
        }

        protected void btnBU_Click(object sender, EventArgs e)
        {
            string UserName = txtBU.Text.Trim().ToLower();

            if (UserName.Length > 2)
            {
                ColorEstadoTxT(txtBU, dBU);

                string Url = string.Format("usuariosproveedores.aspx?buscar={0}", UserName);
                Response.Redirect(Url);
            }
            else
            {
                MostrarMensaje("Búsqueda incorrecta", "Para buscar usuarios debe ingresar un mínimo de 3 caracteres.", "text-danger");
                return;
            }
        }

        private void GetUsuarios(string TextoBusqueda)
        {
            txtBU.Text = TextoBusqueda;
            ViewState.Add("user", TextoBusqueda); //Almaceno valor para el evento gvUsers_PageIndexChanging de la grilla

            List<UsuarioProveedorEntity> Usuarios = AdministradorSistemasBL.GetBuscaUsuarioProveedor(0, TextoBusqueda); //busca por rut, nombre, apellido materno, apellido paterno o email
            gvUsers.DataSource = Usuarios;
            gvUsers.DataBind();

            if (Usuarios.Count > 0)
            {
                dUsers.Visible = true;
                lblMsj.InnerHtml = string.Format("Resultados encontrados: <b>{0}</b>", Usuarios.Count);
                lblMsj.Attributes["style"] = string.Empty;
            }
            else
            {
                dUsers.Visible = false;
                lblMsj.InnerHtml = string.Format("No se encontraron registros para <b>\"{0}\"</b>.", TextoBusqueda);
                lblMsj.Attributes["style"] = "color:#a94442";
            }

            if (Usuarios.Count > 5)
                filtro.Visible = true;
            else
                filtro.Visible = false;
        }

        protected void ddlPg_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvUsers.PageSize = Convert.ToInt16(ddlPg.SelectedItem.Value);
            GetUsuarios(ViewState["user"].ToString());
        }

        protected void gvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvUsers.PageIndex = e.NewPageIndex;
            GetUsuarios(ViewState["user"].ToString());
        }

        protected void gvUsers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                UsuarioProveedorEntity userProv = (UsuarioProveedorEntity)r.DataItem;

                HtmlGenericControl lblEstadoUsuario = ((HtmlGenericControl)r.Controls[0].FindControl("lblEstadoUsuario"));
                LinkButton btnDesbloquearUsuario = ((LinkButton)r.Controls[0].FindControl("btnDesbloquearUsuario"));
                LinkButton btnBloquearUsuario = ((LinkButton)r.Controls[0].FindControl("btnBloquearUsuario"));
                HtmlGenericControl lblAdministra = ((HtmlGenericControl)r.Controls[0].FindControl("lblAdministra"));
                HtmlGenericControl lblPertenece = ((HtmlGenericControl)r.Controls[0].FindControl("lblPertenece"));

                EstadoUsuario(lblEstadoUsuario, btnBloquearUsuario, btnDesbloquearUsuario, userProv.AccesoPortal, userProv.Nombre);



                //Agrega las razones sociales que administra el usuario proveedor (si es que tiene)
                List<ProveedorEntity> listRS_ADMProv = AdministradorSistemasBL.GetRSPerteneceComoUP(userProv.IdUserProv, 1); //1: verifica razones sociales como usuario adminitrador.

                int Cont = listRS_ADMProv.Count;

                string UrlRS = "../user/razonsocial.aspx";

                if (Cont != 0)
                {
                    lblAdministra.InnerHtml = "Administrador de cuenta: ";

                    //un usuario proveedor puede tener 1 o más razones sociales en su poder (administrador)
                    foreach (ProveedorEntity prov in listRS_ADMProv)
                    {
                        lblAdministra.InnerHtml += string.Format("<i class='glyphicon glyphicon-tag'></i> <a href='{0}?ip={1}'>{2}</a>", UrlRS, prov.IdProv, prov.RazonSocial);

                        if (Cont > 1)
                            lblAdministra.InnerHtml += ",&nbsp;&nbsp;";

                        Cont--;
                    }
                }
                else
                {
                    lblAdministra.Visible = false;
                }


                //Agrega la razón social a la que pertenece como USUARIO PROVEEDOR
                List<ProveedorEntity> listRS_PerteneceProv = AdministradorSistemasBL.GetRSPerteneceComoUP(userProv.IdUserProv, 2); //2: verifica razones sociales como usuario proveedor.

                if (listRS_PerteneceProv.Count == 1)
                {
                    //una cuenta puede estar asociada a una razón social, sóla una vez como USUARIO PROVEEDOR
                    lblPertenece.InnerHtml = string.Format("Pertenece a la cuenta: <i class='glyphicon glyphicon-tag'></i> <a href='{0}?ip={1}'>{2}</a>", UrlRS, listRS_PerteneceProv[0].IdProv, listRS_PerteneceProv[0].RazonSocial);
                }
                else
                {
                    lblPertenece.Visible = false;
                }




                //Agrega los sistemas a los que el usuario proveedor tiene acceso
                //List<SistemaEntity> listSisAcceso = AdministradorSistemasBL.GetSistemaAccesoProv(userProv.IdUserProv);
                //HtmlGenericControl lblPermisos = ((HtmlGenericControl)r.Controls[0].FindControl("lblPermisos"));

                //if(listSisAcceso.Count > 0)
                //{
                //    int Cont = listSisAcceso.Count;

                //    foreach(SistemaEntity sis in listSisAcceso)
                //    {
                //        //lblPermisos.InnerHtml += "<i class='glyphicon glyphicon-globe'></i> <a href='" + sis.Url +"'>" + sis.Nombre + "</a>"; //link sistema con acceso
                //        lblPermisos.InnerHtml += "<i class='glyphicon glyphicon-globe'></i> <span>" + sis.Nombre + "</span>";

                //        if (Cont > 1)
                //            lblPermisos.InnerHtml += ",&nbsp;&nbsp;";

                //        Cont--;
                //    }
                //}
                //else
                //{
                //    lblPermisos.InnerHtml = "<i class='glyphicon glyphicon-remove-circle'></i>&nbsp;No tiene acceso a ningún sistemas.";                    
                //}
            }
        }

        private void EstadoUsuario(HtmlGenericControl lblEstadoUsuario, LinkButton btnBloquearUsuario, LinkButton btnDesbloquearUsuario, int EstadoUsuario, string UserName)
        {
            //Estado
            // 0: Usuario "nuevo" que no tiene acceso al portal Web
            // 1: Tiene permiso para acceder al portal Web
            // 2: Usuario bloqueado
            // -1: Error

            switch (EstadoUsuario)
            {
                case 0:
                    lblEstadoUsuario.InnerHtml = string.Format("<i class='glyphicon glyphicon-user'>&nbsp;</i><b>{0}:</b> No tiene acceso al Portal Web", UserName);
                    lblEstadoUsuario.Attributes["class"] = "label label-default";
                    btnBloquearUsuario.Visible = false;
                    btnDesbloquearUsuario.Visible = false;
                    return;
                case 1:
                    lblEstadoUsuario.InnerHtml = string.Format("<i class='glyphicon glyphicon-user'>&nbsp;</i><b>{0}:</b> Tiene acceso al Portal Web", UserName);
                    lblEstadoUsuario.Attributes["class"] = "label label-success";
                    btnBloquearUsuario.Visible = true;
                    btnDesbloquearUsuario.Visible = false;
                    return;
                case 2:
                    lblEstadoUsuario.InnerHtml = string.Format("<i class='glyphicon glyphicon-user'>&nbsp;</i><b>{0}:</b> Está Bloqueado", UserName);
                    lblEstadoUsuario.Attributes["class"] = "label label-warning";
                    btnBloquearUsuario.Visible = false;
                    btnDesbloquearUsuario.Visible = true;
                    return;
            }
        }

        protected void gvUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (App_Code.Util.isIntNumeric(e.CommandName))
            {
                int Command = Convert.ToInt32(e.CommandName);
                int IdProv = Convert.ToInt32(e.CommandArgument);
                switch (Command)
                {
                    case 1:
                        SetPermisoUsuario(IdProv, 1, "El usuario ha sido <b>desbloqueado</b>."); // 1: desbloquea usuario
                        break;
                    case 2:
                        SetPermisoUsuario(IdProv, 2, "El usuario ha sido <b>bloqueado</b>."); // 2: bloquea usuario
                        break;
                }

                GetUsuarios(ViewState["user"].ToString());
            }
        }

        private void SetPermisoUsuario(int IdProv, int Decision, string Mensaje)
        {
            string UserName = ViewState["user"].ToString();
            AdministradorSistemasBL.SetUserProvPortal(IdProv, Decision);
            MostrarMensaje("¡Acción realizada!", Mensaje, "text-success");
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("parent.abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }

        private void ColorEstadoTxT(TextBox txt, HtmlGenericControl div)
        {
            if (txt.Text == string.Empty)
                div.Attributes["class"] = "input-group has-error";
            else
                div.Attributes["class"] = "input-group";
        }
	}
}