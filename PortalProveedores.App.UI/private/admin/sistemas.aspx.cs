﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI.@private.admin
{
	public partial class sistemas : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            List<SistemaEntity> ListSistemas = AdministradorSistemasBL.GetSistema(0); //Trae todos los sistemas

            if (ListSistemas != null)
            {
                gvSistemas.DataSource = ListSistemas;
                gvSistemas.DataBind();

                contSis.InnerText = ListSistemas.Count.ToString();
            }
            else
            {
                contSis.InnerText = "Actualmente no se encuentran sistemas registrados en este portal.";
            }
        }

        protected void gvSistemas_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Adm":
                    Response.Redirect("principal.aspx?val=admin/administrarsistema.aspx?is={" + e.CommandArgument + "}");
                    break;

                case "Go":
                    Response.Redirect(e.CommandArgument.ToString());
                    break;
            }
        }

        protected void gvSistemas_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow Row = e.Row;

            if (Row.RowType == DataControlRowType.DataRow)
            {
                SistemaEntity sys = (SistemaEntity)Row.DataItem;
                Label lblE = (Label)Row.Controls[0].FindControl("lblE");
                HtmlGenericControl eIcon = (HtmlGenericControl)Row.Controls[0].FindControl("eIcon");

                switch (sys.Estado)
                {
                    case 1:
                        eIcon.Attributes["class"] = "glyphicon glyphicon-ok";
                        lblE.Text = "Online";
                        lblE.CssClass = "text-success";
                        break;
                    case 2:
                        eIcon.Attributes["class"] = "glyphicon glyphicon-arrow-down";
                        lblE.Text = "Offline";
                        lblE.CssClass = "text-danger";
                        break;
                }
            }
        }
	}
}