﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using PortalProveedores.BL;
using PortalProveedores.Entities;

namespace PortalProveedores.App.UI
{
    public partial class contactenos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEnviarMsj_Click(object sender, EventArgs e)
        {
            string Nombres = txtNombres.Text;
            string ApellidoPat = txtApellidoPat.Text;
            string ApellidoMat = txtApellidoMat.Text;
            string Email = txtEmail.Text.ToLower().Trim();
            string FonoFijo = txtFonoFijo.Text;
            string FonoMovil = txtFonoMovil.Text;
            string Mensaje = txtMensaje.Text;

            bool EstadoTxtNombre = string.IsNullOrEmpty(Nombres);
            bool EstadoTxtApellidoPat = string.IsNullOrEmpty(ApellidoPat);
            bool EstadoTxtApellidoMat = string.IsNullOrEmpty(ApellidoMat);
            bool EstadoTxtEmail = string.IsNullOrEmpty(Email);
            bool EstadoTxtFonoFijo = string.IsNullOrEmpty(FonoFijo);
            bool EstadoTxtFonoMovil = string.IsNullOrEmpty(FonoMovil);
            bool EstadoTxtMensaje = string.IsNullOrEmpty(Mensaje);

            //Si están vacíos, que queden de color rojo
            NormalizaFormularioRegistro();

            if (EstadoTxtNombre || EstadoTxtApellidoPat || EstadoTxtApellidoMat || EstadoTxtEmail || EstadoTxtFonoFijo || EstadoTxtFonoMovil || EstadoTxtMensaje)
            {
                MostrarMensaje("Datos incompletos", "Le recordamos que todos los campos con asterisco (*) son obligatorios.", "text-danger");
            }
            else
            {
                #region Email
                bool EmailValido = App_Code.Util.isValidEmail(Email); //Valida si el correo tiene el formato correcto

                if (!EmailValido)
                {
                    MostrarMensaje("Error", "Su Email es inválido, por favor corríjalo.", "text-danger"); //Mensaje de error al dejar campos obligatorios vacíos.
                    SetColorTxT(cEmail, "form-group has-danger");
                    return;
                }
                #endregion

                #region Teléfonos de contacto
                //Red fija
                int FonoNumeric = 0;
                if (App_Code.Util.isIntNumeric(FonoFijo))
                {
                    FonoNumeric = Convert.ToInt32(FonoFijo);

                    if (FonoFijo.Length <= 8)
                    {
                        MostrarMensaje("Teléfono fijo inválido", "Su número de teléfono es incorrecto. Hoy en día los números de teléfono red fija contienen 9 dígitos.", "text-danger");
                        return;
                    }
                }
                else
                {
                    MostrarMensaje("Teléfono fijo inválido", "Su número de teléfono debe ser numérico.", "text-danger");
                    SetColorTxT(cFonoFijo, "text-danger");
                    return;
                }

                //Móvil
                int FonoMovilNumeric = 0;
                if (App_Code.Util.isIntNumeric(FonoMovil))
                {
                    FonoMovilNumeric = Convert.ToInt32(FonoMovil);

                    if (FonoMovil.Length <= 7)
                    {
                        MostrarMensaje("Teléfono móvil inválido", "Su número de teléfono móvil es incorrecto. Los teléfonos móviles contienen 8 dígitos.", "text-danger");
                        return;
                    }
                }
                else
                {
                    MostrarMensaje("Teléfono móvil inválido", "Su número de teléfono móvil debe ser numérico.", "text-danger");
                    SetColorTxT(cFonoMovil, "text-danger");
                    return;
                }
                #endregion

                //Largo del mensaje
                if (Mensaje.Length > 500)
                {
                    MostrarMensaje("Mensaje demasiado largo", "El mensaje que acaba de escribir supera el máximo permitido (500 caracteres). Una solución a este problema es que divida su mensaje en dos o mas partes y lo envíe nuevamente uno por uno.", "text-danger");
                    SetColorTxT(cMensaje, "text-danger");
                    return;
                }

                //Inserta Registrp
                int Respuesta = PortalProveedorBL.InsertConsulta(Nombres, ApellidoPat, ApellidoMat, Email, FonoNumeric, FonoMovilNumeric, Mensaje);

                switch (Respuesta)
                {
                    case 1:
                        txtNombres.Text = string.Empty;
                        txtApellidoPat.Text = string.Empty;
                        txtApellidoMat.Text = string.Empty;
                        txtEmail.Text = string.Empty;
                        txtFonoFijo.Text = string.Empty;
                        txtFonoMovil.Text = string.Empty;
                        txtMensaje.Text = string.Empty;
                        MostrarMensaje("¡Datos enviados!", "Los datos ingresados se han enviado con éxito. Ahora sólo queda esperar a que nuestros administradores se contacten con usted, ya sea vía telefónica o por correo electrónico para darle una respuesta a su inquietud.<br><br>¡Gracias por contactarse con nosotros.!", "text-success");
                        return;
                    case 2:
                        MensajeError();
                        return;
                    case -1:
                        MensajeError();
                        return;
                }
            }
        }

        private void MensajeError()
        {
            MostrarMensaje("Error temporal (410)", "Error interno al intentar enviar los datos del formulario. Por favor inténtelo más tarde.", "text-danger");
        }

        private void NormalizaFormularioRegistro()
        {
            ColorEstadoTxT(txtNombres, cNombres);
            ColorEstadoTxT(txtApellidoPat, cApellidoPat);
            ColorEstadoTxT(txtApellidoMat, cApellidoMat);
            ColorEstadoTxT(txtEmail, cEmail);
            ColorEstadoTxT(txtFonoFijo, cFonoFijo);
            ColorEstadoTxT(txtFonoMovil, cFonoMovil);
            ColorEstadoTxT(txtMensaje, cMensaje);
        }

        //Es llamado cuando hay algún dato ingresado con error en el txt
        private void SetColorTxT(HtmlGenericControl div, string Estilo)
        {
            div.Attributes["class"] = Estilo;
        }

        //Valida que los txt no estén vacíos
        private void ColorEstadoTxT(TextBox txt, HtmlGenericControl div)
        {
            if (txt.Text == string.Empty)
                div.Attributes["class"] = "form-group has-error";
            else
                div.Attributes["class"] = "form-group";
        }

        private void MostrarMensaje(string Titulo, string Mensaje, string EstiloMensaje)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("parent.abrePopUp('{0}', '{1}', '{2}');", Titulo, Mensaje, EstiloMensaje), true);
        }
    }
}