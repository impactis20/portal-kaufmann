﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace PortalProveedores.App.UI
{
    public class xSeguridad : IHttpModule
    {
        public xSeguridad()
        {

        }

        public void Dispose()
        {

        }

        /// <summary>
        /// Inicializa el HTTPModule y asigna los EventHandlers a cada Evento
        /// Esta es la parte donde se define a que eventos va a atender el HttpModule
        /// </summary>
        /// <param name="oHttpApp"></param>
        public void Init(HttpApplication oHttpApp)
        {
            // Se Registran los Manejadores de Evento que nos interesa
            oHttpApp.AuthenticateRequest += new EventHandler(this.AuthenticateRequest);
        }

        /// <summary>
        /// Autentica en Cada Request
        /// </summary>
        /// <param name="sender">HttpApplication</param>
        /// <param name="e"></param>
        private void AuthenticateRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated && HttpContext.Current.User.Identity is FormsIdentity)
            {
                FormsIdentity _identity = (FormsIdentity)HttpContext.Current.User.Identity;
                FormsAuthenticationTicket ticket = _identity.Ticket;
            }
            else
            {
                //HttpContext.Current.Server.Transfer("/kf/Login.aspx");
                //Redirecciona automáticamente
            }
        }
    }

    public class xSeguridada
    {
        public static FormsAuthenticationTicket GetCookieUser(HttpRequest Solicitud) //Obtengo los datos del usuario, en la cookie almacenada
        {
            HttpCookie authCookie = Solicitud.Cookies[FormsAuthentication.FormsCookieName];
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
            return ticket;
        }
    }
}