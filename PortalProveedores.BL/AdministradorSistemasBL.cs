﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PortalProveedores.DAL;
using PortalProveedores.Entities;
using PortalProveedores.BL.EnviarEmail;

namespace PortalProveedores.BL
{
    public class AdministradorSistemasBL
    {
        public static List<Tuple<ProveedorEntity, UsuarioProveedorEntity>> GetRegistrosProveedores(int Filtro)
        {
            return AdministradorSistemasDAL.GetRegistrosProveedores(Filtro);
        }

        public static Tuple<ProveedorEntity, UsuarioProveedorEntity, UsuarioProveedorEntity.UsuarioKaufmann, string> GetRegistroProveedor(int IdSolicitud)
        {
            Tuple<ProveedorEntity, UsuarioProveedorEntity, UsuarioProveedorEntity.UsuarioKaufmann, string> RegistroUS = AdministradorSistemasDAL.GetRegistroProveedor(IdSolicitud);

            //Trae los datos del usuario Kaufmann (administrador)
            if (!string.IsNullOrEmpty(RegistroUS.Item3.UserName))
            {
                RegistroUS.Item3.Nombre = UsuarioDAL.GetNombre(RegistroUS.Item3.UserName);
                RegistroUS.Item3.ApellidoPat = UsuarioDAL.GetApellido(RegistroUS.Item3.UserName);
                RegistroUS.Item3.Email = UsuarioDAL.GetEmail(RegistroUS.Item3.UserName);
            }

            return RegistroUS;
        }

        public static int InsertRegistroProveedor(int IdSolicitud, int Decision, string Responsable, string MotivoDecision, DateTime FechaDecision)
        {
            return AdministradorSistemasDAL.InsertRegistroProveedor(IdSolicitud, Decision, Responsable, MotivoDecision, FechaDecision);
        }

        public static List<UsuarioProveedorEntity.Consulta> GetConsultas(int Estado)
        {
            return AdministradorSistemasDAL.GetConsultas(Estado);
        }

        public static UsuarioProveedorEntity.Consulta GetConsulta(int IdConsulta)
        {
            return AdministradorSistemasDAL.GetConsulta(IdConsulta);
        }

        public static int InsertResponsableConsulta(int IdConsulta, string Resp)
        {
            return AdministradorSistemasDAL.InsertResponsableConsulta(IdConsulta, Resp);
        }

        public static int SetCambiarAdmRS(int IdUserProvAdmNuevo, int IdRazonSocial)
        {
            return AdministradorSistemasDAL.SetCambiarAdmRS(IdUserProvAdmNuevo, IdRazonSocial);
        }

        public static int InsertUserCorpPortal(string UserName, string Email, DateTime FechaReg)
        {
            return AdministradorSistemasDAL.InsertUserCorpPortal(UserName, Email, FechaReg);
        }

        public static int ValidaUserCoprPortal(string UserName)
        {
            return AdministradorSistemasDAL.ValidaUserCoprPortal(UserName);
        }

        // Actualiza el permiso de acceso al portal, del usuario corporativo
        public static int SetUserCorpPortal(string UserName, int Permiso)
        {
            return AdministradorSistemasDAL.SetUserCorpPortal(UserName, Permiso);
        }

        // Actualiza a un usuario corporativo, dejándolo como administrador o viceversa
        public static int SetUserCorpToAdm(string UserName)
        {
            return AdministradorSistemasDAL.SetUserCorpToAdm(UserName);
        }

        // Actualiza el permiso de acceso al portal, del usuario proveedor
        public static int SetUserProvPortal(int IdUserProv, int Permiso)
        {
            return AdministradorSistemasDAL.SetUserProvPortal(IdUserProv, Permiso);
        }

        // Actualiza el permiso de acceso al portal, de la razón social
        public static int SetRazonSocialPortal(int IdProv, int Permiso)
        {
            return AdministradorSistemasDAL.SetRazonSocialPortal(IdProv, Permiso);
        }

        public static UsuarioProveedorEntity.UsuarioKaufmann GetUserCorp_RolEstado(UsuarioProveedorEntity.UsuarioKaufmann Username)
        {
            return AdministradorSistemasDAL.GetUserCorp_RolEstado(Username);
        }

        public static List<UsuarioProveedorEntity.UsuarioKaufmann> GetUsuarioCorporativo(string Username)
        {
            string[] users_encontrados = UsuarioDAL.GetUserNameLIKE(Username); //Trae usuarios corporativos encontrados
            List<UsuarioProveedorEntity.UsuarioKaufmann> UsersCorpEncontrados = new List<UsuarioProveedorEntity.UsuarioKaufmann>();

            for (int cont = 0; cont < users_encontrados.Length; cont++) //Llena lista con usuarios encontrados
                UsersCorpEncontrados.Add(new UsuarioProveedorEntity.UsuarioKaufmann(users_encontrados[cont]));

            List<UsuarioProveedorEntity.UsuarioKaufmann> UsuariosCorpRegistrados = AdministradorSistemasDAL.GetUsuarioCorporativo(Username); //trae usuarios corporativos registrados

            foreach (UsuarioProveedorEntity.UsuarioKaufmann user in UsersCorpEncontrados) //Llena los datos de los usuarios encontrados, además, define si están registrados
            {
                bool UsuarioRegistrado = false;

                foreach (UsuarioProveedorEntity.UsuarioKaufmann UsuarioCorp in UsuariosCorpRegistrados)
                {
                    if (user.UserName == UsuarioCorp.UserName) //Si el usuario se encuentra en la lista de usuarios registrados, se le asigna su estado de acceso al portal, de lo contrario, se le asigna un 0 (usuario nunca antes registrado en el portal)... Lo mismo para el campo "EsAdmin"
                    {
                        user.AccesoPortal = UsuarioCorp.AccesoPortal;
                        user.EsAdmin = UsuarioCorp.EsAdmin;
                        UsuarioRegistrado = true;
                        break;
                    }
                    else
                    {
                        user.AccesoPortal = 0;
                    }
                }

                if (UsuarioRegistrado) //Si el usuario ya fue encontrado, entonces lo quito de la lista "UsuariosCorpRegistrados", para agilizar la búsqueda en el siguiente usuario
                    UsuariosCorpRegistrados.RemoveAll(a => a.UserName == user.UserName);

                string Run = UsuarioDAL.GetRut(user.UserName);
                user.Run = Run;

                string Apellidos = UsuarioDAL.GetApellido(user.UserName);

                if (Apellidos.Contains(" ")) //Tiene dos apellidos?
                {
                    user.ApellidoPat = Apellidos.Split(' ')[0];
                    user.ApellidoMat = Apellidos.Split(' ')[1];
                }
                else
                    user.ApellidoPat = Apellidos;

                user.Nombre = UsuarioDAL.GetNombre(user.UserName);
                user.Email = UsuarioDAL.GetEmail(user.UserName);
            }

            return UsersCorpEncontrados;
        }

        public static int AddSistema(string NombreSis, string URL, string UserDesa, string UserSoli, string DescSis, string NombreImg)
        {
            return AdministradorSistemasDAL.AddSistema(NombreSis, URL, UserDesa, UserSoli, DescSis, NombreImg);
        }

        public static void AddModuloSistema(int IdSistema, string NombreMod, string Pagina, string Descripcion, int Estado)
        {
            AdministradorSistemasDAL.AddModuloSistema(IdSistema, NombreMod, Pagina, Descripcion, Estado);
        }

        /// <summary>
        /// Para obtener todos los sistemas, enviar el IdSistema con valor 0.
        /// </summary>
        /// <param name="IdSistema"></param>
        /// <returns></returns>
        public static List<SistemaEntity> GetSistema(int IdSistema)
        {
            return AdministradorSistemasDAL.GetSistema(IdSistema);
        }

        public static List<SistemaEntity.Modulo> GetModulos(int IdSistema)
        {
            return AdministradorSistemasDAL.GetModulos(IdSistema);
        }

        public static void SetSistema(int IdSistema, string Nombre, string Url, string Descripcion, string UserDesa, string UserSoli, string NombreImg, int Estado)
        {
            AdministradorSistemasDAL.SetSistema(IdSistema, Nombre, Url, Descripcion, UserDesa, UserSoli, NombreImg, Estado);
        }

        public static void SetModulo(int IdModulo, string NombreMod, string Pagina, string DescMod, int Estado)
        {
            AdministradorSistemasDAL.SetModulo(IdModulo, NombreMod, Pagina, DescMod, Estado);
        }

        public static List<UsuarioProveedorEntity> GetBuscaUsuarioProveedor(int IdUserProv, string ValorBusqueda)
        {
            return AdministradorSistemasDAL.GetBuscaUsuarioProveedor(IdUserProv, ValorBusqueda);
        }

        public static List<ProveedorEntity> GetRSPerteneceComoUP(int IdUserProv, int TipoUsuario)
        {
            return AdministradorSistemasDAL.GetRSPerteneceComoUP(IdUserProv, TipoUsuario);
        }

        // Obtiene los sistemas a los que el usuario proveedor tiene acceso
        public static List<SistemaEntity> GetSistemaAccesoProv(int IdUserProv)
        {
            return AdministradorSistemasDAL.GetSistemaAccesoProv(IdUserProv);
        }

        public static List<ProveedorEntity> GetRazonesSociales(string TextoBusqueda)
        {
            return AdministradorSistemasDAL.GetRazonesSociales(TextoBusqueda);
        }

        public static int InsertRolSistema(int IdSistema, string NombreRol, int TipoUsuarioRol)
        {
            return AdministradorSistemasDAL.InsertRolSistema(IdSistema, NombreRol, TipoUsuarioRol);
        }

        public static List<SistemaEntity.Roles> GetRolesSistema(int IdSistema)
        {
            return AdministradorSistemasDAL.GetRolesSistema(IdSistema);
        }
    }
}
