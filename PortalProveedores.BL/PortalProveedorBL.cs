﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

using PortalProveedores.DAL;
using PortalProveedores.Entities;
using PortalProveedores.BL.EnviarEmail; //WebService

namespace PortalProveedores.BL
{
    public class PortalProveedorBL
    {
        public static HttpCookie FormulaCookieUsuario(string name, string userData)
        {
            //se guarda el permiso al sistema en el name de la cookie.
            FormsAuthenticationTicket Ticket = new FormsAuthenticationTicket(100, name, DateTime.Now, DateTime.Now.AddDays(2), false, userData, FormsAuthentication.FormsCookiePath);
            string TicketEncriptado = FormsAuthentication.Encrypt(Ticket);
            return new HttpCookie(FormsAuthentication.FormsCookieName, TicketEncriptado);
        }

        /// <summary>
        /// Devuelve un 10 (usuario Proveedor) ó 0 (usuario Kaufmann)
        /// </summary>
        /// <returns></returns>
        public static int GetTipoUsuarioLogueado()
        {
            FormsAuthenticationTicket ticket = GetTicket();
            return ticket.UserData.Split('|')[0].Contains("-") ? 10 : 0; //0: Usuario Corporativo Kaufmann (obtiene username, sólo letras) | 10: Usuario Proveedor (obtiene run con formato "12345678-9")
        }
        public static UsuarioProveedorEntity GetTipoRazón(int idProveedor)
        {
          return PortalProveedorDAL.GetTipoRazón(idProveedor);
        }
        public static UsuarioProveedorEntity GetUsuarioProveedorLogueado()
        {
            FormsAuthenticationTicket ticket = GetTicket();

            if (ticket != null)
            {
                int IdUserProv = Convert.ToInt32(ticket.UserData.Split('*')[0]);
                string Run = ticket.UserData.Split('|')[0].Split('*')[1];
                string Email = ticket.UserData.Split(',')[0].Split('|')[1];
                int IdProv = Convert.ToInt32(ticket.UserData.Split('?')[0].Split(',')[1]);
                int EsAdmin = Convert.ToInt16(ticket.UserData.Split('?')[1]);

                return new UsuarioProveedorEntity(IdUserProv, Run, ticket.Name, string.Empty, string.Empty, Email, IdProv, EsAdmin);
            }
            else
            {
                return null;
            }
        }


        public static UsuarioProveedorEntity.UsuarioKaufmann GetUsuarioCorporativoLogueado()
        {
            FormsAuthenticationTicket ticket = GetTicket();

            if (ticket != null)
            {
                string UserName = ticket.UserData.Split('*')[0];
                string Email = ticket.UserData.Split('|')[0].Split('*')[1];
                int EsAdmin = Convert.ToInt32(ticket.UserData.Split('|')[1]);

                return new UsuarioProveedorEntity.UsuarioKaufmann(UserName, ticket.Name, Email, EsAdmin);
            }
            else
            {
                return null;
            }
        }


        private static FormsAuthenticationTicket GetTicket()
        {
            if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated && HttpContext.Current.User.Identity is FormsIdentity)
            {
                FormsIdentity _identity = (FormsIdentity)HttpContext.Current.User.Identity;
                FormsAuthenticationTicket ticket = _identity.Ticket;
                
                return ticket;
            }
            else
            {
                return null;
            }
        }

        public static int InsertSolicitudRegistro(string RutProveedor, string RazonSocial, string Password, DateTime FechaSolicitud, string Mensaje, string RutUserPRov, string Nombre, string ApellidoPat, string ApellidoMat, string Email, int Fono, int FonoMovil, string tipo_razon)
        {
            return PortalProveedorDAL.InsertSolicitudRegistro(RutProveedor, RazonSocial, Password, FechaSolicitud, Mensaje, RutUserPRov, Nombre, ApellidoPat, ApellidoMat, Email, Fono, FonoMovil, tipo_razon);
        }

        public static Tuple<ProveedorEntity, UsuarioProveedorEntity> ValidaEmail(int IdSolicitud, int ValidaCorreo)
        {
            return PortalProveedorDAL.ValidaEmail(IdSolicitud, ValidaCorreo);
        }

        public static UsuarioProveedorEntity GetUsuarioProveedor(string RunUsuarioProv)
        {
            return PortalProveedorDAL.GetUsuarioProveedor(RunUsuarioProv);
        }

        public static UsuarioProveedorEntity ValidaEmailRun(string Email, string RunUserProv)
        {
            return PortalProveedorDAL.ValidaEmailRun(Email, RunUserProv);
        }

        public static int SetPassword(string RunUserProv, string Password)
        {
            return PortalProveedorDAL.SetPassword(RunUserProv, Password);
        }

        public static int InsertConsulta(string Nombre, string ApellidoPat, string ApellidoMat, string Email, int FonoFijo, int FonoMovil, string Mensaje)
        {
            return PortalProveedorDAL.InsertConsulta(Nombre, ApellidoPat, ApellidoMat, Email, FonoFijo, FonoMovil, Mensaje, DateTime.Now);
        }

        public static int InsertRegistroCambioPass(string RunUserProv, DateTime FechaCreacion)
        {
            return PortalProveedorDAL.InsertRegistroCambioPass(RunUserProv, FechaCreacion);
        }

        public static void SetRegistroCambioPass(int IdSolicitud, DateTime FechaActualizacion)
        {
            PortalProveedorDAL.SetRegistroCambioPass(IdSolicitud, FechaActualizacion);
        }

        public static Tuple<ProveedorEntity, UsuarioProveedorEntity> GetProveedorExistente(string Rut)
        {
            return PortalProveedorDAL.GetProveedorExistente(Rut);
        }

        public static List<UsuarioProveedorEntity> GetAdmRS(int IdUserProv)
        {
            return PortalProveedorDAL.GetAdmRS(IdUserProv);
        }

        public static List<UsuarioProveedorEntity> GetUsersRS(int IdUserProv)
        {
            return PortalProveedorDAL.GetUsersRS(IdUserProv);
        }

        public static ProveedorEntity GetProveedor(int IdProv)
        {
            return PortalProveedorDAL.GetProveedor(IdProv);
        }

        public static int InsertSolicitudRegUsuario(int IdProv, int IdUsuarioProveedor)
        {
            return PortalProveedorDAL.InsertSolicitudRegUsuario(IdProv, IdUsuarioProveedor, DateTime.Now);
        }

        public static int SetDatosContactoProv(string RunProv, string Nombre, string ApellidoPaterno, string ApellidoMaterno, string Email, int Fono, int FonoMovil)
        {
            return PortalProveedorDAL.SetDatosContactoProv(RunProv, Nombre, ApellidoPaterno, ApellidoMaterno, Email, Fono, FonoMovil);
        }

        public static int SetVigenciaUsuarioPortal(int IdUserProv, int EstadoVigencia)
        {
            return PortalProveedorDAL.SetVigenciaUsuarioPortal(IdUserProv, EstadoVigencia, DateTime.Now);
        }

        public static void InsertUserProveedorSolicitada(int IdRegistro, string RutProveedor, string Nombre, string AP, string AM, string Email, int FonoFijo, int FonoMovil, string Password)
        {
            PortalProveedorDAL.InsertUserProveedorSolicitada(IdRegistro, RutProveedor, Nombre, AP, AM, Email, FonoFijo, FonoMovil, Password, DateTime.Now);
        }

        public static Tuple<ProveedorEntity, UsuarioProveedorEntity, int> GetSolicitudRegUsuario(int IdRegistro, int TipoSolicitud)
        {
            return PortalProveedorDAL.GetSolicitudRegUsuario(IdRegistro, TipoSolicitud);
        }

        public static int VerificaSiEmailExiste(string Email)
        {
            return PortalProveedorDAL.VerificaSiEmailExiste(Email);
        }

        public static int GetMiRazonSocial(string RunProv)
        {
            return PortalProveedorDAL.GetMiRazonSocial(RunProv);
        }
        public static int GetMiRazonSocial_ParaUsuarioProveedor(string RunProv)
        {
            return PortalProveedorDAL.GetMiRazonSocial_ParaUsuarioProveedor(RunProv);
        }
        public static int GetMiIdProveedor(string RunProv)
        {
            return PortalProveedorDAL.GetMiIdProveedor(RunProv);
        }
        public static int InsertSolicitudAccesoSistema(int IdSistema, int IdUsuarioProv)
        {
            return PortalProveedorDAL.InsertSolicitudAccesoSistema(IdSistema, IdUsuarioProv, DateTime.Now);
        }

        public static List<SistemaEntity> GetSistemasParaUsersProv(string RunUserProv)
        {
            return PortalProveedorDAL.GetSistemasParaUsersProv(RunUserProv);
        }

        public static UsuarioProveedorEntity.UsuarioKaufmann ValidaLogin(string Email, string Pass, int TipoUser)
        {
            UsuarioProveedorEntity.UsuarioKaufmann user = PortalProveedorDAL.getValidaLogin(Email, Pass, TipoUser);

            if (user != null) //Si el usuario existe en la tabla "usuario_ksa", se valida su email y contraseña en el active directory
            {
                if (TipoUser == 0) //Valido a usuarios kaufmann, usuario y password
                {
                    string UserName = UsuarioDAL.AutenticarUsuario(user.UserName, Pass); //Si trae el usuario es porque es válido
                    //string UserName = UsuarioDAL.AutenticarUsuario(Email, Pass);

                    if (UserName != string.Empty)
                    {
                        user.Nombre = UsuarioBL.GetNombre(UserName);
                        user.ApellidoPat = UsuarioBL.GetApellido(UserName);

                        return user;
                    }
                    else
                        return null; //Datos inválidos
                }

                return user;
            }
            else
                return null; //Datos inválidos

        }

        public static void SetReasignarAdmEnRS(int IdRegistro)
        {
            PortalProveedorDAL.SetReasignarAdmEnRS(IdRegistro, DateTime.Now);
        }

        public static int GetSistemaAcceso(int IdUsuarioProv)
        {
            return PortalProveedorDAL.GetSistemaAcceso(IdUsuarioProv);
        }

        public static int ValidaLinkPass(int IdSolicitud)
        {
            return PortalProveedorDAL.ValidaLinkPass(IdSolicitud);
        }

        /* Inicio Funciones útiles */

        public static string PasswordOculta(string Password)
        {
            int Largo = Password.Length;

            Password = Password.Substring(0, 2);

            for (int x = 0; x < Largo; x++)
                Password += "*";

            return Password;
        }

        /* Fin Funciones útiles */
    }
}
