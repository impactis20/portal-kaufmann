﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PortalProveedores.BL.EnviarEmail;
using PortalProveedores.BL.PagoProveedores;
using PortalProveedores.Entities;

namespace PortalProveedores.BL
{
    public class WebServices
    {
        public static bool EnviaCorreo(string EmailProveedor, string Asunto, string Mensaje)
        {
            string EmailRemitente = ConfigurationManager.AppSettings["EmailRemitente"];
            string PasswordRemitente = ConfigurationManager.AppSettings["PasswordRemitente"];

            try
            {
                EnviarCorreoElectronicoSoapClient client = new EnviarCorreoElectronicoSoapClient();
                client.Open();
                client.EnviarCorreo(EmailRemitente, PasswordRemitente, EmailRemitente, EmailProveedor, "", "", Asunto, string.Format("{0}<br><br>Atte.<br>Sistema de Proveedores<br>Kaufmann", Mensaje), "", "", "", "", "");
                client.Close();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public static List<PagosEntity> RealizarConsulta(DT_ProvListadoInput Consulta)
        {
            var Parametros = new List<DT_ProvListadoInput>();
            Parametros.Add(Consulta);

            MI_os_PortalProvListadoService Servicio = new MI_os_PortalProvListadoService();

            string Ambiente = ConfigurationManager.AppSettings["dns_pi_qas"].ToString();
            Servicio.Url = (Ambiente.Split(',')[0] + ConfigurationManager.AppSettings["UrlWS"].ToString()).Replace("-", Ambiente.Split(',')[1]);
            //Para producción no le asigno URL ya que el wsdl corresponde a dicho ambiente.

            Servicio.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["userPIqas"].ToString(), ConfigurationManager.AppSettings["passPIdq"].ToString());
            Servicio.Timeout = 10000000;

            DT_ProvListadoPago[] arrPagos;

                arrPagos = Servicio.MI_os_PortalProvListado(Parametros.ToArray());


                Servicio.Dispose();
                Servicio = null;

            if (arrPagos.Length > 0)
            {
                List<PagosEntity> ListPagos = new List<PagosEntity>();
                //List<PagosEntity> ListPagosRecorrido = new List<PagosEntity>();
                int x = 0;
                foreach (DT_ProvListadoPago Fila in arrPagos)
                {
                    
                    PagosEntity Pago = new PagosEntity();

                    try
                    {
                        //if (x == 36)
                        //{

                        //}
                        
                        switch (Fila.tipodocumento)
                        {
                            case "PA": //No tiene fecha de compensación
                                Pago.SociedadPagadora = Convert.ToInt32(Fila.socpagadora);
                                Pago.EstadoDocumento = Fila.tipodocumento;
                                Pago.Moneda = Fila.moneda;
                                Pago.Monto = CorrigeMonto(Fila.monto);
                                Pago.FechaPago = CreaFechaValida(Fila.fechapago);
                                //Pago.FechaCompensa = CreaFechaValida(Fila.fechacompensa);
                                Pago = Verifica_Bloqueo_FormaPago(Pago, Fila.nrofactura, Fila.formapago);
                                Pago.estado = Fila.estado;
                                Pago.rut = Fila.rut;
                                Pago.razon = Fila.razonsocial;
                                Pago.noTransa = Fila.docucompensa;
                                Pago.total = Convert.ToInt32(Fila.montocompensa);
                                break;

                            case "PP":
                                Pago.SociedadPagadora = Convert.ToInt32(Fila.socpagadora);
                                Pago.EstadoDocumento = Fila.tipodocumento;
                                Pago.Moneda = Fila.moneda;
                                Pago.Monto = CorrigeMonto(Fila.monto);
                                Pago.FechaPago = CreaFechaValida(Fila.fechapago);
                                Pago.FechaCompensa = CreaFechaValida(Fila.fechacompensa);
                                //Establece FechaPago si es que la FechaCompensa es menor
                                //Pago.FechaPago = Pago.FechaPago > Pago.FechaCompensa ? Pago.FechaCompensa : Pago.FechaPago;
                                Pago.estado = Fila.estado;
                                Pago.rut = Fila.rut;
                                Pago.razon = Fila.razonsocial;
                                Pago = Verifica_Bloqueo_FormaPago(Pago, Fila.nrofactura, Fila.formapago);
                                Pago.noTransa = Fila.docucompensa;
                                Pago.total = Convert.ToInt32(Fila.montocompensa);
                                break;

                            case "PC":
                                Pago.SociedadPagadora = Convert.ToInt32(Fila.socpagadora);
                                Pago.EstadoDocumento = Fila.tipodocumento;
                                Pago.Moneda = Fila.moneda;
                                Pago.Monto = CorrigeMonto(Fila.monto);
                                Pago.FechaPago = CreaFechaValida(Fila.fechapago);
                                Pago.FechaCompensa = CreaFechaValida(Fila.fechacompensa);
                                Pago.estado = Fila.estado;
                                Pago.rut = Fila.rut;
                                Pago.razon = Fila.razonsocial;
                                Pago.noTransa = Fila.docucompensa;
                                Pago.total = Convert.ToInt32(Fila.montocompensa);
                                Pago = Verifica_Bloqueo_FormaPago(Pago, Fila.nrofactura, Fila.formapago);
                                break;

                            case "RS":
                                Pago.FormaPago = Fila.formapago;
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        int valorError = x;
                        throw new Exception(string.Format("{0}.... en el objeto {1}.", ex.Message + x.ToString()));
                    }

                    ListPagos.Add(Pago); x++;
                }

                return ListPagos;
            }
            else
            {
                return null;
            }    
        }

        private static int CorrigeMonto(string Valor)
        {
            Valor = Valor.Replace(".", string.Empty);
            int Monto = 0;

            if(Valor.Contains("-"))
                Valor = string.Format("-{0}", Valor.Replace("-", string.Empty));

            Monto = Convert.ToInt32(Valor);

            return Monto;
        }

        //También asigna el número del documento
        private static PagosEntity Verifica_Bloqueo_FormaPago(PagosEntity Pago, string NroDocumento, string FormaPago)
        {
            NroDocumento = NroDocumento == null ? "0" : NroDocumento;
            //Verifica si el documento está bloqueado
            if (NroDocumento.Contains(","))
            {
                Pago.NroDocumento = NroDocumento.Split(',')[0];
                Pago.DocumentoBloqueado = true;
                Pago.FormaPago = "<span class=\"text-danger\"><b>Factura bloqueada a pago</b></span>";
            }
            else
            {

                Pago.NroDocumento = NroDocumento;
                Pago.DocumentoBloqueado = false;

                //Verifica Forma de Pago 
                if (!string.IsNullOrEmpty(FormaPago) && FormaPago.Contains(","))
                {
                    Pago.FormaPago = "Cheque";

                    int n;
                    bool esNumerico = int.TryParse(FormaPago.Split(',')[1], out n);

                    if (esNumerico)
                        Pago.NroCheque = Convert.ToInt32(FormaPago.Split(',')[1]);
                    else
                        Pago.NroCheque = 300;
                }
                else
                {
                    FormaPago = FormaPago == null ? FormaPago : FormaPago.Trim();
                    switch (FormaPago)
                    {
                        case "2":
                            Pago.FormaPago = "Abono cuenta corriente";
                            Pago.NroCheque = 0;
                            break;
                        case "3":
                            Pago.FormaPago = "Vale vista Banco Santander";
                            Pago.NroCheque = 0;
                            break;
                        case null:
                            Pago.FormaPago = "Otra forma de pago"; //caja chica, compensación con deuda, efectivo
                            Pago.NroCheque = 0;
                            break;
                    }
                }
            }

            return Pago;
        }

        private static DateTime CreaFechaValida(string Fecha)
        {
            int Ano = Convert.ToInt16(Fecha.Substring(0, 4));
            int Mes = Convert.ToInt16(Fecha.Substring(4, 4).Substring(0, 2));
            int Dia = Convert.ToInt16(Fecha.Substring(6, 2));

            return new DateTime(Ano, Mes, Dia);
        }
    }
}
