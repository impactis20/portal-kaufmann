﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PortalProveedores.DAL;

namespace PortalProveedores.BL
{
    public static class UsuarioBL
    {
        public static string GetUserName(string username)
        {
            return UsuarioDAL.GetUserName(username);
        }

        public static string GetApellido(string username)
        {
            return UsuarioDAL.GetApellido(username);
        }

        public static string GetNombre(string username)
        {
            return UsuarioDAL.GetNombre(username);
        }

        public static string GetRut(string username)
        {
            return UsuarioDAL.GetRut(username);
        }

        public static string GetEmail(string username)
        {
            return UsuarioDAL.GetEmail(username);
        }

        public static string[] GetUserNameLIKE(string username)
        {
            return UsuarioDAL.GetUserNameLIKE(username);
        }

        //public static List<string> GetUserNameAD(string prefix)
        //{
        //    return UsuarioDAL.GetUserNameAD(prefix);
        //}

        public static List<Entities.UsuarioProveedorEntity> GetUsuarioAD(string UserName)
        {
            return UsuarioDAL.GetUsuarioAD(UserName);
        }

        public static string AutenticarUsuario(string UserName, string Password)
        {
            return UsuarioDAL.AutenticarUsuario(UserName, Password);
        }
    }
}
