﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PortalProveedores.Entities;
using PortalProveedores.DAL;
using PortalProveedores.BL.PagoProveedores;

namespace PortalProveedores.BL
{
    public class PagosBL
    {
        public static string GetExisteRazonSocialSAP(string RutRS, string TipoUsuario) //verifica también si existe el proveedor en la bd de sap. Ejemplo: enviar (rut razón social, -1)
        {
            DT_ProvListadoInput Consulta = new DT_ProvListadoInput();
            Consulta.rutproveedor = RutRS;
            Consulta.nrodocumento = "BRS";
            Consulta.tipo = TipoUsuario;

            List<PagosEntity> ListPagos = WebServices.RealizarConsulta(Consulta);

            if (!string.IsNullOrEmpty(ListPagos[0].FormaPago))
            {
                return ListPagos[0].FormaPago; //Devuelve la razón social registrada en sap
            }
            else if (string.IsNullOrEmpty(ListPagos[0].FormaPago)) //Si no la encontra, entonces es un proveedor que no existe en sap
            {
                return "NE";
            }
            else // if (ListPagos.Count > 1)
            {
                return "Error";
            }
        }

        public static List<PagosEntity> GetPago(string RutRS, int NroFactura, string tipo) //verifica también si existe el proveedor en la bd de sap. Ejemplo: enviar (rut razón social, -1)
        {
            DT_ProvListadoInput Consulta = new DT_ProvListadoInput();
            Consulta.rutproveedor = RutRS;
            Consulta.nrodocumento = NroFactura.ToString();
            Consulta.tipo = tipo.ToString();

            List<PagosEntity> ListPagos = WebServices.RealizarConsulta(Consulta);
            return ListPagos;
        }

        public static List<SociedadPagadoraEntity> GetPagos(string RunRS, string Sociedades, string FechaDesde, string FechaHasta, string FiltroEstadoDoc, string tipo)
        {
            DT_ProvListadoInput Consulta = new DT_ProvListadoInput();

            //Consulta = new DT_ProvListadoInput();
            //Consulta.socpagadora = "1000";
            //Consulta.rutproveedor = "96992890-6";
            //Consulta.fechadesde = "20140701";
            //Consulta.fechahasta = "20140731";
            //Consulta.nrodocumento = string.Empty;

            Consulta = new DT_ProvListadoInput();
            Consulta.socpagadora = Sociedades;
            Consulta.rutproveedor = RunRS;
            Consulta.fechadesde = FechaDesde;
            Consulta.fechahasta = FechaHasta;
            Consulta.tipo = tipo;

            //Consulta.fechadesde = "20130901";
            //Consulta.fechahasta = "20130930";
            //Consulta.nrodocumento = string.Empty;

            //obtengo todos los pagos
            List<PagosEntity> ListPagos = WebServices.RealizarConsulta(Consulta);
            List<SociedadPagadoraEntity> ListPagosSociedades = new List<SociedadPagadoraEntity>(); //ordena por sociedades

            //asigna pagos a las sociedades pagadoras según corresponda
            string[] Separador = { "," };
            string[] SociedadesSeleccionadas = Sociedades.Split(Separador, StringSplitOptions.RemoveEmptyEntries);

            for (int x = 0; x <= SociedadesSeleccionadas.Length - 1; x++)
            {
                int Sociedad = Convert.ToInt16(SociedadesSeleccionadas[x]);
                SociedadPagadoraEntity SociedadPagos = SociedadPagadoraEntity.GetSociedadPagadora(Sociedad);

                if (ListPagos != null && ListPagos.Count > 0)
                {
                    SociedadPagos.Pagos = ListPagos.Where(e => e.SociedadPagadora == Sociedad).ToList();
                    ListPagosSociedades.Add(SociedadPagos);
                }
            }
            
            //aplica filtro, PA, PP y PC
            string[] FiltroEstadoDoctosSeleccionados = FiltroEstadoDoc.Split(Separador, StringSplitOptions.RemoveEmptyEntries);
            
            //for (int x = 0; x <= FiltroEstadoDoctosSeleccionados.Length - 1; x++)
            //{
                foreach(SociedadPagadoraEntity sociedad in ListPagosSociedades)
                {
                    //sociedad.Pagos = sociedad.Pagos.Where

                    sociedad.Pagos = sociedad.Pagos.Where(s => FiltroEstadoDoctosSeleccionados.Where(a => a.Equals(s.EstadoDocumento)).Any()).ToList();
                }

                
            //}
            
            return ListPagosSociedades;
        }
    }
}
